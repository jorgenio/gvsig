public class GetJavaSystemProperties{
	public static void main(String[] args) {
		Object value;
		for (int i=0;i< args.length;i++) {
			value = System.getProperties().get(args[i]);
			if (value == null) {
				value = "";
			}
			System.out.println(value);
		}
		System.exit(0);
	}
}
