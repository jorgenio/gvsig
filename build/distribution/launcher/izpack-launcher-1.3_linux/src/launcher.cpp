/* Copyright (c) 2004 Julien Ponge - All rights reserved.
 * Some windows 98 debugging done by Dustin Sacks.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <wx/string.h>
#include <wx/file.h>
#include <wx/filename.h>
#include <wx/url.h>

#include "launcher.h"

#ifdef __UNIX__
  #include <stdlib.h>
  #include <stdio.h>
  #include <map>
  #include <list>
#endif


/*
 * Helper function to run an external program. It takes care of the subtule
 * differences between the OS-specific implementations.
 *
 * @param cmd The command to run.
 * @return <code>true</code> in case of a success, <code>false</code> otherwise.
 */
bool run_external(wxString cmd)
{
 //int code = wxExecute(cmd);
	int code = system(cmd );
	return (code == 0);

}

bool run_external_async(wxString cmd)
{
 return (wxExecute(cmd, wxEXEC_ASYNC) != 0);
}

bool run_external_withOut(wxString cmd,wxArrayString& out, wxArrayString& errors){	
	//printf(" run_external_withOut **%s**\n",cmd.c_str());
	int exitCode =wxExecute(cmd, out,errors);
	//printf("\texitCode= %i, out.Count = %i, error.Count= %i\n",exitCode,out.Count(),errors.Count());
	
	/*
	int i;
	for (i=0;i<out.Count();i++){
		printf("\t\tout: %s\n",out[i].c_str());
	}
	for (i=0;i<errors.Count();i++){
		printf("\t\terrors: %s\n",errors[i].c_str());
	}
	*/
	
	return ( exitCode == 0);
}


bool string_to_bool(wxString value, bool defaultValue){
 bool returnValue = defaultValue;
 if (value != wxEmptyString)
    if (
       value.CmpNoCase("s")   == 0  ||
       value.CmpNoCase("si")  == 0  ||
       value.CmpNoCase("1")   == 0  ||
       value.CmpNoCase("y")   == 0  ||
       value.CmpNoCase("yes") == 0
       ) 
    {
       returnValue = true;
    } else {
       returnValue = false;
    }
  return returnValue;
}


void applyFont(wxWindow *dlg, wxFont font){
	puts("0");
	dlg->SetFont(font);
	//bool tmp = win->IsKindOf(CLASSINFO(wxFrame));
	puts("1");
	wxWindowList childs =dlg->GetChildren();
	puts("2");
	if (childs.GetCount() < 1) {
		return;
	}
	puts("3");
	wxWindowListNode *node = childs.GetFirst();
	while (node)
	{
		puts("4");
		wxWindow * win = node->GetData();
		try {
			applyFont(win,font);
		} catch ( ... ) {
		
		}
		puts("5");
		node = node->GetNext();
		puts("6");
	}
	puts("7");
}

/*
class MyWxFileDialog : public wxDialog {
public:

  MyWxFileDialog(wxWindow* parent, const wxString& message = "Choose a file", const wxString& defaultDir = "", const wxString& defaultFile = "", const wxString& wildcard = "*.*", long style = 0, const wxPoint& pos = wxDefaultPosition);

  virtual ~MyWxFileDialog();
}

MyWxFileDialog::MyWxFileDialog(): wxDialog() {

}
*/
long LAUNCHAPP_TIMER_ID = wxNewId();

BEGIN_EVENT_TABLE(LauncherApp, wxApp)
    EVT_TIMER(LAUNCHAPP_TIMER_ID, LauncherApp::OnTimer)
END_EVENT_TABLE()


/* Main Application $Revision: 13338 $
 *
 * $Id: launcher.cpp 13338 2007-08-28 08:44:45Z jmvivo $
 */
LauncherApp::LauncherApp()
  : wxApp()
{
  APPLICATION_NAME = wxString(_("gvSIG Install-Launcher"));
  

  SetAppName(_( APPLICATION_NAME ));
  loadParams();
 /*
  wxFont font;
  font = wxSystemSettings::GetFont(wxSYS_SYSTEM_FONT);
  puts("Fuente de sistema");
  printf("face: %s\n",font.GetFaceName().c_str());
  printf("native: %s\n",font.GetNativeFontInfoDesc().c_str());
  printf("size: %i\n",font.GetPointSize());
  printf("ok: %i\n",font.Ok());
  puts("/Fuente de sistema");
*/
}

void LauncherApp::echo(const wxString &msg) 
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK);
  //applyFont(&dlg,font);
  dlg.ShowModal();
 //wxString m="%s";
  ///m.Printf(msg);
}

void LauncherApp::notifyToUser(const wxString &msg) 
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxICON_INFORMATION);
  //applyFont(&dlg,font);
  dlg.ShowModal();
}

void LauncherApp::notifyErrorToUser(const wxString &msg) 
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxICON_ERROR);
  //applyFont(&dlg,font);
  dlg.ShowModal();
}


LauncherApp::~LauncherApp()
{

}

void LauncherApp::loadParams()
{
  cfgName = wxString( "launcher.ini" );

  wxFileInputStream in( cfgName );
  wxFileConfig cfg( in );
  wxString downloadEnabled;
  wxString launchJarAsync;
  wxString askForCheckingProcess;

  cfg.Read( "jar",                    &paramsJar,                wxEmptyString);
  cfg.Read( "jre",                    &paramsJre,                wxEmptyString);
  cfg.Read( "downloadJre",            &paramsJreDownload,        wxEmptyString);
  cfg.Read( "installJreCmd",            &paramsJreInstallCmd,        wxEmptyString);
  cfg.Read( "installedJreJavaFile",            &paramsJreInstalledJavaFile,        wxEmptyString);
  cfg.Read( "installedJreRootDirectory",            &paramsJreInstalledRootDirectory,        wxEmptyString);
  cfg.Read( "jre_version",            &paramsJreVersion,         wxEmptyString);
  cfg.Read( "jre_version_prefered",   &paramsJreVersionPrefered, wxEmptyString);
  cfg.Read( "jarParams",              &paramsJarParams,          wxEmptyString);
  cfg.Read( "downloadEnabled",        &downloadEnabled,          wxEmptyString);
  cfg.Read( "launchJarAsync",         &launchJarAsync,           wxEmptyString);  
  
  //procesamos el parametro booleano 'downloadEnabled'
  paramsEnabledDownload =string_to_bool(downloadEnabled,false);
  //procesamos el parametro booleano 'launchJarAsync'
  paramsLaunchJarAsync =string_to_bool(launchJarAsync,false);
  
  if (paramsJar == wxEmptyString )
  {
    error(_("The configuration file '") + cfgName + _("' does not contain a jar file entry."));
  }
  if (paramsJreVersionPrefered == wxEmptyString ) {
     paramsJreVersionPrefered = wxString(paramsJreVersion);
  }
}

void LauncherApp::error(const wxString &msg)
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxICON_ERROR);
  //applyFont(&dlg,font);
  dlg.ShowModal();
  exit(1);
}

void LauncherApp::confirm(const wxString &msg)
{
  int response;
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxCANCEL | wxICON_QUESTION );
  //applyFont(&dlg,font);
  response = dlg.ShowModal();
  if (response != wxID_OK) {
    notifyToUser(_("Canceled by user"));
    exit(1);
  }
}
bool LauncherApp::confirmYesNoCancel(const wxString &msg,const int yesDefault = true)
{
  int response;
  long maskDefault;
  if (yesDefault) {
     maskDefault = wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_QUESTION;
  } else {
     maskDefault = wxNO_DEFAULT | wxYES_NO | wxCANCEL | wxICON_QUESTION;       
  }
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME),  maskDefault);
  //applyFont(&dlg,font);
  response = dlg.ShowModal();
  if (response != wxID_YES && response != wxID_NO) {
    notifyToUser(_("Canceled by user"));
    exit(1);
  }
  return (response == wxID_YES);
 
}



bool LauncherApp::OnInit()
{
  wxFont myFont;
  myFont.SetFamily(wxDECORATIVE);
  myFont.SetPointSize(12);
  isStatusDialogVisible = false;
  locale.Init();
  locale.AddCatalog("launcher");

  statusDialog = new StatusDialog( APPLICATION_NAME );

  //applyFont(statusDialog,font);
  statusDialog->Centre();
  statusDialog->Show(TRUE);
  isStatusDialogVisible = true;

  showStatusMsg(_("Intializing..."));
  
  myTimer = new wxTimer(this,LAUNCHAPP_TIMER_ID);
  myTimer->Start(150,true);
  this->MainLoop();
  return true;
}

bool LauncherApp::compareVersions(const wxString localVersion, const wxString requireVersion)
{
    bool result  = (localVersion.Cmp(requireVersion) == 0);
    /*
	if (requireVersion.Len() > localVersion.Len() )	{   
        
		result = (localVersion.Find(requireVersion) == 0);
	}
	else 
	{
		result = (requireVersion.Find(localVersion) == 0);
	}
	*/

    /*
	if (result) {
 	   echo("localversion = '" + localVersion +"' requireVersion = '"+ requireVersion +"' ==> true");
    } else {
       echo("localversion = '" + localVersion +"' requireVersion = '"+ requireVersion +"' ==> false");
    }
    */
     
	return result;
}


void LauncherApp::runJRE()
{
  showStatusMsg(_("Launching installation..."));
  if (!wxFile::Exists(paramsJar))
  {
    error(_("The jar-file in the configuration file ") + cfgName + _(" does not exist."));
  }

  wxString cmd = "export  JAVA_HOME=\"" +  
	  getJavaExecFileHome(javaExecPath) + "\"; unset LD_LIBRARY_PATH ; exec " + 
	  javaExecPath + paramsJarParams + wxString(" -jar ") + paramsJar;
  //echo(cmd);
  if (paramsLaunchJarAsync) {
      if (!run_external_async(cmd))
      {
        error(_("The jar-file ") + paramsJar + _(" could not be executed." ));
      }
  } else {
      if (!run_external(cmd))
      {
        error(_("The jar-file ") + paramsJar + _(" could not be executed." ));
      }  
  }

}

wxString LauncherApp::installJRE(){
	showStatusMsg(_("Preparing to install JRE..."));

	if (!checkInstallerFile(paramsJre, paramsJreDownload,_("Java JRE version ")+paramsJreVersion))
	{
		notifyErrorToUser(_("The JRE could not be setup."));
		return "";
	}

	showStatusMsg(_("Installing JRE..."));

	if (!run_external(paramsJreInstallCmd))
	{
		notifyErrorToUser(_("The JRE could not be setup."));
		return "";
	}

	if (!wxFileExists(paramsJreInstalledJavaFile)){
		notifyErrorToUser(wxString::Format(_("The file %s does not exist."),paramsJreInstalledJavaFile.c_str()));
		return "";
  	}

  return paramsJreInstalledJavaFile;

}

IMPLEMENT_APP(LauncherApp)

bool LauncherApp::downloadFileHttp(const wxString urlOfFile, const wxString filePath, const wxString msg){

  bool isOk;
  //echo(urlOfFile);
  if (urlOfFile == wxEmptyString) {
     return false;
  }
  
  if (filePath == wxEmptyString) {
     return false;
  }
  
  showStatusMsg(msg);
  wxURL url(urlOfFile);
  //echo("url open");
  wxInputStream *in_stream;

  in_stream = url.GetInputStream();
  //echo("in_stream open");
  if (!in_stream){
	return false;
  }
  if (!in_stream->IsOk()) {
     //echo("in_stream.IsOk == false");
     return false;
  }
  //echo("in_stream.IsOk == true");
  //echo("filePath =" + filePath);
  wxFileName fileName(filePath); 
  wxFileName fileDir(fileName.GetPath()); 
  if (!fileDir.DirExists()) {
	if (!fileDir.Mkdir(0777,wxPATH_MKDIR_FULL )) {
		return false;
	}
  }
  /*
  // Preparamos la ruta para el fichero destino
  wxArrayString dirs;
  
  dirs = fileName.GetDirs();
  wxString dir;
  wxFileName curDir(".");
  for (size_t i=0; i < dirs.GetCount(); i++) {
      dir = dirs.Item(i);
      curDir.AppendDir(dir);
      //echo("dir " + curDir.GetPath());
      if (!curDir.DirExists()) {
         //echo("creating dir");
         isOk = curDir.Mkdir();
         if (!isOk) {
            //echo("dir create no ok");
            return false;
         }
         
         //echo("dir create ok");
      }
  }
  */
  
  wxFileOutputStream out_stream  = wxFileOutputStream(filePath);
  //echo("out_stream open");

  //in_stream->Read(out_stream);
  size_t nbytes = 10240;
  char buffer[nbytes];

 
  while (!in_stream->Eof()) {
      in_stream->Read(&buffer,nbytes);  
      if (in_stream->LastError() != wxSTREAM_NO_ERROR && in_stream->LastError() != wxSTREAM_EOF) {
          return false;
      }
      out_stream.Write(&buffer,in_stream->LastRead());
      if (out_stream.LastError() != wxSTREAM_NO_ERROR) {
         return false;
      }
      int totalKb = out_stream.GetSize() / 1024;
      wxString totalMsg =wxString::Format("%i",totalKb);
      showStatusMsg(msg+" "+ totalMsg + " Kb");

  }
 
  
  isOk = true;
 /*
  if (isOk) {
    echo("isOk = true");
  } else {
    echo("isOk = false");
  }
 */
  delete in_stream;
  
  //echo("end");
  return isOk;

}

bool LauncherApp::checkInstallerFile(const wxString filePath, const wxString urlDownload, const wxString fileDescription) {
  if (!wxFile::Exists(filePath))
  {
    if (paramsEnabledDownload) 
    {
      wxString msg;
      msg = wxString::Format(_("The installation requires to download this file:\n%s\nDo you want to continue?"),fileDescription.c_str());
      confirm(msg);
      msg = wxString::Format(_("Downloading %s..."),fileDescription.c_str());
      if (!downloadFileHttp(urlDownload,filePath,msg)) 
      {
        //FIXME: Falta msgError
        return false;
      }
    }
    else
    {
      return false;
    }

  }
  return true;
}

void LauncherApp::showStatusMsg(const wxString msg) {
  if (isStatusDialogVisible) {
     while (this->Pending()) {
           this->Dispatch();
     }
     if (statusDialog->IsCanceled()) {
        int response;
        wxMessageDialog dlg(0, _("Are you sure you want to cancel installation?"), _(APPLICATION_NAME), wxYES_NO | wxICON_QUESTION );
        response = dlg.ShowModal();
        if (response == wxID_YES) {
            notifyToUser(_("Canceled by the user"));
            exit(1);
        }
        statusDialog->DoNotCancel();
     }
     statusDialog->showStatus(msg);
  }
}

void LauncherApp::run(){
  bool ok = false;
  
  wxString mJavaFileName;
  wxString errorMsg;
  SelectionDialog  dlg( checkPreviousJRE(),checkEnvironmentJRE() ,_(APPLICATION_NAME));
  while (!ok) {
	//applyFont(&dlg,font);
	if (dlg.ShowModal() != wxID_OK){
		notifyToUser(_("Canceled by the user"));
		exit(1);
	}
	switch (dlg.getUserAction())
	{
		case PREVIOUS_GVSIG:  mJavaFileName = previousJRE(); break;
		case PROVIDED: mJavaFileName = installJRE();   break;
		case SELECT_MANUALY: mJavaFileName = selectJavaExecFileManualy();  break;
		case ENVIRON: mJavaFileName = useEnvironJRE();  break;
		default: break;
	}
	if (mJavaFileName.Cmp("") == 0) {
		wxString msg =wxString::Format(_("Error executing this installation option. Do you want to try another one?"),errorMsg.c_str());
		confirm(msg);
	} else {		
		//echo("checking..." + mJavaFileName);
		errorMsg = checkJava(mJavaFileName);		
		//echo("check result: *"+errorMsg+"*");
		if (errorMsg.Cmp("") == 0) {
			javaExecPath=mJavaFileName;
			ok=true;
		} else {
			wxString msg =wxString::Format(_("Problems found in the selected JRE:\n%s\n\nDo you want to continue?"),errorMsg.c_str());
			if (confirmYesNoCancel(msg,false)) {
				javaExecPath=mJavaFileName;
				ok=true;	
			}
		}
	}
  }
  runJRE();

  exit(0);    
     
}

void LauncherApp::OnTimer(wxTimerEvent& event) {     
     run();
}

wxString LauncherApp::selectJavaExecFileManualy() {     
    wxString msgDir = _("Manually select the Java VM");   
    wxFileDialog dlg(
        0,
        msgDir,
        wxEmptyString, 
        wxString("java"),
        wxString( wxString(_("Java VM executable file")) + " (java)|java"),
        wxOPEN | wxFILE_MUST_EXIST,
        wxDefaultPosition
    );
    //applyFont(&dlg,font);


    int response = dlg.ShowModal();
    if (response != wxID_OK) {
       notifyToUser(_("Canceled by the user"));
       exit(1);
    }
    return dlg.GetPath();
}


wxString LauncherApp::getJavaExecFileVersion(wxString javaFileName) {
	wxArrayString output, errors;	
	wxString cmd =javaFileName + " GetJavaSystemProperties java.version";
	//echo(cmd);
	if (!run_external_withOut(cmd,output, errors)) {
		return "";
	}
	if (output.Count() == 0){
		return "";
	}
	return output[0];
	
}

wxString LauncherApp::getJavaExecFileHome(wxString javaFileName) {
	wxArrayString output, errors;
	wxString cmd =javaFileName + " GetJavaSystemProperties java.home";
	if (!run_external_withOut(cmd,output, errors)) {
		return "";
	}
	if (output.Count() == 0){
		return "";
	}	
	return output[0];
	
}


wxString LauncherApp::checkJava(const wxString aJavaFileName) {
	wxString msg = "";
	wxString version;
	wxString home;	
	
	
	if (!wxFileExists(aJavaFileName)){
		return wxString::Format(_("File %s not found."),aJavaFileName.c_str());//_("File %s not found."), javaFileName);
	};
	
	
	version = getJavaExecFileVersion(aJavaFileName);
	
	if (version.Cmp("") == 0){
		return wxString::Format(_("Can't identify version of %s."), aJavaFileName.c_str());
	}
	
	version = version.Left(paramsJreVersion.Len());
	if (!compareVersions(version,paramsJreVersion)) {
		msg = "\n" + wxString::Format(_("Java version %s is not the required %s"),version.c_str(),paramsJreVersion.c_str());
	}
	
	home = getJavaExecFileHome(aJavaFileName);
	if (home.Cmp("") == 0){
		return wxString::Format(_("Can't identify java_home of %s."), aJavaFileName.c_str());
	}
	// comprobamos JAI
	if (!wxFileExists(home + "/lib/ext/jai_core.jar")) {
		msg = msg +"\n" + _("The JAI library is not installed");
	}
	// comprobamos JAI i/o
		if (!wxFileExists(home + "/lib/ext/jai_imageio.jar")) {
		msg = msg +"\n" + _("The JAI I/O library is not installed");
	}	
	
	return msg;
}

bool LauncherApp::checkEnvironmentJRE() {
	return (useEnvironJRE().Cmp("") != 0);
}

wxString  LauncherApp::useEnvironJRE() {
	wxString envJavaHome;
	if (wxGetEnv("JAVA_HOME",&envJavaHome)){
		//echo("no Java Home get");
		if (!envJavaHome.Cmp("") == 0) {
			return envJavaHome + "/bin/java";
		}
		//echo("no Java Home blank");
	}
	//echo("no Java Home");

	wxArrayString output, errors;
	if (run_external_withOut("/bin/sh -c 'type -p java'",output, errors)) {
		//printf("ouput.Count() = %i",output.Count());
		//printf("errors.Count() = %i",errors.Count());
		if (output.Count() != 0){
			return output[0];
		}
		//echo("no Java in path count=0");
	}
	//echo("no Java in path");
	return "";
	
}


bool LauncherApp::checkPreviousJRE() {
	wxFileName appHomeJre = paramsJreInstalledRootDirectory;
	
	appHomeJre.MakeAbsolute();
	
	if (!appHomeJre.DirExists())  {
		return false;
	}
	wxString theDir;
	if (paramsJreVersion.Len() > 5){
		theDir = wxString(paramsJreVersion);
	} else {
		theDir = paramsJreVersion+"*";
	}
	
	return (!wxFindFirstFile(appHomeJre.GetFullPath()+"/"+theDir,wxDIR).IsEmpty());
}

wxString  LauncherApp::previousJRE() {
	if (!paramsJreVersionPrefered.IsEmpty()) {
		wxFileName prefered = paramsJreInstalledRootDirectory+"/"+paramsJreVersionPrefered+"/bin/java";
		prefered.Normalize();
		if (prefered.FileExists()){
			return prefered.GetFullPath();
		}
	}
	wxFileName current;


	wxString mask = paramsJreInstalledRootDirectory+"/" + paramsJreVersion + "_%02d/bin/java";
	for(unsigned i=20;i>1;i--) {

        	current = wxString::Format(mask,i);
        	if (current.FileExists()) {
			return current.GetFullPath();
		}
	}
	return "";
}

/**
wxArrayString getDirectoriesFromPath(wxString path){
	wxArrayString directories;
	if (appHome.DirExists())  {		
		wxString f = wxFindFirstFile(appHomeJre+"*",wxDIR);
		while ( !f.IsEmpty() )
		{
			wxArrayString.Add(f);
			f = wxFindNextFile();
		}
	}
	return directories;
}
**/



