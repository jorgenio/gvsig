/* Copyright (c) 2004 Julien Ponge - All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "selectiondialog.h"

SelectionDialog::SelectionDialog(const bool &enablePreviousgvSIG,
                             const bool &enableEnviron, const wxString &wxStringHeadline )
  : wxDialog(0, -1, _(wxStringHeadline), wxDefaultPosition, wxDefaultSize,
             wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER)
{
  userAction     = PROVIDED;
  canUsePreviousgvSIG  = enablePreviousgvSIG;
  canUseEnviron = enableEnviron;

  buildUI();
  //SetIcon(wxICON(package));  ???
}

SelectionDialog::~SelectionDialog()
{

}

void SelectionDialog::buildUI()
{
  // Inits
  //wxFont font;
  //font.SetFamily(wxDECORATIVE);
  //font.SetPointSize(12);
  //this->SetFont(font);

  wxString explanationMsg = _("gvSIG requires a Java Runtime Environment");
  sizer = new wxBoxSizer(wxVERTICAL);

  // Widgets

  explanationText = new wxStaticText(this, -1, explanationMsg, wxDefaultPosition);
  sizer->Add(explanationText, 0, wxALIGN_LEFT | wxALL, 10);
int optionIndex = 0;
wxString rlabels[] = {
      _("use JRE used by a previous gvSIG version"),
      _("install a JRE version ready to run in the user home directory"),
      _("select a JRE manualy"),
      _("use the JRE defined in JAVA_HOME environment variable or finded in the shell execution path")
    };
if( canUsePreviousgvSIG ) 
  {
    //rlabels[0] = _("use JRE used by a previous gvSIG version (Recommended) .");
    rlabels[0] = rlabels[0] + _(" (Recommended).");
    rlabels[1] = rlabels[1] + ".";
  } 
  else 
  {
    rlabels[0] = rlabels[0] + ".";
    rlabels[1] = rlabels[1] +_(" (Recommended).");
    optionIndex = 1;
  }
  optionsBox = new wxRadioBox(this, -1,
                              _("Select one of these options:"),
                              wxDefaultPosition, wxDefaultSize, 4, rlabels, 1,
                              wxRA_SPECIFY_COLS);
/*
  if( canUsePreviousgvSIG ) 
  {
     optionsBox->SetSelection( 0 );
  } 
  else 
  {
     optionsBox->SetSelection( 1 );
  }
*/
optionsBox->SetSelection(optionIndex);
  //optionsBox->SetLabel(
      //optionsBox->GetSelection(),
      //optionsBox->GetLabel(optionsBox->GetSelection()) + _(" (Recommended) ") );

  sizer->Add(optionsBox, 1, wxGROW | wxALL, 10);

  okButton = new wxButton(this, wxID_OK, _("Continue"));
  //cancelButton = new wxButton(this, wxID_CANCEL, _("Cancel"));
  okButton->SetFocus();
  sizer->Add(okButton, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT  //| wxALL
                          , 10);
  
  //sizer->Add(cancelButton, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL , 10);


  optionsBox->Enable(0, canUsePreviousgvSIG);
  optionsBox->Enable(3, canUseEnviron);

  // Sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);
}

bool SelectionDialog::Validate()
{
  switch (optionsBox->GetSelection())
  {
  case 0: userAction = PREVIOUS_GVSIG;   break;
  case 1: userAction = PROVIDED; break;
  case 2: userAction = SELECT_MANUALY; break;
  case 3: userAction = ENVIRON; break;
  default: break;
  }
  return true;
}
