/////////////////////////////////////////////////////////////////////////////
// Name:        wx/choicdgg.h
// Purpose:     Includes generic choice dialog file
// Author:      Julian Smart
// Modified by:
// Created:
// RCS-ID:      $Id: choicdlg.h 6834 2006-08-24 08:23:24Z jmvivo $
// Copyright:   Julian Smart
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_CHOICDLG_H_BASE_
#define _WX_CHOICDLG_H_BASE_

#include "wx/generic/choicdgg.h"

#endif
    // _WX_CHOICDLG_H_BASE_
