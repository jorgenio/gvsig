/////////////////////////////////////////////////////////////////////////////
// Name:        wx/dcps.h
// Purpose:     wxPostScriptDC base header
// Author:      Julian Smart
// Modified by:
// Created:
// Copyright:   (c) Julian Smart
// RCS-ID:      $Id: dcps.h 6834 2006-08-24 08:23:24Z jmvivo $
// Licence:     wxWindows Licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_DCPS_H_BASE_
#define _WX_DCPS_H_BASE_

#include "wx/generic/dcpsg.h"

#endif

