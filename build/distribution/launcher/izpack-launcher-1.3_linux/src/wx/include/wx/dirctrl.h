/////////////////////////////////////////////////////////////////////////////
// Name:        wx/dirctrl.h
// Purpose:     Directory control base header
// Author:      Julian Smart
// Modified by:
// Created:
// Copyright:   (c) Julian Smart
// RCS-ID:      $Id: dirctrl.h 6834 2006-08-24 08:23:24Z jmvivo $
// Licence:     wxWindows Licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_DIRCTRL_H_BASE_
#define _WX_DIRCTRL_H_BASE_

#include "wx/generic/dirctrlg.h"

#endif
    // _WX_DIRCTRL_H_BASE_
