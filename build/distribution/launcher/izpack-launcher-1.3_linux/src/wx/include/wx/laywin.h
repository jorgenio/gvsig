/////////////////////////////////////////////////////////////////////////////
// Name:        wx/laywin.h
// Purpose:     wxSashLayoutWindow base header
// Author:      Julian Smart
// Modified by:
// Created:
// Copyright:   (c) Julian Smart
// RCS-ID:      $Id: laywin.h 6834 2006-08-24 08:23:24Z jmvivo $
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_LAYWIN_H_BASE_
#define _WX_LAYWIN_H_BASE_

#include "wx/generic/laywin.h"

#endif
    // _WX_LAYWIN_H_BASE_
