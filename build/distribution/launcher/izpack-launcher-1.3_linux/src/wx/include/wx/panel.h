/////////////////////////////////////////////////////////////////////////////
// Name:        wx/panel.h
// Purpose:     Base header for wxPanel
// Author:      Julian Smart
// Modified by:
// Created:
// RCS-ID:      $Id: panel.h 6834 2006-08-24 08:23:24Z jmvivo $
// Copyright:   (c) Julian Smart
// Licence:     wxWindows Licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_PANEL_H_BASE_
#define _WX_PANEL_H_BASE_

#include "wx/generic/panelg.h"

#endif
    // _WX_PANELH_BASE_
