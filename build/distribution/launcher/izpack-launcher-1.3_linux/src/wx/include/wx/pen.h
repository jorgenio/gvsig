/////////////////////////////////////////////////////////////////////////////
// Name:        wx/pen.h
// Purpose:     Base header for wxPen
// Author:      Julian Smart
// Modified by:
// Created:
// RCS-ID:      $Id: pen.h 6834 2006-08-24 08:23:24Z jmvivo $
// Copyright:   (c) Julian Smart
// Licence:     wxWindows Licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_PEN_H_BASE_
#define _WX_PEN_H_BASE_

#if defined(__WXMSW__)
#include "wx/msw/pen.h"
#elif defined(__WXMOTIF__)
#include "wx/motif/pen.h"
#elif defined(__WXGTK__)
#include "wx/gtk/pen.h"
#elif defined(__WXX11__)
#include "wx/x11/pen.h"
#elif defined(__WXMGL__)
#include "wx/mgl/pen.h"
#elif defined(__WXMAC__)
#include "wx/mac/pen.h"
#elif defined(__WXPM__)
#include "wx/os2/pen.h"
#elif defined(__WXSTUBS__)
#include "wx/stubs/pen.h"
#endif

#endif
    // _WX_PEN_H_BASE_
