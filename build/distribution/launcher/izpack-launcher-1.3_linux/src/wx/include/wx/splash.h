/////////////////////////////////////////////////////////////////////////////
// Name:        wx/splash.h
// Purpose:     Base header for wxSplashScreen
// Author:      Julian Smart
// Modified by:
// Created:
// RCS-ID:      $Id: splash.h 6834 2006-08-24 08:23:24Z jmvivo $
// Copyright:   (c) Julian Smart
// Licence:     wxWindows Licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_SPLASH_H_BASE_
#define _WX_SPLASH_H_BASE_

#include "wx/generic/splash.h"

#endif
    // _WX_SPLASH_H_BASE_
