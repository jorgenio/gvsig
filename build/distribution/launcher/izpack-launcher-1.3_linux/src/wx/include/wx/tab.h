/////////////////////////////////////////////////////////////////////////////
// Name:        wx/tab.h
// Purpose:     Generic tab class base header
// Author:      Julian Smart
// Modified by:
// Created:
// Copyright:   (c) Julian Smart
// RCS-ID:      $Id: tab.h 6834 2006-08-24 08:23:24Z jmvivo $
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_TAB_H_BASE_
#define _WX_TAB_H_BASE_

#include "wx/generic/tabg.h"

#endif
    // _WX_TAB_H_BASE_
