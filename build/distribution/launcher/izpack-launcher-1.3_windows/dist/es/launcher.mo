��    >        S   �      H     I     ]     i  -   |     �     �     �     �               -     M     Y     b  1   t  A   �  D   �  I   -     w  !   �     �     �     �     �     �            &   -  9   T     �     �     �     �  #   �  +   
	     6	  $   R	  @   w	     �	     �	  $   �	     
  #   )
      M
     n
  3   �
  %   �
  :   �
  =     :   ]  ,   �     �  @   �  L   "     o     �  >   �  !   �     �  /     *   ;  O  f  !   �     �     �  +      ,   ,  (   Y     �     �     �     �  &   �       	          5   /  K   e  G   �  H   �     B  &   a     �  !   �     �     �     �     �       ,     C   B     �     �     �  2   �  /   �  7   )  "   a  /   �  W   �            $   4  "   Y  *   |  "   �     �  ;   �  !   !  E   C  G   �  C   �  ,        B  @   [  F   �     �     �  M     &   [     �  >   �  #   �         &   5         6      3   /       2   ,   =   %         9       4               0          <   $            (   ;              8      '   -                        1              7          "      .   	             )                                           #   :   >   
   *          +   !        %s Install-Launcher %s Launcher %s. Please wait... Are you sure you want to cancel installation? Can't identify java_home of %s. Can't identify version of %s. Cancel Canceled by the user Canceled by user Checking JAI Library... Checking JAI imageIO Library... Checking... Continue Downloading %s... Error copying the file number %i:
 '%s' --> '%s'. Error copying the file number %i:
 missing source or target entry Error copying the file number %i:
 source file '%s' does not exists. Error executing this installation option. Do you want to try another one? File %s not found. Installing JAI imageIO Library... Installing JRE... Installing Provided JRE... Intializing... JAI Library JRE not found. Java JRE version  Java VM executable file Java version %s is not the required %s Launch installation program to try to repare the problem. Launcher Launching application... Launching installation... Manually select the Java VM Preparing to install JAI Library... Preparing to install JAI imageIO Library... Preparing to install JRE... Preparing to install Provided JRE... Problems found in the selected JRE:
%s

Do you want to continue? Searching JRE's... Select one of these options: The JAI I/O library is not installed The JAI could not be setup. The JAI imageIO could not be setup. The JAI library is not installed The JRE could not be setup. The application requires a Java Runtime Environment The command
%s
could not be executed. The configuration file '%s' contains a invalid mode entry. The configuration file '%s' does not contain a command entry. The configuration file '%s' does not contain a mode entry. The configuration file '%s' does not exists. The file %s does not exist. The file entry 'jre_home' can not be empty when 'doChecks = No'. The installation requires to download this file:
%s
Do you want to continue? Updating the system... copying %s... install a JRE version ready to run in the user home directory. install requisites in the system. select a JRE manualy. use JRE used by a previous application version. use the default JRE defined in the system. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2006-11-02 09:52+0100
PO-Revision-Date: 2006-11-02 10:28+0100
Last-Translator: Jose Manuel Vivó (chema) <josemanuel.vivo@iver.es>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 Lanzador de la instalación de %s Lanzador de %s %s. Por favor, espere... ¿Está seguro de cancelar la instalación? No se pudo identificar el 'java_home' de %s. No se pudo identificar la version de %s. Cancelar Cancelado por el usuario. Cancelado por el usuario Comprobando la libreria JAI... Comprobando la libreria JAI imageIO... Comprobando... Continuar Descargando %s... Error copiando el fichero número %i:
 '%s' --> '%s'. Error copiando el fichero número %i:
 falta la entrada 'source' o 'target' Error copiando el fichero número %i:
No existe el fichero origen '%s'. Error ejecutando la opcion de instalación. ¿Desea intentarlo con otra? No se encontró el fichero %s. Instalando la librerira JAI imageIO... Instalando la JRE... Instalando la JRE suministrada... Inicializando Libreria JAI No se encontro la JRE. Versión de JRE Ejecutable MV Java  La versión de Java %s no es la requerida %s Lance el programa de instalacion para intentar reparar el problema. Lanzador Lanzando la aplicación... Lanzando la instalación... Seleccionar manualmente una Máquina Virtual Java  Preparando la instalacion de la libreria JAI... Preparando la instalacion de la libreria JAI imageIO... Preparando para instalar la JRE... Preparando para instalar la JRE suministrada... Se encontraron problemas con la JRE seleccionada:
%s
¿Desea continuar de todas formas? Buscando JRE... Seleccione una opción: La liberia JAI I/O bi esta instalada No pudo instalarse la liberia JAI. No pudo instalarse la liberia JAI imageIO. La liberia JAI no está instalada. No pudo instalarse la JRE. La aplicación necesita un Entrono de Ejecución Java (JRE) No pudo ejecutarse el comando %s. El fichero de configuracion '%s' contiene una clave de modo invalida. El fichero de configuración '%s' no contiene un clave para el comando. El fichero de configuracion '%s' no contiene un calve para el modo. El fichero de configuración '%s' no existe. El fichero %s no existe. La clave 'jre_home' no puede estar vacia cuando 'doChecks = No'. La instalación requiere descargar este fichero:
%s
¿Desea continuar? Actualizando el sistema... Copiando %s... Instalar una versión JRE  lista para funcionar en el directorio del usuario. Instalar los requisitos en el sistema. Seleccionar un JRE manualmente. Usar un JRE usado por una versión anterior de la aplicación. Usar el JRE definido en el sistema. 