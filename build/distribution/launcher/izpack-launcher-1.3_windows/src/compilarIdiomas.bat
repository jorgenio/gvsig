@echo off
color F0
PROMPT=$g


echo Compilando Idioma da...
gettext\bin\msgfmt.exe -o da\launcher.mo da\launcher.po

echo Compilando Idioma de...
gettext\bin\msgfmt.exe -o de\launcher.mo de\launcher.po

echo Compilando Idioma es...
gettext\bin\msgfmt.exe -o es\launcher.mo es\launcher.po

echo Compilando Idioma fr...
gettext\bin\msgfmt.exe -o fr\launcher.mo fr\launcher.po

echo Compilando Idioma it...
gettext\bin\msgfmt.exe -o it\launcher.mo it\launcher.po

echo Compilando Idioma nl...
gettext\bin\msgfmt.exe -o nl\launcher.mo nl\launcher.po

echo Compilando Idioma pt-BR...
gettext\bin\msgfmt.exe -o pt-BR\launcher.mo pt-BR\launcher.po

echo Termiando.
