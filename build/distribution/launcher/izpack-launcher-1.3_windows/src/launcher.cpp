/* Copyright (c) 2004 Julien Ponge - All rights reserved.
 * Some windows 98 debugging done by Dustin Sacks.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <wx/string.h>
#include <wx/file.h>
#include <wx/filename.h>
#include <wx/url.h>

#include "launcher.h"

#ifdef __UNIX__
  #include <stdlib.h>
  #include <stdio.h>
  #include <map>
  #include <list>
#endif


/*
 * Helper function to run an external program. It takes care of the subtule
 * differences between the OS-specific implementations.
 *
 * @param cmd The command to run.
 * @return <code>true</code> in case of a success, <code>false</code> otherwise.
 */
bool run_external(wxString cmd)
{
 int code = wxExecute(cmd, wxEXEC_SYNC);
 return (code == 0);

}


bool run_external_async(wxString cmd)
{
 return (wxExecute(cmd, wxEXEC_ASYNC) != 0);
}

bool run_external_withOut(wxString cmd,wxArrayString& out, wxArrayString& errors){	
	//printf(" run_external_withOut **%s**\n",cmd.c_str());
	int exitCode =wxExecute(cmd, out,errors);
	//printf("\texitCode= %i, out.Count = %i, error.Count= %i\n",exitCode,out.Count(),errors.Count());
	
	/*
	int i;
	for (i=0;i<out.Count();i++){
		printf("\t\tout: %s\n",out[i].c_str());
	}
	for (i=0;i<errors.Count();i++){
		printf("\t\terrors: %s\n",errors[i].c_str());
	}
	*/
	
	return ( exitCode == 0);
}


bool string_to_bool(wxString value, bool defaultValue){
 bool returnValue = defaultValue;
 if (value != wxEmptyString)
    if (
       value.CmpNoCase("s")   == 0  ||
       value.CmpNoCase("si")  == 0  ||
       value.CmpNoCase("1")   == 0  ||
       value.CmpNoCase("y")   == 0  ||
       value.CmpNoCase("yes") == 0
       ) 
    {
       returnValue = true;
    } else {
       returnValue = false;
    }
  return returnValue;
}


long LAUNCHAPP_TIMER_ID = wxNewId();

BEGIN_EVENT_TABLE(LauncherApp, wxApp)
    EVT_TIMER(LAUNCHAPP_TIMER_ID, LauncherApp::OnTimer)
END_EVENT_TABLE()


/* Main Application $Revision: 8474 $
 *
 * $Id: launcher.cpp 8474 2006-11-02 09:47:49Z jmvivo $
 */
LauncherApp::LauncherApp()
  : wxApp()
{
  APPLICATION_NAME = wxString(_("Launcher"));
  

  SetAppName(_( APPLICATION_NAME ));
}

void LauncherApp::echo(const wxString &msg) 
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK);
  dlg.ShowModal();
}

void LauncherApp::notifyToUser(const wxString &msg) 
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxICON_INFORMATION);
  dlg.ShowModal();
}

void LauncherApp::notifyErrorToUser(const wxString &msg) 
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxICON_ERROR);
  dlg.ShowModal();
}


LauncherApp::~LauncherApp()
{

}

void LauncherApp::loadParams()
{
  //cfgName = wxString( "launcher.ini" );
  cfgName =wxString(argv[0]);
  wxFileName execFileName(cfgName);
  execFileName.Normalize();
  execFileName.SetExt("ini");
  cfgName = execFileName.GetFullPath();
  if (!wxFileExists(cfgName)) {    
    error(cfgName.Format(_("The configuration file '%s' does not exists."),cfgName.c_str()));
  }
  wxString downloadEnabled;
  wxString launchJarAsync;
  wxString doChecks;


  paramsApplicationName = readPathFromINI( "appname","gvSIG");
  paramsJar = readPathFromINI( "jar",wxEmptyString);
  paramsCommand = readPathFromINI( "command",wxEmptyString);

  paramsJre = readPathFromINI( "jre",   wxEmptyString);
  paramsJreDownload = readFromINI( "downloadJre", wxEmptyString);

  paramsProvidedJre = readPathFromINI( "jreProvided",wxEmptyString);
  paramsProvidedJreDownload = readPathFromINI( "downloadjreProvided",   wxEmptyString);
  paramsProvidedJreInstalledJavaHome = readPathFromINI( "jreProvidedJavaHome",   wxEmptyString);
  paramsProvidedJreInstalledRootDirectory = readPathFromINI( "jreProvidedRootDirectory",   wxEmptyString);
  paramsProvidedJreInstallCmd = readFromINI("jreProvidedInstallCommand", wxEmptyString);



  paramsJai = readPathFromINI( "jai",              wxEmptyString);
  paramsJaiDownload = readFromINI( "downloadJai",        wxEmptyString);
  paramsJaiIo = readPathFromINI( "jai_io",wxEmptyString);
  paramsJaiIoDownload = readFromINI( "downloadJai_io",wxEmptyString);

  paramsJreVersion = readFromINI( "jre_version",wxEmptyString);
  paramsJreVersionPrefered = readFromINI( "jre_version_prefered",wxEmptyString);

  downloadEnabled = readFromINI( "downloadEnabled",wxEmptyString);
  launchJarAsync = readFromINI( "launchJarAsync", wxEmptyString);
  doChecks = readFromINI( "doChecks",wxEmptyString);
  paramsJreHome = readPathFromINI( "jre_home", wxEmptyString);
  paramsLaunchMode = readPathFromINI( "launchMode", wxEmptyString);



  //procesamos el parametro booleano 'downloadEnabled'
  paramsEnabledDownload =string_to_bool(downloadEnabled,false);
  //procesamos el parametro booleano 'launchJarAsync'
  paramsLaunchJarAsync =string_to_bool(launchJarAsync,false);
  //procesamos el parametro booleano 'doChekc'
  paramsDoChecks=string_to_bool(doChecks,true);
  
  if (paramsCommand == wxEmptyString )
  {
    error(cfgName.Format(_("The configuration file '%s' does not contain a command entry."),cfgName.c_str()));
  }
  if (paramsLaunchMode == wxEmptyString )
  {
    error(cfgName.Format(_("The configuration file '%s' does not contain a mode entry."),cfgName.c_str()));
  }
  paramsLaunchMode = paramsLaunchMode.MakeUpper();
  if ((!paramsLaunchMode.IsSameAs("APPLICATION")) && (!paramsLaunchMode.IsSameAs("INSTALL"))) {
	error(cfgName.Format(_("The configuration file '%s' contains a invalid mode entry."),cfgName.c_str()));
  }
  
  if (paramsJreVersionPrefered == wxEmptyString ) {
     paramsJreVersionPrefered = wxString(paramsJreVersion);
  }
  if ((!paramsDoChecks) && paramsJreHome == wxEmptyString) {
     error(cfgName.Format(_("The file entry 'jre_home' can not be empty when 'doChecks = No'."),cfgName.c_str()));
  }



  variables = sectionKeysFromINI("Variables");

  variablesValues = sectionKeysValuesFromINI("Variables",variables);

}

void LauncherApp::error(const wxString &msg)
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxICON_ERROR);
  dlg.ShowModal();
  exit(1);
}

void LauncherApp::confirm(const wxString &msg)
{
  int response;
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxCANCEL | wxICON_QUESTION );
  response = dlg.ShowModal();
  if (response != wxID_OK) {
    notifyToUser(_("Canceled by user"));
    exit(1);
  }
}
bool LauncherApp::confirmYesNoCancel(const wxString &msg,const int yesDefault = true)
{
  int response;
  long maskDefault;
  if (yesDefault) {
     maskDefault = wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_QUESTION;
  } else {
     maskDefault = wxNO_DEFAULT | wxYES_NO | wxCANCEL | wxICON_QUESTION;       
  }
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME),  maskDefault);
  response = dlg.ShowModal();
  if (response != wxID_YES && response != wxID_NO) {
    notifyToUser(_("Canceled by user"));
    exit(1);
  }
  return (response == wxID_YES);
 
}



bool LauncherApp::OnInit()
{
  isStatusDialogVisible = false;
  wxFileName exeFileName(argv[0]);
  exeFileName.Normalize();
  wxFileName exePath = exeFileName.GetPath();
  exePath.Normalize();
  
  exePath.SetCwd();

  locale.Init();
  locale.AddCatalog("launcher");
  loadParams();

  
  if (paramsLaunchMode.IsSameAs("INSTALL")){  
	APPLICATION_NAME = wxString(APPLICATION_NAME.Format(_("%s Install-Launcher"),paramsApplicationName.c_str()));
	SetAppName(_(APPLICATION_NAME));
	showStatusWindow();
  } else {
	APPLICATION_NAME = wxString(APPLICATION_NAME.Format(_("%s Launcher"),paramsApplicationName.c_str()));
	SetAppName(_(APPLICATION_NAME));
  }

  showStatusMsg(_("Intializing..."));
  
  myTimer = new wxTimer(this,LAUNCHAPP_TIMER_ID);
  myTimer->Start(150,true);
  this->MainLoop();
  return true;
}

bool LauncherApp::compareVersions(const wxString localVersion, const wxString requireVersion)
{
    bool result  = (localVersion.Cmp(requireVersion) == 0);
    /*
	if (requireVersion.Len() > localVersion.Len() )	{   
        
		result = (localVersion.Find(requireVersion) == 0);
	}
	else 
	{
		result = (requireVersion.Find(localVersion) == 0);
	}
	*/

    /*
	if (result) {
 	   echo("localversion = '" + localVersion +"' requireVersion = '"+ requireVersion +"' ==> true");
    } else {
       echo("localversion = '" + localVersion +"' requireVersion = '"+ requireVersion +"' ==> false");
    }
    */
     
	return result;
}


void LauncherApp::runJRE()
{  
  if (paramsLaunchMode.IsSameAs("INSTALL"))	{
    showStatusMsg(_("Launching installation..."));
  } else {
    showStatusMsg(_("Launching application..."));
  }
  wxString cmd = parseCommand();
  if (paramsLaunchJarAsync) {
      if (!run_external_async(cmd))
      {
         error(cmd.Format(_("The command\n%s\ncould not be executed."),cmd.c_str()));
      }
  } else {
      if (!run_external(cmd))
      {
        error(cmd.Format(_("The command\n%s\ncould not be executed."),cmd.c_str()));
      }  
  }

}

wxString LauncherApp::installProvidedJRE(){
	showStatusWindow();
	showStatusMsg(_("Preparing to install Provided JRE..."));

	if (!checkInstallerFile(paramsProvidedJre, paramsProvidedJreDownload,_("Java JRE version ")+paramsJreVersion))
	{
		notifyErrorToUser(_("The JRE could not be setup."));
		return "";
	}

	showStatusMsg(_("Installing Provided JRE..."));

	wxString cmd = parseString(paramsProvidedJreInstallCmd);

	if (!run_external(cmd))
	{
		notifyErrorToUser(_("The JRE could not be setup."));
		return "";
	}

	wxString myJavaFile = calculateJavaExePath(parseString(paramsProvidedJreInstalledJavaHome));

	if (!wxFileExists(myJavaFile)){
		notifyErrorToUser(wxString::Format(_("The file %s does not exist."),myJavaFile.c_str()));
		return "";
  	}

  return myJavaFile;

}

IMPLEMENT_APP(LauncherApp)

bool LauncherApp::downloadFileHttp(const wxString urlOfFile, const wxString filePath, const wxString msg){

  bool isOk;
  //echo(urlOfFile);
  if (urlOfFile == wxEmptyString) {
     return false;
  }
  
  if (filePath == wxEmptyString) {
     return false;
  }
  
  showStatusMsg(msg);
  wxURL url(urlOfFile);
  //echo("url open");
  wxInputStream *in_stream;

  in_stream = url.GetInputStream();
  //echo("in_stream open");
  if (!in_stream){
	return false;
  }
  if (!in_stream->IsOk()) {
     //echo("in_stream.IsOk == false");
     return false;
  }
  //echo("in_stream.IsOk == true");
  //echo("filePath =" + filePath);
  wxFileName fileName(filePath); 
  fileName.Normalize();
  wxFileName fileDir(fileName.GetPath()); 
  if (!fileDir.DirExists()) {
	if (!fileDir.Mkdir(0777,wxPATH_MKDIR_FULL )) {
		return false;
	}
  }
 
  wxFileOutputStream out_stream  = wxFileOutputStream(filePath);
  //echo("out_stream open");

  //in_stream->Read(out_stream);
  size_t nbytes = 10240;
  char buffer[nbytes];

 
  while (!in_stream->Eof()) {
      in_stream->Read(&buffer,nbytes);  
      if (in_stream->LastError() != wxSTREAM_NO_ERROR && in_stream->LastError() != wxSTREAM_EOF) {
          return false;
      }
      out_stream.Write(&buffer,in_stream->LastRead());
      if (out_stream.LastError() != wxSTREAM_NO_ERROR) {
         return false;
      }
      int totalKb = out_stream.GetSize() / 1024;
      wxString totalMsg =wxString::Format("%i",totalKb);
      showStatusMsg(msg+" "+ totalMsg + " Kb");

  }
 
  
  isOk = true;
 /*
  if (isOk) {
    echo("isOk = true");
  } else {
    echo("isOk = false");
  }
 */
  delete in_stream;
  
  //echo("end");
  return isOk;

}

bool LauncherApp::checkInstallerFile(const wxString filePath, const wxString urlDownload, const wxString fileDescription) {
  if (!wxFile::Exists(filePath))
  {
    if (paramsEnabledDownload) 
    {
      wxString msg;
      msg = wxString::Format(_("The installation requires to download this file:\n%s\nDo you want to continue?"),fileDescription.c_str());
      confirm(msg);
      msg = wxString::Format(_("Downloading %s..."),fileDescription.c_str());
      if (!downloadFileHttp(urlDownload,filePath,msg)) 
      {
        //FIXME: Falta msgError
        return false;
      }
    }
    else
    {
      return false;
    }

  }
  return true;
}

void LauncherApp::showStatusMsg(const wxString msg) {
  if (isStatusDialogVisible) {
     while (this->Pending()) {
           this->Dispatch();
     }
     if (statusDialog->IsCanceled()) {
        int response;
        wxMessageDialog dlg(0, _("Are you sure you want to cancel installation?"), _(APPLICATION_NAME), wxYES_NO | wxICON_QUESTION );
        response = dlg.ShowModal();
        if (response == wxID_YES) {
            notifyToUser(_("Canceled by the user"));
            exit(1);
        }
        statusDialog->DoNotCancel();
     }
     statusDialog->showStatus(msg);
  }
}

void LauncherApp::run(){
  bool ok = false;
  
  wxString mJavaFileName;
  wxString errorMsg;
  if (paramsLaunchMode.IsSameAs("INSTALL")){ 
	  showSelectionDialog();
  }else{
	  if (!paramsDoChecks) {
		  setJavaExePath(calculateJavaExePath(paramsJreHome));
	  } else {
		errorMsg = checkJava(mJavaFileName);		
		//echo("check result: *"+errorMsg+"*");
		if (errorMsg.Cmp("") == 0) {
			setJavaExePath(mJavaFileName);
			ok=true;
		} else {
			wxString msg =wxString::Format(_("Problems found in the selected JRE:\n%s\n\nDo you want to continue?"),errorMsg.c_str());
			if (confirmYesNoCancel(msg,false)) {
				setJavaExePath(mJavaFileName);
			} else {
				notifyToUser(_("Launch installation program to try to repare the problem."));
				notifyToUser(_("Canceled by the user"));
				exit(1);
			}
		}
	  }
	  

  }

  copyRequiredFiles();

  runJRE();

  exit(0);    
     
}

void LauncherApp::OnTimer(wxTimerEvent& event) {     
     run();
}

wxString LauncherApp::selectJavaExeFileManualy() {     
    wxString msgDir = _("Manually select the Java VM");   
    wxFileDialog dlg(
        0,
        msgDir,
        wxEmptyString, 
        wxString("java.exe"),
        wxString( wxString(_("Java VM executable file")) + " (java.exe)|java.exe"),
        wxOPEN | wxFILE_MUST_EXIST,
        wxDefaultPosition
    );


    int response = dlg.ShowModal();
    if (response != wxID_OK) {
       notifyToUser(_("Canceled by the user"));
       exit(1);
    }
    wxFileName fileName(dlg.GetPath());

    fileName.SetFullName("");
    fileName.AppendDir("..");
    fileName.Normalize();
    return calculateJavaExePath(fileName.GetPath());

}


wxString LauncherApp::getJavaExeFileVersion(wxString javaFileName) {
	wxArrayString output, errors;	
	wxString cmd =javaFileName + " GetJavaSystemProperties java.version";
	//echo(cmd);
	if (!run_external_withOut(cmd,output, errors)) {
		return "";
	}
	if (output.Count() == 0){
		return "";
	}
	return output[0];
	
}

wxString LauncherApp::getJavaExeFileHome(wxString javaFileName) {
	wxArrayString output, errors;
	wxString cmd =javaFileName + " GetJavaSystemProperties java.home";
	if (!run_external_withOut(cmd,output, errors)) {
		return "";
	}
	if (output.Count() == 0){
		return "";
	}	
	return output[0];
	
}


wxString LauncherApp::checkJava(const wxString aJavaFileName) {
	wxString msg = "";
	wxString version;
	wxString home;	
	
	
	if (!wxFileExists(aJavaFileName)){
		return wxString::Format(_("File %s not found."),aJavaFileName.c_str());//_("File %s not found."), javaFileName);
	};
	
	
	version = getJavaExeFileVersion(aJavaFileName);
	
	if (version.Cmp("") == 0){
		return wxString::Format(_("Can't identify version of %s."), aJavaFileName.c_str());
	}
	
	version = version.Left(5);
	if (!compareVersions(version,paramsJreVersion)) {
		msg = "\n" + wxString::Format(_("Java version %s is not the required %s"),version.c_str(),paramsJreVersion.c_str());
	}
	
	home = getJavaExeFileHome(aJavaFileName);
	if (home.Cmp("") == 0){
		return wxString::Format(_("Can't identify java_home of %s."), aJavaFileName.c_str());
	}
	// comprobamos JAI
	if (!checksJai(home)) {
		msg = msg +"\n" + _("The JAI library is not installed");
	}
	// comprobamos JAI i/o
	if (!checksJaiIo(home)) {
		msg = msg +"\n" + _("The JAI I/O library is not installed");
	}	
	
	return msg;
}

bool LauncherApp::checkEnvironmentJRE() {
	return (useEnvironJRE().Cmp("") != 0);
}

wxString  LauncherApp::useEnvironJRE() {
  wxString version("");

  // Windows[tm] registry lookup
  wxString baseKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft"
                     "\\Java Runtime Environment\\";

  wxRegKey bKey(baseKey);
  if (!bKey.Exists())  {
     return "";
  }

  if (!bKey.QueryValue("CurrentVersion", version)){
     return "";
  }
  

  if (version == "") {
     return "";
  }            
  wxRegKey vKey(baseKey + version);
  wxString theJavaHome;

  if (!vKey.QueryValue("JavaHome", theJavaHome)){
     return "";
  }
  
  
  return calculateJavaExePath(theJavaHome);
	
}


bool LauncherApp::checkPreviousJRE() {
	wxFileName appHomeJre = wxString(parseString(paramsProvidedJreInstalledRootDirectory));
	
	//echo(appHomeJre.GetFullPath());
	appHomeJre.MakeAbsolute();
	//echo(appHomeJre.GetFullPath());
	appHomeJre.Normalize();

	//echo(appHomeJre.GetFullPath());
	
	if (!appHomeJre.DirExists())  {
		return false;
	}
	
	
	return (!wxFindFirstFile(appHomeJre.GetFullPath()+"/"+paramsJreVersion+"*",wxDIR).IsEmpty()); 
}

wxString  LauncherApp::previousJRE() {
	wxString root(parseString(paramsProvidedJreInstalledRootDirectory));
	if (paramsJreVersionPrefered.IsEmpty()) {
		wxFileName prefered = calculateJavaExePath(root+"/"+paramsJreVersionPrefered);
		prefered.Normalize();
		if (prefered.FileExists()){
			return prefered.GetFullPath();
		}
	}
	wxString mask = calculateJavaExePath(root+"/" + paramsJreVersion + "_%02d");
	for(unsigned i=20;i>1;i--) {
		wxFileName current;
        	current = wxString::Format(mask,i);
		current.Normalize();
        	if (current.FileExists()) {
			return current.GetFullPath();
		}
	}
	return "";
}


wxString LauncherApp::readPathFromINI(wxString section, wxString key, const wxString defaultValue){
	char* charResult;

	charResult =myGetPrivateProfileString(section.c_str(),key.c_str(),defaultValue.c_str(),cfgName.c_str()); 

	//Remplazamos \ por /
	
	register char* s = charResult;
	for (;*s;s++) {
		if (*s == '\\') {
			*s = '/';
		}
	}

	wxString result(charResult);
	return result;


}



wxString LauncherApp::readPathFromINI(wxString key, const wxString defaultValue){
	return readPathFromINI("default",key,defaultValue);

}

wxString LauncherApp::readFromINI(wxString section, wxString key, const wxString defaultValue){
	char* charResult;

	charResult =myGetPrivateProfileString(section.c_str(),key.c_str(),defaultValue.c_str(),cfgName.c_str()); 

	wxString result(charResult);
	return result;
}

wxString LauncherApp::readFromINI(wxString key, const wxString defaultValue){
	return readFromINI("default",key,defaultValue);
}

wxArrayString LauncherApp::sectionKeysFromINI(const wxString section){
	char* charResult;

	charResult =myGetPrivateProfileString(section.c_str(),NULL,NULL,cfgName.c_str()); 
	//echo(section +" = "+charResult);

	wxArrayString rvalue;
	char* token;
	
	for (
		token = strtok(charResult,"="); 
		token;
		token = strtok(NULL,"=") 
		
	    ) {
		rvalue.Add(token);
	}
	return rvalue;
}


wxArrayString LauncherApp::sectionKeysValuesFromINI(const wxString section,wxArrayString keys) {
	wxArrayString rvalue;
	unsigned int i;
	wxString key;
	wxString value;
	for (i=0;i < keys.GetCount(); i++){
		rvalue.Add(readPathFromINI( section, keys[i], "")); 
	}	
	return rvalue;
}





bool LauncherApp::copyRequiredFiles(){
  int i = 0;
  //wxFileInputStream in( cfgName );
  //wxFileConfig cfg( in );
  //cfg.SetPath("/CopyRequiredFiles");

  wxString source;
  wxString target;
  wxString msg;
  wxFileName fileName;

  while (true) {
	i++;

	source = readPathFromINI("CopyRequiredFiles",source.Format("source%i",i),wxEmptyString);
	target = readPathFromINI("CopyRequiredFiles",target.Format("target%i",i),wxEmptyString);
	if (source == wxEmptyString && target == wxEmptyString) {
		return true;
	}
	if (source == wxEmptyString || target == wxEmptyString) {
		error(source.Format(_("Error copying the file number %i:\n missing source or target entry"),i));
		return false;
	}
	//source = parseString(source);
	//target = parseString(target);
	if (wxFileExists(target)){
		continue;
	} else {
	    if (wxDirExists(target)) {
	        fileName = wxFileName(target);
		fileName.Normalize();
		wxFileName tempFileName(source);
		tempFileName.Normalize();
	        fileName.SetFullName(tempFileName.GetFullName());
		if (fileName.FileExists()) {
			continue;
		}
	    }
				
	}
	if (!wxFileExists(source)){
		error(source.Format(_("Error copying the file number %i:\n source file '%s' does not exists."),i,source.c_str()));
		return false;
	}

	fileName = wxFileName(source);
	fileName.Normalize();
	msg = msg.Format(_("copying %s..."), fileName.GetFullName().c_str());
	//echo(msg);
	//echo(msg.Format("%s --> %s",source.c_str(),target.c_str()));
	if (!copyFile(source,target, msg)){
		error(source.Format(_("Error copying the file number %i:\n '%s' --> '%s'."),i,source.c_str(),target.c_str()));
		return false;
	}

  }
}

bool LauncherApp::copyFile(const wxString source, const wxString target, const wxString msg){

  bool isOk;
  //echo(source);
  if (source == wxEmptyString) {
     return false;
  }
  
  //echo(target);
  if (target == wxEmptyString) {
     return false;
  }
  
  showStatusMsg(msg);

  wxFileName targetFileName(target);
  targetFileName.Normalize();
  
  
  // Preparamos la ruta para el fichero destino
  wxArrayString dirs;
  
  dirs = targetFileName.GetDirs();
  wxString dir;
  wxFileName curDir;
  curDir.Normalize();
  if (targetFileName.IsAbsolute()) {
	curDir.Assign("\\");
	curDir.SetVolume(targetFileName.GetVolume());
  }else{
	curDir.Assign(".");
  }
  for (size_t i=0; i < dirs.GetCount(); i++) {
      dir = dirs.Item(i);
      curDir.AppendDir(dir);
      curDir.Normalize();
      //echo("dir " + curDir.GetPath());
      if (!curDir.DirExists()) {
         //echo("creating dir");
         isOk = curDir.Mkdir();
         if (!isOk) {
            //echo("dir create no ok");
            return false;
         }
         
         //echo("dir create ok");
      }
  }

  wxString finalTarget;
  if (targetFileName.IsDir() || targetFileName.DirExists()) { 
      //echo("targetFileName.IsDir -> true  " + targetFileName.GetFullPath());
      wxFileName sourceFileName(source);
      sourceFileName.Normalize();
      targetFileName.SetFullName(sourceFileName.GetFullName());
      finalTarget = targetFileName.GetFullPath();
  } else {
      //echo("targetFileName.IsDir -> false  " + targetFileName.GetFullPath());
      finalTarget = target;
  }


  //echo(msg.Format("%s --> %s",source.c_str(),finalTarget.c_str()));
  isOk = wxCopyFile(source,finalTarget,false);
	

  return isOk;

}



void LauncherApp::showStatusWindow(){
  if (isStatusDialogVisible) {
    return;
  }
  
  statusDialog = new StatusDialog( APPLICATION_NAME );

  statusDialog->Centre();
  statusDialog->Show(TRUE);
  isStatusDialogVisible = true;
}



wxString LauncherApp::parseCommand(){

  return parseString(paramsCommand);
}

wxString LauncherApp::parseString(const wxString theString){
  //echo("IN: " +theString);
  wxString resultCommand(theString);

  // #JAVA#
  resultCommand.Replace("#JAVA#",javaExePath.c_str());
  
  // #JAVA_HOME#
  resultCommand.Replace("#JAVA_HOME#",javaHome.c_str());

  // #JAR#
  resultCommand.Replace("#JAR#",paramsJar.c_str());

  // calculamos la cadena args y sustituimos los #ARG?#
  wxString theArgs("");
  wxString theArg("");
  int i;
  for (i=1;i<this->argc;i++) {
    theArg = argv[i];
    if (i!=1){
    	theArgs = theArgs + " \"" + theArg + "\"";
    }else{
    	theArgs = "\"" + theArg + "\"";
    }

    resultCommand.Replace(theArg.Format("\"#ARG%i#\"",i),theArg.c_str());
  }
  // Dejamos a blanco los argumento que no hemos recivido y existen en el comando
  theArg = "";
  for (i=i;i<10;i++) {
    resultCommand.Replace(theArg.Format("#ARG%i#",i),theArg.c_str());
  }
  //echo(theArgs);

  // #ARGS#
  resultCommand.Replace("#ARGS#",theArgs.c_str());
  
  // #ARG0#
  resultCommand.Replace("#ARG0#",argv[0]);

  // variables de la seccion variables

  //echo(resultCommand);
  
//  
  if (!variables.IsEmpty()){
	  //echo(theArg.Format("No empty: count =%i",keys.GetCount()));
	  unsigned int i;
	  for (i=0;i<variables.GetCount();i++){
		  //echo("#"+variables[i]+"#="+variablesValues[i]);
		  resultCommand.Replace("#"+variables[i]+"#",variablesValues[i]);
	  }

  //echo(resultCommand);

  }


  //Despejamos las variables de sistema (WINDOWS)
  //
  //De momento solo algunas
  
  wxString var;
  wxGetEnv("windir",&var);
  resultCommand.Replace("%windir%",var);

  wxGetEnv("USERPROFILE",&var);
  resultCommand.Replace("%USERPROFILE%",var);
 
  wxGetEnv("USERNAME",&var);
  resultCommand.Replace("%USERNAME%",var);

  wxGetEnv("TEMP",&var);
  resultCommand.Replace("%TEMP%",var);

  wxGetEnv("TMP",&var);
  resultCommand.Replace("%TMP%",var);

  wxGetEnv("ProgramFiles",&var);
  resultCommand.Replace("%ProgramFiles%",var);

  wxGetEnv("SystemRoot",&var);
  resultCommand.Replace("%SystemRoot%",var);


  //echo("OUT: " +resultCommand);
  return resultCommand;
}

wxString LauncherApp::calculateJavaExePath(const wxString aJavaHome){
  int osVerMayor;
  int osVerMinor;

  wxGetOsVersion(&osVerMayor,&osVerMinor);
  if (osVerMayor < 5) {
    return aJavaHome + "/bin/java.exe";
  }else{
    return aJavaHome + "/bin/javaw.exe";
  }
    
}

wxString LauncherApp::searchJRE()
{
  showStatusMsg(_("Searching JRE's..."));
  wxString version("");

  // Windows[tm] registry lookup
  wxString baseKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft"
                     "\\Java Runtime Environment\\";

  wxRegKey bKey(baseKey);
  if (!bKey.Exists())  {
     return "";
  }

  if (!bKey.QueryValue("CurrentVersion", version)){
     return "";
  }
  
  if (version == "1.1") {
     return "";
  }

  if (!compareVersions(version, paramsJreVersionPrefered)){
	//Nos recorremos las versiones instaladas
	version = "";
	wxString strTemp;
	wxRegKey sKey(baseKey);
	if (sKey.HasSubKey(paramsJreVersionPrefered)) {
      version = wxString(paramsJreVersionPrefered);
    } else {              
	  for(unsigned i=20;i>1;i--) {
        strTemp = wxString::Format(paramsJreVersion + "_%02d",i);
        if (sKey.HasSubKey(strTemp)) {
			version = strTemp;
			break;
		}
	  }               
    }
  }

	
  if (version == "") {
     return "";
  }            
  return version;


 
}

wxString LauncherApp::getJavaHomeFromVersion(const wxString aVersion){
  wxString baseKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft"
                     "\\Java Runtime Environment\\";
  wxRegKey vKey(baseKey + aVersion);
  wxString theJavaHome;

  if (!vKey.QueryValue("JavaHome", theJavaHome)){
     return "";
  }

  return theJavaHome;

}

wxString LauncherApp::installJRE(){

  wxString localVersion = searchJRE();
  
  if (localVersion.Cmp("")!=0){
      jreInstall();
      localVersion = searchJRE();
  }

  wxString theJavaHome = getJavaHomeFromVersion(localVersion);


  fixSystemJREConfig(localVersion);

  jaiInstall(theJavaHome,localVersion);

  jaiIoInstall(theJavaHome,localVersion);

  return calculateJavaExePath(theJavaHome);
}



void LauncherApp::jreInstall()
{
  showStatusWindow();
  
  showStatusMsg(_("Preparing to install JRE..."));
  
  if (!checkInstallerFile(paramsJre, paramsJreDownload,_("Java JRE version ")+paramsJreVersion))
  {
    error(_("The JRE could not be setup."));  
  }

  showStatusMsg(_("Installing JRE..."));
  if (!run_external(paramsJre))
  {
    error(_("The JRE could not be setup."));
  }
  if (!searchJRE()) {
    error(_("The JRE could not be setup."));
  }    
}


void LauncherApp::fixSystemJREConfig(const wxString aVersionToUse){
  showStatusMsg(_("Updating the system..."));
  //actualizamos CurrentVersion
  wxString baseKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft"
                     "\\Java Runtime Environment\\";
                     
  wxString genericVersion = paramsJreVersion.Left(3);
  
  wxString baseKeyGeneric = baseKey + genericVersion + "\\";
  wxString baseKeyVersion = baseKey + aVersionToUse + "\\";

  wxRegKey *pRegKey = new wxRegKey(baseKey);
  if( !pRegKey->Exists() ) {
    error(_("JRE not found."));
  }
  
  // compiamos el contenido de la rama de la version
  // que queremos a la generica (1.4 o 1.5 o ...)
  wxRegKey *pRegKeyGeneric = new wxRegKey(baseKeyGeneric);
  wxRegKey *pRegKeyVersion = new wxRegKey(baseKeyVersion);
  
  if ( !pRegKeyGeneric->Exists() ){
    pRegKeyGeneric->Create();  
  }
  wxString tempKey;
  wxString tempValue;
  size_t nValues;
  
  pRegKeyVersion->GetKeyInfo(NULL,NULL,&nValues,NULL);
  long pos = 1;
  pRegKeyVersion->GetFirstValue(tempKey,pos);
  for(unsigned i=0;i<nValues;i++)	{
      //echo("copy " + tempKey);
      pRegKeyVersion->QueryValue(tempKey,tempValue);
      pRegKeyGeneric->SetValue(tempKey,tempValue);
      pRegKeyVersion->GetNextValue(tempKey,pos);
  }
    
  
}

bool LauncherApp::checksJai(const wxString aJavaHome){
   return (wxFileExists(aJavaHome + "\\lib\\ext\\jai_core.jar"));
}

bool LauncherApp::checksJaiIo(const wxString aJavaHome){
   return (wxFileExists(aJavaHome + "\\lib\\ext\\jai_imageio.jar"));
}

void LauncherApp::jaiInstall(const wxString aJavaHome, const wxString aVersionToUse)
{
  bool isOK;
  showStatusMsg(_("Checking JAI Library..."));
  
  wxString baseKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft"
                     "\\Java Runtime Environment\\";

  wxString currentVersion;
  wxRegKey *pRegKey = new wxRegKey(baseKey);
  if( !pRegKey->Exists() )
    error(_("JRE not found."));
  if (!pRegKey->QueryValue("CurrentVersion", currentVersion)) error(_("JRE not found."));
  
  isOK=true;  
  if (!checksJai(aJavaHome)) {
      //confirm(_("JAI library is required, Install it?"));
      showStatusWindow();
      
      showStatusMsg(_("Preparing to install JAI Library..."));
      if (!checkInstallerFile(paramsJai, paramsJaiDownload, _("JAI Library")))
      {
        isOK=false;  
      } else {
        pRegKey->SetValue("CurrentVersion",aVersionToUse);
        showStatusMsg(_("Preparing to install JAI Library..."));
        if (run_external(paramsJai))
        {
          isOK=(checksJai(aJavaHome));
        } else {
          isOK=false;
        }  
        pRegKey->SetValue("CurrentVersion",currentVersion);
      }
      
  }  
  if (!isOK) {
    error(_("The JAI could not be setup."));
  }
}



void LauncherApp::jaiIoInstall(const wxString aJavaHome, const wxString aVersionToUse)
{
  bool isOK;
  showStatusMsg(_("Checking JAI imageIO Library..."));
  
  wxString baseKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft"
                     "\\Java Runtime Environment\\";

  wxString currentVersion;
  wxRegKey *pRegKey = new wxRegKey(baseKey);
  if( !pRegKey->Exists() )
    error(_("JRE not found."));
  if (!pRegKey->QueryValue("CurrentVersion", currentVersion)) error(_("JRE not found."));
  

  isOK=true;  
  if (!checksJaiIo(aJavaHome)) {
      //confirm(_("JAI ImageIO library is required, Install it?"));
      showStatusWindow();
      
      showStatusMsg(_("Preparing to install JAI imageIO Library..."));
      if (!checkInstallerFile(paramsJaiIo, paramsJaiIoDownload,"JAI ImageIO Library"))
      {
        isOK=false;  
      } else {
        pRegKey->SetValue("CurrentVersion",aVersionToUse);
        showStatusMsg(_("Installing JAI imageIO Library..."));
        if (run_external(paramsJaiIo))
        {                                    
          isOK=(checksJaiIo(aJavaHome));
        } else {
          isOK=false;        
        }
         pRegKey->SetValue("CurrentVersion",currentVersion);
      }
     
  }  
  if (!isOK) {
             error(_("The JAI imageIO could not be setup."));
  }
}


void LauncherApp::showSelectionDialog(){
  bool ok = false;
  
  wxString mJavaFileName;
  wxString errorMsg;
  SelectionDialog  dlg( checkPreviousJRE(),checkEnvironmentJRE() ,_(APPLICATION_NAME));
  while (!ok) {
	if (dlg.ShowModal() != wxID_OK){
		notifyToUser(_("Canceled by the user"));
		exit(1);
	}
	switch (dlg.getUserAction())
	{
		case PREVIOUS_GVSIG:  mJavaFileName = previousJRE(); break;
		case PROVIDED: mJavaFileName = installProvidedJRE(); break;
		case INSTALL: mJavaFileName = installJRE(); break;
		case SELECT_MANUALY: mJavaFileName = selectJavaExeFileManualy();  break;
		case ENVIRON: mJavaFileName = useEnvironJRE(); break;
		default: break;
	}
	if (mJavaFileName.Cmp("") == 0) {
		wxString msg =wxString::Format(_("Error executing this installation option. Do you want to try another one?"),errorMsg.c_str());
		confirm(msg);
	} else {		
		//echo("checking..." + mJavaFileName);
		errorMsg = checkJava(mJavaFileName);		
		//echo("check result: *"+errorMsg+"*");
		if (errorMsg.Cmp("") == 0) {
			setJavaExePath(mJavaFileName);
			ok=true;
		} else {
			wxString msg =wxString::Format(_("Problems found in the selected JRE:\n%s\n\nDo you want to continue?"),errorMsg.c_str());
			if (confirmYesNoCancel(msg,false)) {
				setJavaExePath(mJavaFileName);
				ok=true;	
			}
		}
	}
  }

}

void LauncherApp::setJavaExePath(const wxString aJavaExePath){
	javaExePath = aJavaExePath;
	javaHome = getJavaExeFileHome(aJavaExePath);

}
