
#include <windows.h>

#include "myIniReader.h"

char* myGetPrivateProfileString(const char* section,const  char* key, const char* defaultValue, const char* fileName) {
	static char szResult[2048];

	memset(szResult, 0x00, 2048);

	GetPrivateProfileString(section,  key, defaultValue, szResult, 2048, fileName); 
	return szResult;
}
