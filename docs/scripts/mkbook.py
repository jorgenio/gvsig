#!/usr/bin/python
import os
import sys
import shutil
import re


class RootDir(object):
    def __init__(self,path):
        self._path = os.path.abspath(path)
        
    def __str__(self):
        return self._path
    
    def data(self,*dirs):
        return os.path.join(self._path,"datos",*dirs)
        
    def docbook(self,*dirs):
        return os.path.join(self._path,"docbook",*dirs)

    def __call__(self,*dirs):
        return os.path.join(self._path,*dirs)

def getExtension(fname):
    """
    Retorna la extension del fichero con punto,
        "fff.xxx" -> ".xxx"
    """
    return os.path.splitext(fname)[1]
        
def removeExtension(fname):
    """
    Retorna el nombre del fichero sin la extension.
    Quita tambien el punto.
        "fff.xxx"  -> "fff"
    """
    return os.path.splitext(fname)[0]

def shell(cmd,head=None):
	status = 0
	childstdin, childstdout, childstderr = os.popen3(cmd)
	while True:
		line = childstdout.readline()
		if line in (None,""):
			break
		print line
	if head == None:
		head = "Error ejecutando '%s'" % (cmd)
	while True:
		line = childstderr.readline()                    	
		if line in (None,""):
			break
		if head != None:
			print head
			head = None
			status=1
		print ">>> "+line
	return status
	
class ProcessDocBook(object):
    def __init__(self, sourceDir, tempDir, distDir):
        self.sourceDir = RootDir(sourceDir)
        self.tempDir = RootDir(tempDir)
        self.distDir = RootDir(distDir)
        self.imageCounter = 1
        self.status=0
    
    def __str__(self):
    	return "ProcessDocBook( sourceDir =%s, tempDir=%s, distDir=%s, imageCounter=%s, status=%s)"%(
    		self.sourceDir,
        	self.tempDir,
	        self.distDir,
    	    self.imageCounter,
    	    self.status
    	    )
    	    
    def nextImageCounter(self):
        self.imageCounter += 1
        return self.imageCounter
        
    def toTempPath(self,*path):
      path = os.path.join(*path)
      return os.path.join(
          str(self.tempDir),
          path[ len(str(self.sourceDir))+1: ]
      )

    def copyFromTo(self,source,dest):

		try:
			shutil.copy(source,dest)
		except OSError, e:
			if e.errno!=1:
				print "copyFromTo: Error copiando la fuente %s a destino %s. Error %s, %s"%(
				sourceImage,
				targetImage,
				str(e.__class__),
				str(e)
				)
				raise                
				
		return
    	
    def makedirs(self,*dirs):
        path = os.path.join(*dirs)
        try:
            os.makedirs(path)
        except OSError, e:
            if e.errno!=17:
                print "makedirs: Se ha producido un error creando el directorio '%s', error %s, %s" % (
                    path,
                    str(e.__class__),
                    str(e)
                )
                raise
        return
    
    def sxw2docbook(self, pathnameSXW):
        """
        Copia el fichero SXW al temp, y lo transforma en un 
        docbook.
        """
        pathnameSXWInTemp = self.toTempPath(pathnameSXW)
        #print "Procesando %s --> %s" % (pathnameSXW,pathnameSXWInTemp)
        #shutil.copyfile(pathnameSXW,pathnameSXWInTemp)
        self.copyFromTo(pathnameSXW,pathnameSXWInTemp)
        os.chdir(os.path.dirname(pathnameSXWInTemp))
        shell("ooo2dbk -a -c/etc/ooo2dbk.xml '%s'" % pathnameSXWInTemp)
        os.unlink(pathnameSXWInTemp)
        pathnameSXWInTempWhitoutExt = removeExtension(pathnameSXWInTemp).replace(" ","_")
        os.rename(pathnameSXWInTempWhitoutExt+".docb.xml",pathnameSXWInTempWhitoutExt+".docbook")

    def getImageListOfSXW(self, pathnameSXW):
        """
        Retorna una lista con las imagenes extraidas
        del fichero SXW, y una propuesta de nombre para
        renombrarla.
        """
        #print "obteniendo la lista de imagenes de  %s" % (pathnameSXW)
        imagesPath = os.path.join(
            os.path.dirname(self.toTempPath(pathnameSXW)),
            "images"
        )
        images=list()
        files= os.listdir(imagesPath)
        for fname in files:
            if fname[:-4]!="_dbk": 
                image = ( 
                    fname,
                    "_dbk%03d_%s"%(self.nextImageCounter(),fname)
                )
                images.append(image)
        return images
    
    def moveImagesOfSXW(self, pathnameSXW, images):   
        """
        Recibe una lista de imagenes del fichero SXW con
        las propuestas para renombrarlas, y las renombra,
        las mueve a directorio data/images del temp.
        """
        #print "Moviendo las imagenes de  %s" % (pathnameSXW)
        sourcePath = os.path.join(
            os.path.dirname(self.toTempPath(pathnameSXW)),
            "images"
        )
        targetPath = self.tempDir.data("images")
        self.makedirs(targetPath)
        for image in images:
            shutil.move( 
                os.path.join(sourcePath,image[0]),
                os.path.join(targetPath,image[1])
            )
        return True
    
    
    def fixImagesInDocBookOfSXW(self, pathnameSXW, images):   
        """
        Actualiza los nombres de las imagenes del fichero
        docbook generado a partir del SXW.
        """
        pathname = removeExtension(self.toTempPath(pathnameSXW))+".docbook"
        f=file(pathname,"r")
        todo=f.readlines()
        f.close()
        f=file(pathname,"w")
        for line in todo:    
          for oldName,newName in images:
              oldStr="<imagedata fileref=\"images/%s\""%oldName
              newStr="<imagedata fileref=\"images/%s\""%newName
              line=line.replace(oldStr,newStr)
          f.write(line)
        f.close()
        
    def fixTagsHeaderFromSXW(self, pathnameSXW):   
        """
        """
        pathname = removeExtension(self.toTempPath(pathnameSXW))+".docbook"
        f=file(pathname,"r")
        todo=f.readlines()
        f.close()

        del todo[0] # Eliminamos <? xml ...
        del todo[0] # Eliminanos <ENTITY ....
        
        if re.match("\s*[<]article[> ]",todo[0] )!= None and re.match("\s*[<]/articleinfo[>]\s*",todo[5] )!= None :
	        #print "Eliminando tag <article> de %s" % (pathname)
	        
	        del todo[0] # Eliminamos <article lang="es-ES">
	        del todo[0] # Eliminamos <?ooogenerator Ope
	        del todo[0] # Eliminamos <?oooversion ?>
	        del todo[0] # Eliminamos <articleinfo>
	        del todo[0] # Eliminamos <authorgroup/>
	        del todo[0] # Eliminamos </articleinfo>        
	        del todo[-1] # Eliminamos </article>
	        
        f=file(pathname,"w")
        for line in todo:    
          f.write(line)
        f.close()

    def SXWsToDocbookInTemp(self):
        """
        Se recorre el directorio datos de los fuentes y convierte
        los ficheros sxw en docbook dejandolos en el directorio datos
        de temp.
        """
        
        for root, dirs, files in os.walk(self.sourceDir.data()):
            #print "root %s\, dirs %s\n, files%s\n"%(root, dirs, files)
            if os.path.split(root)[1] !="CVS":
                self.makedirs( self.toTempPath(root) )
                for fname in files:
                    sys.stdout.write(".")
                    sys.stdout.flush()
                    #print fname
                    extension = getExtension(fname).lower()
                    if extension ==".sxw":
                        pathname = os.path.join(root,fname)
                        self.sxw2docbook(pathname)
                        self.fixTagsHeaderFromSXW(pathname)
                        images = self.getImageListOfSXW(pathname)
                        self.moveImagesOfSXW(pathname, images)
                        self.fixImagesInDocBookOfSXW(pathname, images)
        print
        return True

    def copyDocbookFromSourceToTemp(self):
        """
        Copia los ficheros docbook del directorio source al
        directorio temp, procesandolos a taves del CPP.
        """
        for root, dirs, files in os.walk(self.sourceDir.docbook()):
            if os.path.split(root)[1] !="CVS":
                self.makedirs( self.toTempPath(root) )
                for fname in files:
                    extension = getExtension(fname).lower()
                    if extension ==".docbook":
                        cmd = """cpp -I%s -CC %s""" %(
                            str(self.tempDir),
                            os.path.join(root,fname)
                        )
                        childstdin, childstdout, childstderr = os.popen3(cmd)
                        fout = file(self.toTempPath(root,fname),"w")
                    	while True:
                    		line = childstdout.readline()
                    		if line in (None,""):
                    			break
                    		if re.match("^#\s[0-9][0-9]*\s",line) == None:
                    			fout.write(line)
                    	head = "Error preprocesando el fichero '%s' (%s)" % (fname, cmd)
                    	while True:
                    		line = childstderr.readline()
                    		if line in (None,""):
                    			break
                    		if head != None:
                    			print head
                    			head = None
                    			self.status=1
                    		print "  "+line
        return True

    def copyImagesToDocBookDirInTemp(self):
        """
        Copia las imagenes referenciadas en el los ficheros
        docbook a un directorio "images" junto a cada docbook.
        """
	
        for root, dirs, files in os.walk(self.tempDir.docbook()):
            for fname in files:
                try:
                    #print "Moviendo imagenes de %s al directorio %s " %( fname,root)
                    extension = getExtension(fname).lower()
                    if extension ==".docbook":
                        f=file(os.path.join(root,fname),"r")
                        todo=f.read()
                        f.close()
                        matchs=re.finditer('[<]imagedata fileref="([^"]*)"',todo)
                        for match in matchs:
                            imageName=match.group(1)
                            sourceImage = self.tempDir.data(
                                "images",
                                os.path.basename(imageName)
                            )
                            targetImage = os.path.join( root, imageName)
                            self.makedirs(os.path.dirname(targetImage))
                            #shutil.copyfile(sourceImage, targetImage)
                            #shutil.copy(sourceImage,self.distDir("images"))
                            self.copyFromTo(sourceImage, targetImage)
                            self.copyFromTo(sourceImage,self.distDir("images"))
                except Exception,e:
                    print "Error: copyImagesToDocBookDirInTemp(procesisng file %s/%s imageName=%s), error %s, %s" % (
                        root,
			fname,
                        imageName,
                        str(e.__class__),
                        str(e)
                   )
                       
        return True
        
    def buildHtmlFromDocbookOfTemp(self,mainfname):
        xsltFile="/usr/share/xml/docbook/stylesheet/nwalsh/xhtml/chunk.xsl"

        if not os.path.isabs(mainfname):
            mainfname = self.tempDir.docbook(mainfname)
                
        os.chdir( self.tempDir.docbook() )
        cmd = "xsltproc -o %s/ %s %s 2>&1"% (
            self.distDir(),
            xsltFile,
            mainfname
        )
        print os.path.abspath(os.curdir)
        print cmd
        shell(cmd)
        
    def process(self,mainfname="principal.docbook"):
        self.makedirs(self.distDir("images"))
        print "Procesando ficheros SXW"
        self.SXWsToDocbookInTemp()
        
        print "Preprocesando ficheros docbook"
        self.copyDocbookFromSourceToTemp()
        
        print "Moviendo las imagenes"
        self.copyImagesToDocBookDirInTemp()
        
        print "Construyendo el html"
        self.buildHtmlFromDocbookOfTemp(mainfname)    
        
        return True

    def __call__(self,mainfname="principal.docbook"):
        return self.process(mainfname)
        
    
def main():
    process = ProcessDocBook(
        sourceDir=sys.argv[1],
        tempDir=sys.argv[2],
        distDir=sys.argv[3]
    )                
    process()    
    if process.status !=0:
        sys.exit(process.status)

if __name__ == "__main__":
	main()
