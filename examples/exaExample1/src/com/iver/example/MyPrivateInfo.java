/*
 * Created on 25-may-2005
 *
 * gvSIG. Sistema de Información Geográfica de la Generalitat Valenciana
 * 
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 * 
 */
package com.iver.example;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.tools.Behavior.PointBehavior;
import com.iver.cit.gvsig.project.documents.view.IProjectView;
import com.iver.cit.gvsig.project.documents.view.gui.View;
import com.iver.example.tools.MyInfoListener;


/**
 * @author Fjp
 *
 * Main class of this extension example. 
 * The first time the method "execute" is called, we check if
 * we have already created MyInfoListener, and if not, we 
 * create it and add to the mapControl. Then, if we receive the 
 * acction command "MYINFO", we set this tool as active. 
 */
public class MyPrivateInfo extends Extension {
    
    MyInfoListener il = null;
    public void initialize() {


    }

    public void execute(String actionCommand) {
        View vista = (View) PluginServices.getMDIManager().getActiveWindow();
        MapControl mapCtrl = vista.getMapControl();
        if (il == null) // We create it for the first time.
        {
            il = new MyInfoListener(mapCtrl);
            mapCtrl.addMapTool("myInfo", new PointBehavior(il));
        }
        
        if (actionCommand.compareTo("MYINFO") == 0)
        {
            mapCtrl.setTool("myInfo");
        }
    }

    public boolean isEnabled() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#isVisible()
     * 
     * This tool will be visible only if the View class is the active view.
     */
    public boolean isVisible() {
        IWindow f = PluginServices.getMDIManager()
         .getActiveWindow();

            if (f == null) {
                return false;
            }
            
            if (f.getClass() == View.class) {
                View vista = (View) f;
                IProjectView model = vista.getModel();
                MapContext mapa = model.getMapContext();
                
                return mapa.getLayers().getLayersCount() > 0;
            } else {
                return false;
            }
    }

}
