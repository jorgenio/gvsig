/*
 * Created on 13-jun-2005
 *
 * gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 * 
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 * 
 *    or
 * 
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 * 
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.example;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.iver.andami.plugins.Extension;
import com.iver.example.socket.InputThread;

public class MySocket extends Extension {

    public void initialize() {
        // Here we start the socket. This method is always
        // executed at the beginning.
        // TODO: Change method to Initialize in Extension interface.
        try {
            ServerSocket serverSocket = new ServerSocket(5757);
            Socket clientSocket = null;
                // accept waits till a client socket connects
                // (only one client in this example)
                System.out.println("Listening in port 5757");
                Thread input = new InputThread(serverSocket);
                input.start(  );
                
        } catch (IOException e) {
            System.out.println("Could not listen on port: 5757");
            System.exit(-1);
        } 
              
    }

    public void execute(String actionCommand) {
        // TODO Auto-generated method stub

    }

    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isVisible() {
        // TODO Auto-generated method stub
        return false;
    }

}
