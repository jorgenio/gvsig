/*
 * Created on 26-may-2005
 *
 * gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 * 
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 * 
 *    or
 * 
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 * 
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.example.dialogs;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.SingletonWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;

public class MyInfoDlg extends JPanel implements IWindow, SingletonWindow  {

    private JLabel jLabel = null;
    private JTextField jTxtProvin = null;
    
    private WindowInfo mViewInfo = null;

    /**
     * This is the default constructor
     */
    public MyInfoDlg() {
        super();
        initialize();
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        jLabel = new JLabel();
        jLabel.setText("Provincia:");
        jLabel.setBounds(32, 29, 118, 32);
        jLabel.setFont(new java.awt.Font("Arial", java.awt.Font.PLAIN, 14));
        this.setLayout(null);
        this.setSize(300, 200);
        this.add(getJTxtProvin(), null);
        this.add(jLabel, null);
    }

    /**
     * This method initializes jTextField	
     * 	
     * @return javax.swing.JTextField	
     */    
    private JTextField getJTxtProvin() {
    	if (jTxtProvin == null) {
    		jTxtProvin = new JTextField();
    		jTxtProvin.setBounds(33, 78, 233, 43);
    		jTxtProvin.setFont(new java.awt.Font("Arial", java.awt.Font.BOLD, 14));
    	}
    	return jTxtProvin;
    }
    
    public void setProvince(String strProvin)
    {
        jTxtProvin.setText(strProvin);
    }

    /* (non-Javadoc)
     * @see com.iver.andami.ui.mdiManager.View#getViewInfo()
     * Windows in Andami must implement this method. The ViewInfo
     * class states how a window will be shown.
     */
    public WindowInfo getWindowInfo() {
        if (mViewInfo == null)
        {
            mViewInfo=new WindowInfo(WindowInfo.MODELESSDIALOG);
            mViewInfo.setTitle("INFO PROVIN");
        }
        return mViewInfo;
    }

    public Object getWindowModel() {
        return "ID_MYVIEWINFO";
    }

}
