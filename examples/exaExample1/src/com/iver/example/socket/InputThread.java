/*
 * Created on 13-jun-2005
 *
 */
package com.iver.example.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class InputThread extends Thread {
    ServerSocket serverSocket;
    BufferedReader in;

    public InputThread(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public void run() {

        try {
            Socket clientSocket = serverSocket.accept();
            System.out.println("Connection established with " 
                    + clientSocket);
            
            this.in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));      
            
            String inputLine, outputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println("Received:" + inputLine);
                if (inputLine.equals("option1"))
                {
                    // do something for option 1
                }
                if (inputLine.equals("option2"))
                {
                    // do something for option 2
                }
                if (inputLine.equals("Bye."))
                    break;               
            } // while
            serverSocket.close();

        } catch (SocketException e) {
            // output thread closed the socket
        } catch (IOException e) {
            System.out.println("Accept failed: 5757");
            System.exit(-1);
        }
        try {
            in.close();
        } catch (IOException e) {
        }
    }
}
