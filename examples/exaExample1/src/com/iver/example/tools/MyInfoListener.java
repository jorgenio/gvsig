/*
 * gvSIG. Sistema de Información Geográfica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 */
package com.iver.example.tools;

import java.awt.Cursor;
import java.awt.geom.Point2D;

import com.hardcode.gdbms.driver.exceptions.ReadDriverException;
import com.hardcode.gdbms.engine.data.DataSource;
import com.hardcode.gdbms.engine.values.Value;
import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.exceptions.visitors.VisitorException;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.layers.FBitSet;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLayers;
import com.iver.cit.gvsig.fmap.layers.layerOperations.AlphanumericData;
import com.iver.cit.gvsig.fmap.layers.layerOperations.VectorialData;
import com.iver.cit.gvsig.fmap.operations.strategies.QueryByPointVisitor;
import com.iver.cit.gvsig.fmap.tools.BehaviorException;
import com.iver.cit.gvsig.fmap.tools.Events.PointEvent;
import com.iver.cit.gvsig.fmap.tools.Listeners.PointListener;
import com.iver.example.dialogs.MyInfoDlg;

public class MyInfoListener implements PointListener {

    private MapControl mapCtrl;
    private Cursor cur = java.awt.Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);


    public MyInfoListener(MapControl mc) {
        this.mapCtrl = mc;
    }


    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.tools.Listeners.PointListener#point(com.iver.cit.gvsig.fmap.tools.Events.PointEvent)
     * The PointEvent method bring you a point in pixel coordinates. You
     * need to transform it to world coordinates. The class to do conversions
     * is ViewPort, obtained thru the MapContext of mapCtrl.
     */
    public void point(PointEvent event) throws BehaviorException {

        Point2D pReal = mapCtrl.getMapContext().getViewPort().toMapPoint(event.getPoint());

        MyInfoDlg dlg = new MyInfoDlg();
        dlg = (MyInfoDlg) PluginServices.getMDIManager().addWindow(dlg);


        /* We will use a Visitor pattern to query the layer and
         * to obtain a FBitSet (the selected items). Then, we iterate thru
         * selection and extract the needed information from the dataSource.
         */
        QueryByPointVisitor visitor = new QueryByPointVisitor();
        FLayers lyrs = mapCtrl.getMapContext().getLayers();
        FLayer lyrProvin = lyrs.getLayer("Provin.shp");
        visitor.setLayer(lyrProvin);
        visitor.setQueriedPoint(pReal);
        visitor.setTolerance(5.0);
        try {
            ((VectorialData) lyrProvin).process(visitor);
            FBitSet selection = visitor.getBitSet();

            DataSource ds = ((AlphanumericData) lyrProvin).getRecordset();
            ds.start();
            int idField = ds.getFieldIndexByName("NOM_PROVIN");
            int numReg = 0;
            Value strNomProvin = null;

            while ((numReg = selection.nextSetBit(numReg)) > 0)
            {
                strNomProvin = ds.getFieldValue(numReg, idField);
                numReg++;
            }
            ds.stop();
            dlg.setProvince(strNomProvin.toString());
            // dlg.setProvince("En un lugar de la Mancha");
        } catch (ReadDriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (VisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }

    public Cursor getCursor() {
        return cur;
    }

    public boolean cancelDrawing() {
        return false;
    }


	public void pointDoubleClick(PointEvent event) throws BehaviorException {
		// TODO Auto-generated method stub

	}

}
