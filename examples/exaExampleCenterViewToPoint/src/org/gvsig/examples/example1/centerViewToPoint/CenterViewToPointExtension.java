/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

package org.gvsig.examples.example1.centerViewToPoint;

import java.awt.Color;
import java.util.prefs.Preferences;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.layers.FLayers;
import com.iver.cit.gvsig.project.documents.view.IProjectView;
import com.iver.cit.gvsig.project.documents.view.gui.View;
import com.iver.utiles.StringUtilities;

/**
 * The CenterViewToPointExtension class allows to center the View over a
 * concrete point given by its coordinates.
 *
 * @author Vicente Caballero Navarro
 */
public class CenterViewToPointExtension extends Extension{

	private View vista;
	public static Color COLOR=Color.red;
	private static Preferences prefs = Preferences.userRoot().node( "centerviewtopoint" );

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#inicializar()
     */
    public void initialize() {
    	String colorString=prefs.get("colorcenterviewtopoint",StringUtilities.color2String(CenterViewToPointExtension.COLOR));
		Color color=StringUtilities.string2Color(colorString);
		CenterViewToPointExtension.COLOR=color;

    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#execute(java.lang.String)
     */
    public void execute(String actionCommand) {
		if ("CENTERVIEWTOPOINT".equals(actionCommand)){
			vista = (View)PluginServices.getMDIManager().getActiveWindow();
			InputCoordinatesPanel dataSelectionPanel = new InputCoordinatesPanel(vista);
			PluginServices.getMDIManager().addWindow(dataSelectionPanel);
		}
    }

    public View getView(){
    	return vista;
    }
    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#isEnabled()
     */
    public boolean isEnabled() {
		com.iver.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
		 .getActiveWindow();
		if (f == null) {
		    return false;
		}
		if (f.getClass() == View.class) {
		    View vista = (View) f;
		    IProjectView model = vista.getModel();
		    MapContext mapa = model.getMapContext();
		    FLayers layers = mapa.getLayers();
		    for (int i=0;i < layers.getLayersCount();i++) {
               if (layers.getLayer(i).isAvailable()) return true;
		    }
		}
		return false;

    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#isVisible()
     */
    public boolean isVisible() {
		com.iver.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
		 .getActiveWindow();
		if (f == null) {
		    return false;
		}
		if (f.getClass() == View.class) {
		    View vista = (View) f;
		    IProjectView model = vista.getModel();
		    MapContext mapa = model.getMapContext();
            if (mapa.getLayers().getLayersCount() > 0) {
                return true;
            }
            return false;
        }
		return false;
	}

}
