/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

package org.gvsig.examples.example1.centerViewToPoint;

import java.awt.Color;
import java.util.prefs.Preferences;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import com.iver.andami.PluginServices;
import com.iver.andami.preferences.AbstractPreferencePage;
import com.iver.andami.preferences.StoreException;
import com.iver.cit.gvsig.gui.panels.ColorChooserPanel;
import com.iver.utiles.StringUtilities;


/**
 * References page of center view to point options.
 *
 * @author Vicente Caballero Navarro
 */
public class CenterViewToPointPage extends AbstractPreferencePage {
    private static Preferences prefs = Preferences.userRoot().node("centerviewtopoint");
    private ColorChooserPanel colorPoint;
    private ImageIcon icon;

    public CenterViewToPointPage() {
        super();
        icon = new ImageIcon(this.getClass().getClassLoader().getResource("images/CenterView.png"));
        addComponent(PluginServices.getText(this, "color_point") + ":",
            colorPoint = new ColorChooserPanel());
    }

    /* (non-Javadoc)
     * @see com.iver.andami.preferences.IPreference#initializeValues()
     */
    public void initializeValues() {
        String colorString = prefs.get("colorcenterviewtopoint",
                StringUtilities.color2String(CenterViewToPointExtension.COLOR));
        Color color = StringUtilities.string2Color(colorString);
        CenterViewToPointExtension.COLOR = color;
        colorPoint.setColor(color);
        colorPoint.setAlpha(255);
    }

    /* (non-Javadoc)
     * @see com.iver.andami.preferences.IPreference#getID()
     */
    public String getID() {
        return this.getClass().getName();
    }

    /* (non-Javadoc)
     * @see com.iver.andami.preferences.IPreference#getTitle()
     */
    public String getTitle() {
        return PluginServices.getText(this, "Centrar_la_Vista_sobre_un_punto");
    }

    /* (non-Javadoc)
     * @see com.iver.andami.preferences.IPreference#getPanel()
     */
    public JPanel getPanel() {
        return this;
    }

    /* (non-Javadoc)
     * @see com.iver.andami.preferences.AbstractPreferencePage#storeValues()
     */
    public void storeValues() throws StoreException {
        String colorString;
        colorString = StringUtilities.color2String(colorPoint.getColor());
        prefs.put("colorcenterviewtopoint", colorString);
        CenterViewToPointExtension.COLOR = colorPoint.getColor();
    }

    /* (non-Javadoc)
     * @see com.iver.andami.preferences.IPreference#initializeDefaults()
     */
    public void initializeDefaults() {
        colorPoint.setColor(CenterViewToPointExtension.COLOR);
    }

    /* (non-Javadoc)
     * @see com.iver.andami.preferences.IPreference#getIcon()
     */
    public ImageIcon getIcon() {
        return icon;
    }

    /* (non-Javadoc)
     * @see com.iver.andami.preferences.IPreference#isValueChanged()
     */
    public boolean isValueChanged() {
        return super.hasChanged();
    }

    /* (non-Javadoc)
     * @see com.iver.andami.preferences.AbstractPreferencePage#setChangesApplied()
     */
    public void setChangesApplied() {
        setChanged(false);
    }
}
