/*
 * Created on 08-nov-2005
 *
 * gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.examples.example1.centerViewToPoint;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cresques.cts.IProjection;
import org.gvsig.gui.beans.AcceptCancelPanel;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.core.ShapeFactory;
import com.iver.cit.gvsig.fmap.core.v02.FConstant;
import com.iver.cit.gvsig.fmap.core.v02.FSymbol;
import com.iver.cit.gvsig.fmap.layers.GraphicLayer;
import com.iver.cit.gvsig.fmap.rendering.FGraphic;
import com.iver.cit.gvsig.fmap.tools.BehaviorException;
import com.iver.cit.gvsig.fmap.tools.Events.PointEvent;
import com.iver.cit.gvsig.gui.panels.ColorChooserPanel;
import com.iver.cit.gvsig.project.documents.view.gui.View;
import com.iver.cit.gvsig.project.documents.view.toolListeners.InfoListener;


/**
 * The InputCoordinatesPanel class creates a JPanel where the
 * user can input the coordinates of the point of reference
 * for center the View.
 *
 * @author jmorell
 */
public class InputCoordinatesPanel extends JPanel implements IWindow {
    private static final long serialVersionUID = 1L;
    private JLabel labelX = null;
    private JTextField textX = null;
    private JLabel labelY = null;
    private JTextField textY = null;
    private MapControl mapControl;
    private WindowInfo viewInfo = null;
    private String firstCoordinate;
    private String secondCoordinate;
    private Point2D center;
    private GraphicLayer lyr;
    private ColorChooserPanel colorPanel;
	private AcceptCancelPanel okCancelPanel = null;
	private JButton bRemovePoint = null;
	private View view=null;
	/**
     * This is the default constructor
     */
    public InputCoordinatesPanel(View view) {
        super();
        this.view=view;
        this.mapControl = view.getMapControl();
        lyr=mapControl.getMapContext().getGraphicsLayer();
        initializeCoordinates();
        initialize();
    }

    private void initializeCoordinates() {
        IProjection proj = mapControl.getProjection();
        String projName = proj.getAbrev();
        if (projName.equals("EPSG:23030") || projName.equals("EPSG:27492") || projName.equals("EPSG:42101") || projName.equals("EPSG:42304")) {
            firstCoordinate = "X";
            secondCoordinate = "Y";
        } else if (projName.equals("EPSG:4230") || projName.equals("EPSG:4326")) {
            firstCoordinate = "Lon";
            secondCoordinate = "Lat";
        } else {
            System.out.println("Proyecci�n: " + projName);
            System.out.println("Proyecci�n no soportada.");
        }
    }

    private void zoomToCoordinates() throws Exception {
       try{
    	Rectangle2D oldExtent = mapControl.getViewPort().getAdjustedExtent();
        double oldCenterX = oldExtent.getCenterX();
        double oldCenterY = oldExtent.getCenterY();
        double centerX = (new Double((String)textX.getText())).doubleValue();
        double centerY = (new Double((String)textY.getText())).doubleValue();
        center=new Point2D.Double(centerX,centerY);
        double movX = centerX-oldCenterX;
        double movY = centerY-oldCenterY;
        double upperLeftCornerX = oldExtent.getMinX()+movX;
        double upperLeftCornerY = oldExtent.getMinY()+movY;
        double width = oldExtent.getWidth();
        double height = oldExtent.getHeight();
        Rectangle2D extent = new Rectangle2D.Double(upperLeftCornerX, upperLeftCornerY, width, height);
        mapControl.getViewPort().setExtent(extent);
       }catch (NumberFormatException e) {
    	   throw new Exception();
       }

    }

    /**
     * This method initializes this
     *
     * @return void
     */
    private void initialize() {
        labelY = new JLabel();
        labelY.setBounds(10, 35, 28, 20);
        labelY.setText(secondCoordinate + ":");
        labelX = new JLabel();
        labelX.setBounds(10, 10, 28, 20);
        labelX.setText(firstCoordinate + ":");
        this.setLayout(null);
        this.setSize(307, 111);
        this.add(labelX, null);
        this.add(getTextX(), null);
        this.add(labelY, null);
        this.add(getTextY(), null);
        this.add(getColorPanel());
        this.add(getOkCancelPanel(), null);
        this.add(getBRemovePoint(), null);
    }

    /**
     * This method initializes textX
     *
     * @return javax.swing.JTextField
     */
    private JTextField getTextX() {
    	if (textX == null) {
    		textX = new JTextField();
    		textX.setBounds(40, 10, 260, 20);
    		textX.addActionListener(new java.awt.event.ActionListener() {
    			public void actionPerformed(java.awt.event.ActionEvent e) {
    				textX.transferFocus();
    			}
    		});
    	}
    	return textX;
    }

    /**
     * This method initializes textY
     *
     * @return javax.swing.JTextField
     */
    private JTextField getTextY() {
    	if (textY == null) {
    		textY = new JTextField();
    		textY.setBounds(40, 35, 260, 20);
    		textY.addActionListener(new java.awt.event.ActionListener() {
    			public void actionPerformed(java.awt.event.ActionEvent e) {
    				textY.transferFocus();
    			}
    		});
    	}
    	return textY;
    }

    /* (non-Javadoc)
     * @see com.iver.andami.ui.mdiManager.View#getViewInfo()
     */
    public WindowInfo getWindowInfo() {
        if (viewInfo == null) {
            viewInfo=new WindowInfo(WindowInfo.MODALDIALOG);
            viewInfo.setTitle(PluginServices.getText(this,"Centrar_la_Vista_sobre_un_punto"));
            viewInfo.setWidth(this.getWidth()+8);
            viewInfo.setHeight(this.getHeight());
        }
        return viewInfo;
    }
    private void openInfo(){
    	InfoListener infoListener=new InfoListener(mapControl);
    	MouseEvent e=new MouseEvent((Component)view,MouseEvent.BUTTON1,MouseEvent.ACTION_EVENT_MASK,MouseEvent.MOUSE_CLICKED,500,400,1,true);
    	Point2D centerPixels=mapControl.getViewPort().fromMapPoint(center.getX(),center.getY());
    	PointEvent pe=new PointEvent(centerPixels,e);
    	try {
			infoListener.point(pe);
		} catch (BehaviorException e1) {
			e1.printStackTrace();
		}
		if (mapControl.getMapContext().getLayers().getActives().length==0){
			JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),PluginServices.getText(this,"no_hay_ninguna_capa_seleccionada")+" \n"+
					PluginServices.getText(this,"debe_seleccionar_las_capas_de_las_que_quiera_obtener_informacion"));
		}
    }
    private void drawPoint(Color color){
    	CenterViewToPointExtension.COLOR=color;
    	lyr.clearAllGraphics();
    	FSymbol theSymbol = new FSymbol(FConstant.SYMBOL_TYPE_POINT,color);
        int idSymbol = lyr.addSymbol(theSymbol);
        IGeometry geom = ShapeFactory.createPoint2D(center.getX(),center.getY());
        FGraphic theGraphic = new FGraphic(geom, idSymbol);
        lyr.addGraphic(theGraphic);
        mapControl.drawGraphics();
	    mapControl.drawMap(false);

    }


	/**
	 * This method initializes jPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getColorPanel() {
		if (colorPanel==null){
		 	colorPanel=new ColorChooserPanel();
		 	colorPanel.setAlpha(250);
		 	colorPanel.setColor(CenterViewToPointExtension.COLOR);
		 	colorPanel.setBounds(new java.awt.Rectangle(40,59,123,24));
		}
		 	return colorPanel;
	}

	/**
	 * This method initializes okCancelPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private AcceptCancelPanel getOkCancelPanel() {
		if (okCancelPanel == null) {
			ActionListener okAction, cancelAction;
			okAction = new java.awt.event.ActionListener() {
    			public void actionPerformed(java.awt.event.ActionEvent e) {
    				try{
    				zoomToCoordinates();
    				}catch (Exception e1) {
    					JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),PluginServices.getText(this,"formato_de_numero_incorrecto"));
    					return;
    				}
    				// y sale.
                    if (PluginServices.getMainFrame() == null)
                        ((JDialog) (getParent().getParent().getParent().getParent())).dispose();
                    else
                        PluginServices.getMDIManager().closeWindow(InputCoordinatesPanel.this);

                    openInfo();
                    drawPoint(((ColorChooserPanel)getColorPanel()).getColor());
    			}
    		};
    		cancelAction = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					closeThis();
				}
    		};
			okCancelPanel = new AcceptCancelPanel(okAction, cancelAction);
			okCancelPanel.setBounds(new java.awt.Rectangle(40, 88, 260, 30));
		}
		return okCancelPanel;
	}

	private void closeThis() {
		PluginServices.getMDIManager().closeWindow(this);

	}

	/**
	 * This method initializes bRemovePoint
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getBRemovePoint() {
		if (bRemovePoint == null) {
			bRemovePoint = new JButton();
			bRemovePoint.setBounds(new java.awt.Rectangle(181,59,101,25));
			bRemovePoint.setText(PluginServices.getText(this,"remove"));
			bRemovePoint.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					lyr.clearAllGraphics();
					mapControl.drawMap(false);
				}
			});
		}
		return bRemovePoint;
	}


}  //  @jve:decl-index=0:visual-constraint="103,18"
