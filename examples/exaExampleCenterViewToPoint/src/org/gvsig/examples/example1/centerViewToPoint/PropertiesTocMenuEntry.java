
/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

package org.gvsig.examples.example1.centerViewToPoint;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.project.documents.view.gui.View;
import com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction;
import com.iver.cit.gvsig.project.documents.view.toc.ITocItem;



/**
 * Properties toc menu entry center view to point.
 *
 * @author Vicente Caballero Navarro
 */
public class PropertiesTocMenuEntry extends AbstractTocContextMenuAction {
    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.project.documents.IContextMenuAction#getGroup()
     */
    public String getGroup() {
        return "examplegroup";
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.project.documents.IContextMenuAction#getGroupOrder()
     */
    public int getGroupOrder() {
        return 1;
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.project.documents.IContextMenuAction#getOrder()
     */
    public int getOrder() {
        return 1;
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.project.documents.IContextMenuAction#getText()
     */
    public String getText() {
        return PluginServices.getText(this, "Centrar_la_Vista_sobre_un_punto");
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction#isEnabled(com.iver.cit.gvsig.project.documents.view.toc.ITocItem, com.iver.cit.gvsig.fmap.layers.FLayer[])
     */
    public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
        return true;
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction#isVisible(com.iver.cit.gvsig.project.documents.view.toc.ITocItem, com.iver.cit.gvsig.fmap.layers.FLayer[])
     */
    public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
        return (isTocItemBranch(item));
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction#execute(com.iver.cit.gvsig.project.documents.view.toc.ITocItem, com.iver.cit.gvsig.fmap.layers.FLayer[])
     */
    public void execute(ITocItem item, FLayer[] selectedItems) {
        View vista = (View) PluginServices.getMDIManager().getActiveWindow();
        InputCoordinatesPanel dataSelectionPanel = new InputCoordinatesPanel(vista);
        PluginServices.getMDIManager().addWindow(dataSelectionPanel);
    }
}
