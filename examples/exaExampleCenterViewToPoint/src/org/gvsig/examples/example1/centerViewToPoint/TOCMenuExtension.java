package org.gvsig.examples.example1.centerViewToPoint;


import com.iver.andami.plugins.Extension;
import com.iver.utiles.extensionPoints.ExtensionPoints;
import com.iver.utiles.extensionPoints.ExtensionPointsSingleton;

/**
 * This extensions shows how to add a context menu to the TOC (table of
 * contents) of the view).
 * 
 * @author cesar
 *
 */
public class TOCMenuExtension extends Extension {

	public void execute(String actionCommand) {
		
	}

	public void initialize() {
        ExtensionPoints extensionPoints = ExtensionPointsSingleton.getInstance();
        extensionPoints.add("View_TocActions", "CenterViewToPoint",
            new PropertiesTocMenuEntry());
        // after this, a new entry appears in the context menu
        // When we click on this entry, PropertiesTocMenuEntry.execute() is called.
	}

	public boolean isEnabled() {
		// the context menu will be visible regardless the extension status
		return false;
	}

	public boolean isVisible() {
		// the context menu will be visible regardless the extension status
		return false;
	}

}
