/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2007 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

package org.gvsig.examples.example1.exclusiveUI;

import java.util.HashSet;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;

/**
 * This extension shows how to use an ExclusiveUIExtension, an extension which
 * takes over the control of the GUI and decides which extensions will be
 * visible (or hidden).
 * 
 * @author Cesar Martinez Izquierdo <cesar.martinez@iver.es>
 * @author Vicente Caballero Navarro
 *
 */
public class ExclusiveUIExtension extends Extension {
	private HashSet goodExtensions = new HashSet();
	
	public void initialize() {

		goodExtensions.add("com.iver.core.Consola");
		goodExtensions.add("com.iver.core.StatusBar");
		goodExtensions.add("com.iver.core.PreferencesExtension");
		goodExtensions.add("com.iver.cit.gvsig.ViewControls");
		goodExtensions.add("com.iver.cit.gvsig.AddLayer");
		goodExtensions.add("com.iver.cit.gvsig.ProjectExtension");
		goodExtensions.add("com.iver.cit.gvsig.About");
		goodExtensions.add("com.iver.cit.gvsig.LayoutControls");
		goodExtensions.add("com.iver.cit.gvsig.ViewPropertiesExtension");
		goodExtensions.add("com.iver.cit.gvsig.ZoomPrev");
		goodExtensions.add("com.iver.cit.gvsig.ViewProjectWindow");
		goodExtensions.add("com.iver.cit.gvsig.Print");
		goodExtensions.add("com.iver.cit.gvsig.PrintProperties");
		goodExtensions.add("com.iver.cit.gvsig.LayoutEditableControls");
		goodExtensions.add("com.iver.cit.gvsig.LayoutUndoExtension");
		goodExtensions.add("com.iver.cit.gvsig.LayoutRedoExtension");
		goodExtensions.add("com.iver.cit.gvsig.LayoutInsertToolsExtension");
		goodExtensions.add("com.iver.cit.gvsig.LayoutGraphicControls");
		goodExtensions.add("com.iver.cit.gvsig.LayoutEditVertexExtension");
		goodExtensions.add("com.iver.cit.gvsig.ViewSelectionControls");
		goodExtensions.add("org.gvsig.examples.example1.extension.CenterViewToPointExtension");
		goodExtensions.add("org.gvsig.examples.example1.extension.InfoToolExampleExtension");
		goodExtensions.add("org.gvsig.examples.example1.extension.OpenCSVToPointsExtension");
		goodExtensions.add("org.gvsig.examples.example1.PreferencePageExtension");
		goodExtensions.add("org.gvsig.examples.example1.AboutExtension");
		goodExtensions.add("org.gvsig.examples.example1.PreferencePageExtension");
		goodExtensions.add("org.gvsig.examples.example1.TOCMenuExtension");

		PluginServices.setExclusiveUIExtension(this);
	}
	
	public boolean isVisible(Extension ext) {
		// if the extension is in our good list, ask the extension for visibility
		if (goodExtensions.contains(ext.getClass().getName())) {
			return ext.isVisible();	
		}
		return false;
	}


	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isVisible() {
		// TODO Auto-generated method stub
		return false;
	}

	public void execute(String actionCommand) {
		// TODO Auto-generated method stub
		
	}

}
