
/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

package org.gvsig.examples.example1.infotool.tool;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

import org.gvsig.examples.example1.centerViewToPoint.CenterViewToPointExtension;
import org.gvsig.examples.example1.infotool.tool.gui.InfoWindowExample;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.core.ShapeFactory;
import com.iver.cit.gvsig.fmap.core.v02.FConstant;
import com.iver.cit.gvsig.fmap.core.v02.FSymbol;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLayerGenericVectorial;
import com.iver.cit.gvsig.fmap.layers.GraphicLayer;
import com.iver.cit.gvsig.fmap.rendering.FGraphic;
import com.iver.cit.gvsig.fmap.tools.BehaviorException;
import com.iver.cit.gvsig.fmap.tools.Events.PointEvent;
import com.iver.cit.gvsig.fmap.tools.Listeners.PointListener;
import com.iver.cit.gvsig.project.documents.view.toolListeners.InfoListener;


/**
 * Implementation of the interface PointListener as tool
 * of information and center the extent to that point.
 *
 * @author Vicente Caballero Navarro
 */
public class InfoToolListenerImpl implements PointListener {
    private final Image icentertopoint = new ImageIcon(MapControl.class.getResource(
                "images/InfoCursor.gif")).getImage();
    private Cursor cur = Toolkit.getDefaultToolkit().createCustomCursor(icentertopoint,
            new Point(16, 16), "");
    private MapControl mapControl;
    private InfoListener infoListener;
    /**
     * Create a new InfoToolListenerImpl.
     *
     * @param mapControl MapControl.
     */
    public InfoToolListenerImpl(MapControl mapControl) {
        this.mapControl = mapControl;
        infoListener=new InfoListener(mapControl);
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.tools.Listeners.PointListener#point(com.iver.cit.gvsig.fmap.tools.Events.PointEvent)
     */
    public void point(PointEvent event) {
    	 FLayer[] layers=mapControl.getMapContext().getLayers().getActives();
         if (layers[0] instanceof FLayerGenericVectorial){
     		if (layers[0].getProperty("CapaDePruebaCSV")!=null){
     			Point2D centerPoint = mapControl.getViewPort().toMapPoint(event.getPoint());
     	        zoomToCoordinates(centerPoint);
     	        drawPoint(centerPoint);
     	        InfoWindowExample iwe=new InfoWindowExample();
     	        PluginServices.getMDIManager().addWindow(iwe);
     		}
     	 } else {
             try {
				infoListener.point(event);
			} catch (BehaviorException e) {
				e.printStackTrace();
			}
         }

    }

    private void drawPoint(Point2D center) {
        GraphicLayer lyr = mapControl.getMapContext().getGraphicsLayer();
        lyr.clearAllGraphics();

        FSymbol theSymbol = new FSymbol(FConstant.SYMBOL_TYPE_POINT,
                CenterViewToPointExtension.COLOR);
        int idSymbol = lyr.addSymbol(theSymbol);
        IGeometry geom = ShapeFactory.createPoint2D(center.getX(), center.getY());
        FGraphic theGraphic = new FGraphic(geom, idSymbol);
        lyr.addGraphic(theGraphic);
        mapControl.drawGraphics();
        mapControl.drawMap(false);
    }

    private void zoomToCoordinates(Point2D center) {
        Rectangle2D oldExtent = mapControl.getViewPort().getAdjustedExtent();
        double oldCenterX = oldExtent.getCenterX();
        double oldCenterY = oldExtent.getCenterY();
        double movX = center.getX() - oldCenterX;
        double movY = center.getY() - oldCenterY;
        double upperLeftCornerX = oldExtent.getMinX() + movX;
        double upperLeftCornerY = oldExtent.getMinY() + movY;
        double width = oldExtent.getWidth();
        double height = oldExtent.getHeight();
        Rectangle2D extent = new Rectangle2D.Double(upperLeftCornerX,
                upperLeftCornerY, width, height);
        mapControl.getViewPort().setExtent(extent);
        mapControl.getMapContext().clearAllCachingImageDrawnLayers();
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#getCursor()
     */
    public Cursor getCursor() {
        return cur;
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.tools.Listeners.ToolListener#cancelDrawing()
     */
    public boolean cancelDrawing() {
        return true;
    }

    /* (non-Javadoc)
     * @see com.iver.cit.gvsig.fmap.tools.Listeners.PointListener#pointDoubleClick(com.iver.cit.gvsig.fmap.tools.Events.PointEvent)
     */
    public void pointDoubleClick(PointEvent event) {
    }
}
