package org.gvsig.examples.example1.infotool.tool.gui;

import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.gvsig.gui.beans.AcceptCancelPanel;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;

public class InfoWindowExample extends JPanel implements IWindow{

	private JLabel jLabel = null;
	private AcceptCancelPanel accept;

	/**
	 * This method initializes
	 *
	 */
	public InfoWindowExample() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 */
	private void initialize() {
        jLabel = new JLabel();
        jLabel.setText(PluginServices.getText(this,"this_is_a_window_example"));
        this.setSize(new java.awt.Dimension(380,78));

        this.add(jLabel, null);
        this.add(getAcceptCancelPanel(), null);
	}

	public WindowInfo getWindowInfo() {
		WindowInfo wi=new WindowInfo(WindowInfo.MODALDIALOG);
		return wi;
	}
	private AcceptCancelPanel getAcceptCancelPanel() {
		if (accept == null) {
			ActionListener okAction, cancelAction;
			okAction = new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					PluginServices.getMDIManager().closeWindow(
							InfoWindowExample.this);
				}
			};
			cancelAction = new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					PluginServices.getMDIManager().closeWindow(
							InfoWindowExample.this);
				}
			};
			accept = new AcceptCancelPanel(okAction, cancelAction);
			accept.setPreferredSize(new java.awt.Dimension(300, 300));
			// accept.setBounds(new java.awt.Rectangle(243,387,160,28));
			accept.setEnabled(true);
			accept.setVisible(true);
		}
		return accept;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
