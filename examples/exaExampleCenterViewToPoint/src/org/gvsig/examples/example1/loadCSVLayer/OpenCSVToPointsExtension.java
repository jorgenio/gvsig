package org.gvsig.examples.example1.loadCSVLayer;

import java.net.URL;

import com.hardcode.driverManager.DriverLoadException;
import com.hardcode.gdbms.engine.data.DataSource;
import com.hardcode.gdbms.engine.data.DataSourceFactory;
import com.hardcode.gdbms.engine.data.NoSuchTableException;
import com.hardcode.gdbms.engine.data.driver.DriverException;
import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.cit.gvsig.ProjectExtension;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.layers.FLayerGenericVectorial;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.project.Project;
import com.iver.cit.gvsig.project.ProjectFactory;
import com.iver.cit.gvsig.project.documents.view.ProjectView;
import com.iver.cit.gvsig.project.documents.view.gui.View;
import com.iver.gvsig.addeventtheme.AddEventThemeDriver;
import com.iver.utiles.extensionPoints.ExtensionPoint;
import com.iver.utiles.extensionPoints.ExtensionPointsSingleton;

public class OpenCSVToPointsExtension extends Extension{

	public void initialize() {
		// TODO Auto-generated method stub

	}

	public void postInitialize() {
		try {
			addCSVPointLayer();
		} catch (DriverLoadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchTableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void execute(String actionCommand) {
		// TODO Auto-generated method stub

	}

	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isVisible() {
		// TODO Auto-generated method stub
		return false;
	}



	private void addCSVPointLayer() throws DriverLoadException, NoSuchTableException, DriverException, InstantiationException, IllegalAccessException{
//		  Funcion encargada de crear y cargar en la vista la capa
//		  de trabajo a partir del fichero csv

		ProjectView pv=ProjectFactory.createView("Ejemplo CSV to Points");
		View view=(View)pv.createWindow();
		ProjectExtension pe=(ProjectExtension)PluginServices.getExtension(ProjectExtension.class);
		Project project=pe.getProject();
		project.addDocument(pv);
		PluginServices.getMDIManager().addWindow(view);

//		   csv
		  DataSourceFactory dataSourceFactory=LayerFactory.getDataSourceFactory();
		  URL fileURL = PluginServices.getPluginServices(this).getClassLoader().getResource("data/prueba.csv");
		  
		  dataSourceFactory.addFileDataSource("csv string", "prueba",fileURL.getPath());
		  DataSource ds = dataSourceFactory.createRandomDataSource("prueba");
		  ds.start();

//		   Crearemos el driver que gestiona la capa de eventos y lo enlazaremos con
//		   la fuente de datos que acabamos de crear indicandole que columnas de esta
//		   representan los puntos de la geomretria
		  int xFieldIndex = ds.getFieldIndexByName("X");
		  int yFieldIndex = ds.getFieldIndexByName("Y");
//		  DriverManager dm=new DriverManager();
		  AddEventThemeDriver driver=new AddEventThemeDriver();//(AddEventThemeDriver)dm.getDriver("csv string");
		  driver.setData(ds, xFieldIndex, yFieldIndex);

		  MapContext mapContext=view.getMapControl().getMapContext();

//		   Crearemos ahora la nueva capa basada en este driver
		  FLayerGenericVectorial layer = (FLayerGenericVectorial)(((ExtensionPoint)ExtensionPointsSingleton.getInstance().get("Layers")).create("com.iver.cit.gvsig.fmap.layers.FLayerGenericVectorial"));
		  layer.setName("prueba");
		  layer.setDriver(driver);
		  layer.setProjection(mapContext.getProjection());
	      mapContext.getLayers().addLayer(layer);



//          capa = LayerFactory.createLayer("ademuz", driver, mapContext.getProjection());
//	      capa.addLayerListener(new AddEventThemListener());
//	      mapContext.getLayers().addLayer(layer);
//		   Una vez creada la capa se le a�ade una propiedad para reconocerla
//		   como nuestra capa de trabajo
		  layer.setProperty("CapaDePruebaCSV","propiedad para distinguir la capa de prueba");

//		   Indicamos al mapContext que se debe repintar
		  mapContext.invalidate();
		  mapContext.redraw();
	}
}
