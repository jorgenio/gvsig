package org.gvsig.example.memory;

import java.util.ArrayList;

import com.hardcode.gdbms.engine.values.Value;
import com.hardcode.gdbms.engine.values.ValueFactory;
import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.core.FPolyline2D;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.GeneralPathX;
import com.iver.cit.gvsig.fmap.drivers.ConcreteMemoryDriver;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.project.documents.view.gui.View;

/**
 * @author fjp
 * 
 * Example of using a ConcreteMemoryDriver to obtain a "editable layer" where
 * you can write your features.
 *
 */
public class CreateMemoryLayer extends Extension {
	
	public void initialize() {
		
	}

	public void execute(String actionCommand) {
		View v = (View) PluginServices.getMDIManager().getActiveWindow();
		MapControl mapCtrl = v.getMapControl();
		        
	    
        // DRIVER DEFINITION (SHAPE TYPE AND FIELDS)
        ConcreteMemoryDriver driver = new ConcreteMemoryDriver();
        driver.setShapeType(FShape.LINE);
        
		ArrayList arrayFields = new ArrayList();
		arrayFields.add("ID");
		Value[] auxRow = new Value[1];
		
		driver.getTableModel().setColumnIdentifiers(arrayFields.toArray());
        
        
		// GEOMETRY DEFINITION
        GeneralPathX gp = new GeneralPathX();
        
        gp.moveTo(20,80);
        gp.lineTo(80,60);
        gp.lineTo(100, 140);
        
        FShape shp = new FPolyline2D(gp);
        
        
        // ATRIBUTES
        auxRow[0] = ValueFactory.createValue(0);
        
        // ADD RECORD
        driver.addShape(shp, auxRow);
	        

        // CREATE AND ADD LAYER
	    FLayer lyr;
	    String layerName = "Example";
		lyr = LayerFactory.createLayer(layerName,driver, mapCtrl.getProjection());

		if (lyr != null) {
			lyr.setVisible(true);
			mapCtrl.getMapContext().getLayers().addLayer(lyr);
            
		}
        
		
	}

	public boolean isEnabled() {
		return true;
	}

	public boolean isVisible() {
		IWindow v = PluginServices.getMDIManager().getActiveWindow();

		if  (v instanceof View)
			return true;
		else
			return false;
	}
	

}

