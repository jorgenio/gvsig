package org.gvsig.example.postgis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;

import com.hardcode.driverManager.DriverLoadException;
import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.ViewPort;
import com.iver.cit.gvsig.fmap.core.ICanReproject;
import com.iver.cit.gvsig.fmap.crs.CRSFactory;
import com.iver.cit.gvsig.fmap.drivers.ConnectionFactory;
import com.iver.cit.gvsig.fmap.drivers.ConnectionJDBC;
import com.iver.cit.gvsig.fmap.drivers.DBException;
import com.iver.cit.gvsig.fmap.drivers.DBLayerDefinition;
import com.iver.cit.gvsig.fmap.drivers.IConnection;
import com.iver.cit.gvsig.fmap.drivers.IVectorialJDBCDriver;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.project.documents.view.gui.View;

public class LoadLayer extends Extension {

	public void initialize() {

	}

	public void execute(String actionCommand) {
		// IN CONFIG.XML, the actionCommand should be the layer name. (table name)
		View v = (View) PluginServices.getMDIManager().getActiveWindow();
		MapControl mapCtrl = v.getMapControl();

        String dbURL = "jdbc:postgresql://localhost:5432/latin1"; // latin1 is the catalog name
        String user = "postgres";
        String pwd = "aquilina";
        String layerName = actionCommand;
        String tableName = actionCommand;
        IConnection conn;
		try {
			conn = ConnectionFactory.createConnection(dbURL, user, pwd);
	        ((ConnectionJDBC)conn).getConnection().setAutoCommit(false);

	        String fidField = "gid"; // BE CAREFUL => MAY BE NOT!!!
	        String geomField = "the_geom"; // BE CAREFUL => MAY BE NOT!!! => You should read table GEOMETRY_COLUMNS.
	        								// See PostGIS help.

	        // To obtain the fields, make a connection and get them.
			Statement st = ((ConnectionJDBC)conn).getConnection().createStatement();
			ResultSet rs = st.executeQuery("select * from " + tableName + " LIMIT 1");
			ResultSetMetaData rsmd = rs.getMetaData();
			String[] fields = new String[rsmd.getColumnCount()-1]; // We don't want to include the_geom field
			int j = 0;
			for (int i = 0; i < fields.length; i++) {
				if (!rsmd.getColumnName(i+1).equalsIgnoreCase(geomField))
				{
					fields[j++] = rsmd.getColumnName(i+1);
				}
			}
			rs.close();

	        /* String[] fields = new String[1];
	        fields[0] = "gid"; */

	        String whereClause = "";

	        IVectorialJDBCDriver driver = (IVectorialJDBCDriver) LayerFactory.getDM()
	        			.getDriver("PostGIS JDBC Driver");

	        // Here you can set the workingArea
	        // driver.setWorkingArea(dbLayerDefinition.getWorkingArea());


	        String strEPSG = mapCtrl.getViewPort()
	                    .getProjection().getAbrev()
	                    .substring(5);
	        DBLayerDefinition lyrDef = new DBLayerDefinition();
	        lyrDef.setName(layerName);
	        lyrDef.setTableName(tableName);
	        lyrDef.setWhereClause(whereClause);
	        lyrDef.setFieldNames(fields);
	        lyrDef.setFieldGeometry(geomField);
	        lyrDef.setFieldID(fidField);
	        // if (dbLayerDefinition.getWorkingArea() != null)
	        //     lyrDef.setWorkingArea(dbLayerDefinition.getWorkingArea());

	        lyrDef.setSRID_EPSG(strEPSG);
	        if (driver instanceof ICanReproject)
	        {
	            ((ICanReproject)driver).setDestProjection(strEPSG);
	        }
	        driver.setData(conn, lyrDef);
	        IProjection proj = null;
	        if (driver instanceof ICanReproject)
	        {
	        	 proj = CRSFactory.getCRS("EPSG:" + ((ICanReproject)driver).getSourceProjection(null,null));
	        }

	        FLayer lyr = LayerFactory.createDBLayer(driver, layerName, proj);

			if (lyr != null) {
				lyr.setVisible(true);
				v.getMapControl().getMapContext().beginAtomicEvent();
				// You shoud checkProjection to see if it is necesary to reproject
	            checkProjection(lyr, v.getMapControl().getViewPort());
				v.getMapControl().getMapContext().getLayers()
					   .addLayer(lyr);
				v.getMapControl().getMapContext().endAtomicEvent();

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DriverLoadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	public boolean isEnabled() {
		return true;
	}

	public boolean isVisible() {
		IWindow v = PluginServices.getMDIManager().getActiveWindow();

		if  (v instanceof View)
			return true;
		else
			return false;
	}

    private void checkProjection(FLayer lyr, ViewPort viewPort)
    {
        if (lyr instanceof FLyrVect)
        {
            FLyrVect lyrVect = (FLyrVect) lyr;
            IProjection proj = lyr.getProjection();
            // Comprobar que la projecci�n es la misma que la vista
            if (proj == null)
            {
                // SUPONEMOS que la capa est� en la proyecci�n que
                // estamos pidiendo (que ya es mucho suponer, ya).
                lyrVect.setProjection(viewPort.getProjection());
                return;
            }
            if (proj != viewPort.getProjection()) {
                int option = JOptionPane.showConfirmDialog(null,
                        PluginServices.getText(this, "reproyectar_aviso"),
                        PluginServices.getText(this, "reproyectar_pregunta"),
                        JOptionPane.YES_NO_OPTION);

                if (option == JOptionPane.NO_OPTION) {
                    return;
                } else {
                    ICoordTrans ct = proj.getCT(viewPort.getProjection());
                    lyrVect.setCoordTrans(ct);
                    System.err.println("coordTrans = " +
                        proj.getAbrev() + " " +
                        viewPort.getProjection().getAbrev());
                }
            }
        }

    }


}
