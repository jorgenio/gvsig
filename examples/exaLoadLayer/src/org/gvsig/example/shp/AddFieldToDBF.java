/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.example.shp;

import java.io.File;
import java.sql.Types;
import java.util.Calendar;

import com.hardcode.gdbms.driver.exceptions.CloseDriverException;
import com.hardcode.gdbms.driver.exceptions.InitializeWriterException;
import com.hardcode.gdbms.driver.exceptions.OpenDriverException;
import com.hardcode.gdbms.driver.exceptions.ReadDriverException;
import com.hardcode.gdbms.engine.values.Value;
import com.hardcode.gdbms.engine.values.ValueFactory;
import com.iver.cit.gvsig.exceptions.visitors.ProcessWriterVisitorException;
import com.iver.cit.gvsig.exceptions.visitors.StartWriterVisitorException;
import com.iver.cit.gvsig.exceptions.visitors.StopWriterVisitorException;
import com.iver.cit.gvsig.fmap.core.DefaultRow;
import com.iver.cit.gvsig.fmap.drivers.FieldDescription;
import com.iver.cit.gvsig.fmap.drivers.ITableDefinition;
import com.iver.cit.gvsig.fmap.drivers.dbf.DBFDriver;
import com.iver.cit.gvsig.fmap.edition.DefaultRowEdited;
import com.iver.cit.gvsig.fmap.edition.IRowEdited;

/**
 * @author fjp
 *
 * Abrimos un dbf y le a�adimos un campo. Lo rellenamos con la fecha actual con formato cadena.
 *
 */
public class AddFieldToDBF  {

	public static void main(String[] args) {
		AddFieldToDBF prueba = new AddFieldToDBF();
		prueba.execute("");
	}

	public void execute(String actionCommand) {
        DBFDriver driver = new DBFDriver();
        File myFile = new File("c:/0libro.dbf");
        try {
        	// Abrimos el fichero
			driver.open(myFile);

			// Comprobamos que tenemos acceso de escritura.
			if (driver.canSaveEdits())
			{

				// Creamos el nuevo campo

				FieldDescription dateField = new FieldDescription();
				dateField.setFieldName("Date");
				dateField.setFieldType(Types.VARCHAR);
				dateField.setFieldLength(20);

				// Creamos un array con el nuevo campo
				ITableDefinition tableDef = driver.getTableDefinition();
				FieldDescription[] oldFields = tableDef.getFieldsDesc();
				int numOldFields = oldFields.length;
				FieldDescription[] newFields = new FieldDescription[numOldFields + 1];
				System.arraycopy(oldFields, 0, newFields, 0, numOldFields);
				newFields[numOldFields] = dateField;

				tableDef.setFieldsDesc(newFields);

				driver.initialize(tableDef);
				driver.preProcess();
				Value[] att = new Value[newFields.length];
				Calendar today = Calendar.getInstance();
				for (int i=0; i < driver.getRowCount(); i++)
				{
					for (int j=0; j < numOldFields; j++)
					{
						att[j] = driver.getFieldValue(i, j);
					}
					att[numOldFields] = ValueFactory.createValue(today.getTime().toLocaleString());
					DefaultRow row = new DefaultRow(att);
					DefaultRowEdited edRow = new DefaultRowEdited(row,
							IRowEdited.STATUS_MODIFIED, i);

					driver.process(edRow);
				}
				driver.postProcess();

				System.out.println("Fichero modificado");

			}
			else
			{
				System.err.println("El fichero no tiene permiso de edici�n");
			}
			driver.close();
		} catch (ProcessWriterVisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CloseDriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OpenDriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ReadDriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InitializeWriterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (StartWriterVisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (StopWriterVisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}


}


