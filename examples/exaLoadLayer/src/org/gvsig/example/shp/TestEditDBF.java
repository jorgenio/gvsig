/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package org.gvsig.example.shp;

import java.io.File;

import com.iver.cit.gvsig.fmap.drivers.dbf.DbaseFile;

public class TestEditDBF {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestEditDBF aux = new TestEditDBF();
		aux.editField2();

	}
	
//	public void editField() {
//        DbaseFileNIO driver = new DbaseFileNIO();
//        String dbfPath = "c:/0libro.dbf";
//        File myFile = new File(dbfPath);
//        try {
//        	// Abrimos el fichero
//			driver.open(myFile);
//	        RandomAccessFile raf = new RandomAccessFile(myFile, "rw");
//	        FileChannel channel = raf.getChannel();
//
//			DbaseFileHeaderNIO myHeader = driver.getDBaseHeader(); 
//			DbaseFileWriterNIO dbfWrite = new DbaseFileWriterNIO(myHeader,
//					channel);
//			Object[] record = new Object[myHeader.getNumFields()];
//			
//            for (int r = 0; r < driver.getFieldCount(); r++) {
//                record[r] = driver.getStringFieldValue(2, r);
//            }
//
//            record[2] = ValueFactory.createValue("coleguilla");
//			
//			dbfWrite.writeRecord(record, 2);
//
//			
//        }
//        catch (Exception e)
//        {
//        	e.printStackTrace();
//        }
//	}
	public void editField2() {
        DbaseFile driver = new DbaseFile();
        String dbfPath = "c:/0libro.dbf";
        File myFile = new File(dbfPath);
        try {
        	// Abrimos el fichero
			driver.open(myFile);
			driver.setFieldValue(1,0, new Integer(54));
			System.out.println("Valor nuevo:" + driver.getStringFieldValue(1,0));
			driver.close();

			
        }
        catch (Exception e)
        {
        	e.printStackTrace();
        }
	}

}


