package com.iver.templateLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;


/**
 * Clase que extiende JPanel e implementa View para poder a�adirse como una
 * ventana m�s a la aplicaci�n.
 *
 *Es como un di�logo que consulta al usuario el modo de mandar a impresi�n de las fichas.
 *
 * @author Vicente Caballero Navarro
 * 
 */
public class AskAll extends JPanel implements IWindow {
    private WindowInfo m_viewinfo = null;
    private JLabel jLabel = null;
    private JButton jButton = null;
    private JButton jButton1 = null;
    //Array de booleanos que contiene en la posici�n:
    // [0]-->Si se selecciona la impresi�n de todas las fichas generadas se pone a true.
    // [1]-->Si se selecciona cancelar de todas las fichas generadas se pone a false.
    private boolean[] printAll = null;
    private JPanel jPanel = null;
    private JButton jButton2 = null;

    /**
     * Constructor.
     *
     * @param b Array de booleanos que contiene en la posici�n:
     * [0]-->Si se selecciona la impresi�n de todas las fichas generadas se pone a true.
     * [1]-->Si se selecciona cancelar de todas las fichas generadas se pone a false.
     */
    public AskAll(boolean[] b) {
        super();
        printAll = b;
        initialize();
    }

    /**
     * This method initializes this
     */
    private void initialize() {
        jLabel = new JLabel();
        this.setSize(618, 70);
        jLabel.setText(PluginServices.getText(this, "imprimir_todas_o_una"));
        this.add(jLabel, null);
        this.add(getJPanel(), null);
    }

    /**
     * @see com.iver.mdiApp.ui.MDIManager.View#getViewInfo()
     */
    public WindowInfo getWindowInfo() {
        if (m_viewinfo == null) {
            m_viewinfo = new WindowInfo(WindowInfo.MODALDIALOG);
            m_viewinfo.setTitle(PluginServices.getText(this, "todo"));
        }

        return m_viewinfo;
    }

    /**
     * @see com.iver.mdiApp.ui.MDIManager.View#viewActivated()
     */
    public void viewActivated() {
    }

    /**
     * This method initializes jButton
     *
     * @return javax.swing.JButton
     */
    private JButton getJButton() {
        if (jButton == null) {
            jButton = new JButton();
            jButton.setText(PluginServices.getText(this, "todas"));
            jButton.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent e) {
                        printAll[0] = true;
                        PluginServices.getMDIManager().closeWindow(AskAll.this);
                    }
                });
        }

        return jButton;
    }

    /**
     * This method initializes jButton1
     *
     * @return javax.swing.JButton
     */
    private JButton getJButton1() {
        if (jButton1 == null) {
            jButton1 = new JButton();
            jButton1.setText(PluginServices.getText(this, "cancelar"));
            jButton1.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent e) {
                        printAll[1] = false;
                        PluginServices.getMDIManager().closeWindow(AskAll.this);
                    }
                });
        }

        return jButton1;
    }

    /**
     * This method initializes jPanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getJPanel() {
        if (jPanel == null) {
            jPanel = new JPanel();
            jPanel.add(getJButton(), null);
            jPanel.add(getJButton2(), null);
            jPanel.add(getJButton1(), null);
        }

        return jPanel;
    }

    /**
     * This method initializes jButton2
     *
     * @return javax.swing.JButton
     */
    private JButton getJButton2() {
        if (jButton2 == null) {
            jButton2 = new JButton();
            jButton2.setText(PluginServices.getText(this, "una"));
            jButton2.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent e) {
                        printAll[0] = false;
                        PluginServices.getMDIManager().closeWindow(AskAll.this);
                    }
                });
        }

        return jButton2;
    }
} //  @jve:decl-index=0:visual-constraint="10,10"
