package com.iver.templateLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.Print;
import com.iver.cit.gvsig.project.documents.layout.gui.Layout;


/**
 * Clase que extiende JPanel e implementa View para poder a�adirse como una
 * ventana m�s a la aplicaci�n.
 *
 *Es como un di�logo que consulta al usuario si se desea enviar una ficha en concreto a impresi�n.
 *
 * @author Vicente Caballero Navarro
 * 
 */
public class AskOne extends JPanel implements IWindow {
	private WindowInfo m_viewinfo = null;
	private Layout layout = null;
	private JLabel jLabel = null;
	private JButton jButton = null;
	private JButton jButton1 = null;

	/**
	 * Constructor.
	 *
	 * @param l Layout que contiene la ficha actual.
	 */
	public AskOne(Layout l) {
		super();
		layout = l;
		initialize();
	}

	/**
	 * This method initializes this
	 */
	private void initialize() {
		jLabel = new JLabel();
		this.setSize(198, 68);
		jLabel.setText(PluginServices.getText(this, "imprimir_ficha_actual"));
		this.add(jLabel, null);
		this.add(getJButton(), null);
		this.add(getJButton1(), null);
	}

	/**
	 * @see com.iver.mdiApp.ui.MDIManager.View#getViewInfo()
	 */
	public WindowInfo getWindowInfo() {
		if (m_viewinfo == null) {
			m_viewinfo = new WindowInfo(WindowInfo.MODALDIALOG);
			m_viewinfo.setTitle(PluginServices.getText(this, "uno"));
		}

		return m_viewinfo;
	}

	/**
	 * @see com.iver.mdiApp.ui.MDIManager.View#viewActivated()
	 */
	public void viewActivated() {
	}

	/**
	 * This method initializes jButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButton() {
		if (jButton == null) {
			jButton = new JButton();
			jButton.setText(PluginServices.getText(this, "aceptar"));
			jButton.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						Print printExtension = new Print();
						printExtension.OpenDialogToPrint(layout);
						PluginServices.getMDIManager().closeWindow(AskOne.this);
					}
				});
		}

		return jButton;
	}

	/**
	 * This method initializes jButton1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setText(PluginServices.getText(this, "cancelar"));
			jButton1.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						PluginServices.getMDIManager().closeWindow(AskOne.this);
					}
				});
		}

		return jButton1;
	}
} //  @jve:decl-index=0:visual-constraint="10,10"
