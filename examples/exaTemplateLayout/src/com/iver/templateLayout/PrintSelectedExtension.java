package com.iver.templateLayout;

import com.hardcode.gdbms.driver.exceptions.ReadDriverException;
import com.hardcode.gdbms.engine.data.driver.DriverException;
import com.iver.andami.PluginServices;
import com.iver.andami.messages.NotificationManager;
import com.iver.andami.plugins.Extension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLayers;
import com.iver.cit.gvsig.project.documents.view.gui.View;

public class PrintSelectedExtension extends Extension {

	public void initialize() {
		// TODO Auto-generated method stub

	}

	public void execute(String actionCommand) {
		IWindow andamiView = PluginServices.getMDIManager().getActiveWindow();

		if (andamiView instanceof View)
		{
			View theView = (View) andamiView;
			FLayers layers = theView.getMapControl().getMapContext().getLayers();
//			FLayer[] actives = layers.getActives();
			FLayer layerProvin = layers.getLayer("Provin.shp");
			PrintingManager printingManager = new PrintingManager(layerProvin);
			try {
				printingManager.startPrinting();
			} catch (ReadDriverException e) {
				e.printStackTrace();
				NotificationManager.addError(e);
			}
		}

	}

	public boolean isEnabled() {
		return true;
	}

	public boolean isVisible() {
		return true;
	}

}
