package com.iver.templateLayout;

import com.hardcode.gdbms.driver.exceptions.ReadDriverException;
import com.hardcode.gdbms.engine.data.driver.DriverException;
import com.hardcode.gdbms.engine.values.ValueWriter;
import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.ProjectExtension;
import com.iver.cit.gvsig.exceptions.expansionfile.ExpansionFileReadException;
import com.iver.cit.gvsig.fmap.core.IFeature;
import com.iver.cit.gvsig.fmap.layers.FBitSet;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.SelectableDataSource;
import com.iver.cit.gvsig.fmap.layers.layerOperations.Selectable;
import com.iver.cit.gvsig.project.Project;
import com.iver.cit.gvsig.project.documents.layout.ProjectMap;
import com.iver.cit.gvsig.project.documents.layout.ProjectMapFactory;
import com.iver.cit.gvsig.project.documents.layout.fframes.FFrameText;
import com.iver.cit.gvsig.project.documents.layout.fframes.FFrameView;
import com.iver.cit.gvsig.project.documents.layout.fframes.IFFrame;
import com.iver.cit.gvsig.project.documents.layout.gui.Layout;

public class PrintingManager {
	FLyrVect layer;
	Layout layout;

	public PrintingManager(FLayer layerProvin) {
		layer = (FLyrVect) layerProvin;
		ProjectExtension aux = (ProjectExtension) PluginServices.getExtension(ProjectExtension.class);
		Project theProject = aux.getProject();
		layout = ((ProjectMap) theProject.getProjectDocumentByName(
				"FichaCurso", ProjectMapFactory.registerName)).getModel();

	}

	public void startPrinting() throws ReadDriverException {
		Selectable select = (Selectable) layer;
		FBitSet bs = select.getSelection();
		SelectableDataSource rs = layer.getRecordset();

		for(int i=bs.nextSetBit(0); i>=0; i=bs.nextSetBit(i+1)) {
			// operate on index i here }
			IFeature feat;
			try {
				feat = layer.getSource().getFeature(i);
			} catch (ExpansionFileReadException e) {
				throw new ReadDriverException(layer.getName(),e);
			}

			IFFrame[] theFrames = layout.getLayoutContext().getFFrames();
			for (int j=0; j < theFrames.length; j++)
			{
				if (theFrames[j].getTag() != null)
				{
					processFrame(theFrames[j], feat, rs );
				}
			}
			layout.getLayoutControl().refresh();
			layout.showPrintDialog(null);
		}

	}

	private void processFrame(IFFrame frame, IFeature feat, SelectableDataSource rs) throws ReadDriverException {
		String tag = frame.getTag();
		if (tag.compareTo("VISTA1")==0)
		{
			FFrameView fv = (FFrameView) frame;
			fv.getMapContext().getViewPort().setExtent(feat.getGeometry().getBounds2D());
		}
		if (frame instanceof FFrameText)
		{
			FFrameText ft = (FFrameText) frame;
			ft.clearText();
			int fieldId = rs.getFieldIndexByName(tag);
			ft.addText(feat.getAttribute(fieldId).getStringValue(ValueWriter.internalValueWriter));
		}
//		if (tag.compareTo("1")==0)
//		{
//			FFrameView fv = (FFrameView) frame;
//			fv.getFMap().getViewPort().setExtent(feat.getGeometry().getBounds2D());
//		}

	}

}
