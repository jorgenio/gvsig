package com.iver.templateLayout;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Hashtable;

import com.hardcode.gdbms.driver.exceptions.ReadDriverException;
import com.hardcode.gdbms.engine.data.driver.DriverException;
import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.Print;
import com.iver.cit.gvsig.exceptions.expansionfile.ExpansionFileReadException;
import com.iver.cit.gvsig.fmap.drivers.DriverIOException;
import com.iver.cit.gvsig.fmap.layers.SelectableDataSource;
import com.iver.cit.gvsig.fmap.layers.layerOperations.AlphanumericData;
import com.iver.cit.gvsig.fmap.layers.layerOperations.SingleLayer;
import com.iver.cit.gvsig.project.documents.layout.fframes.FFrameText;
import com.iver.cit.gvsig.project.documents.layout.fframes.FFrameView;
import com.iver.cit.gvsig.project.documents.layout.fframes.IFFrame;
import com.iver.cit.gvsig.project.documents.layout.gui.Layout;


/**
 * Extensi�n de ejemplo que genera una ficha por cada shape seleccionado,
 * cambiando los tag por su valor en la tabla.
 *
 * @author Vicente Caballero Navarro
 */
public class TemplateExtension extends Extension {
	//Indica si es la primera ficha lanzada a imprimir.
	private boolean isFirst = true;

	//Layout que modificamos para la creaci�n de las fichas.
	private Layout layout = null;

	//Array de booleanos que contiene en la posici�n:
	//"0"-->Si se selecciona la impresi�n de todas las fichas generadas se pone a true.
	//"1"-->Si se selecciona cancelar de todas las fichas generadas se pone a false.
	private boolean[] printAll = { false, true };

	//Otra extensi�n, que es utlizada para lanzar el Layout a impresi�n.
	private Print printExtension = new Print();

	//Almacena el texto de todos los FFrameText del Layout.
	private Hashtable hashtext = new Hashtable();

	//ArrayList que almacena todos los FFrames que contiene el Layout.
	private IFFrame[] fframes = null;

	/**
	 * @see com.iver.mdiApp.plugins.Extension#isEnabled()
	 */
	public boolean isEnabled() {
		//Devuelvo true, y de esta forma siempre estar�n activos.
		return true;
	}

	/**
	 * @see com.iver.mdiApp.plugins.Extension#isVisible()
	 */
	public boolean isVisible() {
		//Se obtiene la vista activada.
		IWindow f = PluginServices.getMDIManager().getActiveWindow();

		//Se comprueba si la vista es null y se devuelve un false para no hacer visible la extensi�n.
		if (f == null) {
			return false;
		}

		//Se comprueba si la vista activa contiene un Layout y
		//si es as� se devuelve true como visible o false si no contiene un Layout.
		if (f instanceof Layout) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Guarda el contenido de todos los FFrameText para poder  recuperarlos al
	 * final.
	 */
	private void initFFrameText() {
		//Recorro todos los FFrames que contiene el Layout y compruebo que son una instancia de FFrameText,
		//para guardar en hastext el texto y su FFrameText.
		for (int ft = 0; ft < fframes.length; ft++) {
			if (fframes[ft] instanceof FFrameText) {
				ArrayList al = new ArrayList();

				for (int i = 0;
						i < ((FFrameText) fframes[ft]).getText().size();
						i++) {
					al.add(((FFrameText) fframes[ft]).getText().get(i));
				}

				hashtext.put(fframes[ft], al);
			}
		}
	}

	/**
	 * Recupera el texto que inicialmente ten�an todos FFrameText.
	 */
	private void endFFrameText() {
		//Recorro todos los FFrames que contiene el Layout y compruebo que son una instancia de FFrameText,
		//para inicializarlo con su texto original almacenado en hashtext.
		for (int ft = 0; ft < fframes.length; ft++) {
			if (fframes[ft] instanceof FFrameText) {
				ArrayList text = (ArrayList) hashtext.get(fframes[ft]);
				((FFrameText) fframes[ft]).getText().clear();

				for (int i = 0; i < text.size(); i++) {
					((FFrameText) fframes[ft]).addText((String) text.get(i));
				}
			}
		}
	}

	/**
	 * Inicia la creaci�n de todas las platillas posibles a partir de un
	 * FFrameView.
	 *
	 * @param fframeview FFrameView a modificar.
	 * @throws ReadDriverException
	 *
	 * @throws DriverException
	 * @throws com.hardcode.gdbms.engine.data.DriverException
	 * @throws DriverIOException
	 * @throws com.hardcode.gdbms.engine.data.driver.DriverException
	 */
	private void initTemplates(FFrameView fframeview) throws ReadDriverException {
		//Inicio la opci�n de cancelar la impresi�n de todas las fichas a true.
		printAll[1] = true;

		//Rect�ngulo del extent inicial de la vista contenida en el FFrameView.
		Rectangle2D.Double rectIni = new Rectangle2D.Double();
		rectIni.setRect(fframeview.getMapContext().getViewPort().getExtent());

		//Entero que representa el tipo de escala que tiene seleccionada el FFrameView.
		int typeScaleIni = fframeview.getTypeScale();

		//Guarda los textos iniciales de todos los FFrameText
		initFFrameText();

		//Recorro todas las capas de la vista.
		for (int i = 0; i < fframeview.getMapContext().getLayers().getLayersCount();
				i++) {
			//Compruebo que la capa este activada.
			if (fframeview.getMapContext().getLayers().getLayer(i).isActive()) {
				//Obtengo su recordSet
				if (fframeview.getMapContext().getLayers().getLayer(i) instanceof AlphanumericData) {
					SelectableDataSource dataSource = ((AlphanumericData) fframeview.getMapContext()
																					.getLayers()
																					.getLayer(i)).getRecordset();

					//Recorro todos los registros.
					for (long k = 0; k < dataSource.getRowCount(); k++) {
						//Compruebo que est� seleccionado.
						if (dataSource.isSelected((int) k)) {
							//Actualizo la posici�n del puntero del recordSet.
							////dataSource.moveTo(k);
							//Obtengo el rect�ngulo del shape para despu�s ponerlo como extent del FFrameView
							Rectangle2D rec;
							try {
								rec = ((SingleLayer) fframeview
												   .getMapContext().getLayers().getLayer(i)).getSource()
												   .getShape((int) k).getBounds2D();
							} catch (ExpansionFileReadException e) {
								throw new ReadDriverException(fframeview
										   .getMapContext().getLayers().getLayer(i).getName(),e);
							}

							//fframeview.setLinked(false);
							//Cambio el extent de la vista que contiene fframeview,
							//del que ten�a al extent del shape seleccionado.
							fframeview.setNewExtent(rec);

							//Cambio el tipo de escala de fframeview a Escala especificada por el usuario.
							fframeview.setTypeScale(FFrameView.MANUAL);

							//Recorro todos los FFrames que contiene el Layout y compruebo que son una instancia de FFrameText,
							//para inicializarlo con el valor correspondiente.
							for (int ft = 0; ft < fframes.length; ft++) {
								if (fframes[ft] instanceof FFrameText) {
									if (fframeview != null) {
										initFFrameText((FFrameText) fframes[ft], dataSource, k);
									}
								}
							}

							if (isFirst) {
								//Si es el primer registro seleccionado se abre el di�logo
								//para preguntar si se quiere imprimir todas las fichas.
								askAll();
								isFirst = false;
							}

							if (!printAll[1]) {
								//Si se cancela la impresi�n desde el di�logo askAll se termina la creaci�n de fichas.
								//Pongo isFirst a true para que la pr�xima vez que se llame
								//a este m�todo se pregunte si se quiere imprimir todas las fichas.
								isFirst = true;

								return;
							}

							if (printAll[0]) {
								//Imprime la ficha actual.
								printExtension.printLayout(layout);
							} else {
								//Refrescar el Layout para ver la ficha actual en pantalla.
								layout.getLayoutControl().refresh();
								//Preguntar si se quiere imprimir la ficha actual.
								askOne();
							}
						}
					}
				}
			}
		}

		//Recupera el Extent inicial del FFrameView.
		fframeview.getMapContext().getViewPort().setExtent(rectIni);

		//Recupera el tipo de escala que ten�a seleccionada el FFrameView.
		fframeview.setTypeScale(typeScaleIni);

		//Recupera el texto de todos los FFrameText del Layout.
		endFFrameText();

		//Refresca el Layout en pantalla.
		layout.getLayoutControl().refresh();

		//Volvemos a inicializar el boolean que sirve para saber si estamos en la primera ficha a imprimir.
		isFirst = true;
	}

	/**
	 * Modifica el FFrameText que se le pasa como par�metro cambiando su texto
	 * por el valor del campo que representa el tag.
	 *
	 * @param fframetext FFrameText a modificar.
	 * @param dataSource DataSource para extraer el valor.
	 * @param k indice del registro.
	 * @throws ReadDriverException
	 * @throws com.hardcode.gdbms.engine.data.driver.DriverException
	 *
	 * @throws com.hardcode.gdbms.engine.data.DriverException
	 */
	private void initFFrameText(FFrameText fframetext,
		SelectableDataSource dataSource, long k) throws ReadDriverException {
		//Recorro todos los campos.
		dataSource.start();
		for (int j = 0; j < dataSource.getFieldCount(); j++) {
			//Compruebo si el tag es igual al campo, y si es as�,
			//borro el texto que contiene y le a�ado el valor.
			if (dataSource.getFieldName(j).equals(fframetext.getTag())) {
				fframetext.getText().clear();
				fframetext.addText(dataSource.getFieldValue(k, j)
											 .toString());
			}
		}
		dataSource.stop();
	}

	/**
	 * Abre el di�logo que pregunta si se quieren imprimir todas las fichas que
	 * se generan.
	 */
	public void askAll() {
		AskAll all = new AskAll(printAll);

		//A�ade el objeto all que extiende JPanel
		//e implementa com.iver.mdiApp.ui.MDIManager.View como una
		//ventana m�s de la aplicaci�n.
		PluginServices.getMDIManager().addWindow(all);
	}

	/**
	 * Abre el di�logo que pregunta si la ficha que se esta visualizando se
	 * quiere imprimir.
	 */
	public void askOne() {
		AskOne one = new AskOne(layout);

		//A�ade el objeto one que extiende JPanel
		//e implementa com.iver.mdiApp.ui.MDIManager.View como una
		//ventana m�s de la aplicaci�n.
		PluginServices.getMDIManager().addWindow(one);
	}

	/**
	 * @see com.iver.andami.plugins.Extension#inicializar()
	 */
	public void initialize() {
	}

	/**
	 * @see com.iver.andami.plugins.Extension#execute(java.lang.String)
	 */
	public void execute(String actionCommand) {
		//Obtengo el Layout que contiene la vista actualmente seleccionada en ANDAMI.
		layout = (Layout) PluginServices.getMDIManager().getActiveWindow();

		//Obtengo todos los FFrames que contiene el Layout.
		fframes = layout.getLayoutContext().getFFrames();

		//Recorro todos los FFrames comprobando si es una instacia de FFrameView
		//para ejecutar initTemplates en el caso de que as� sea.
		for (int i = 0; i < fframes.length; i++) {
			if (fframes[i] instanceof FFrameView) {
				try {
					//Inicia la creaci�n de todas las platillas posibles a partir de fframeview.
					initTemplates((FFrameView) fframes[i]);
				} catch (ReadDriverException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
