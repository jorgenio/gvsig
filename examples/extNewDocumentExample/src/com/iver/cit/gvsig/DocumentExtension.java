package com.iver.cit.gvsig;

import com.iver.andami.plugins.Extension;
import com.iver.cit.gvsig.project.documents.thedocument.TheDocumentFactory;

public class DocumentExtension extends Extension{

	public void initialize() {
		TheDocumentFactory.register();
	}

	public void execute(String actionCommand) {
		// TODO Auto-generated method stub

	}

	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isVisible() {
		// TODO Auto-generated method stub
		return false;
	}

}
