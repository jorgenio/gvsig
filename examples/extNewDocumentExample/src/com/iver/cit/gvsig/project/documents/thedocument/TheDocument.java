package com.iver.cit.gvsig.project.documents.thedocument;

import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.fmap.layers.XMLException;
import com.iver.cit.gvsig.project.Project;
import com.iver.cit.gvsig.project.documents.ProjectDocument;
import com.iver.cit.gvsig.project.documents.exceptions.OpenException;
import com.iver.cit.gvsig.project.documents.exceptions.SaveException;
import com.iver.cit.gvsig.project.documents.thedocument.gui.DocumentGUI;
import com.iver.cit.gvsig.project.documents.thedocument.gui.DocumentProperties;
import com.iver.utiles.XMLEntity;


/**
 * New document.
 *
 * @author Vicente Caballero Navarro
 */
public class TheDocument extends ProjectDocument {
    /**
     * Returns the XMEntity with all the characteristics that want to store in the project.
     *
     * @return XMLEntity
     * @throws SaveException
     * @throws XMLException
     *
     * @throws DriverException
     */
    public XMLEntity getXMLEntity() throws SaveException {
        XMLEntity xml = super.getXMLEntity();

        return xml;
    }

    /**
     * @throws OpenException
     * @see com.iver.cit.gvsig.project.documents.ProjectDocument#setXMLEntity(com.iver.utiles.XMLEntity)
     */
    public void setXMLEntity(XMLEntity xml, Project p)
        throws OpenException {
        try {
            for (int i = 0; i < xml.getChildrenCount(); i++) {
                XMLEntity child = xml.getChild(i);
                if (child.contains("className") &&
                        child.getStringProperty("className").equals("com.iver.cit.gvsig.gui.layout.Layout") &&
                        child.contains("name") &&
                        child.getStringProperty("name").equals("layout")) {
                }
            }
        } catch (Exception e) {
            throw new OpenException(e, this.getClass().getName());
        }
    }

    /**
     * @see com.iver.cit.gvsig.project.documents.ProjectDocument#setXMLEntity(com.iver.utiles.XMLEntity)
     */
    public void setXMLEntity03(XMLEntity xml, Project p) {
    }
    /**
     * Creates the gui that we want in which we can introduce our document as model.
     */
    public IWindow createWindow() {
        DocumentGUI gui = new DocumentGUI();
        gui.setModel(this);

        return gui;
    }

    /**
     * Creates the gui that we want in which we can modify the properties of our document.
     */
    public IWindow getProperties() {
        DocumentProperties dp = new DocumentProperties(this);

        return dp;
    }
    /**
     * Code to apply when a document is removed.
     */
    public void afterRemove() {
    }
    /**
     * Code to apply when a document is added.
     */
    public void afterAdd() {
    }

	public void exportToXML(XMLEntity root, Project project) throws SaveException {
		// TODO Auto-generated method stub

	}

	public void importFromXML(XMLEntity root, XMLEntity typeRoot, int elementIndex, Project project, boolean removeDocumentsFromRoot) throws XMLException, OpenException {
		// TODO Auto-generated method stub

	}

	public boolean isModified() {
		return false;
	}

	public void setModified(boolean modified) {

	}

}
