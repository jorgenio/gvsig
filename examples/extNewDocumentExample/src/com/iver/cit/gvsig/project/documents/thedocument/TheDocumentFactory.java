package com.iver.cit.gvsig.project.documents.thedocument;

import com.iver.andami.PluginServices;

import com.iver.cit.gvsig.project.Project;
import com.iver.cit.gvsig.project.documents.ProjectDocument;
import com.iver.cit.gvsig.project.documents.ProjectDocumentFactory;
import com.iver.utiles.XMLEntity;

import java.text.DateFormat;

import java.util.Date;
import java.util.Hashtable;

import javax.swing.ImageIcon;


/**
 * DOCUMENT ME!
 *
 * @author Vicente Caballero Navarro
 */
public class TheDocumentFactory extends ProjectDocumentFactory {
    private static String registerName = "theDocument";
    private int numDocument = 0;

    /**
     * Returns the icon utilized to show in the button of the type of document.
     *
     * @return Image button
     */
    public ImageIcon getButtonIcon() {
        return new ImageIcon(TheDocument.class.getClassLoader().getResource("images/Document.png"));
    }

    /**
     * Returns the icon utilized to show in the button of the type of document
     * when is selected.
     *
     * @return Image selected button
     */
    public ImageIcon getSelectedButtonIcon() {
        return new ImageIcon(TheDocument.class.getClassLoader().getResource("images/Document_Sel.png"));
    }

    /**
     * Creates a new document.
     *
     * @param project Name of document.
     *
     * @return new ProjectDocument.
     */
    public ProjectDocument create(Project project) {
        TheDocument m = new TheDocument();
        m.setName(PluginServices.getText(this, "untitle") + " - " +
            numDocument);
        m.setCreationDate(DateFormat.getInstance().format(new Date()));
        numDocument++;
        m.setProject(project, 0);

        return m;
    }

    /**
     * Returns the name of register at extension point.
     *
     * @return Name of register.
     */
    public String getRegisterName() {
        return registerName;
    }

    /**
     * Returns the name of our document.
     *
     * @return Name of document
     */
    public String getNameType() {
        return PluginServices.getText(null, "Document");
    }

    /**
     * Register the factory at point extension.
     */
    public static void register() {
        register(registerName, new TheDocumentFactory());
    }

    /**
     * Returns the priority of this document.
     *
     * @return Priority of document.
     */
    public int getPriority() {
        return 4;
    }

	public boolean resolveImportXMLConflicts(XMLEntity root, Project project, Hashtable conflicts) {
		// TODO Auto-generated method stub
		return false;
	}
}
