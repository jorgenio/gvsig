package com.iver.cit.gvsig.project.documents.thedocument.gui;

import javax.swing.JPanel;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.SingletonWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.project.documents.thedocument.TheDocument;

public class DocumentGUI extends JPanel implements IWindow, SingletonWindow {
	private InfoPanel infoPanel=null;
	private TheDocument model;

	/**
	 * This method initializes
	 *
	 */
	public DocumentGUI() {
		super();
		initialize();
	}
	/**
	 * This method initializes this
	 *
	 */
	private void initialize() {
        this.setSize(new java.awt.Dimension(472,255));
        infoPanel=new InfoPanel();
        infoPanel.setText("Prueba de GUI");
        this.add(infoPanel);

	}
	public void setModel(TheDocument doc) {
		model=doc;
	}
	public WindowInfo getWindowInfo() {
		WindowInfo wi= new WindowInfo(WindowInfo.ICONIFIABLE | WindowInfo.RESIZABLE |
	                WindowInfo.MAXIMIZABLE);

			wi.setWidth(500);
			wi.setHeight(300);
			wi.setTitle(PluginServices.getText(this, "Documento de prueba") + " : " +
					model.getName());
   		return wi;
	}
	public Object getWindowModel() {
		return "document";
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
