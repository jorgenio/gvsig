package com.iver.cit.gvsig.project.documents.thedocument.gui;
import javax.swing.JPanel;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.project.documents.thedocument.TheDocument;

public class DocumentProperties extends JPanel implements IWindow {
	private TheDocument model;
	private InfoPanel infoPanel;

	public DocumentProperties(TheDocument doc) {
		model=doc;
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 */
	private void initialize() {
		infoPanel=new InfoPanel();
        infoPanel.setText("Propiedades");
        this.add(infoPanel);
	}
	public WindowInfo getWindowInfo() {
		WindowInfo wi= new WindowInfo(WindowInfo.ICONIFIABLE | WindowInfo.RESIZABLE |
	                WindowInfo.MAXIMIZABLE);

			wi.setWidth(500);
			wi.setHeight(300);
			wi.setTitle(PluginServices.getText(this, "Documento de prueba") + " : " +
					model.getName());
   		return wi;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"