package com.iver.cit.gvsig.project.documents.thedocument.gui;

import javax.swing.JPanel;
import javax.swing.JTextField;

public class InfoPanel extends JPanel {

	private JTextField jTextField = null;

	/**
	 * This is the default constructor
	 */
	public InfoPanel() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		this.setSize(489, 245);
		this.add(getJTextField(), null);
	}

	/**
	 * This method initializes jTextField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextField() {
		if (jTextField == null) {
			jTextField = new JTextField();
			jTextField.setPreferredSize(new java.awt.Dimension(400,200));
			jTextField.setFont(new java.awt.Font("Dialog", java.awt.Font.PLAIN, 72));
		}
		return jTextField;
	}
	public void setText(String s) {
		getJTextField().setText(s);
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
