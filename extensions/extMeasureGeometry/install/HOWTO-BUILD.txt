 C�MO GENERAR EL INSTALADOR:
 ===========================
- gvSIG debe estar correctamente construido en ../_fwAndami/bin (en el caso de una extensi�n aislada, basta con que est� construida la extensi�n)
- El proyecto 'install' debe estar presente en la ra�z del workspace (contiene el Izpack, entre otras cosas)
- En el fichero variables.sh hay algunas variables que nos pueden interesar, que permiten personalizar el nombre del plugin, la versi�n, el directorio destino, las extensiones incluidas, etc. La variable  APPNAME normalmente valdr� "gvSIG", pero puede tiener nombre si estamos empaquetando una extensi�n (por ejemplo,  "nomenclatorIGN").
- Definir los packs adecuados en el fichero XML de configuraci�n (install.xml), y revisar el fichero en general.
- En el fichero resources/userInputSpec.xml modificar los directorios candidatos, si se ha producido un cambio de la versi�n de gvSIG sobre la que queremos instalar
- Lanzar el script distribucion.sh, pas�ndole como par�metro el n�mero de build deseado
