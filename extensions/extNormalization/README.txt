   
   extNormalization

## DESCRPICION  #######################################################
Con la extesi�n de Normalizaci�n se a�ade a gvSIG la funcionalidad de separar
cadenas de texto en diferentes partes y almacenarlas en campos de una tabla.
Estas cadenas de texto pueden tener su origen en un fichero de texto plano o
en los diferentes registro de un campo de una tabla. 

## NOVEDADES  #########################################################
* Normalizar cadenas de texto de un fichero de texto plano y almacenarlas
	 en una nueva tabla.
* Normalizar cadenas de texto de un campo de una tabla y almacenarlas en
	 nuevos campos en esa tabla.
* Normalizar cadenas de texto de un campo de una tabla y almacenarlas en
	 una tabla nueva con la posibilidad de a�adir campos de la tabla origen
	 para poder realizar un posterior JOIN.

## CREDITOS ###########################################################
* Promueve: Conselleria de Infraestructuras y Transporte. Generalitat Valenciana.
* Desarrollan:  Prodevelop y IVER Tecnolog�as de la Informaci�n.

## INSTRUCCIONES DE INSTALACI�N ########################################

La extensi�n se instala sobre la versi�n 1.1.3 de gvSIG. 

Los pasos son: 
* Apagar la aplicaci�n gvSIG
* Ejecutar el fichero instalable de la extensi�n.
* Realizar la comprobaci�n de prerrequisitos.
* Aceptar la licencia GPL.
* Definir la ubicaci�n de la aplicaci�n gvSIG. Normalmente en windows 'c:/Archivos de Programa/gvSIG'.
* Terminar la instalaci�n.
* Arrancar de nuevo gvSIG  


## LICENCIA #############################################################
gvSIG. Geographic Information System of the Valencian Government

Copyright (C) 2007-2008 Infrastructures and Transports Department
of the Valencian Government (CIT)
 
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
MA  02110-1301, USA.


