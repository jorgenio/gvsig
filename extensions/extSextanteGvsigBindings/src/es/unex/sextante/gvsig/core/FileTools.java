package es.unex.sextante.gvsig.core;

import java.io.File;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.raster.layers.FLyrRasterSE;
import org.gvsig.raster.dataset.RasterDataset;

import com.hardcode.driverManager.Driver;
import com.hardcode.gdbms.engine.data.DataSource;
import com.hardcode.gdbms.engine.data.DataSourceFactory;
import com.iver.cit.gvsig.exceptions.layers.LoadLayerException;
import com.iver.cit.gvsig.fmap.drivers.VectorialFileDriver;
import com.iver.cit.gvsig.fmap.edition.EditableAdapter;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.fmap.layers.SelectableDataSource;
import com.iver.cit.gvsig.project.Project;
import com.iver.cit.gvsig.project.ProjectFactory;
import com.iver.cit.gvsig.project.documents.table.ProjectTable;

public class FileTools {

   public final static String[] RASTER_EXT_IN     = { "tif", "asc", "dat", "tiff", "bmp", "gif", "img", "jpg", "png", "vrt",
            "lan", "gis", "pix", "aux", "adf", "mpr", "mpl", "map", "hdr" };
   public final static String[] RASTER_DRIVERS_IN = { "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver",
            "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver",
            "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver",
            "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver",
            "gvSIG Image Driver"                 };

   public final static String[] VECTOR_EXT_IN     = { "shp", "gml", "dxf", "dgn", "dwg" };
   public final static String[] VECTOR_DRIVERS_IN = { "gvSIG shp driver", "gvSIG GML Memory Driver", "gvSIG DXF Memory Driver",
            "gvSIG DGN Memory Driver", "gvSIG DWG Memory Driver" };
   public final static String[] TABLE_EXT         = { "dbf" };
   public static final String[] LAYERS_EXT_IN     = { "tif", "asc", "dat", "tiff", "bmp", "gif", "img", "jpg", "png", "vrt",
            "lan", "gis", "pix", "aux", "adf", "mpr", "mpl", "map", "shp", "gml", "dxf", "dgn", "dwg" };
   public static final String[] LAYER_DRIVERS_IN  = { "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver",
            "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver",
            "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver",
            "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver", "gvSIG Image Driver",
            "gvSIG shp driver", "gvSIG GML Memory Driver", "gvSIG DXF Memory Driver", "gvSIG DGN Memory Driver",
            "gvSIG DWG Memory Driver"            };


   public static FLayer openLayer(final String sFilename,
                                  final String sName,
                                  final IProjection projection) {

      final String sExtension = sFilename.substring(sFilename.lastIndexOf('.') + 1, sFilename.length());

      final String[] extensionSupported = RasterDataset.getExtensionsSupported();
      FLyrRasterSE rlayer = null;
      for (int i = 0; i < extensionSupported.length; i++) {
         if (sExtension.equals(extensionSupported[i])) {
            try {
               rlayer = FLyrRasterSE.createLayer(sName, new File(sFilename), projection);
            }
            catch (final LoadLayerException e) {
               e.printStackTrace();
               return null;
            }
         }
         if (rlayer != null && rlayer.isAvailable()) {
            return rlayer;
         }
      }

      for (int i = 0; i < LAYERS_EXT_IN.length; i++) {
         if (sExtension.equals(LAYERS_EXT_IN[i])) {
            try {
               FLayer layer;
               final Driver driver = LayerFactory.getDM().getDriver(FileTools.LAYER_DRIVERS_IN[i]);

               layer = LayerFactory.createLayer(sName, (VectorialFileDriver) driver, new File(sFilename), projection);


               if (layer != null && layer.isAvailable()) {
                  return layer;
               }
               else {
                  return null;
               }
            }
            catch (final Exception e) {
               e.printStackTrace();
               return null;
            }
         }
      }
      return null;
   }


   public static ProjectTable openTable(final String sFilename,
                                        final String sName) {

      LayerFactory.getDataSourceFactory().addFileDataSource("gdbms dbf driver", sName, sFilename);
      DataSource dataSource;
      try {
         dataSource = LayerFactory.getDataSourceFactory().createRandomDataSource(sName, DataSourceFactory.AUTOMATIC_OPENING);
         final SelectableDataSource sds = new SelectableDataSource(dataSource);
         final EditableAdapter ea = new EditableAdapter();
         ea.setOriginalDataSource(sds);
         final ProjectTable pt = ProjectFactory.createTable(sName, ea);
         return pt;

      }
      catch (final Exception e) {
         return null;
      }

   }


   public static Object open(final String sFilename) {

      final FLayer layer = openLayer(sFilename, sFilename, Project.getDefaultProjection());
      if (layer != null) {
         return layer;
      }

      final ProjectTable table = openTable(sFilename, sFilename);

      return table;


   }

}
