package es.unex.sextante.gvsig.core;

import com.hardcode.gdbms.driver.exceptions.ReadDriverException;
import com.hardcode.gdbms.engine.values.Value;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.layers.FBitSet;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.ReadableVectorial;
import com.vividsolutions.jts.geom.Geometry;

import es.unex.sextante.core.Sextante;
import es.unex.sextante.dataObjects.FeatureImpl;
import es.unex.sextante.dataObjects.IFeature;
import es.unex.sextante.dataObjects.IFeatureIterator;
import es.unex.sextante.exceptions.IteratorException;

public class gvFeatureIterator
         implements
            IFeatureIterator {

   private int               m_iIndex, m_iSelectedIndex;
   private final ReadableVectorial m_RV;
   private FBitSet           m_BitSet;
   private final int               m_iCardinality;


   public gvFeatureIterator(final FLyrVect layer) {

      //m_iIndex = 0;
      //m_RV = layer.getSource();

      m_iIndex = 0;
      m_iSelectedIndex = 0;
      m_RV = layer.getSource();
      try {
         m_BitSet = m_RV.getRecordset().getSelection();
      }
      catch (final ReadDriverException e) {}

      m_iCardinality = m_BitSet.cardinality();

   }


   public boolean hasNext() {

      if (m_iCardinality != 0) {
         return m_iCardinality > m_iSelectedIndex;
      }
      else {
         try {
            return m_RV.getShapeCount() > m_iSelectedIndex;
         }
         catch (final ReadDriverException e) {
            return false;
         }
      }
   }


   public IFeature next() throws IteratorException {

      try {
         getNextIndex();
         final IGeometry shape = m_RV.getShape(m_iIndex);
         final Geometry geom = shape.toJTSGeometry();
         final Value[] gvSIGValues = m_RV.getRecordset().getRow(m_iIndex);
         final FeatureImpl feature = new FeatureImpl(geom, DataTools.getSextanteValues(gvSIGValues));
         m_iIndex++;
         m_iSelectedIndex++;
         return feature;
      }
      catch (final Exception e) {
         Sextante.addErrorToLog(e);
         m_iIndex++;
         m_iSelectedIndex++;
         throw new IteratorException();
      }

   }


   private void getNextIndex() throws IteratorException {

      if (m_iCardinality != 0 && m_iCardinality <= m_iSelectedIndex) {
         throw new IteratorException();
      }
      if (m_iCardinality != 0) {
         while (!m_BitSet.get(m_iIndex)) {
            m_iIndex++;
         }
      }

   }


   public void close() {}

}
