/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *
 */
package es.unex.sextante.gvsig.core;

import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.logging.Logger;

import org.cresques.cts.IProjection;
import org.gvsig.fmap.raster.layers.FLyrRasterSE;
import org.gvsig.raster.buffer.RasterBufferInvalidException;
import org.gvsig.raster.buffer.WriterBufferServer;
import org.gvsig.raster.dataset.GeoRasterWriter;
import org.gvsig.raster.dataset.IBuffer;
import org.gvsig.raster.dataset.NotSupportedExtensionException;
import org.gvsig.raster.dataset.Params;
import org.gvsig.raster.dataset.io.RasterDriverException;
import org.gvsig.raster.grid.Grid;
import org.gvsig.raster.grid.GridException;

import com.iver.andami.Utilities;

import es.unex.sextante.dataObjects.AbstractRasterLayer;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.rasterWrappers.GridExtent;

/**
 * {@link IRasterLayer} implementation to be used in gvSIG. Allows to read
 * information from a {@link FLyrRasterSE} or to create a new Grid to be
 * written.
 * 
 * @author gvSIG Team
 * @version $Id$
 */
public class gvRasterLayer extends AbstractRasterLayer {

	private IProjection projection = null;
	private FLyrRasterSE rasterLayer;
	private Grid grid = null;
	private int dataType = -1;
	/**
	 * Cached value optimization: the getNoDataValue() method seems to be called
	 * by sextante for each pixel.
	 */
	private double noDataValue;
	/**
	 * Cached value optimization: the getBandsCount() method seems to be called
	 * by sextante for each pixel.
	 */
	private int bandsCount;

	private String filename = null;
	private String name;

	private GridExtent sextanteExtent;

	private double DEFAULT_NO_DATA_VALUE = -99999.0D;

	public void create(Grid grid, int bandsCount, double noDataValue,
			IProjection projection, String name, String fileName) {

		this.grid = grid;
		this.bandsCount = bandsCount;
		this.noDataValue = noDataValue;
		this.projection = projection;
		this.name = name;
		this.filename = fileName;
		if (grid != null) {
			this.dataType = grid.getDataType();
		}
	}

	public void create(FLyrRasterSE rasterLayer) {

		Grid grid = createReadOnlyGrid(rasterLayer);
		create(grid, rasterLayer.getBandCount(), rasterLayer.getNoDataValue(),
				rasterLayer.getProjection(), rasterLayer.getName(), rasterLayer
						.getDataSource().getDataset(0)[0].getFName());
		this.rasterLayer = rasterLayer;
		this.m_BaseDataObject = rasterLayer;
	}

	public void create(String sName, String sFilename, GridExtent ge,
			int iDataType, int iNumBands, Object crs) {
		
		int[] bands = new int[iNumBands];
		for (int i = 0; i < bands.length; i++) {
			bands[i] = i;
		}
		org.gvsig.raster.grid.GridExtent gridExtent = new org.gvsig.raster.grid.GridExtent(
				ge.getXMin(), ge.getYMin(), ge.getXMax(), ge.getYMax(),
				ge.getCellSize());
		try {
			grid = new Grid(gridExtent, gridExtent, iDataType, bands);
		} catch (RasterBufferInvalidException e) {
			throw new RuntimeException(
					"Error creating a new empty writable Grid with: GridExtent = "
							+ gridExtent + ", data type = " + iDataType
							+ ", bands = " + iNumBands, e);
		}
		grid.setNoDataValue(DEFAULT_NO_DATA_VALUE);

		IProjection projection = crs instanceof IProjection ? (IProjection) crs
				: null;

		this.sextanteExtent = ge;
		create(grid, iNumBands, DEFAULT_NO_DATA_VALUE, projection, sName, sFilename);
	}

	public void create(String sName, String sFilename, GridExtent ge,
			int iDataType, Object crs) {

		create(sName, sFilename, ge, iDataType, 1, crs);
	}

	public int getDataType() {
		return grid.getDataType();
	}

	@Override
	public void assign(double dValue) {
		if (isWritable()) {
			super.assign(dValue);
		}
	}

	@Override
	public void assign(IRasterLayer layer) {
		if (isWritable()) {
			super.assign(layer);
		}
	}

	/**
	 * If the contained grid is a writable one.
	 * 
	 * @return if the contained grid is a writable one
	 */
	private final boolean isWritable() {
		return rasterLayer == null;
	}

	@Override
	public void setNoData(int x, int y) {
		if (isWritable()) {
			try {
				grid.setNoData(x, y);
			} catch (InterruptedException e) {
				throw new RuntimeException(
						"Interrupted while setting to NoData the cell x = " + x
								+ ", y = " + y, e);
			}
		}
	}

	public void setNoDataValue(double dNoDataValue) {
		grid.setNoDataValue(dNoDataValue);
		this.noDataValue = dNoDataValue;
	}

	public double getNoDataValue() {
		// Safer approach:
		// return isWritable() ? noDataValue : rasterLayer.getNoDataValue();
		// More efficient approach:
		return noDataValue;
	}

	public double getCellValueInLayerCoords(int x, int y, int band) {

		IBuffer buffer = grid.getRasterBuf();
		try {
			switch (dataType) {
			case IBuffer.TYPE_DOUBLE:
				return buffer.getElemDouble(y, x, band);
			case IBuffer.TYPE_FLOAT:
				return buffer.getElemFloat(y, x, band);
			case IBuffer.TYPE_INT:
				return buffer.getElemInt(y, x, band);
			case IBuffer.TYPE_SHORT:
			case IBuffer.TYPE_USHORT:
				return buffer.getElemShort(y, x, band);
			case IBuffer.TYPE_BYTE:
				return (buffer.getElemByte(y, x, band) & 0xff);
			default:
				return grid.getNoDataValue();
			}
		} catch (InterruptedException e) {
			throw new RuntimeException(
					"Interrupted while getting value of cell x = " + x
							+ ", y = " + y + ", band = " + band, e);
		}
	}

	public void setCellValue(int x, int y, int band, double dValue) {
		if (isWritable()) {
			IBuffer buffer = grid.getRasterBuf();
			try {
				switch (grid.getDataType()) {
				case IBuffer.TYPE_DOUBLE:
					buffer.setElem(y, x, band, dValue);
					break;
				case IBuffer.TYPE_FLOAT:
					buffer.setElem(y, x, band, (float) dValue);
					break;
				case IBuffer.TYPE_INT:
					buffer.setElem(y, x, band, (int) dValue);
					break;
				case IBuffer.TYPE_SHORT:
				case IBuffer.TYPE_USHORT:
					buffer.setElem(y, x, band, (short) dValue);
					break;
				case IBuffer.TYPE_BYTE:
					buffer.setElem(y, x, band, (byte) dValue);
					break;
				}
			} catch (InterruptedException e) {
				throw new RuntimeException(
						"Interrupted while setting value of cell x = " + x
								+ ", y = " + y + ", band = " + band
								+ ", value = " + dValue, e);
			}
		}
	}

	public int getBandsCount() {
		return isWritable() ? bandsCount : rasterLayer.getBandCount();
	}

	public void fitToGridExtent(GridExtent ge) {
		if (isWritable()) {
			int iNX, iNY;
			int x, y;
			int iBand;

			if (ge != null) {
				if (!ge.equals(getWindowGridExtent())) {
					if (isWritable()) {

						FLyrRasterSE gvSIGLayer = getRasterLayer(
								Long.toString(System.currentTimeMillis())
										.toString(), projection);
						gvRasterLayer orgLayer = new gvRasterLayer();
						orgLayer.create(gvSIGLayer);
						orgLayer.setFullExtent();

						iNX = ge.getNX();
						iNY = ge.getNY();

						m_BaseDataObject = null;

						create(this.getName(), getFilename(), ge,
								this.getDataType(), this.getBandsCount());
						for (iBand = 0; iBand < this.getBandsCount(); iBand++) {
							for (y = 0; y < iNY; y++) {
								for (x = 0; x < iNX; x++) {
									setCellValue(x, y, iBand,
											orgLayer.getCellValueAsDouble(x, y,
													iBand));
								}
							}
						}
					}
				}
			}
		}
	}

	public FLyrRasterSE getRasterLayer(String sDescription,
			IProjection projection) {

		FLyrRasterSE layer;
		String sFilename = getFilename(sDescription, "tif");
		try {
			exportToGeoTiff(sFilename, projection);
			layer = FLyrRasterSE.createLayer(sDescription, new File(sFilename),
					projection);
			layer.setNoDataValue(this.getNoDataValue());
			return layer;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static String getFilename(String sRoot, String sExtension) {

		String sFilename;
		int i = 1;

		sRoot = sRoot.toLowerCase();
		sRoot = sRoot.replaceAll(" ", "_");
		sRoot = sRoot.replaceAll("\\)", "");
		sRoot = sRoot.replaceAll("\\(", "_");
		sRoot = sRoot.replaceAll("\\[", "_");
		sRoot = sRoot.replaceAll("\\]", "");
		sRoot = sRoot.replaceAll("<", "_");
		sRoot = sRoot.replaceAll(">", "_");
		sRoot = sRoot.replaceAll("__", "_");

		while (true) {
			sFilename = Utilities.createTempDirectory() + File.separator
					+ sRoot + Integer.toString(i) + "." + sExtension;
			File file = new File(sFilename);
			if (file.exists()) {
				i++;
			} else {
				return sFilename;
			}
		}

	}

	public void postProcess() {
		if (isWritable()) {
			export(getFilename(), projection);
			FLyrRasterSE rasterLayer = (FLyrRasterSE) FileTools.openLayer(
					getFilename(), getName(), null);
			create(rasterLayer);
			rasterLayer.setNoDataValue(getNoDataValue());
		}
	}

	private Grid createReadOnlyGrid(FLyrRasterSE rasterLayer) {
		try {
			return rasterLayer.getReadOnlyFullGrid(false);
		} catch (GridException e) {
			throw new RuntimeException(
					"Error getting a read only grid from the FlyrRasterSE: "
							+ rasterLayer, e);
		} catch (InterruptedException e) {
			throw new RuntimeException(
					"Error getting a read only grid from the FlyrRasterSE: "
							+ rasterLayer, e);
		}
	}

	public void open() {
		if (!isWritable() && grid == null) {
			this.grid = createReadOnlyGrid(this.rasterLayer);
			this.dataType = grid.getDataType();
		}
	}

	public void close() {
		if (grid != null) {
			grid.getRasterBuf().free();
		}
		grid = null;
		dataType = -1;
		// bandsCount = -1;
		// projection = null;
		// name = null;
		// rasterLayer = null;
		// m_BaseDataObject = null;
		// noDataValue = DEFAULT_NO_DATA_VALUE;
		// filename = null;
	}

	public Rectangle2D getFullExtent() {
		if (isWritable()) {
			return getLayerGridExtent().getAsRectangle2D();
		}
		else {
			return rasterLayer.getFullExtent();
		}
	}

	public GridExtent getLayerGridExtent() {
		if (isWritable()) {
			return sextanteExtent;
		} else {
			final GridExtent extent = new GridExtent();
			extent.setXRange(rasterLayer.getMinX(), rasterLayer.getMaxX());
			extent.setYRange(rasterLayer.getMinY(), rasterLayer.getMaxY());
			extent.setCellSize(rasterLayer.getCellSize());
			return extent;
		}
	}

	public double getLayerCellSize() {
		return grid.getCellSize();
	}

	public String getFilename() {
		return isWritable() ? filename : rasterLayer.getFileName()[0];
	}

	public Object getCRS() {
		return isWritable() ? projection : rasterLayer.getProjection();
	}

	public String getName() {
		return isWritable() ? name : rasterLayer.getName();
	}

	public void setName(String name) {
		this.name = name;
		if (rasterLayer != null) {
			rasterLayer.setName(name);
		}
	}

	private void export(String sFilename, IProjection projection) {

		if (sFilename.endsWith("asc")) {
			exportToArcInfoASCIIFile(sFilename);
		} else {
			exportToGeoTiff(sFilename, projection);
		}

	}

	private void exportToArcInfoASCIIFile(String sFilename) {

		try {
			FileWriter f = new FileWriter(sFilename);
			BufferedWriter fout = new BufferedWriter(f);
			DecimalFormat df = new DecimalFormat("##.###");
			df.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
			df.setDecimalSeparatorAlwaysShown(true);

			GridExtent m_GridExtent = getLayerGridExtent();

			fout.write("ncols " + Integer.toString(m_GridExtent.getNX()));
			fout.newLine();
			fout.write("nrows " + Integer.toString(m_GridExtent.getNY()));
			fout.newLine();
			fout.write("xllcorner " + Double.toString(m_GridExtent.getXMin()));
			fout.newLine();
			fout.write("yllcorner " + Double.toString(m_GridExtent.getYMin()));
			fout.newLine();
			fout.write("cellsize "
					+ Double.toString(m_GridExtent.getCellSize()));
			fout.newLine();
			fout.write("nodata_value " + Double.toString(getNoDataValue()));
			fout.newLine();

			for (int i = 0; i < m_GridExtent.getNY(); i++) {
				for (int j = 0; j < m_GridExtent.getNX(); j++) {
					fout.write(df.format(getCellValueAsDouble(j, i)) + " ");
				}
				fout.newLine();
			}
			fout.close();
			f.close();
		} catch (IOException e) {
			throw new RuntimeException(
					"Error exporting the generated raster to the file: "
							+ sFilename, e);
		}
	}

	private void exportToGeoTiff(String sFilename, IProjection projection) {

		IBuffer buf = grid.getRasterBuf();
		WriterBufferServer writerBufferServer = new WriterBufferServer();
		writerBufferServer.setBuffer(buf, -1);

		GridExtent m_GridExtent = getLayerGridExtent();
		/*
		 * Extent ext = new Extent(m_GridExtent.getAsRectangle2D());
		 * ViewPortData vpData = new ViewPortData(projection, ext, new
		 * Dimension(m_GridExtent.getNX(),m_GridExtent.getNY()));
		 */
		try {
			Params params = GeoRasterWriter.getWriter(sFilename).getParams();
			AffineTransform affineTransform = new AffineTransform(
					m_GridExtent.getCellSize(), 0, 0,
					-m_GridExtent.getCellSize(), m_GridExtent.getXMin(),
					m_GridExtent.getYMax());
			GeoRasterWriter writer = GeoRasterWriter
					.getWriter(writerBufferServer, sFilename,
							buf.getBandCount(), affineTransform,
							m_GridExtent.getNX(), m_GridExtent.getNY(),
							buf.getDataType(), params, projection);
			writer.dataWrite();
			writer.writeClose();
		} catch (NotSupportedExtensionException e) {
			throw new RuntimeException(
					"Error exporting the generated raster to the file: "
							+ sFilename, e);
		} catch (RasterDriverException e) {
			throw new RuntimeException(
					"Error exporting the generated raster to the file: "
							+ sFilename, e);
		} catch (IOException e) {
			throw new RuntimeException(
					"Error exporting the generated raster to the file: "
							+ sFilename, e);
		} catch (InterruptedException e) {
			throw new RuntimeException(
					"Error exporting the generated raster to the file: "
							+ sFilename, e);
		}

	}

}
