package es.unex.sextante.gvsig.core;

import java.io.File;
import java.io.IOException;
import java.sql.Types;

import com.hardcode.gdbms.driver.exceptions.ReadDriverException;
import com.hardcode.gdbms.engine.data.DataSource;
import com.hardcode.gdbms.engine.data.driver.FileDriver;
import com.iver.cit.gvsig.fmap.core.IFeature;
import com.iver.cit.gvsig.fmap.drivers.ITableDefinition;
import com.iver.cit.gvsig.fmap.edition.DefaultRowEdited;
import com.iver.cit.gvsig.fmap.edition.EditableAdapter;
import com.iver.cit.gvsig.fmap.edition.IRowEdited;
import com.iver.cit.gvsig.fmap.edition.IWriteable;
import com.iver.cit.gvsig.fmap.edition.IWriter;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.fmap.layers.SelectableDataSource;
import com.iver.cit.gvsig.project.ProjectFactory;
import com.iver.cit.gvsig.project.documents.table.ProjectTable;

import es.unex.sextante.dataObjects.AbstractTable;
import es.unex.sextante.dataObjects.IRecordsetIterator;

public class gvTable
         extends
            AbstractTable {

   private Object m_BaseDataObject;
   private String m_sName;
   private String m_sFilename;


   public String getName() {

      if (m_BaseDataObject instanceof ProjectTable) {
         final ProjectTable table = (ProjectTable) m_BaseDataObject;
         return table.getName();
      }
      else {
         return m_sName;
      }

   }


   public void create(final Object obj) {

      if (obj instanceof ProjectTable) {
         m_BaseDataObject = obj;
      }

   }


   public void create(final String sName,
                      final String sFilename,
                      final Class[] types,
                      final String[] sFields) {

      m_sFilename = sFilename;
      final TableMemoryDriver table = new TableMemoryDriver(sFields, DataTools.getgvSIGTypes(types));
      m_sName = sName;
      m_BaseDataObject = table;

   }


   public void addRecord(final Object[] record) {

      if (m_BaseDataObject instanceof TableMemoryDriver) {
         final TableMemoryDriver table = (TableMemoryDriver) m_BaseDataObject;
         table.addRow(DataTools.getGVSIGValues(record));
      }

   }


   public IRecordsetIterator iterator() {

      return new gvRecordsetIterator(m_BaseDataObject);

   }


   public String getFieldName(final int i) {

      if (m_BaseDataObject instanceof ProjectTable) {
         final ProjectTable table = (ProjectTable) m_BaseDataObject;
         try {
            return table.getModelo().getRecordset().getFieldName(i);
         }
         catch (final Exception e) {
            // TODO Auto-generated catch block
            return "";
         }
      }
      else {
         final TableMemoryDriver table = (TableMemoryDriver) m_BaseDataObject;
         return table.getFieldName(i);
      }

   }


   public Class getFieldType(final int i) {

      if (m_BaseDataObject instanceof ProjectTable) {
         final ProjectTable table = (ProjectTable) m_BaseDataObject;
         try {
            final int iType = table.getModelo().getRecordset().getFieldType(i);
            return DataTools.getTypeClass(iType);
         }
         catch (final Exception e) {
            return String.class;
         }
      }
      else {
         final TableMemoryDriver table = (TableMemoryDriver) m_BaseDataObject;
         int iType;
         iType = table.getFieldType(i);
         return DataTools.getTypeClass(iType);
      }

   }


   public int getFieldCount() {

      if (m_BaseDataObject instanceof ProjectTable) {
         final ProjectTable table = (ProjectTable) m_BaseDataObject;
         try {
            return table.getModelo().getRecordset().getFieldCount();
         }
         catch (final Exception e) {
            return 0;
         }
      }
      else {
         final TableMemoryDriver table = (TableMemoryDriver) m_BaseDataObject;
         return table.getFieldCount();
      }

   }


   public long getRecordCount() {

      if (m_BaseDataObject instanceof ProjectTable) {
         final ProjectTable table = (ProjectTable) m_BaseDataObject;
         try {
            return table.getModelo().getRecordset().getRowCount();
         }
         catch (final Exception e) {
            return 0;
         }
      }
      else {
         final TableMemoryDriver table = (TableMemoryDriver) m_BaseDataObject;
         return table.getRowCount();
      }

   }


   public void postProcess() {

      SelectableDataSource source;
      ITableDefinition orgDef;
      FileDriver driver;
      File file;
      try {
         LayerFactory.getDataSourceFactory().addDataSource((TableMemoryDriver) m_BaseDataObject, m_sName);
         final DataSource dataSource = LayerFactory.getDataSourceFactory().createRandomDataSource(m_sName);
         dataSource.start();
         final SelectableDataSource sds = new SelectableDataSource(dataSource);
         final EditableAdapter auxea = new EditableAdapter();
         auxea.setOriginalDataSource(sds);
         final ProjectTable table = ProjectFactory.createTable(m_sName, auxea);
         file = new File(m_sFilename);
         driver = (FileDriver) LayerFactory.getDM().getDriver("gdbms dbf driver");
         source = table.getModelo().getRecordset();
         source.start();
         orgDef = table.getModelo().getTableDefinition();
      }
      catch (final Exception e) {
         return;
      }

      try {
         if (!file.exists()) {
            driver.createSource(file.getAbsolutePath(), new String[] { "0" }, new int[] { Types.INTEGER });
            file.createNewFile();
         }
         driver.open(file);
      }
      catch (final IOException e) {
         e.printStackTrace();
         return;
      }
      catch (final ReadDriverException ex) {
         ex.printStackTrace();
         return;
      }

      final IWriter writer = ((IWriteable) driver).getWriter();
      try {
         writer.initialize(orgDef);
         writer.preProcess();
         final SourceIterator sourceIter = new SourceIterator(source);
         IFeature feature;
         int i = 0;
         while (sourceIter.hasNext()) {
            feature = sourceIter.nextFeature();

            final DefaultRowEdited edRow = new DefaultRowEdited(feature, IRowEdited.STATUS_ADDED, i);
            writer.process(edRow);
            i++;
         }
         writer.postProcess();
      }
      catch (final Exception e) {
         return;
      }

      final ProjectTable table = FileTools.openTable(m_sFilename, m_sName);
      create(table);

   }


   public void open() {

      if (m_BaseDataObject instanceof ProjectTable) {
         final ProjectTable table = (ProjectTable) m_BaseDataObject;
         try {
            table.getModelo().getRecordset().start();
         }
         catch (final Exception e) {
            e.printStackTrace();
         }
      }

   }


   public void close() {

      if (m_BaseDataObject instanceof ProjectTable) {
         final ProjectTable table = (ProjectTable) m_BaseDataObject;
         try {
            table.getModelo().getRecordset().stop();
         }
         catch (final Exception e) {
            e.printStackTrace();
         }
      }

   }


   public String getFilename() {

      return m_sFilename;

   }


   public void setName(final String name) {

      if (m_BaseDataObject instanceof ProjectTable) {
         final ProjectTable table = (ProjectTable) m_BaseDataObject;
         table.setName(name);
      }
      else {
         m_sName = name;
      }

   }


   @Override
   public Object getBaseDataObject() {

      return m_BaseDataObject;

   }


}
