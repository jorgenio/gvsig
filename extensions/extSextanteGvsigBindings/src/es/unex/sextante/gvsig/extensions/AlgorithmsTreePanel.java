package es.unex.sextante.gvsig.extensions;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.Collator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

import com.iver.andami.PluginServices;

import es.unex.sextante.core.GeoAlgorithm;
import es.unex.sextante.core.Sextante;
import es.unex.sextante.gui.core.SextanteGUI;
import es.unex.sextante.gui.modeler.ModelAlgorithmIO;
import es.unex.sextante.modeler.ModelAlgorithm;

public class AlgorithmsTreePanel
         extends
            JPanel {

   private JTree                        jTree;
   private JScrollPane                  jScrollPane;
   private final ButtonDefinitionDialog m_Dialog;


   public AlgorithmsTreePanel(final ButtonDefinitionDialog dialog) {

      super();

      m_Dialog = dialog;

      initGUI();

   }


   private void initGUI() {

      final BorderLayout thisLayout = new BorderLayout();
      this.setLayout(thisLayout);
      jTree = new JTree();
      jTree.setCellRenderer(new MyCellRenderer());
      final MouseListener ml = new MouseAdapter() {

         @Override
         public void mousePressed(MouseEvent e) {
            TreePath path = jTree.getPathForLocation(e.getX(), e.getY());
            if (path != null) {
               try {
                  DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
                  GeoAlgorithm alg = ((GeoAlgorithm) node.getUserObject());
                  m_Dialog.setCommand(alg);
               }
               catch (Exception ex) {
                  //
               }
            }
         }
      };
      jTree.addMouseListener(ml);

      fillTree();
      jScrollPane = new JScrollPane(jTree, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
               ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

      this.add(jScrollPane, BorderLayout.CENTER);

   }


   private void fillTree() {

      DefaultMutableTreeNode node;
      final DefaultMutableTreeNode mainNode = new DefaultMutableTreeNode(Sextante.getText("Procedures"));
      final DefaultMutableTreeNode toolsNode = new DefaultMutableTreeNode(PluginServices.getText(this, "Algorithms"));
      DefaultMutableTreeNode child;
      final HashMap map = new HashMap();
      String sGroup;
      String sKey;
      final HashMap algs = Sextante.getAlgorithms();
      Set set = algs.keySet();
      Iterator iter = set.iterator();
      while (iter.hasNext()) {
         sKey = (String) iter.next();
         final GeoAlgorithm alg = (GeoAlgorithm) algs.get(sKey);
         child = new DefaultMutableTreeNode(alg);
         sGroup = (alg.getGroup());
         if (map.containsKey(sGroup)) {
            node = (DefaultMutableTreeNode) map.get(sGroup);
         }
         else {
            node = new DefaultMutableTreeNode(sGroup);
            map.put(sGroup, node);
         }
         addNodeInSortedOrder(node, child);
      }

      set = map.keySet();
      iter = set.iterator();

      while (iter.hasNext()) {
         sKey = (String) iter.next();
         node = (DefaultMutableTreeNode) map.get(sKey);
         addNodeInSortedOrder(toolsNode, node);
      }

      mainNode.add(toolsNode);

      //final DefaultMutableTreeNode modelsNode = new DefaultMutableTreeNode(PluginServices.getText(this, "Models"));
      final GeoAlgorithm[] models = ModelAlgorithmIO.loadModelsAsAlgorithms(SextanteGUI.getModelsFolder());
      if ((models != null) && (models.length != 0)) {
         for (final GeoAlgorithm element : models) {
            if (element.isSuitableForModelling()) {
               sGroup = element.getGroup();
               node = (DefaultMutableTreeNode) map.get(sGroup);
               if (node == null) {
                  node = new DefaultMutableTreeNode(sGroup);
                  map.put(sGroup, node);
                  addNodeInSortedOrder(toolsNode, node);
               }
               child = new DefaultMutableTreeNode(element);
               addNodeInSortedOrder(node, child);
            }
         }
      }

      jTree.setModel(new DefaultTreeModel(mainNode));

   }


   private void addNodeInSortedOrder(final DefaultMutableTreeNode parent,
                                     final DefaultMutableTreeNode child) {

      final int n = parent.getChildCount();
      if (n == 0) {
         parent.add(child);
         return;
      }
      final Collator collator = Collator.getInstance();
      collator.setStrength(Collator.PRIMARY);
      DefaultMutableTreeNode node = null;
      for (int i = 0; i < n; i++) {
         node = (DefaultMutableTreeNode) parent.getChildAt(i);
         try {
            if (collator.compare(node.toString(), child.toString()) > 0) {
               parent.insert(child, i);
               return;
            }
         }
         catch (final Exception e) {}
      }
      parent.add(child);

   }

   static class MyCellRenderer
            extends
               JLabel
            implements
               TreeCellRenderer {

      ImageIcon m_AlgorithmIcon = new ImageIcon(SextanteGUI.class.getClassLoader().getResource("images/module2.png"));
      ImageIcon m_ModelIcon     = new ImageIcon(getClass().getClassLoader().getResource("images/model.png"));


      protected Icon getCustomIcon(final Object value) {

         final Object obj = ((DefaultMutableTreeNode) value).getUserObject();
         if (obj instanceof GeoAlgorithm) {
            if (obj instanceof ModelAlgorithm) {
               return m_ModelIcon;
            }
            else {
               return m_AlgorithmIcon;
            }
         }
         else {
            return null;
         }

      }


      public Component getTreeCellRendererComponent(final JTree tree,
                                                    final Object value,
                                                    final boolean sel,
                                                    final boolean expanded,
                                                    boolean leaf,
                                                    final int row,
                                                    final boolean hasFocus) {

         String sName;

         setFont(tree.getFont());
         this.setIcon(getCustomIcon(value));

         sName = tree.convertValueToText(value, sel, expanded, leaf, row, hasFocus);


         setEnabled(tree.isEnabled());
         setText(sName);
         if (!leaf) {
            setFont(new java.awt.Font("Tahoma", 1, 11));
            setForeground(Color.black);
         }
         else {
            if (sel) {
               setForeground(Color.blue);
            }
            else {
               setForeground(Color.black);
            }
         }

         return this;

      }

   }


}
