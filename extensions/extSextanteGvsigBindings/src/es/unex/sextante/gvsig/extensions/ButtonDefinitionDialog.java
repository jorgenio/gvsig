package es.unex.sextante.gvsig.extensions;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstants;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.iver.andami.PluginServices;

import es.unex.sextante.core.GeoAlgorithm;
import es.unex.sextante.gui.algorithm.FileSelectionPanel;
import es.unex.sextante.modeler.ModelAlgorithm;

public class ButtonDefinitionDialog
         extends
            JDialog {

   private JLabel              jLabelName;
   private JTextField          jTextFieldName;
   private JTextField          jTextFieldCmd;
   private JLabel              jLabelCmd;
   private AlgorithmsTreePanel jPanelAlgorithms;
   private JButton             jButtonCancel;
   private JButton             jButtonOK;
   private FileSelectionPanel  jTextFieldIcon;
   private JLabel              jLabelIcon;
   private Button              m_Button;


   public ButtonDefinitionDialog(final ToolbarConfigDialog toolbarConfigDialog) {

      super(toolbarConfigDialog, "SextanteToolbar", true);

      initGUI();

   }


   private void initGUI() {
      {
         final TableLayout thisLayout = new TableLayout(new double[][] {
                  { 7.0, TableLayoutConstants.FILL, TableLayoutConstants.FILL, 100.0, 100.0, 7.0 },
                  { TableLayoutConstants.FILL, TableLayoutConstants.MINIMUM, TableLayoutConstants.MINIMUM, TableLayoutConstants.MINIMUM, 7.0, 30.0, 7.0 } });
         thisLayout.setHGap(5);
         thisLayout.setVGap(5);
         getContentPane().setLayout(thisLayout);
         this.setBounds(0, 0, 365, 496);
         {
            jLabelName = new JLabel();
            getContentPane().add(jLabelName, "1, 2");
            jLabelName.setText(PluginServices.getText(this, "Name"));
         }
         {
            jLabelIcon = new JLabel();
            getContentPane().add(jLabelIcon, "1, 3");
            jLabelIcon.setText(PluginServices.getText(this, "Icon"));
         }
         {
            jTextFieldName = new JTextField();
            getContentPane().add(jTextFieldName, "2, 2, 4, 2");
         }
         {
            jTextFieldIcon = new FileSelectionPanel(false, true, new String[] { "jpg", "gif", "png", "bmp" },
                     "*.jpg, *.bmp, *.gif, *.png");
            getContentPane().add(jTextFieldIcon, "2, 3, 4, 3");
         }
         {
            jButtonOK = new JButton();
            getContentPane().add(jButtonOK, "3, 5");
            jButtonOK.setText(PluginServices.getText(this, "OK"));
            jButtonOK.addActionListener(new ActionListener() {
               public void actionPerformed(final ActionEvent evt) {
                  jButtonOKActionPerformed(evt);
               }
            });
         }
         {
            jButtonCancel = new JButton();
            getContentPane().add(jButtonCancel, "4, 5");
            jButtonCancel.setText(PluginServices.getText(this, "Cancel"));
            jButtonCancel.addActionListener(new ActionListener() {
               public void actionPerformed(final ActionEvent evt) {
                  jButtonCancelActionPerformed(evt);
               }
            });
         }
         {
            jPanelAlgorithms = new AlgorithmsTreePanel(this);
            getContentPane().add(jPanelAlgorithms, "1, 0, 4, 0");
            {
               jLabelCmd = new JLabel();
               getContentPane().add(jLabelCmd, "1, 1");
               jLabelCmd.setText(PluginServices.getText(this, "Command"));
            }
            {
               jTextFieldCmd = new JTextField();
               getContentPane().add(jTextFieldCmd, "2, 1, 4, 1");
               jTextFieldCmd.setEditable(false);
               jTextFieldCmd.setEnabled(false);
            }
         }
      }

   }


   public void setCommand(final GeoAlgorithm alg) {

      String sCmd = alg.getClass().toString();
      if (alg instanceof ModelAlgorithm) {
         sCmd = sCmd + "," + ((ModelAlgorithm) alg).getFilename();
      }
      else {
         sCmd = sCmd + "," + " ";
      }

      jTextFieldCmd.setText(sCmd);

   }


   public Button getButton() {

      return m_Button;

   }


   private void jButtonOKActionPerformed(final ActionEvent evt) {

      if (createButton()) {
         dispose();
         setVisible(false);
      }
      else {
         JOptionPane.showMessageDialog(null, PluginServices.getText(this, "Wrong_or_missing_parameters_definition"),
                  PluginServices.getText(this, "Warning"), JOptionPane.WARNING_MESSAGE);
      }

   }


   private boolean createButton() {

      m_Button = new Button();
      m_Button.sIconFilename = jTextFieldIcon.getFilepath() + " ";
      m_Button.sCommand = jTextFieldCmd.getText();
      m_Button.sName = jTextFieldName.getText() + " ";

      return !(m_Button.sCommand.trim().equals(""));

   }


   private void jButtonCancelActionPerformed(final ActionEvent evt) {

      m_Button = null;
      dispose();
      setVisible(false);

   }

}
