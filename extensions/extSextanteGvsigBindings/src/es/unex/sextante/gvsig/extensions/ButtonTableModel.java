package es.unex.sextante.gvsig.extensions;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.iver.andami.PluginServices;

public class ButtonTableModel
         extends
            AbstractTableModel {

   private final String[]    NAMES;
   private final ArrayList<Button> m_Buttons;


   public ButtonTableModel(final ArrayList<Button> buttons) {

      m_Buttons = buttons;
      NAMES = new String[3];
      NAMES[0] = PluginServices.getText(this, "Icon");
      NAMES[1] = PluginServices.getText(this, "Name");
      NAMES[2] = PluginServices.getText(this, "Command");

   }


   @Override
   public Class<?> getColumnClass(final int columnIndex) {

      return String.class;

   }


   public int getColumnCount() {

      return 3;
   }


   @Override
   public String getColumnName(final int columnIndex) {

      return NAMES[columnIndex];

   }


   public int getRowCount() {

      return m_Buttons.size();

   }


   public void removeRow(final int iRow) {

      m_Buttons.remove(iRow);

      this.fireTableRowsDeleted(iRow, iRow);

   }


   public Object getValueAt(final int rowIndex,
                            final int columnIndex) {

      if (columnIndex == 0) {
         return m_Buttons.get(rowIndex).sIconFilename;
      }
      else if (columnIndex == 1) {
         return m_Buttons.get(rowIndex).sName;
      }
      else if (columnIndex == 2) {
         return m_Buttons.get(rowIndex).sCommand;
      }

      return null;

   }


   @Override
   public boolean isCellEditable(final int rowIndex,
                                 final int columnIndex) {

      return false;

   }

}
