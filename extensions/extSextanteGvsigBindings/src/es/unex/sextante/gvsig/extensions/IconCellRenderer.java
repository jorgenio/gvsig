package es.unex.sextante.gvsig.extensions;

import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

public class IconCellRenderer
         extends
            DefaultTableCellRenderer {

   @Override
   protected void setValue(final Object value) {
      if (value instanceof String) {
         if (value != null) {
            ImageIcon icon;
            final File file = new File((String) value);
            if (file.isAbsolute()) {
               icon = new ImageIcon((String) value);
            }
            else {
               final String sPath = System.getProperty("user.dir") + File.separator + "gvSIG" + File.separator + "extensiones"
                              + File.separator + "es.unex.sextante" + File.separator + "images";
               icon = new ImageIcon(sPath + File.separator + file.getName());
            }
            setText("");
            setIcon(icon);
            setHorizontalTextPosition(SwingConstants.RIGHT);
            setVerticalTextPosition(SwingConstants.CENTER);
            setHorizontalAlignment(SwingConstants.LEFT);
            setVerticalAlignment(SwingConstants.CENTER);
         }
         else {
            setText("");
            setIcon(null);
         }
      }
      else {
         super.setValue(value);
      }
   }

}
