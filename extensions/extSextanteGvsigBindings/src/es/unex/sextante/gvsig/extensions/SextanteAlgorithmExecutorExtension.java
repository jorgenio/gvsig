package es.unex.sextante.gvsig.extensions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JOptionPane;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;

import es.unex.sextante.core.GeoAlgorithm;
import es.unex.sextante.core.ObjectAndDescription;
import es.unex.sextante.core.Sextante;
import es.unex.sextante.gui.algorithm.AlgorithmDialog;
import es.unex.sextante.gui.algorithm.GeoAlgorithmParametersPanel;
import es.unex.sextante.gui.core.GeoAlgorithmExecutors;
import es.unex.sextante.gui.core.IGUIFactory;
import es.unex.sextante.gui.core.SextanteGUI;
import es.unex.sextante.gui.history.History;
import es.unex.sextante.gui.modeler.ModelAlgorithmIO;
import es.unex.sextante.modeler.ModelAlgorithm;

public class SextanteAlgorithmExecutorExtension
         extends
            Extension {

   public void execute(final String actionCommand) {

      final int iIndex = Integer.parseInt(actionCommand);
      final String[] args = SextanteToolbarManager.getButton(iIndex).sCommand.split(",");
      final GeoAlgorithm alg = getAlgorithm(args[0], args[1]);

      if (alg != null) {
         final int iRet = showAlgorithmDialog(alg);
         if (iRet == IGUIFactory.OK) {
            final String[] cmd = alg.getAlgorithmAsCommandLineSentences();
            if (cmd != null) {
               History.addToHistory(cmd);
            }
            GeoAlgorithmExecutors.execute(alg, null);
         }
         SextanteGUI.getInputFactory().clearDataObjects();
      }

   }


   private int showAlgorithmDialog(final GeoAlgorithm alg) {

      GeoAlgorithmParametersPanel paramPanel = null;
      Class<? extends GeoAlgorithmParametersPanel> paramPanelClass = SextanteGUI.getParametersPanel(alg.getClass());

      if (paramPanelClass == null) {
         paramPanelClass = SextanteGUI.getGUIFactory().getDefaultParametersPanel();
      }
      try {
         paramPanel = paramPanelClass.newInstance();
      }
      catch (final Exception e) {
         try {
            paramPanel = (GeoAlgorithmParametersPanel) SextanteGUI.getGUIFactory().getDefaultParametersPanel().newInstance();
         }
         catch (final Exception e1) {}
      }

      SextanteGUI.getInputFactory().createDataObjects();
      final Object[] objs = SextanteGUI.getInputFactory().getDataObjects();
      if (alg.meetsDataRequirements(objs)) {
         AlgorithmDialog dialog;
         dialog = new AlgorithmDialog(alg, paramPanel, null);
         dialog.pack();
         dialog.setVisible(true);
         return dialog.getDialogReturn();
      }
      else {
         JOptionPane.showMessageDialog(null, PluginServices.getText(this, "Not_enough_data_to_run_this_command"),
                  PluginServices.getText(this, "Warning"), JOptionPane.WARNING_MESSAGE);
         return IGUIFactory.CANCEL;
      }
   }


   private static GeoAlgorithm getAlgorithm(final String sAlgorithm,
                                            final String sModelFile) {

      GeoAlgorithm alg;

      if (sAlgorithm.equals(ModelAlgorithm.class.toString())) {
         final ArrayList inputKeys = new ArrayList();
         final HashMap dao = new HashMap();
         final HashMap coords = new HashMap();
         final ModelAlgorithm model = ModelAlgorithmIO.open(new File(sModelFile), dao, inputKeys, coords);
         if (model != null) {
            model.getInputs().clear();
            final Set set = dao.keySet();
            final Iterator iter = set.iterator();
            while (iter.hasNext()) {
               final String sKey = (String) iter.next();
               final ObjectAndDescription oad = (ObjectAndDescription) dao.get(sKey);
               model.getInputs().put(sKey, oad.getObject());
            }
         }
         return model;
      }
      else {
         final HashMap algs = Sextante.getAlgorithms();
         final Set set = algs.keySet();
         final Iterator iter = set.iterator();
         while (iter.hasNext()) {
            final String sKey = (String) iter.next();
            alg = (GeoAlgorithm) algs.get(sKey);
            if (alg.getClass().toString().equals(sAlgorithm)) {
               try {
                  return alg.getNewInstance();
               }
               catch (final InstantiationException e) {
                  Sextante.addErrorToLog(e);
                  return null;
               }
               catch (final IllegalAccessException e) {
                  Sextante.addErrorToLog(e);
                  return null;
               }
            }
         }
         return null;
      }

   }


   public void initialize() {

   }


   public boolean isEnabled() {

      return true;

   }


   public boolean isVisible() {

      return true;

   }

}
