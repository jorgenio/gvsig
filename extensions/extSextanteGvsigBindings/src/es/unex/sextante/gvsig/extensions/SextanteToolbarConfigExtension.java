package es.unex.sextante.gvsig.extensions;

import com.iver.andami.plugins.Extension;

public class SextanteToolbarConfigExtension
         extends
            Extension {

   public void execute(final String actionCommand) {

      final ToolbarConfigDialog dialog = new ToolbarConfigDialog();
      dialog.pack();
      dialog.setVisible(true);

   }


   public void initialize() {

   }


   public boolean isEnabled() {

      return true;

   }


   public boolean isVisible() {

      return true;

   }

}
