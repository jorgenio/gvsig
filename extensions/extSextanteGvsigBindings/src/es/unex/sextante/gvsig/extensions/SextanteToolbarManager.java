package es.unex.sextante.gvsig.extensions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

import es.unex.sextante.core.Sextante;

public class SextanteToolbarManager {

   private static ArrayList<Button> m_Buttons = null;


   public static Button getButton(final int iIndex) {

      if (m_Buttons == null) {
         init();
      }

      return m_Buttons.get(iIndex);

   }


   public static ArrayList<Button> getButtons() {

      if (m_Buttons == null) {
         init();
      }

      return m_Buttons;

   }


   private static void init() {

      m_Buttons = new ArrayList<Button>();

      BufferedReader input = null;
      try {
         input = new BufferedReader(new FileReader(getConfigFile()));
         String sLine = null;
         while ((sLine = input.readLine()) != null) {
            final String[] sTokens = sLine.split(",");
            final Button button = new Button();
            button.sCommand = sTokens[0] + "," + sTokens[1];
            button.sName = sTokens[2];
            button.sIconFilename = sTokens[3];
            m_Buttons.add(button);
         }
      }
      catch (final FileNotFoundException ex) {
         //ex.printStackTrace();
      }
      catch (final IOException ex) {
         //ex.printStackTrace();
      }
      finally {
         try {
            if (input != null) {
               input.close();
            }
         }
         catch (final IOException ex) {
            ex.printStackTrace();
         }
      }

   }


   private static String getConfigFile() {

      String sPath = System.getProperty("user.home") + File.separator + "sextante";


      final File sextanteFolder = new File(sPath);
      if (!sextanteFolder.exists()) {
         sextanteFolder.mkdir();
      }

      sPath = sPath + File.separator + "sextante_toolbar.settings";

      return sPath;

   }


   public static void addButton(final Button button) {

      m_Buttons.add(button);

   }


   public static void save() {

      Writer output = null;
      try {
         output = new BufferedWriter(new FileWriter(getConfigFile()));
         for (int i = 0; i < m_Buttons.size(); i++) {
            final Button button = m_Buttons.get(i);
            output.write(button.sCommand + "," + button.sName + "," + new File(button.sIconFilename).getName() + "\n");
         }
      }
      catch (final IOException e) {
         Sextante.addErrorToLog(e);
      }
      finally {
         if (output != null) {
            try {
               output.close();
            }
            catch (final IOException e) {
               Sextante.addErrorToLog(e);
            }
         }
      }

   }

}
