package es.unex.sextante.gvsig.extensions;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstants;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.iver.andami.PluginServices;

import es.unex.sextante.core.Sextante;
import es.unex.sextante.gui.core.SextanteGUI;

public class ToolbarConfigDialog
         extends
            JDialog {

   private static final String BASE_CONFIG_FILE = "config_base.xml";
   private static final String CONFIG_FILE      = "config.xml";

   private JTable              jTable;
   private JButton             jButtonCancel;
   private JButton             jButtonRemove;
   private JButton             jButtonAdd;
   private JScrollPane         jScrollPane;
   private JButton             jButtonOK;


   public ToolbarConfigDialog() {

      super(SextanteGUI.getMainFrame(), "SextanteToolbar", true);

      this.setResizable(true);

      initGUI();

   }


   private void initGUI() {

      try {
         final TableLayout thisLayout = new TableLayout(new double[][] {
                  { 7.0, 50.0, 50.0, TableLayoutConstants.FILL, TableLayoutConstants.FILL, TableLayoutConstants.FILL, 7.0 },
                  { 10.0, TableLayoutConstants.FILL, TableLayoutConstants.FILL, 30.0, 30.0, 30.0, 7.0 } });
         thisLayout.setHGap(5);
         thisLayout.setVGap(5);
         getContentPane().setLayout(thisLayout);
         {
            jButtonOK = new JButton();
            getContentPane().add(jButtonOK, "4, 5");
            jButtonOK.setText(PluginServices.getText(this, "OK"));
            jButtonOK.addActionListener(new ActionListener() {
               public void actionPerformed(final ActionEvent evt) {
                  jButtonOKActionPerformed(evt);
               }
            });
         }
         {
            jButtonCancel = new JButton();
            getContentPane().add(jButtonCancel, "5, 5");
            jButtonCancel.setText(PluginServices.getText(this, "Cancel"));
            jButtonCancel.addActionListener(new ActionListener() {
               public void actionPerformed(final ActionEvent evt) {
                  jButtonCancelActionPerformed(evt);
               }
            });
         }
         {
            jScrollPane = new JScrollPane();
            getContentPane().add(jScrollPane, "1, 0, 5, 2");
            updateTable();
         }
         {
            jButtonAdd = new JButton();
            getContentPane().add(jButtonAdd, "1, 3");
            jButtonAdd.setText("+");
            jButtonAdd.addActionListener(new ActionListener() {
               public void actionPerformed(final ActionEvent evt) {
                  jButtonAddActionPerformed(evt);
               }
            });
         }
         {
            jButtonRemove = new JButton();
            getContentPane().add(jButtonRemove, "2, 3");
            jButtonRemove.setText("-");
            jButtonRemove.addActionListener(new ActionListener() {
               public void actionPerformed(final ActionEvent evt) {
                  jButtonRemoveActionPerformed(evt);
               }
            });
         }
         pack();
         this.setSize(519, 255);
      }
      catch (final Exception e) {
         e.printStackTrace();
      }
   }


   private void updateTable() {

      final TableModel tableModel = new ButtonTableModel(SextanteToolbarManager.getButtons());
      jTable = new JTable();
      jScrollPane.setViewportView(jTable);
      jTable.setModel(tableModel);

      final TableColumnModel tcm = jTable.getColumnModel();
      tcm.getColumn(0).setPreferredWidth(30);
      final DefaultTableCellRenderer renderer = new IconCellRenderer();
      tcm.getColumn(0).setCellRenderer(renderer);

   }


   private void jButtonAddActionPerformed(final ActionEvent evt) {

      final ButtonDefinitionDialog dialog = new ButtonDefinitionDialog(this);
      dialog.pack();
      dialog.setVisible(true);
      final Button button = dialog.getButton();
      if (button != null) {
         SextanteToolbarManager.addButton(button);
         updateTable();
      }

   }


   private void jButtonRemoveActionPerformed(final ActionEvent evt) {

      int iRow;

      iRow = jTable.getSelectedRow();
      if (iRow != -1) {
         ((ButtonTableModel) jTable.getModel()).removeRow(iRow);
      }

   }


   private void jButtonOKActionPerformed(final ActionEvent evt) {

      saveChanges();
      dispose();
      setVisible(false);
      JOptionPane.showMessageDialog(null, PluginServices.getText(this, "You_need_to_restart"), PluginServices.getText(this,
               "Warning"), JOptionPane.INFORMATION_MESSAGE);

   }


   private void saveChanges() {

      SextanteToolbarManager.save();

      final String sPath = System.getProperty("user.dir") + File.separator + "gvSIG" + File.separator + "extensiones" + File.separator
                     + "es.unex.sextante";

      final File inFile = new File(sPath + File.separator + BASE_CONFIG_FILE);
      final File outFile = new File(sPath + File.separator + CONFIG_FILE);
      try {
         final BufferedReader reader = new BufferedReader(new FileReader(inFile));
         final BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));

         String line = null;
         while ((line = reader.readLine()) != null) {
            writer.write(line);
            writer.newLine();
         }

         reader.close();


         for (int i = 0; i < SextanteToolbarManager.getButtons().size(); i++) {
            final Button button = SextanteToolbarManager.getButton(i);
            String sIconFilename = button.sIconFilename;
            if (sIconFilename == null || sIconFilename.trim().equals("")) {
               sIconFilename = "module2.png";
            }
            else {
               final File iconFile = new File(sIconFilename);
               sIconFilename = (iconFile.getName());
               final File outputFile = new File(sPath + File.separator + "images" + File.separator + iconFile.getName());
               final InputStream in = new FileInputStream(iconFile);
               final OutputStream out = new FileOutputStream(outputFile);
               final byte[] buf = new byte[1024];
               int len;
               while ((len = in.read(buf)) > 0) {
                  out.write(buf, 0, len);
               }
               in.close();
               out.close();
            }
            writer.write("<extension class-name=\"es.unex.sextante.gvsig.extensions.SextanteAlgorithmExecutorExtension\"\n");
            writer.write("\t description=\"" + button.sName + "\"");
            writer.write("\t active=\"true\">\n");
            writer.write("\t <tool-bar name=\"SEXTANTEToolbar\" position=\"3\">\n" + "<action-tool icon=\"images/"
                         + sIconFilename.trim() + "\"  tooltip=\"" + button.sName + "\" position=\"" + Integer.toString(i + 2)
                         + "\" action-command=\"" + Integer.toString(i) + "\"/>\n" + "</tool-bar>");
            writer.write("</extension>\n");
         }
         writer.write("</extensions>\n");
         writer.write("</plugin-config>");

         writer.close();

      }
      catch (final IOException e) {
         Sextante.addErrorToLog(e);

      }

   }


   private void jButtonCancelActionPerformed(final ActionEvent evt) {

      dispose();
      setVisible(false);

   }

}
