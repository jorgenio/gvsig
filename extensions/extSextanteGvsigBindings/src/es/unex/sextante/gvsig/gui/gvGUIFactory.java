package es.unex.sextante.gvsig.gui;

import com.iver.andami.PluginServices;

import es.unex.sextante.gui.cmd.BSHDialog;
import es.unex.sextante.gui.core.DefaultGUIFactory;
import es.unex.sextante.gui.core.SextanteGUI;
import es.unex.sextante.gui.dataExplorer.DataExplorerDialog;
import es.unex.sextante.gui.history.HistoryDialog;

public class gvGUIFactory
         extends
            DefaultGUIFactory {

   @Override
   public void showToolBoxDialog() {

      final ToolboxDialog toolbox = new ToolboxDialog();
      m_Toolbox = toolbox.getToolboxPanel();
      PluginServices.getMDIManager().addWindow(toolbox);

   }


   @Override
   public void showModelerDialog() {

      SextanteGUI.getInputFactory().createDataObjects();

      final ModelerDialog dialog = new ModelerDialog(m_Toolbox);
      PluginServices.getMDIManager().addWindow(dialog);


   }


   @Override
   public void showHistoryDialog() {

      SextanteGUI.getInputFactory().createDataObjects();

      final HistoryDialog dialog = new HistoryDialog(SextanteGUI.getMainFrame());
      SextanteGUI.setLastCommandOrigin(SextanteGUI.HISTORY);
      SextanteGUI.setLastCommandOriginParentDialog(dialog);
      m_History = dialog.getHistoryPanel();
      dialog.pack();
      dialog.setVisible(true);

      if (m_Toolbox == null) {
         SextanteGUI.getInputFactory().clearDataObjects();
      }

      m_History = null;

   }


   @Override
   public void showCommandLineDialog() {

      SextanteGUI.getInputFactory().createDataObjects();

      final BSHDialog dialog = new BSHDialog(SextanteGUI.getMainFrame());
      SextanteGUI.setLastCommandOrigin(SextanteGUI.COMMANDLINE);
      SextanteGUI.setLastCommandOriginParentDialog(dialog);
      dialog.pack();
      dialog.setVisible(true);

      if (m_Toolbox == null) {
         SextanteGUI.getInputFactory().clearDataObjects();
      }

   }


   @Override
   public void showDataExplorer() {

      SextanteGUI.getInputFactory().createDataObjects();

      final DataExplorerDialog dialog = new DataExplorerDialog(SextanteGUI.getMainFrame());
      dialog.pack();
      dialog.setVisible(true);

      if (m_Toolbox == null) {
         SextanteGUI.getInputFactory().clearDataObjects();
      }

   }


   @Override
   public void updateToolbox() {

      super.updateToolbox();

   }


   public void setToolboxHidden() {

      m_Toolbox = null;

   }

}
