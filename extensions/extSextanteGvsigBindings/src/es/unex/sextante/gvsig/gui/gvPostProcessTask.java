package es.unex.sextante.gvsig.gui;

import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;

import org.gvsig.fmap.raster.layers.FLyrRasterSE;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.ProjectExtension;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLayers;
import com.iver.cit.gvsig.fmap.layers.LayersIterator;
import com.iver.cit.gvsig.project.Project;
import com.iver.cit.gvsig.project.ProjectFactory;
import com.iver.cit.gvsig.project.documents.table.ProjectTable;
import com.iver.cit.gvsig.project.documents.view.ProjectView;
import com.iver.cit.gvsig.project.documents.view.ProjectViewFactory;

import es.unex.sextante.core.GeoAlgorithm;
import es.unex.sextante.core.ObjectAndDescription;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.core.Sextante;
import es.unex.sextante.dataObjects.ILayer;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.dataObjects.ITable;
import es.unex.sextante.dataObjects.IVectorLayer;
import es.unex.sextante.gui.additionalResults.AdditionalResults;
import es.unex.sextante.outputs.Output;
import es.unex.sextante.parameters.Parameter;
import es.unex.sextante.parameters.RasterLayerAndBand;

public class gvPostProcessTask
         implements
            Runnable {

   private MapContext       m_MapContext;
   GeoAlgorithm             m_Algorithm;
   private final OutputObjectsSet m_Output;


   public gvPostProcessTask(final GeoAlgorithm algorithm) {

      m_Output = algorithm.getOutputObjects();
      m_Algorithm = algorithm;

   }


   public void run() {

      setOutputView();

      addResults();

   }


   private void setOutputView() {

      ProjectView view = null;
      final ParametersSet parameters = m_Algorithm.getParameters();
      for (int i = 0; i < parameters.getNumberOfParameters(); i++) {
         final Parameter param = parameters.getParameter(i);
         final Object object = param.getParameterValueAsObject();
         if (object instanceof ILayer) {
            view = getViewFromLayer((ILayer) object);
            m_MapContext = view.getMapContext();
            return;
         }
         else if (object instanceof ArrayList) {
            final ArrayList list = (ArrayList) object;
            for (int j = 0; j < list.size(); j++) {
               final Object obj = list.get(j);
               if (obj instanceof ILayer) {
                  view = getViewFromLayer((ILayer) obj);
                  m_MapContext = view.getMapContext();
                  return;
               }
               else if (obj instanceof RasterLayerAndBand) {
                  final RasterLayerAndBand rlab = (RasterLayerAndBand) obj;
                  view = getViewFromLayer(rlab.getRasterLayer());
                  m_MapContext = view.getMapContext();
                  return;
               }
            }
         }
      }

      final ProjectView newView = ProjectFactory.createView(Sextante.getText("GRASS_new_view_name"));
      ((ProjectExtension) PluginServices.getExtension(ProjectExtension.class)).getProject().addDocument(newView);
      final IWindow window = newView.createWindow();

      final Runnable doWorkRunnable = new Runnable() {
         public void run() {
            PluginServices.getMDIManager().addWindow(window);
            m_MapContext = newView.getMapContext();
         }
      };
      try {
         SwingUtilities.invokeAndWait(doWorkRunnable);
      }
      catch (final InterruptedException e) {
         e.printStackTrace();
      }
      catch (final InvocationTargetException e) {

         e.printStackTrace();
      }

   }


   private ProjectView getViewFromLayer(final ILayer layer) {

      final FLayer gvSIGBaseLayer = (FLayer) layer.getBaseDataObject();
      final Project project = ((ProjectExtension) PluginServices.getExtension(ProjectExtension.class)).getProject();
      final ArrayList views = project.getDocumentsByType(ProjectViewFactory.registerName);

      for (int i = 0; i < views.size(); i++) {
         final ProjectView view = (ProjectView) views.get(i);
         final FLayers layers = view.getMapContext().getLayers();
         final LayersIterator iter = new LayersIterator(layers);
         while (iter.hasNext()) {
            final FLayer gvSIGLayer = iter.nextLayer();
            if (gvSIGLayer.equals(gvSIGBaseLayer)) {
               return view;
            }
         }

      }

      return null;

   }


   private void addResults() {

      FLayer layer = null;
      String sDescription;
      boolean bInvalidate = false;
      boolean bShowAdditionalPanel = false;

      if (m_MapContext != null) {
         m_MapContext.beginAtomicEvent();
      }

      for (int i = 0; i < m_Output.getOutputObjectsCount(); i++) {

         final Output out = m_Output.getOutput(i);
         sDescription = out.getDescription();
         final Object object = out.getOutputObject();
         if (object instanceof IVectorLayer) {
            layer = (FLayer) ((IVectorLayer) object).getBaseDataObject();
            if (layer != null) {
               layer.setName(sDescription);
               m_MapContext.getLayers().addLayer(layer);
               bInvalidate = true;
            }
         }
         else if (object instanceof ITable) {
            try {
               final ProjectTable table = (ProjectTable) ((ITable) object).getBaseDataObject();
               if (table != null) {
                  ((ProjectExtension) PluginServices.getExtension(ProjectExtension.class)).getProject().addDocument(table);
               }
            }
            catch (final Exception e) {
               e.printStackTrace();
            }
         }
         else if (object instanceof IRasterLayer) {
            final IRasterLayer rasterLayer = (IRasterLayer) object;
            layer = (FLayer) rasterLayer.getBaseDataObject();
            if (layer != null) {
               ((FLyrRasterSE) layer).setNoDataValue(rasterLayer.getNoDataValue());
               layer.setName(sDescription);
               m_MapContext.getLayers().addLayer(layer);
               bInvalidate = true;
            }
         }
         else if (object instanceof String) {
            JTextPane jTextPane;
            JScrollPane jScrollPane;
            jTextPane = new JTextPane();
            jTextPane.setEditable(false);
            jTextPane.setContentType("text/html");
            jTextPane.setText((String) object);
            jScrollPane = new JScrollPane();
            jScrollPane.setViewportView(jTextPane);
            jScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
            jTextPane.setBorder(BorderFactory.createEtchedBorder(BevelBorder.LOWERED));
            AdditionalResults.addComponent(new ObjectAndDescription(sDescription, jScrollPane));
            bShowAdditionalPanel = true;
         }
         else if (object instanceof Component) {
            AdditionalResults.addComponent(new ObjectAndDescription(sDescription, object));
            bShowAdditionalPanel = true;
         }

      }

      if (m_MapContext != null) {
         m_MapContext.endAtomicEvent();
      }

      if (bInvalidate) {
         m_MapContext.invalidate();
      }

      if (bShowAdditionalPanel) {
         AdditionalResults.showPanel();
      }

   }

}
