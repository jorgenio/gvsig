package com.iver.cit.gvsig.geoprocess.impl.build;

import java.net.URL;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.layers.FLayers;
import com.iver.cit.gvsig.geoprocess.core.GeoprocessPluginAbstract;
import com.iver.cit.gvsig.geoprocess.core.IGeoprocessController;
import com.iver.cit.gvsig.geoprocess.core.IGeoprocessPlugin;
import com.iver.cit.gvsig.geoprocess.core.gui.IGeoprocessUserEntries;
import com.iver.cit.gvsig.geoprocess.impl.build.gui.BuildGeoprocessPanel;
import com.iver.cit.gvsig.project.documents.view.gui.View;

public class BuildGeoprocessPlugin extends GeoprocessPluginAbstract implements
		IGeoprocessPlugin {
	
	private static String dataConvertPkg;
	private static String geoprocessName;
	
	static{
		dataConvertPkg = PluginServices.getText(null, "Conversion_de_datos");
		geoprocessName = PluginServices.getText(null, "Build");
	}

	public IGeoprocessUserEntries getGeoprocessPanel() {
		com.iver.andami.ui.mdiManager.IWindow view = PluginServices.getMDIManager().getActiveWindow();
		View vista = (View) view;
		FLayers layers = vista.getModel().getMapContext().getLayers();
		return (IGeoprocessUserEntries) new BuildGeoprocessPanel(layers);
	}

	public IGeoprocessController getGpController() {
		return new BuildGeoprocessController();
	}

	public URL getImgDescription() {
		URL url = PluginServices.getIconTheme().getURL("build-icon");
		return url;
	}

	public String getNamespace() {
		return dataConvertPkg + "/" + geoprocessName;
	}
	
	public String toString() {
		return geoprocessName;
	}

}
