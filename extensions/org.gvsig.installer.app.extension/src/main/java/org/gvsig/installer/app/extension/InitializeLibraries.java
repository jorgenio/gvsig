package org.gvsig.installer.app.extension;

import org.gvsig.installer.lib.api.InstallerLibrary;
import org.gvsig.installer.lib.impl.DefaultInstallerLibrary;
import org.gvsig.installer.prov.plugin.PluginInstallerProviderLibrary;
import org.gvsig.installer.swing.api.SwingInstallerLibrary;
import org.gvsig.installer.swing.impl.DefaultSwingInstallerLibrary;
import org.gvsig.tools.ToolsLibrary;
import org.gvsig.tools.library.Library;

public class InitializeLibraries {

	private static boolean initialized = false;
	
	public static void initialize(){

		if (initialized){
			return;
		}
		Library library = new ToolsLibrary();
		library.initialize();
		library.postInitialize();
		library = new InstallerLibrary();
		library.initialize();
		library.postInitialize();
		library = new DefaultInstallerLibrary();
		library.initialize();
		library.postInitialize();
		library = new SwingInstallerLibrary();
		library.initialize();
		library.postInitialize();
		library = new DefaultSwingInstallerLibrary();
		library.initialize();
		library.postInitialize();
		library = new PluginInstallerProviderLibrary();
		library.initialize();
		library.postInitialize();
		
		initialized = true;
	}

}
