OVERVIEW
==========
This ANT script builds a gvSIG Debian package. 

PRECONDITIONS
==========
- operative system with a GLIBC compatible version for all platforms
Ex: If it's built on a Ubuntu 9.04 it won't run in a Debian etch 

- external commands cp, ln, dpck 
- external command make and cmake to build natives

- a workspace in built without errors and the directory _fwAndami/gvSIG contains
all the extensions

The directory binaries/deb-etch must have the symbolic links to the native libraries

The main dependencies are:
libgdal1.5.0 for raster support (conflicts with v1.6 in order to support grass format)
libstdc++5 for mrsid support

BUILD
==========

1.- Building JNI gdal
Edit libjni-gdal/CMakeModules/FindGDAL.cmake and put the path where 
gdal headers are placed.
The easy way is install from repository (sudo aptitude install libgdal1-1.5.0-grass) and
set the directory /usr/include/gdal
Define the enviroment JAVA_HOME
Finally, run the ant build script.

2.- Building JNI ECW (v3.3)
Edit libjni-ecw/CMakeModules/FindECW.cmake and put the path where 
ECW headers are placed (ie. /usr/local/include)
Define the enviroment JAVA_HOME 
Finally, run the ant build script.

3.- Building JNI MrSID  (v7.0.0)
Edit libjni-mrsid/CMakeModules/FindMRSID.cmake and put the path where 
MrSID headers are placed (ie /usr/local/Geo_DSDK-7.0.6.2167)
NOTE: each group of lines has a different include directory!!
Set gcc-3.4 as the default gcc compiler
Set g++-3.4 as the default g++ compiler
Change libstdc++ in the compiler ( /usr/lib/gcc/i486-linux-gnu/3.4.6). Its needed to point libstdc++.so.5
Define the enviroment JAVA_HOME
Finally, run the ant build script 

4.- Building JNI proj
Edit libjni-proj4/Makefile to set the directory where is placed the JNI headers
For example:
"default_PROJECT_INCS= -Iinclude -I/usr/lib/jvm/java-1.5.0-sun/include -I/usr/lib/jvm/java-1.5.0-sun/include/linux" 
In a shell execute make

5.- Building the package
Run the ant script and the binary will be placed in the directory "dist"


POSTCONDITIONS
==============

-gvSIG Files:
/opt/gvSIG/bin  --> script to run gvSIG and extensions
/opt/gvSIG/lib  --> java binaries (jars) 
/opt/gvSIG/libs --> native libraries (so)

-System files:
/usr/share/mime/packages/gvsig.xml --> gvSIG mime type definition 
/usr/share/applications/gvsig.desktop --> desktop shortcut definition
/usr/share/icons/ico-gvSIG.png --> shortcut icon

TODO
=====
check /usr/share/mime/packages/gvsig.xml
Version number automatic

KNOWN PROBLEMS
==============
In the build.xml copy task doesn't preserve permission. Use task exec (with command cp) instead.

HOWTO INSTALL
==============
aptitude install libgdal1-1.5.0-grass sun-java5-jre
aptitude remove libgdal1-1.6.0
dpkg -i *.deb


ANY DOUBT
==========
jvhigon@gvsig.org