JavaTM Advanced Imaging API v1.1.2 Readme

Contents

   * Introduction
        o Installation and System Requirements
        o Documentation
        o Bugs
   * Overview of JAI Functionality
        o Core Functionality
        o Operators
             + Native Acceleration
        o How to Run the JAI 1.1 version of Remote Imaging
        o How to Run the JAI 1.0.2 version of Remote Imaging
   * What's New
        o Core Classes
        o Operations
        o Network Imaging
        o Widget Package
        o Fixed Bugs
   * Changes From JAI 1.1.2-beta to JAI 1.1.2-RC
        o Bugs Fixed in JAI 1.1.2-RC
        o Enhancements Added in JAI 1.1.2-RC
   * Changes From JAI 1.1.2-RC to JAI 1.1.2
        o Bugs Fixed in JAI 1.1.2
        o Enhancements Added in JAI 1.1.2

   * Feedback

Introduction

The Maintenance Review release of the 1.1 version of the Java TM Advanced
Imaging (JAI) specification contains some changes since the JAI 1.1
specification Final Release. The present reference implementation, called
Java Advanced Imaging 1.1.2, implements the Maintenance Review version of
the JAI 1.1 specification and also contains bug fixes and performance
enhancements with respect to the most recent implementation of the JAI 1.1
specification Final Release which was called JAI 1.1.1_01. It represents
development pursuant to maintenance of the Java Advanced Imaging 1.1 JSR.

The changes since JAI 1.1.1_01 are described in the "What's New" section of
this document.

The Java Advanced Imaging API home page is located at
http://java.sun.com/products/java-media/jai/. There you will find binaries,
documentation, answers to frequently asked questions, and other information.

Installation and System Requirements

For general installation instructions please refer to the INSTALL page. For
system requirements please refer to the system requirements section of the
INSTALL page.

Documentation

Links to Java Advanced Imaging documentation are available at
http://java.sun.com/products/java-media/jai/docs/

Bugs

Some bugs are known to exist - see the BUGS page for details.

Overview of JAI Functionality

Core Functionality

All areas of the JAI specification have been implemented for this release.
The com.sun.media.jai.codec package continues to be an uncommitted part of
JAI. For mode information on image reading and writing images on the Java 2
platform and in JAI please refer to the page Image I/O in Java Advanced
Imaging.

All operators outlined in the Java Advanced Imaging API specification are
implemented.

The major areas of JAI functionality are described below:

  1. Tiling

     Images are made up of tiles. Different images may have different tile
     sizes. Each tile can be processed and stored in memory separately.
     Tiles may be stored in a centrally-maintained cache for performance.
     The use of tiling also facilitates the use of multiple threads for
     computation. Previously allocated tiles may also be re-used to save
     memory.

  2. Deferred execution

     Image operations performed on an image do not take place immediately
     when they are defined. Calculation of pixels happens only when a
     portion of the resultant image is requested, and only tiles that are
     needed are processed. However, operations always appear semantically to
     be completed immediately.

  3. Threaded Computation

     Requests for tiles are given to several running threads, allowing
     potential speedups on multi-processor systems or when requesting data
     over the network.

  4. Object-Oriented Extensibility

     Users can add their own image operators or override existing operators
     by registering the new operators with the operation registry. Please
     see the "The Java Advanced Imaging API White Paper," the API
     documentation, the JAI tutorial, the sample code, and the jai-interest
     archives for more information (in the archives search for subjects
     beginning with "sample code").

     Additionally, users may extend a number of non-image classes in order
     to add functionality to JAI:

        o Border Extension

          Operators may obtain an extended view of their sources using an
          extensible mechanism for generating pixel values outside of the
          source image bounds. New subclasses of BorderExtender may be used
          to obtain customized functionality.

        o Image Warping

          Subclasses of Warp may be written to perform customized image
          warping.

        o Pixel Interpolation

          Subclasses of Interpolation may be written to perform customized
          pixel interpolation.

        o Color Spaces

          Subclasses of ColorSpaceJAI may be written to implement
          mathematically defined color space transformations without the
          need for ICC profiles.

  5. Graphics2D-Style Drawing

     Graphics2D-style drawing may be performed on a TiledImage in a manner
     analogous to that available for java.awt.image.BufferedImage.

     The RenderableGraphics class provides a way to store a sequence of
     drawing commands and to "replay" them at an arbitrary output
     resolution.

  6. Regions of interest (ROIs)

     Non-rectangular portions of an image may be specified using a ROI or
     ROIShape object. These ROIs may be used as parameters to the Extrema,
     Mean, Histogram or Mosaic operations and the TiledImage.set() and
     TiledImage.setData() methods. Operations produce an appropriate ROI
     property on their output when one exists on their input.

  7. Image file handling

     This release of Java Advanced Imaging supports BMP, FlashPIX, GIF,
     JPEG, PNG, PNM, and TIFF images as defined in the TIFF 6.0
     specification, and WBMP type 0 B/W uncompressed bitmaps. TIFF G3 (1D
     and 2D), G4, PackBits, LZW, JPEG, and DEFLATE (Zip) compression types
     are understood.

     The classes dealing with image file handling (the
     com.sun.media.jai.codec package and private implementation packages
     that provide support for it) are provided in a separate jai file,
     jai_codec.jar. This jar file may be used separately from the
     jai_core.jar file containing the various javax.media.jai packages and
     their supporting classes.

     As described in the Core Functionality section of this document, the
     image codec classes should be considered as temporary helper functions.
     They will be replaced by a new API for image I/O that has been defined
     under the Java Community Process.

        o BMP File Handling:

               The BMP reader can read Version 2.x, 3.x and some 4.x BMP
               images. BMP images with 1, 4, 8, 24 bits can be read with
               this reader. Support for 16 and 32 bit images has also been
               implemented, although such images are not very common.

               Reading of compressed BMPs is supported. BI_RGB, BI_RLE8,
               BI_RLE4 and BI_BITFIELDS compressions are handled.

               The BMP reader emits properties such as type of compression,
               bits per pixel etc. Use the getPropertyNames() method to get
               the names of all the properties emitted.

               BMP Limitations:

                  + Only the default RGB color space is supported.
                  + Alpha channels are not supported.

               BMP Writer:
                  + The BMP writer is capable of writing images in the
                    Version 3 format. Images which make use of a
                    IndexColorModel with 2, 16, or 256 palette entries will
                    be written in palette form.
                  + RLE4 and RLE8 compression is supported when compatible
                    with the image data.

        o FlashPIX file handling:

               A limited FlashPIX reader is provided that is capable of
               extracting a single resolution from a FlashPIX image file.
               Only simple FlashPix files are decoded properly.

               The image view object is ignored, and image property
               information is not exported.

               There is no FlashPIX writer.

        o GIF file handling:

               There is no GIF writer due to the patent on the LZW
               compression algorithm.

        o JPEG file handling:

             + JPEG files are read and written using the classes found in
               the com.sun.image.codec.jpeg package of the JDK. A set of
               simple JAI wrapper classes around these classes is provided.
             + The JPEG decoder cannot read abbreviated streams, either
               tables-only or image-only.

        o PNG file handling:

               All files in the PNGSuite test suite have been read and
               written successfully. See the documentation in
               com.sun.media.jai.codec.PNGDecodeParam and PNGEncodeParam for
               more information.

        o PNM file handling:

               PNM files may be read and written in both ASCII and raw
               formats. The encoder automatically selects between PBM
               (bitmap), PGM (grayscale) and PPM (RGB) files according to
               the number of bands and bit depth of the source image.

               Due to the limitations of the format, only images with 1 or 3
               bands may be written.

        o TIFF file handling:

          TIFF support has the following limitations:

             + The TIFF encoder does not support LZW compression due to the
               patent on the algorithm.
             + Planar format (PlanarConfiguration field has value 2) is not
               supported for decoding or encoding.

        o WBMP file handling:

               The WBMP codec reads and writes images in the Wireless Bitmap
               format described in chapter 6 and Appendix A of the Wireless
               Application Protocol (WAP) Wireless Application Environment
               Specification, Version 1.3, 29 March 2000. The WBMP type
               supported is WBMP Type 0: B/W, Uncompressed Bitmap. There are
               no limitations on the image dimensions.

  8. Image Layouts

     Images with arbitrary pixel layouts may be processed in a uniform
     manner using the PixelAccessor and RasterAccessor classes.

     Source images with ComponentColorModels and IndexColorModels are
     supported. DirectColorModel images are not supported.

     PixelAccessor and RasterAccessor provide the most efficient support for
     the ComponentSampleModel/ComponentColorModel combination.

  9. Image Collections

     The output of a standard image operator on an instance of
     java.util.Collection is a collection of the same type. Nested
     collections are supported. Operators may also emit collections of their
     choice, or take collections as sources and emit a single image.

 10. Remote Imaging

     JAI allows operations to be performed remotely to be created in a
     manner similar to local operations. RemoteJAI.create() and
     RemoteJAI.createRenderable() can be used to create operations that are
     performed on remote hosts. Operation chains are created on the client
     and can contain a mix of local and remote operations by using
     JAI.create() and RemoteJAI.create(), respectively to create the
     operations.

     The "fileload" and "filestore" operations can allow files that reside
     only on remote filesystems to be loaded and stored remotely. This can
     be accomplished by setting the checkFileLocally argument to the
     operation to be false, in which case the presence of the file to be
     loaded or stored is not checked on the local file system when the
     operation is first created.

     See sections below for instructions on how to use the JAI 1.0.2 and 1.1
     or later versions of remote imaging.

 11. Iterators

     Optimized Rect and Random iterators exist as well as a non-optimized
     version of the Rook iterator.

 12. Snapshotting of External Sources

     SnapshotImage provides an arbitrary number of synchronous views of a
     possibly changing WritableRenderedImage.

 13. Meta-data Handling

     Meta-data handling is provided via a name-value database of properties
     associated with each JAI image. Mechanisms are provided by which such
     properties may be generated and processed in the course of image
     manipulation. The ability to defer computation of such data is also
     provided in a manner conceptually equivalent to that available for
     image data. Please refer to the DeferredData and DeferredProperty
     classes for more information.

 14. Serialization Support

     SerializerFactory provides a framework is provided to assist in
     serializing instances of classes which do not implement
     java.io.Serializable. Such objects must be serialized by extracting a
     serializable version of their state from which the original object may
     be extracted after deserialization.

Operators

Java Advanced Imaging extends the imaging functionality provided in the Java
2D API by providing a more flexible and scalable architecture targeted for
complex, high performance imaging requirements. In this context a large
number of imaging operators are provided.

   * Native Acceleration

     Pure Java implementations are provided for all image operators and
     imaging performance is addressed for some of these by providing C-based
     native code. Native C-code based acceleration for a certain subset of
     operators under specific conditions (listed in the table below) is
     available for the Sun/Solaris, Win32 and Linux (x86 only) platforms. On
     Sun UltraSPARC-based platforms, additional performance is gained with
     hardware acceleration via the VIS instructions for most natively
     supported operators. On Win32 platforms which support MMX instructions,
     hardware acceleration is gained for a subset of the natively supported
     operators.

     If a native implementation is present it is, by default, the preferred
     implementation. But if the nature of the sources and parameters of the
     operation are incompatible with the native operation then processing
     will revert to Java code. In general the following minimum requirements
     must be adhered to for the mediaLib native implementation of an
     operation to be executed:

        o All sources must be RenderedImages.
        o All sources and destination must have
             + a SampleModel which is a ComponentSampleModel and
             + a ColorModel which is a ComponentColorModel or no ColorModel
               (i.e., it is null).
        o All sources and the destination must have at most 4 bands of pixel
          data.
        o If Interpolation type is one of the arguments to the operator,
          then native acceleration is available only for Nearest, Bilinear,
          Bicubic and Bicubic2 cases. Additionally for byte images
          InterpolationTable is also supported for native acceleration.

     Further restrictions may be imposed by individual operations but the
     above are the most common requirements.

   * Imaging Operators

     The following image operators are implemented in this release. Only a
     brief description of each operator is provided here. For detailed
     information on these operators, refer to the package
     javax.media.jai.operator in the full documentation available at
     http://java.sun.com/products/java-media/jai/docs/index.html.

     All operations are performed on a per-band basis, unless specified
     otherwise. C acceleration applies to all platforms whereas VIS is
     specific to Sun UltraSPARC and MMX to Windows.

       1. Point and Arithmetic Operators

                                                     Native Acceleration
              Operator Name       Description
                                                 C VIS MMX      Notes
                                Computes the
           absolute             absolute value   X  X   X
                                of the pixels of
                                an image.
                                Adds the pixel
           add                  values of two    X  X   X
                                source images.
                                Adds a
           addcollection        collection of
                                images to one
                                another.
                                Adds a set of
                                constant values
           addconst             to the pixel     X  X   X
                                values of a
                                source image.
                                Adds a set of
                                constant values
           addconsttocollection to the pixel     X  X   X
                                values of a
                                Collection of
                                source images.
                                And's the pixel
           and                  values of two    X  X   X
                                source images.
                                And's the pixel
                                values of a
           andconst             source image     X  X   X
                                with a set of
                                constants.
                                Computes a
                                linear
           bandcombine          combination of   X  X   X  3x4 matrix only.
                                the bands of an
                                image.
                                Creates an image
                                consisting of
                                all bands of all
           bandmerge            sources
                                concatenated in
                                the order
                                encountered.
                                Selects a subset
                                of the bands of            Only if the band
           bandselect           an image,        X  X   X  selection is
                                possibly                   monotonically
                                reordering them.           increasing.
                                Thresholds a
                                single-band
           binarize             image to two     X  X
                                levels to
                                generate a
                                bilevel output.
                                Set all pixel
                                values below the
                                low value to
                                that low value,
           clamp                set all the      X  X   X
                                pixel values
                                above the high
                                value to that
                                high value.
                                Converts an
           colorconvert         image to a given
                                ColorSpace.
                                Generates an
                                optimal LUT by
           colorquantizer       executing a
                                color
                                quantization
                                algorithm
                                Combines two
                                images based on
           composite            their alpha      X  X   X
                                values at each
                                pixel.
                                Creates an image
           constant             with constant
                                pixel values.
                                Divides the
                                pixel values of
                                the first source
           divide               image by the     X
                                pixel values of
                                the second
                                source image.
                                Divides the
                                pixel values of
           dividebyconst        a source image   X
                                by a set of
                                constants.
                                Divides a set of
           divideintoconst      constants by the X
                                pixel values of
                                a source image.
                                Computes the
           exp                  exponential of   X
                                the pixel values
                                of an image.
                                Performs
                                reformatting on
                                an image,
                                including data
           format               type casting,
                                replacing the
                                SampleModel and
                                ColorModel, and
                                restructuring
                                the tile grid.
                                Inverts the
           invert               pixel values of  X  X   X
                                an image.
                                Computes the
                                natural
           log                  logarithm of the X
                                pixel values of
                                an image.

                                Performs general           Only if table
           lookup               table lookup on  X  X   X  has less than or
                                an image.                  equal to 4
                                                           bands.
                                Performs a
                                piecewise linear
                                remapping of
           matchcdf             pixel values to
                                match a given
                                cumulative
                                distribution
                                function.
                                Chooses the
           max                  maximum pixel    X  X   X
                                values between
                                two images.
                                Chooses the
           min                  minimum pixel    X  X   X
                                values between
                                two images.
                                Multiplies the
           multiply             pixel values of  X  X
                                two source
                                images.
                                Multiplies the
                                pixel values of
           multiplyconst        a source image   X  X
                                by a set of
                                constants.
                                Inverts the
           not                  pixel values of  X  X   X
                                a source image.
                                Or's the pixel
           or                   values of two    X  X   X
                                source images.
                                Or's the pixel
                                values of a
           orconst              source image     X  X   X
                                with a set of
                                constants.
                                Overlays one
           overlay              image on top of
                                another image.
                                Performs
                                piecewise linear
           piecewise            remapping of the
                                pixel values of
                                an image.
                                Performs a
                                linear remapping
           rescale              of the pixel     X  X
                                values of an
                                image.
                                Subtracts the
                                pixel values of
           subtract             one image from   X  X   X
                                those of
                                another.
                                Subtracts a set
                                of constant
           subtractconst        values from the  X  X   X
                                pixel values of
                                an image.
                                Subtracts a set
                                of constant
           subtractfromconst    values from the  X  X   X
                                pixel values of
                                an image.
                                Maps the pixel
                                values that fall
           threshold            between a low    X  X   X
                                and high value
                                to a set of
                                constants.
                                Xor's the pixel
           xor                  values of two    X  X   X
                                source images.
                                Xor's a source
           xorconst             image with a set X  X   X
                                of constants.

       2. Area and Geometric Operators

                                                      Native Acceleration
               Operator Name        Description
                                                  C VIS MMX       Notes

                                 Performs first             InterpolationTable
           affine                order geometric  X  X   X  is not MMX
                                 image warping.             accelerated for
                                                            even byte images.

           border                Adds a border
                                 around an image.
                                 Convolves an
           boxfilter             image using a
                                 two-dimensional
                                 box filter.
                                 Performs an MxN
           convolve              image            X  X   X  General and
                                 convolution.               separable cases.
                                 Extracts a
           crop                  subarea of an
                                 image.
                                 Performs
                                                            Only single band,
           dilate                morphological    X  X   X  3x3 kernels
                                 dilation on an
                                 image.                     centered at 1,1
                                 Performs
                                                            Only single band,
           erode                 morphological    X  X   X  3x3 kernels
                                 erosion on an
                                 image.                     centered at 1,1
                                 Performs a
                                 combined integral
           filteredsubsample     subsample and    X  X   X
                                 symmetric
                                 product-separable
                                 filter.
                                 Performs edge
           gradientmagnitude     detection using  X  X   X
                                 orthogonal
                                 gradient masks.
                                 Computes the               Only single band;
           maxfilter             maximum value of X  X   X  only for a SQUARE
                                 a pixel                    mask of size 3, 5,
                                 neighborhood.              or 7
                                 Computes the
           medianfilter          median value of aX  X   X
                                 pixel
                                 neighborhood.
                                 Computes the               Only single band;
           minfilter             minimum value of X  X   X  only for a SQUARE
                                 a pixel                    mask of size 3, 5,
                                 neighborhood.              or 7
                                 Creates a mosaic
           mosaic                of two or more   X  X   X
                                 rendered images.

                                 Rotates an image           InterpolationTable
           rotate                about an         X  X   X  is not MMX
                                 arbitrary point.           accelerated for
                                                            even byte images.

                                 Scales and                 InterpolationTable
           scale                 translates an    X  X   X  is not MMX
                                 image.                     accelerated for
                                                            even byte images.
                                                            InterpolationTable
           shear                 Shears an image. X  X   X  is not MMX
                                                            accelerated for
                                                            even byte images.
                                 Subsamples an
           subsampleaverage      image by         X  X
                                 averaging over a
                                 moving window.
                                 Subsamples a
           subsamplebinarytogray bilevel image to X  X
                                 a grayscale
                                 image.
                                 Translates an
                                 image by an                InterpolationTable
           translate             integral or      X  X   X  is not MMX
                                 fractional                 accelerated for
                                 amount.                    even byte images.
                                 Reflects an image
                                 in a specified
           transpose             direction or     X  X   X
                                 rotates an image
                                 in multiples of
                                 90 degrees.
                                 Sharpens an image
           unsharpmask           by suppressing   X  X   X  General and
                                 the low                    separable kernels.
                                 frequencies.
                                 Performs
           warp                  geometric warpingX  X   X  polynomial and
                                 on an image.               grid only.

       3. Frequency-domain, Transform, and Complex Operators

                                                               Native
            Operator Name           Description             Acceleration
                                                          C  VIS  MMX Notes

           conjugate        Computes the complex
                            conjugate of an image.

           dct              Computes the Discrete Cosine  X
                            Transform of an image.
                            Computes the Discrete
           dft              Fourier Transform of an       X
                            image, possibly resulting in
                            a complex image.

           dividecomplex    Computes the quotient of two
                            complex images.
                            Computes the inverse
           idct             Discrete Cosine Transform of  X
                            an image.
                            Computes the inverse
           idft             Discrete Fourier Transform    X
                            of an image.

           magnitude        Computes the magnitude of a
                            complex image.
                            Computes the squared
           magnitudesquared magnitude of a complex
                            image.

           multiplycomplex  Computes the product of two
                            complex images.

           periodicshift    Shifts an image
                            periodically.

           phase            Computes the phase angle of
                            a complex image.
                            Creates a complex image from
           polartocomplex   two images representing
                            magnitude and phase.

       4. Statistical Operators

                                              Native
            Operator                       Acceleration
              Name        Description
                                         C  VIS MMX  Notes
                      Computes the
                      maximum and                         Only if the ROI
           extrema    minimum pixel      X   X   X        is null or
                      values of an                        encloses the
                      image.                              entire image.
                      Computes the
           histogram  histogram of an    X   X   X
                      image.
                                                          Only if the ROI
                      Computes the mean                   is null or
           mean       pixel value of a   X   X   X        encloses the
                      region of an                        entire image and
                      image.                              the sampling
                                                          period is 1.

       5. Sourceless Operators

           Operator Name                    Description
           imagefunction Creates an image by evaluating a function.
           pattern       Creates an image consisting of a repeated pattern.

       6. File and Stream Operators

           Operator Name                    Description
           awtimage      Converts a java.awt.Image into a PlanarImage.
           bmp           Loads an image in BMP format.
           encode        Writes an image to an OutputStream.
           fileload      Loads an image from a file.
           filestore     Writes an image to a file in a given format.
           fpx           Loads an image in FlashPIX format.
           gif           Loads an image in GIF format.
                         Reads an image from a remote IIP server,
           iip           performing IIP view transforms (affine,
                         colortwist, filter, crop).

           iipresolution Reads a single resolution of an image from a
                         remote IIP server.
           jpeg          Loads an image in JPEG format.
           png           Loads an image in PNG 1.0 or 1.1 format.
           pnm           Loads an image in PBM, PGM, or PPM format.
           stream        Loads an image from a stream.
           tiff          Loads an image in TIFF 6.0 format.
           url           Loads an image from a URL.

       7. Other Operators

           Operator Name                     Description

           errordiffusion Performs error diffusion color quantization using
                          a specified color map and error filter.
                          Performs no processing. Useful as a placeholder
           null           in an operation chain or as a node which emits
                          meta-data.

           ordereddither  Performs color quantization using a specified
                          color map and a fixed dither mask.

           renderable     Constructs a RenderableImage from a RenderedImage
                          source.

How to Run the JAI 1.1 version of Remote Imaging

1. Create a Security Policy File

If $JAI is the base directory where Java Advanced Imaging is installed,
create a text file named $JAI/policy containing the following:

  grant {
    // Allow everything for now
    permission java.security.AllPermission;
  };

Note that this policy file is for testing purposes only, and it is not
recommended that full permission be given to all programs.

For more information on policy files and permissions please see:

http://java.sun.com/products/jdk/1.3/docs/guide/security/PolicyFiles.html

http://java.sun.com/products/jdk/1.3/docs/guide/security/permissions.html

2. Start the RMI Registry

Log in to the remote machine where the image server will be running and
start the RMI registry. For example, in the Solaris operating environment
using a Bourne-compatible shell (e.g., /bin/sh):

  $ unset CLASSPATH
  $ rmiregistry &

Note that the CLASSPATH environment variable is deliberately not set.

3. Start the JAI Remote Image Server

While still logged in to the remote server machine, set the CLASSPATH and
LD_LIBRARY_PATH environment variables as required for JAI (see the INSTALL
file) and start the remote imaging server:

  $ CLASSPATH=$JAI/lib/jai_core.jar:$JAI/lib/jai_codec.jar:\
              $JAI/lib/mlibwrapper_jai.jar
  $ export CLASSPATH
  $ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$JAI/lib
  $ export LD_LIBRARY_PATH
  $ java \
  -Djava.rmi.server.codebase=\
  "file:$JAI/lib/jai_core.jar file:$JAI/lib/jai_codec.jar" \
  -Djava.rmi.server.useCodebaseOnly=false \
  -Djava.security.policy=file:$JAI/policy \
  com.sun.media.jai.rmi.JAIRMIImageServer

For example, when the above steps are executed on a machine with IP address
123.456.78.90 the following is printed:

  Server: using host 123.456.78.90 port 1099
  Registering image server as "rmi://123.456.78.90:1099/JAIRMIRemoteServer1.1".
  Server: Bound RemoteImageServer into the registry.

4. Run the Local Application

Run the local application making sure that the serverName parameter of any
javax.media.jai.remote.RemoteJAI constructors corresponds to the machine on
which the remote image server is running. For example, if the machine with
IP address 123.456.78.90 above is named myserver the serverName parameter of
any RemoteJAI constructors should be "myserver".

How to Run the JAI 1.0.2 version of Remote Imaging

For more information on RMI (remote method invocation) please refer to:
http://java.sun.com/products/jdk/rmi/index.html

1. Create a Security Policy File

If $JAI is the base directory where Java Advanced Imaging is installed,
create a text file named $JAI/policy containing the following:

grant {
  // Allow everything for now
  permission java.security.AllPermission;
};

Note that this policy file is for testing purposes only.

For more information on policy files and permissions please see:

http://java.sun.com/products/jdk/1.2/docs/guide/security/PolicyFiles.html

http://java.sun.com/products/jdk/1.2/docs/guide/security/permissions.html

2. Start the RMI Registry

Log in to the remote machine where the image server will be running and
start the RMI registry. For example, in the Solaris operating environment
using a Bourne-compatible shell (e.g., /bin/sh):

$ unset CLASSPATH
$ rmiregistry &

Note that the CLASSPATH environment variable is deliberately not set.

3. Start the JAI Remote Image Server

While still logged in to the remote server machine, set the CLASSPATH and
LD_LIBRARY_PATH environment variables as required for JAI (see the INSTALL
file) and start the remote imaging server:

$ CLASSPATH=$JAI/lib/jai_core.jar:$JAI/lib/jai_codec.jar:\
            $JAI/lib/mlibwrapper_jai.jar
$ export CLASSPATH
$ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$JAI/lib
$ export LD_LIBRARY_PATH
$ java \
-Djava.rmi.server.codebase=\
"file:$JAI/lib/jai_core.jar file:$JAI/lib/jai_codec.jar" \
-Djava.rmi.server.useCodebaseOnly=false \
-Djava.security.policy=file:$JAI/policy \
com.sun.media.jai.rmi.RMIImageImpl

For example, when the above steps are executed on a machine with IP address
123.456.78.90 the following is printed:

Server: using host 123.456.78.90 port 1099
Registering image server as
  "rmi://123.456.78.90:1099/RemoteImageServer".
Server: Bound RemoteImageServer into
   the registry.

4. Run the Local Application

Run the local application making sure that the serverName parameter of any
RemoteImage constructors corresponds to the machine on which the remote
image server is running. For example, if the machine with IP address
123.456.78.90 above is named myserver the serverName parameter of any
RemoteImage() constructors should be "myserver".

What's New

This section describes the significant changes and improvements to the Java
Advanced Imaging API and its reference implementation since the previous
release.

Core Classes

   * Internal Data Array Recycling

     To improve memory usage, the concept of "array recycling" was developed
     and implemented. Array recycling effectively traps tiles which will no
     longer be used. Subsequent tile, i.e., Raster creation attempts first
     to use the memory "salvaged" from these recycled tiles. Only if this is
     not possible will new data arrays be allocated.

     The internal arrays from recycled tiles are stored in a mapping from
     derived key values to References to the arrays. Thus these arrays are
     subject to garbage collection. The keys in the mapping are derived from
     the data type of the array, the number of data banks, and the size of
     the data banks. Maximal recycling therefore will occur when tiles
     having the same characteristics are used throughout an application.

     The only tiles which are recycled by default are those which are
     created within JAI and also go out of scope within JAI. This occurs for
     example in OpImage implementations which create Rasters to contain data
     cobbled from sources.

     In order to get more tile recycling, some action is required on the
     part of the developer:

       1. Set a RenderingHint.

          A mapping of the key JAI.KEY_CACHED_TILE_RECYCLING_ENABLED should
          be provided to the appropriate rendering hint, e.g., as

              // Enable recycling of cached tiles for all ops in this instance.
              JAI.getDefaultInstance().setRenderingHint(
                  JAI.KEY_CACHED_TILE_RECYCLING_ENABLED,
                  Boolean.TRUE);

              // Enable recycling of tiles for this op alone.
              ParameterBlock paramBlock;
              RenderingHints hints;
              hints.put(JAI.KEY_CACHED_TILE_RECYCLING_ENABLED, Boolean.TRUE);
              RenderedOp someOp = JAI.create("mosaic", paramBlock, hints);

          The effect of this will be to allow all tiles stored in a
          TileCache for a given image to be recycled when that image is
          finalized by the garbage collector.

       2. Invoke dispose() on defunct PlanarImages

          Invoking dispose() will cause the tiles to be flushed from the
          cache and recycled. Care should be taken when performing this
          procedure since unexpected results may occur if an application
          subsequently refers to a disposed image.

          For more information please refer to the documentation of the
          RenderingHints.Key definitions KEY_TILE_FACTORY,
          KEY_TILE_RECYCLER, and KEY_CACHED_TILE_RECYCLING_ENABLED in the
          JAI class, the interface definitions of TileRecycler and
          TileFactory and the class definition of RecyclingTileFactory, as
          well as the constructors of OpImage and PlanarImage, and finally
          the dispose() methods of PlanarImage, RenderedOp and OpImage.

   * Exception Handling

     To improve exception handling, an interface ImagingListener and a new
     class ImagingException are added in the package javax.media.jai.util.
     An ImagingListener provides the possibility to monitor, process,
     recover and log abnormal situations. An ImagingException holds its
     cause, so that its (root) cause can be retrieved. The class
     javax.media.jai.remote.RemoteImagingException is reparented from
     RuntimeException to ImagingException.

     These changes were made in response to customer comments observed on
     the jai-interest mailing list.

   * Type-safe Image create() Methods

     To each OperationDescriptor in the javax.media.jai.operator package was
     added a type-safe static create method for each supported mode of
     operation. The methods added are named as follows:

              Mode                   Method
      rendered            create()
      renderable          createRenderable()
      Collection          createCollection()
      renderableCollectioncreateRenderableCollection()

     These convenience methods behave as the identically named static
     methods in the JAI class. Please refer to
     javax.media.jai.operator.*Descriptor for more details.

     These changes were made in response to customer comments observed on
     the jai-interest mailing list.

   * Expansion of IndexColor Data

     In the previous versions of JAI, the operators provided by JAI operated
     on the image's pixel data only. No color translation was performed
     prior to the actual computation by the operator, regardless of the type
     of the ColorModel an image had. If it was intended to have an operation
     performed on the color data, the color translation needed to be
     performed explicitly by the user prior to invoking the operation.

     This policy has been changed in JAI 1.1.2 to make handling sources with
     IndexColorModels more straightforward. A new RenderingHint
     JAI.KEY_REPLACE_INDEX_COLOR_MODEL has been provided that allows for
     automatic color translation for colormapped imagery in those situations
     where not doing so would result in unexpected / incorrect results (such
     as geometric operations). Operations that are implemented as subclasses
     of javax.media.jai.AreaOpImage and javax.media.jai.GeometricOpImage set
     this RenderingHint to true, such that these operations are performed
     correctly on the colormapped imagery, not treating the indices into the
     color map as pixel data. The user no longer needs to perform color
     translation before invoking such operations.

     For further details, please refer to Javadoc comments for the
     KEY_REPLACE_INDEX_COLOR_MODEL RenderingHints defined in
     javax.media.jai.JAI, Javadoc comments for the javax.media.jai.OpImage
     constructor, class comments for javax.media.jai.AreaOpImage,
     javax.media.jai.GeometricOpImage, and javax.media.jai.ScaleOpImage etc.

     The affected operations are affine, convolve, dilate, erode,
     filteredsubsample, gradientmagnitude, max, median, min, rotate, scale,
     shear, translate, transpose and warp. For further details, please refer
     to the class comments for the OperationDescriptors for these
     operations.

     One of the common uses of the format operator is to cast the pixel
     values of an image to a given data type. In such a case, the format
     operation adds a RenderingHints object for
     JAI.KEY_REPLACE_INDEX_COLOR_MODEL with the value of Boolean.TRUE, if
     the source image provided has an IndexColorModel. Due to the addition
     of this new RenderingHint, using the "format" operation with source(s)
     that have an IndexColorModel will cause the destination to have an
     expanded non-IndexColorModel ColorModel. This expansion ensures that
     the conversion to a different data type, ColorModel or SampleModel
     happens correctly such that the indices into the color map (for
     IndexColorModel images) are not treated as pixel data.

     These changes were made in response to customer comments observed on
     the jai-interest mailing list.

   * Real-valued DataBuffer Portability

     Some problems interoperating with JavaTM 2 version 1.4 were observed
     due to the classes DataBufferFloat and DataBufferDouble which are found
     in Java2D and in JAI. Internal changes in JAI were made such that
     floating point DataBuffers are handled using the reflection API. Users
     of JavaTM 2 v1.4 should use the floating point DataBuffer classes in
     java.awt.image while users of earlier versions of JavaTM 2 should use
     those in JAI. Both should function equivalently.

     These changes were made in response to customer comments observed on
     the jai-interest mailing list.

   * Tile Dimension Clamping

     To optimize storage requirements for the results of an imaging
     operation, as of JAI 1.1.2, the destination image's tile dimensions are
     by default clamped to it's image dimensions when these are smaller, if
     the tile dimensions were not specified by the user or the operation
     itself.

     This change was made in response to customer comments observed on the
     jai-interest mailing list.

   * Suppression of Colormap Acceleration

     In the previous versions of JAI, to accelerate the ColormapOpImages,
     the transform is performed on the color map if the source and
     destination images are both color-indexed. To suppress the
     acceleration, a new type of rendering hint key,
     KEY_TRANSFORM_ON_COLORMAP, is defined. Operations to which this is
     pertinent are

     AddConst
     AndConst
     DivideIntoConst
     Exp
     Invert
     Log
     Lookup
     MultiplyConst
     Not
     OrConst
     Piecewise
     Rescale
     SubtractFromConst
     Threshold
     XorConst

     These changes were made in response to customer comments observed on
     the jai-interest mailing list.

   * Image Coordinate Mapping

     Forward and inverse coordinate mapping methods mapSourcePoint() and
     mapDestPoint(), respectively were added to RenderedOp, OpImage and its
     subclasses, and Warp and its subclasses. These are intended to allow
     mapping from destination to source images within rendered operation
     chains and from source to destination images depending on the
     invertibility of the transformation.

   * Destination Rectangle Mapping

     The specification of the method OpImage.mapDestRect() was clarified in
     general and specifically with respect to the case wherein the source
     rectangle can be determined but has an empty intersection with the
     actual source image bounds.

   * ColorSpaceJAI and IHSColorSpace

     The non-linear transformation of the sRGB color space conversion was
     implemented and the overall specification of the sRGB-CIEXYZ color
     conversion was clarified. The implementation was changed to match this
     specification change.

   * ColorModelFactory

     A new interface javax.media.jai.ColorModelFactory was added to provide
     more fine-grained control over creating the ColorModel of an OpImage.
     For more information please refer to the ColorModelFactory class
     documentation as well as the links to be found to therein.

   * TileCache and TileCache Key Properties

     Properties named "tile_cache_key" and "tile_cache" were added to the
     list of synthetic properties emitted by a RenderedOp. These properties
     have values which are instances of RenderedImage and TileCache,
     respectively. For more information please refer to the class
     documentation of RenderedOp. The purpose of these properties is to
     provide a reliable mechanism for obtaining the cache used by the
     rendering of a RenderedOp as well as the key used therewith.

   * TileScheduler ThreadGroups

     ThreadGroups were added to allow observation of the scheduler threads.
     There is a single ThreadGroup named

     "SunTileScheduler"+instanceIndex

     where instanceIndex is initially zero and is incremented for each
     instance, and a thread group for standard and prefetch threads which
     are respectively named

     "SunTileScheduler"+instanceIndex+"Standard"
     "SunTileScheduler"+instanceIndex+"Prefetch"

   * ImageAdapter Classes

     Added getWrappedImage() method to each of RenderedImageAdapter,
     RenderableImageAdapter, and WritableRenderedImageAdapter.

   * BorderExtenderConstant

     Added getConstants() method.

   * getImageID()

     A new public method getImageID() was added to PlanarImage and
     SerializableRenderedImage. This was in response to a customer-reported
     problem wherein the value returned by hashCode() did not guarantee
     sufficient uniqueness of keys of tiles in the TileCache when the
     application was run for an extended period. The default JAI TileCache
     has been changed to use this new ID type.

Operations

   * New Operations

        o ColorQuantizer

          This operation generates a LookupTableJAI-valued property which
          may be used to quantize the colors in an image via the
          ErrorDiffusion operation. The operation may also be used to apply
          this same colormap to generate a color quantized image using the
          nearest color approach. Color quantization algorithms supported
          are Median Cut, Oct-tree, and Neural Network. Only a Java
          implementation is provided. Please refer to
          javax.media.jai.operator.ColorQuantizer for the details.

          This operation was added in response to customer comments observed
          on the jai-interest mailing list.

        o Mosaic

          This operation combines two or more source images to create a
          montage or image mosaic depending on the supplied parameters. Both
          native and Java implementations are provided. Please refer to
          javax.media.jai.operator.MosaicDescriptor for the details.

        o SubsampleAverage

          This operation subsamples an image by averaging over a
          scale-dependent moving window. Both native and Java
          implementations are provided. Please refer to
          javax.media.jai.operator.SubsampleAverageDescriptor for the
          details. This operation should prove quite useful in generating
          high quality thumbnail images and in scaling down bilevel images
          such as documents.

          This operation was added in response to customer comments observed
          on the jai-interest mailing list.

   * Modifications of Existing Operations

        o Geometric Operations

          A double[]-valued parameter backgroundValues was appended to the
          parameter list of each of the "Affine", "Rotate", "Shear", and
          "Warp" operations. The value so specified will be used to fill all
          pixels in the destination image the locations of which cannot be
          backward mapped to a source image position amenable to
          interpolation.

          These changes were made in response to customer comments observed
          on the jai-interest mailing list.

        o Composite

          The specification of the "composite" operation is updated such
          that the source1Alpha and source1Alpha parameters are
          RenderedImages in the "rendered" mode and are RenderableImages in
          the "renderable" mode.

        o Extrema

          The operation was changed to allow the locations of extrema to be
          obtained.Each location is saved as a run length code. A boolean
          parameter saveLocations indicates whether to save the locations of
          the extrema and an integer parameter maxRuns specifies the maximum
          number of run lengths to save. The locations of minima are
          available on the operation node as a List-valued property named
          minLocations and the locations of the maxima are available as a
          List-valued property named maxLocations.

        o Filestore

          A pair of modifications were made to this opertion.

          Firstly, the FileStore operation formerly threw an
          IllegalArgumentException if the operation was created on a client
          and the path existed only on the server. This was fixed by
          appending a Boolean parameter called checkFileLocally to the
          parameter list. The use of this parameter is self-descriptive.
          Please refer to javax.media.jai.operator.FileStoreDescriptor for
          more information on the operation.

          Secondly, the operation was modified to use a SeekableOutputStream
          so that (TIFF) compression may be effected with less effort.

        o FilteredSubsample

          A scale-dependent default value was added for the quadrant
          symmetric filter parameter. This should improve the usability of
          this operation as the developer no longer is required to derive
          any filter coefficients.

          This operation was modified in response to customer comments
          observed on the jai-interest mailing list.

        o Histogram

          Native acceleration support was added for the cases of
          non-floating point data.

        o Scale

          The specification of the scale operation has been updated such
          that the resultant image dimensions are no longer dependent on the
          interpolation scheme used in the scale operation. Please refer to
          javax.media.jai.operator.ScaleDescriptor and
          javax.media.jai.ScaleOpImage for details on the new specification.

          This operation was modified in response to customer comments
          observed on the jai-interest mailing list.

Network Imaging

   * Server Configuration

     This release provides options for configuring the Remote Imaging server
     for the "jairmi" protocol that is provided with JAI. These server
     configuration parameters can be specified via the command line when
     starting the server. All the command line options are detailed in the
     Javadoc for javax.media.jai.remote.JAIRMIDescriptor

     Another mechanism to perform server configuration is by implemeting the
     JAIServerConfigurationSpi interface and putting the implementation on
     the CLASSPATH. For further details please refer to
     javax.media.jai.remote.JAIRMIDescriptor and
     com.sun.media.jai.remote.JAIServerConfigurationSpi Javadoc.

Widget Package

   * com.sun.media.jai.widget Package

     This package was added containing for the time being the single class
     DisplayJAI which has been previously supplied to the JAI mailing list.
     It is likely that the current content of the deprecated package
     javax.media.jai.widget will be moved to this package eventually.

Fixed Bugs

Many problems originally reported as bugs have actually been fixed by virtue
of the changes described above. Some of the other salient problems which
have also been fixed include the following.

   * Native implementation of the SubsampleBinaryToGray operation throws an
     ArrayIndexOutOfBoundsException.

     This was observed by a customer processing document images.

   * TiledImageGraphics cannot draw objects correctly if a transform is done
     on it.

     This was observed by a customer as a discrepancy with respect to
     Graphics2D.

   * JAI may not work with JRE due to missing default ICC profiles.

     This problem was observed by a customer when using a version of the JRE
     some of which do not include in particular the PYCC profile.

   * ClassCastException for multi-source point ops with non- PlanarImage
     sources.

     This bug was observed by a customer attempting to take the "max" of two
     BufferedImages.

   * Eliminate window pop-up on Windows when using native acceleration.

     Customers reported observing a black window pop up and disappear. This
     was due to the application used to check for MMX availability. This
     application has been modified so that this does not occur.

   * Format operation did not convert an indexed-color image.

     When the Format operation was applied to an image having an
     IndexColorModel and RenderingHints specifying a 24-bit ColorModel and
     compatible SampleModel, the output image did not have the desired
     24-bit ColorModel.

   * RenderableOp.createScaledRendering() did not produce an image with the
     desired dimensions.

     If the scale factors derived internally had a ratio within a tolerance
     of the unity value then one factor was changed. This computation has
     been modified not to change the derived scale factors.

Changes From JAI 1.1.2-beta to JAI 1.1.2-RC

The changes made to JAI between the release of version 1.1.2-beta and
1.1.2-RC are all bug fixes or minor enhancements. Many of these changes were
made in response to customer feedback primarily on the jai-interest mailing
list.

Bugs Fixed in JAI 1.1.2-RC

   * Regression in TIFF LZW decoder in JAI 1.1.1-to-1.1.1_01 transition.

     Some TIFF images when compressed using the LZW algorithm were able to
     be loaded in JAI 1.1.1 but not in JAI 1.1.1_01.

   * TiledImage sources caused ArrayOutOfBoundsExceptions when used in
     statistical operations.

     This problem was observed as of JAI 1.1.1_01. This problem was
     previously reported on the JAI bugs page as follows:
     ArrayOutOfBoundsExceptions may be thrown when PixelAccessor is used
     with Rasters derived from an image when the image and Raster have
     different SampleModels.

   * Code to disable property com.sun.media.jai.disableMediaLib moved ahead
     of native library search.

     This eliminates a warning message being printed.

   * JAI should silence exception on RenderingKeys being re-registered.

     When JAI runs in a server environment the JAI class may be garbage
     collected and reloaded causing re-registration of RenderingKeys. This
     exception should be caught and suppressed. Note that the fix works only
     in version 1.4 or higher of J2SE.

   * Tiles from the "fileload" operation are not recycled.
   * Native acceleration does not support 8-bit to bilevel dithering.
   * Exception occurs when native acceleration used with 3x3 kernel and
     bilevel image.
   * "Extrema" operation incorrectly initializes the extremal values.

     The first pixel of the image was always used to initialize the extremal
     values which is incorrect when the ROI does not include this pixel.

   * "Mosaic" alpha-blend variant has numerical innaccuracies when used with
     native acceleration.

     New methods added to the native libraries allow alpha-blend mosaicking
     to be performed with full precision. This problem was previously
     reported on the JAI bugs page.

   * Native acceleration of dithering fails on Linux.
   * javax.media.jai.operator.MosaicType missing documentation.

Enhancements Added in JAI 1.1.2-RC

   * Native USHORT (unsigned short) acceleration not enabled for many
     operations.

     Although available in the native libraries, acceleration for a number
     of operations had not been enabled for the USHORT data type. The
     operations are as follows:

        o Absolute
        o AddConst
        o Affine (case using InterpolationTable only; nearest neighbor,
          bilinear, and bicubic cases were already accelerated)
        o AndConst
        o BandCombine
        o BandSelect
        o Binarize
        o Clamp
        o Composite
        o Convolve (general case: kernel is non-separable and does not have
          dimensions 3x3 or 5x5; cases with kernels which are separable or
          have dimensions 3x3 or 5x5 were already accelerated)
        o DCT
        o DFT
        o Dilate
        o DivideByConst
        o Divide
        o DivideIntoConst
        o Erode
        o Exp
        o Extrema
        o FilteredSubsample
        o Gradient
        o IDCT
        o IDFT
        o Invert
        o Log
        o Lookup
        o Max
        o MaxFilter
        o Mean
        o MedianFilter
        o Min
        o MinFilter
        o Mosaic
        o MultiplyConst
        o Multiply
        o OrConst
        o Rescale
        o Scale (case using InterpolationTable only; nearest neighbor,
          bilinear, and bicubic cases were already accelerated)
        o SubsampleBinaryToGray
        o SubtractConst
        o SubtractFromConst
        o Threshold
        o Transpose
        o Warp
        o XorConst

   * "Mosaic" operation should allow zero sources.

     The operation was modified to allow zero initial sources. In this case
     an ImageLayout must be provided and must have valid width, height, and
     SampleModel settings. Sources may be added subsequently using rendered
     chain editing.

   * "Mosaic" operation should allow sources with an alpha channel.

     The operation was modified to remove the constraint that the number of
     data bands equal the number of color components. This will allow images
     with an alpha channel and index-color images to be used as sources.

   * source instance variable of com.sun.media.jai.widget.DisplayJAI does
     not have an accessor.

     An accessor getSource() of this variable was added.

   * Alpha image parameters of "Composite" operation should be
     mode-dependent.

     The alpha-image parameters were modified to be RenderableImages in
     renderable mode and RenderedImages in rendered mode. The
     RenderableImage alpha parameters are converted to RenderedImages at
     rendering time using the same RenderContext as used to render the
     operation result.

   * "warp" variants do not set destination bounds.

     Automatic initialization of the "warp" destination image bounds was
     added for the cases of "WarpAffine" and "WarpPerspective" mappings.
     This problem was previously reported on the JAI bugs page.

   * ColormapOpImage should not force destination to have an
     IndexColorModel.

     This was causing problems when the source image had an IndexColorModel
     and it was desired that the destination did not.

   * SerializableRenderedImage javadoc not clear about serializable
     properties.
   * UNINSTALL-jai-{jdk,jre} scripts display "cannot remove" messages for
     non-sparc systems.

Changes From JAI 1.1.2-RC to JAI 1.1.2

The changes made to JAI between the release of version 1.1.2-RC and 1.1.2
are all bug fixes or minor enhancements. Most of these changes were made in
response to customer feedback primarily on the jai-interest mailing list.

Bugs Fixed in JAI 1.1.2

   * JPEG encoder throws ClassCastException.

     This was observed to occur when attempting to write a RenderedOp to a
     file.

   * TIFF decoder sets wrong transparency type for RGBA image.

     When the ExtraSamples field indicated unassociated alpha (not
     premultiplied) the decoder incorrectly set the Transparency to BITMASK
     instead of to TRANSLUCENT.

   * TIFF decoder throws ArrayIndexOutOfBoundsException for some CCITT T.6
     images.

   * WBMP decoder does not properly handle images above 512 bytes.

   * BMP encoder does not correctly handle the BI_BITFIELDS compression
     type.

     Corrected code to handle 555 configuration for 16-bit data and 888
     configuration for 32-bit data.

   * Repeated evaluation of JAI operation chain provokes exception.

     The problem was due to lack of appropriate thread synchronization in
     PlanarImage.getSinks().

   * The server thread in SerializableRenderedImage is non-daemon.

     The thread in question was changed to a daemon thread.
   * Performance degradation compared to JAI versions 1.1.1 and 1.1.1_01
     when processing floating point data and using J2SE 1.3*.

     This problem was found to be due to a lack of optimization in the
     implementation added to solve the problem described in the above
     section Real-valued DataBuffer Portability.

   * "Mosaic" operation should always use bounds provided in ImageLayout
     hint.

     If bounds (minX, minY, width, height) are provided via an ImageLayout
     in a RenderingHints object passed to the operation, then these should
     be used even if they do not intersect the union of the bounds of all
     sources of the operation.

   * "Mosaic" operation native implementation crashes VM when using
     MosaicDescriptor.MOSAIC_TYPE_BLEND on Solaris.

   * RuntimeException: "Connection reset by peer: JVM_recv in socket input"
     observed when using remote imaging.

     This problem was determined to be a socket communication timing issue
     in SerializableRenderedImage when socket connections are used to
     transfer image data: the client was closing its socket connection
     before the server finished reading the close message.

   * The values of KernelJAI.GRADIENT_MASK_SOBEL_HORIZONTAL and
     KernelJAI.GRADIENT_MASK_SOBEL_VERTICAL were interchanged.

   * "Subsampleaverage" operation pure java implementation crops image
     leaving a black area at the right.

Enhancements Added in JAI 1.1.2

   * PNG codec should support ICC profiles and better handle color metadata.

     Several modifications were made to classes in the package
     com.sun.media.jai.codec:
        o ImageCodec

          Added version of createComponentColorModel() which accepts a
          ColorSpace parameter for the ColorModel to be created.

        o PNGDecodeParam

          Changed default behavior not to perform gamma correction, i.e.,
          the default of the value returned by getPerformGammaCorrection()
          is false.

        o PNGEncodeParam

          Added methods getICCProfileName() and setICCProfileName().

Copyright 2002 Sun Microsystems, Inc. All rights reserved. Use is subject to
license terms. Third-party software, including font technology, is
copyrighted and licensed from Sun suppliers. Portions may be derived from
Berkeley BSD systems, licensed from U. of CA. Sun, Sun Microsystems, the Sun
logo, Java, and Solaris are trademarks or registered trademarks of Sun
Microsystems, Inc. in the U.S. and other countries. Federal Acquisitions:
Commercial Software - Government Users Subject to Standard License Terms and
Conditions.

Copyright 2002 Sun Microsystems, Inc. Tous droits r�serv�s. Distribu� par
des licences qui en restreignent l'utilisation. Le logiciel d�tenu par des
tiers, et qui comprend la technologie relative aux polices de caract�res,
est prot�g� par un copyright et licenci� par des fournisseurs de Sun. Des
parties de ce produit pourront �tre d�riv�es des syst�mes Berkeley BSD
licenci�s par l'Universit� de Californie. Sun, Sun Microsystems, le logo
Sun, Java, et Solaris sont des marques de fabrique ou des marques d�pos�es
de Sun Microsystems, Inc. aux Etats-Unis et dans d'autres pays.
