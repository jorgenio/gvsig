#!/bin/sh
# gvSIG.sh
# Script de arranque de gvSIG para paquetes deb (directorio de instalación /opt/gvSIG-1.9)
export GVSIG_LIBS="/opt/gvSIG-1.9/libs:/opt/gvSIG-1.9/libs/gdal:/opt/gvSIG-1.9/libs/proj:/opt/gvSIG-1.9/libs/ecw"
export GRASS_LIB="/usr/lib/grass64/lib"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$GVSIG_LIBS:$GRASS_LIB"
export PROJ_LIB="/opt/gvSIG-1.9/bin/gvSIG/extensiones/org.gvsig.crs/data"
export GDAL_DATA="/usr/share/gdal15"
cd "/opt/gvSIG-1.9/bin"

for i in ./lib/*.jar ; do
  LIBRARIES=$LIBRARIES:"$i"
done
for i in ./lib/*.zip ; do
  LIBRARIES=$LIBRARIES:"$i"
done

/usr/lib/jvm/java-1.5.0-sun/bin/java -Djava.library.path=/usr/lib:/opt/gvSIG-1.9/libs:/opt/gvSIG-1.9/libs/gdal:/opt/gvSIG-1.9/libs/proj:/opt/gvSIG-1.9/libs/ecw -cp andami.jar$LIBRARIES -Xmx500M com.iver.andami.Launcher gvSIG gvSIG/extensiones "$@"

