#!/bin/sh

cd "$INSTALL_PATH/libs"

rm -f libjecw.so libjecw.so.0
ln -s libjecw.so.0.0.7 libjecw.so
ln -s libjecw.so.0.0.7 libjecw.so.0

rm -f libNCSCnet.so libNCSCNet.so libNCSCnet.so.0 libNCSCNet.so.0 libNCScnet.so.0
ln -s libNCScnet.so libNCSCnet.so
ln -s libNCScnet.so libNCSCNet.so
ln -s libNCScnet.so libNCSCnet.so.0
ln -s libNCScnet.so libNCSCNet.so.0
ln -s libNCScnet.so libNCScnet.so.0


rm -f libNCSEcwC.so.0
ln -s libNCSEcwC.so libNCSEcwC.so.0

rm -f libNCSEcw.so.0
ln -s libNCSEcw.so libNCSEcw.so.0

rm -f libNCSUtil.so.0
ln -s libNCSUtil.so libNCSUtil.so.0