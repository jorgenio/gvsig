v=$(xdg-user-dir DESKTOP)
USER_HOME=$(xdg-user-dir)

if [  $v != "$USER_HOME/Desktop" ]; then
	if [ -f "$USER_HOME/Desktop/gvSIG.desktop" ]; then
		sed '/^Path=/d' $USER_HOME/Desktop/gvSIG.desktop > $v/gvSIG.desktop
		chmod a+r $v/gvSIG.desktop
		rm -f $USER_HOME/Desktop/gvSIG.desktop
	fi
	if [ -f "$USER_HOME/Desktop/gvSIG Uninstaller.desktop" ]; then
		sed '/^Path=/d' "$USER_HOME/Desktop/gvSIG Uninstaller.desktop" > "$v/gvSIG Uninstaller.desktop"
		chmod a+r "$v/gvSIG Uninstaller.desktop"
		rm -f "$USER_HOME/Desktop/gvSIG Uninstaller.desktop"
	fi
fi