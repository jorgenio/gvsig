#!/bin/sh
cd "$INSTALL_PATH/libs"
rm -f libcrypt.so
ln -s libcrypt.so.1 libcrypt.so

rm -f libcrypto.so libcrypto.so.0
ln -s libcrypto.so.0.9.7 libcrypto.so
ln -s libcrypto.so.0.9.7 libcrypto.so.0

rm -f libgdal.so libgdal.so.1
ln -s libgdal1.5.0.so.1 libgdal.so
ln -s libgdal1.5.0.so.1 libgdal.so.1

rm -f libjasper-1.701.so
ln -s libjasper-1.701.so.1 libjasper-1.701.so

rm -f libjgdal.so libjgdal.so.0
ln -s libjgdal.so.0.9.2 libjgdal.so
ln -s libjgdal.so.0.9.2 libjgdal.so.0

rm -f libjpeg.so
ln -s libjpeg.so.62 libjpeg.so

rm -f libm.so
ln -s libm.so.6 libm.so

rm -f libnsl.so
ln -s libnsl.so.1 libnsl.so

rm -f libodbc.so
ln -s libodbc.so.1 libodbc.so

rm -f libpng.so
ln -s libpng.so.3 libpng.so

rm -f libpq.so libpq.so.5
ln -s libpq.so.5.1 libpq.so.5
ln -s libpq.so.5.1 libpq.so.3
ln -s libpq.so.5.1 libpq.so

rm -f libssl.so libssl.so.0
ln -s libssl.so.0.9.7 libssl.so
ln -s libssl.so.0.9.7 libssl.so.0

rm -f libz.so
ln -s libz.so.1 libz.so
