#!/bin/sh

cd "$INSTALL_PATH/libs"

rm -f libgrass_datetime.so
ln -s libgrass_datetime.6.0.2.so libgrass_datetime.so

rm -f libgrass_dbmibase.so
ln -s libgrass_dbmibase.6.0.2.so libgrass_dbmibase.so

rm -f libgrass_dbmiclient.so
ln -s libgrass_dbmiclient.6.0.2.so libgrass_dbmiclient.so

rm -f libgrass_dgl.so
ln -s libgrass_dgl.6.0.2.so libgrass_dgl.so

rm -f libgrass_dig2.so
ln -s libgrass_dig2.6.0.2.so libgrass_dig2.so



rm -f libgrass_gis.so
ln -s libgrass_gis.6.0.2.so libgrass_gis.so

rm -f libgrass_gmath.so
ln -s libgrass_gmath.6.0.2.so libgrass_gmath.so

rm -f libgrass_gproj.so
ln -s libgrass_gproj.6.0.2.so libgrass_gproj.so

rm -f libgrass_I.so
ln -s libgrass_I.6.0.2.so libgrass_I.so

rm -f libgrass_linkm.so
ln -s libgrass_linkm.6.0.2.so libgrass_linkm.so

rm -f libgrass_rtree.so
ln -s libgrass_rtree.6.0.2.so libgrass_rtree.so

rm -f libgrass_vask.so
ln -s libgrass_vask.6.0.2.so libgrass_vask.so

rm -f libgrass_vect.so
ln -s libgrass_vect.6.0.2.so libgrass_vect.so
