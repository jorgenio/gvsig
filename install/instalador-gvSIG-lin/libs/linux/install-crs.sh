#!/bin/sh

VER=0.6.0
VERPROJ=0.5.0
VERJNIPROJ=0.1.1

cd "$INSTALL_PATH/libs"

rm -f libproj.so.0 libproj.so
ln -s libproj.so.$VERPROJ libproj.so
ln -s libproj.so.$VERPROJ libproj.so.0

rm -f libcrsjniproj.so.0 libcrsjniproj.so
ln -s libcrsjniproj.so.$VERJNIPROJ libcrsjniproj.so
ln -s libcrsjniproj.so.$VERJNIPROJ libcrsjniproj.so.0
