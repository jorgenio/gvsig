FasdUAS 1.101.10   ��   ��    k             l     ��  ��     set JAIIO_installed to      � 	 	 . s e t   J A I I O _ i n s t a l l e d   t o     
  
 l     ��  ��     set better_uninstall to     �   . s e t   b e t t e r _ u n i n s t a l l   t o      l     ����  r         I    �� ��
�� .sysolocSutxt        TEXT  m        �    K e e p   I t��    o      ���� 0 but1  ��  ��        l    ����  r        I   �� ��
�� .sysolocSutxt        TEXT  m    	   �    U n i n s t a l l &��    o      ���� 0 but2  ��  ��         l   ( !���� ! I   (�� " #
�� .sysodisAaleR        TEXT " l    $���� $ I   �� %��
�� .sysolocSutxt        TEXT % m     & & � ' ' 0 J A I - I m a g e I O   i s   i n s t a l l e d��  ��  ��   # �� ( )
�� 
mesS ( l    *���� * I   �� +��
�� .sysolocSutxt        TEXT + m     , , � - -b P r e v i o u s   g v S I G   v e r s i o n s   n e e d e d   i t   a n d   o f f e r e d   t o   i n s t a l l   i t .   B u t   i t   h a s   b e e n   f o u n d   t o   c a u s e   p r o b l e m s   w i t h   J a v a   a p p l e t s   i n   w e b   b r o w s e r s   l i k e   F i r e f o x ,   C a m i n o   a n d   O p e r a .   T h e r e f o r e ,   w e   r e c o m m e n d   J A I - I m a g e I O   t o   b e   u n i n s t a l l e d .   C u r r e n t   a n d   f u t u r e   v e r s i o n s   o f   g v S I G   d o   n o t   n e e d   i t .   D o   y o u   w a n t   t o   u n i n s t a l l   i t ?��  ��  ��   ) �� . /
�� 
btns . J      0 0  1 2 1 o    ���� 0 but1   2  3�� 3 o    ���� 0 but2  ��   / �� 4 5
�� 
dflt 4 o   ! "���� 0 but2   5 �� 6��
�� 
as A 6 m   # $��
�� EAlTwarN��  ��  ��      7 8 7 l     ��������  ��  ��   8  9 : 9 l  ) , ;���� ; r   ) , < = < 1   ) *��
�� 
rslt = o      ���� 0 	theresult 	theResult��  ��   :  > ? > l     ��������  ��  ��   ?  @ A @ l  - r B���� B Z   - r C D�� E C =  - 4 F G F n   - 2 H I H 1   . 2��
�� 
bhit I o   - .���� 0 	theresult 	theResult G o   2 3���� 0 but2   D k   7 @ J J  K L K I  7 >�� M��
�� .sysoexecTEXT���     TEXT M m   7 : N N � O O X r m   ~ / L i b r a r y / J a v a / E x t e n s i o n s / j a i _ i m a g e i o . j a r��   L  P�� P l  ? ?�� Q R��   Q � �display alert (localized string "Uninstalled correctly.") message (localized string "JAI-ImageIO has been uninstalled.") as informational    R � S S d i s p l a y   a l e r t   ( l o c a l i z e d   s t r i n g   " U n i n s t a l l e d   c o r r e c t l y . " )   m e s s a g e   ( l o c a l i z e d   s t r i n g   " J A I - I m a g e I O   h a s   b e e n   u n i n s t a l l e d . " )   a s   i n f o r m a t i o n a l��  ��   E k   C r T T  U V U r   C L W X W I  C J�� Y��
�� .sysolocSutxt        TEXT Y m   C F Z Z � [ [  L a u n c h   g v S I G��   X o      ���� 0 but1   V  \ ] \ l  M M�� ^ _��   ^ 8 2set but2 to localized string "Ask Again Next Time"    _ � ` ` d s e t   b u t 2   t o   l o c a l i z e d   s t r i n g   " A s k   A g a i n   N e x t   T i m e " ]  a b a I  M h�� c d
�� .sysodisAaleR        TEXT c l  M T e���� e I  M T�� f��
�� .sysolocSutxt        TEXT f m   M P g g � h h J J A I - I m a g e I O   h a s   n o t   b e e n   u n i n s t a l l e d .��  ��  ��   d �� i j
�� 
mesS i l 	 U \ k���� k l  U \ l���� l I  U \�� m��
�� .sysolocSutxt        TEXT m m   U X n n � o o^ I f   y o u   w a n t   t o   u n i n s t a l l   i t   i n   a   f u t u r e ,   y o u   c a n   d o   s o   b y   h a n d .   T o   u n i n s t a l l ,   d e l e t e   t h e   f i l e   j a i _ i m a g e i o . j a r   c o n t a i n e d   i n :   y o u r   H o m e   f o l d e r   - >   L i b r a r y   - >   J a v a   - >   E x t e n s i o n s .��  ��  ��  ��  ��   j �� p q
�� 
btns p J   ] ` r r  s�� s o   ] ^���� 0 but1  ��   q �� t u
�� 
dflt t o   a b���� 0 but1   u �� v��
�� 
as A v m   c d��
�� EAlTwarN��   b  w x w l  i i��������  ��  ��   x  y z y l  i i�� { |��   {  set theResult to result    | � } } . s e t   t h e R e s u l t   t o   r e s u l t z  ~  ~ l  i i�� � ���   � 2 ,if button returned of theResult is but1 then    � � � � X i f   b u t t o n   r e t u r n e d   o f   t h e R e s u l t   i s   b u t 1   t h e n   � � � I  i p�� ���
�� .sysoexecTEXT���     TEXT � m   i l � � � � � � d e f a u l t s   w r i t e   ~ / g v S I G / l a u n c h e r   " K e e p   i n s t a l l e d   J A I - I m a g e I O "   - b o o l   y e s��   �  ��� � l  q q�� � ���   �  end if    � � � �  e n d   i f��  ��  ��   A  ��� � l     ��������  ��  ��  ��       �� � � � � ���   � ��������
�� .aevtoappnull  �   � ****�� 0 but1  �� 0 but2  �� 0 	theresult 	theResult � �� ����� � ���
�� .aevtoappnull  �   � **** � k     r � �   � �   � �   � �  9 � �  @����  ��  ��   �   �  ���� �� &�� ,������������������ N�� Z g n �
�� .sysolocSutxt        TEXT�� 0 but1  �� 0 but2  
�� 
mesS
�� 
btns
�� 
dflt
�� 
as A
�� EAlTwarN�� 
�� .sysodisAaleR        TEXT
�� 
rslt�� 0 	theresult 	theResult
�� 
bhit
�� .sysoexecTEXT���     TEXT�� s�j E�O�j E�O�j ��j ���lv����� O�E�O�a ,�  a j OPY 1a j E�Oa j �a j ��kv����� Oa j OP � � � �  E j e c u t a r   g v S I G � � � �  D e s i n s t a l a r & � �� ���
�� 
bhit � � � �  N o   D e s i n s t a l a r��  ascr  ��ޭ