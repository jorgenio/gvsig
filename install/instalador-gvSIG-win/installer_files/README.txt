gvSIG Desktop v1.11.

 - ©gvSIG Association
 - Software under GNU/GPL license (http://www.fsf.org/licensing/licenses/gpl.html)
 - Contact: gvsig@gvsig.org


Application Requirements:

 System
  - At least: Intel/AMD - 512 MB RAM.
  - Operative Systems: platforms Windows and Linux. Tested in Windows XP, Linux Ubuntu/Debian.

 Installed software (available at http://www.gvsig.gva.es o en http://java.sun.com)
  - Java VM 1.5.0 (12 or above).

Further information and support:

 - Official web site: http://www.gvsig.org

 - Professional support: http://www.gvsig.com

 - Mailing lists: http://www.gvsig.org/web/home/community/mailing-lists