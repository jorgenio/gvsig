��    ,      |  ;   �      �     �     �     �                     +     C     c  6   o  g   �          '  1   4  A   f  D   �     �  -   
  !   8     Z     l     {     �     �     �     �     �     �  #      +   $     P     l       #   �     �  %   �  :     =   <  :   z  ,   �  @   �     #	     :	  e  H	     �
  !   �
     �
     �
               0  &   O     v  6   �  m   �     *     =  4   I  D   ~  G   �  (     4   4  %   i     �     �     �     �     �     �     �          .  0   N  8     %   �     �  #   �  +        A  "   \  I     I   �  F     ,   Z  M   �     �     �     %         	                                                  *             #   +                    $   '      "   &   ,                    )                           
         !      (                   %s
Java %s recommended %s Install-Launcher %s Launcher %s. Please wait... Cancel Canceled by user Checking JAI Library... Checking JAI imageIO Library... Checking... Do you really want to cancel the installation process? Do you want to check the application requirements? 
This will install missing components (Recommended). Do you want to continue? Downloading  Error copying the file number %i:
 '%s' --> '%s'. Error copying the file number %i:
 missing source or target entry Error copying the file number %i:
 source file '%s' does not exists. Failed checking JVM version. Installation requires to download this file:
 Installing JAI imageIO Library... Installing JRE... Intializing... JAI Library JRE not found. Java JRE version  Launcher Launching application... Launching installation... Please, select the Java VM. Preparing to install JAI Library... Preparing to install JAI imageIO Library... Preparing to install JRE... Searching JRE's... The JAI could not be setup. The JAI imageIO could not be setup. The JRE could not be setup. The command
%s
could not be executed. The configuration file '%s' contains a invalid mode entry. The configuration file '%s' does not contain a command entry. The configuration file '%s' does not contain a mode entry. The configuration file '%s' does not exists. The file entry 'jre_home' can not be empty when 'doChecks = No'. Updating the system... copying %s... Project-Id-Version: gvSIG-IzPack Launcher
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-02-16 12:34+0100
PO-Revision-Date: 2007-02-16 12:37+0100
Last-Translator: Jose Manuel Vivó (chema) <josemanuel.vivo@iver.es>
Language-Team:  <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Poedit-Country: SPAIN
 %s
Java %s recomendado Lanzador de la instalación de %s Lanzador de %s %s. Por favor espere... Cancelar Cancelado por el usuario Comprobando la libreria JAI... Comprobando la libreria JAI imageIO... Comprobando... ¿Está seguro de cancelar el proceso de instalación? ¿Desea comprobar los requisitos de la aplicación? 
Se instalarán los componentes que falten (Recomendado). ¿Desea continuar? Descargando Error copiando el fichero numero %i:
 '%s' --> '%s'. Error copying the file number %i:
 falta la entrada  source o target Error copiando el fichero numero %i:
 el fichero origen '%s' no existe. Fallo comprobando la versión de la JVM. La instancion requiere la descarga el este fichero:
 Instalando la libreria JAI ImageIO... Instalando el JRE... Inicializando... Libreria JAI La JRE no se encontró. Version de Java JRE  Lanzador Lanzando la aplicacion... Lanzando la instalación... Por favor, selecion la Java VM. Preparando la instalación de la libreria JAI... Preparando la instalación de la libreria JAI ImageIO... Preparando la instalación del JRE... Buscando el JRE... La libreria JAI no pudo instalarse. La libreria JAI imageIO no pudo instalarse. No pudo instalarse el JRE. El comando
%s
no se pudo ejecutar. El fichero de configuración '%s' contiene una entrada de modo inválida. El fichero de configuración '%s' no contiene una entrada con el comando. El fichero de configuración '%s' no contiene una entrada con el modo. El fichero de configuración '%s' no existe. La entrada 'jre_home del fichero no puede estar vacia cuando 'doChecks = No'. Actualizando el sistema copiando %s... 