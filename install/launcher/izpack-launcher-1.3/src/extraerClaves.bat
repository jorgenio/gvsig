@echo off
color F0
PROMPT=$g

echo Extraemos las calves de los ficheros cpp...
gettext\bin\xgettext -C -n -k_ -o launcher.pot launcher.cpp statusdialog.cpp

echo Fusionando con Idioma da...
IF NOT EXIST da\launcher.po copy launcher.pot da\launcher.po
gettext\bin\msgmerge.exe da\launcher.po launcher.pot -o da\launcher.po

echo Fusionando con Idioma de...
IF NOT EXIST de\launcher.po copy launcher.pot de\launcher.po
gettext\bin\msgmerge.exe de\launcher.po launcher.pot -o de\launcher.po

echo Fusionando con Idioma es...
IF NOT EXIST es\launcher.po copy launcher.pot es\launcher.po
gettext\bin\msgmerge.exe es\launcher.po launcher.pot -o es\launcher.po

echo Fusionando con Idioma fr...
IF NOT EXIST fr\launcher.po copy launcher.pot fr\launcher.po
gettext\bin\msgmerge.exe fr\launcher.po launcher.pot -o fr\launcher.po

echo Fusionando con Idioma it...
IF NOT EXIST it\launcher.po copy launcher.pot it\launcher.po
gettext\bin\msgmerge.exe it\launcher.po launcher.pot -o it\launcher.po

echo Fusionando con Idioma nl...
IF NOT EXIST nl\launcher.po copy launcher.pot nl\launcher.po
gettext\bin\msgmerge.exe nl\launcher.po launcher.pot -o nl\launcher.po

echo Fusionando con Idioma pt-BR...
IF NOT EXIST pt-BR\launcher.po copy launcher.pot pt-BR\launcher.po
gettext\bin\msgmerge.exe pt-BR\launcher.po launcher.pot -o pt-BR\launcher.po

echo Termiando.
