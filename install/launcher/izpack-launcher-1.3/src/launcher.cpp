/* Copyright (c) 2004 Julien Ponge - All rights reserved.
 * Some windows 98 debugging done by Dustin Sacks.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <wx/string.h>
#include <wx/file.h>
#include <wx/filename.h>
#include <wx/url.h>
#include <wx/process.h>
#include <wx/dir.h>

#include "launcher.h"

#include "myIniReader.h"

#ifdef __UNIX__
  #include <stdlib.h>
  #include <map>
  #include <list>
#endif


/*
 * Helper function to run an external program. It takes care of the subtule
 * differences between the OS-specific implementations.
 *
 * @param cmd The command to run.
 * @return <code>true</code> in case of a success, <code>false</code> otherwise.
 */
bool run_external(wxString cmd)
{
 int code = wxExecute(cmd, wxEXEC_SYNC);
 return (code == 0);

}

bool run_external_async(wxString cmd)
{
 return (wxExecute(cmd, wxEXEC_ASYNC) != 0);
}

//wxString escape_backSlash(wxString aStr){
//  aStr.Replace("\\","\\\\");
//  return aStr;
//}

bool string_to_bool(wxString value, bool defaultValue){
 bool returnValue = defaultValue;
 if (value != wxEmptyString)
    if (
       value.CmpNoCase("s")   == 0  ||
       value.CmpNoCase("si")  == 0  ||
       value.CmpNoCase("1")   == 0  ||
       value.CmpNoCase("y")   == 0  ||
       value.CmpNoCase("yes") == 0
       ) 
    {
       returnValue = true;
    } else {
       returnValue = false;
    }
  return returnValue;
}

long LAUNCHAPP_TIMER_ID = wxNewId();

BEGIN_EVENT_TABLE(LauncherApp, wxApp)
    EVT_TIMER(LAUNCHAPP_TIMER_ID, LauncherApp::OnTimer)
END_EVENT_TABLE()


/* Main Application $Revision: 13364 $
 *
 * $Id: launcher.cpp 13364 2007-08-29 06:14:41Z mhaloui $
 */
LauncherApp::LauncherApp()
  : wxApp()
{
  APPLICATION_NAME = wxString(_("Launcher"));
  completed = false;

  SetAppName(_(APPLICATION_NAME));
  //loadParams();
}

void LauncherApp::echo(const wxString &msg) 
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK);
  dlg.ShowModal();
}

void LauncherApp::notifyToUser(const wxString &msg) 
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxICON_INFORMATION);
  dlg.ShowModal();
}


LauncherApp::~LauncherApp()
{

}

void LauncherApp::loadParams()
{
  //cfgName = wxString( "launcher.ini" );
  cfgName =wxString(argv[0]);
  wxFileName execFileName(cfgName);
  execFileName.SetExt("ini");
  cfgName = execFileName.GetFullPath();
  if (!wxFileExists(cfgName)) {    
    error(cfgName.Format(_("The configuration file '%s' does not exists."),cfgName.c_str()));
  }

  //wxFileInputStream in( cfgName );
  //wxFileConfig cfg( in );
  wxString downloadEnabled;
  wxString launchJarAsync;
  wxString askForCheckingProcess;
  wxString doChecks;
  wxString checksJREExecutionTerminationCode;
  wxString checksJai;
  wxString checksJaiIo;

  paramsApplicationName = readPathFromINI( "appname","gvSIG");
  paramsJar = readPathFromINI( "jar",wxEmptyString);
  paramsCommand = readPathFromINI( "command",wxEmptyString);
  paramsJre = readPathFromINI( "jre",   wxEmptyString);
  paramsJreDownload = readFromINI( "downloadJre", wxEmptyString);
  checksJai = readFromINI( "checkJai",wxEmptyString);
  paramsJai = readPathFromINI( "jai",wxEmptyString);
  paramsJaiDownload = readFromINI( "downloadJai",        wxEmptyString);
  checksJaiIo = readFromINI( "checkJai_io",wxEmptyString);
  paramsJaiIo = readPathFromINI( "jai_io",wxEmptyString);
  paramsJaiIoDownload = readFromINI( "downloadJai_io",wxEmptyString);
  paramsJreVersion = readFromINI( "jre_version",wxEmptyString);
  paramsJreVersionPrefered = readFromINI( "jre_version_prefered",wxEmptyString);
  downloadEnabled = readFromINI( "downloadEnabled",wxEmptyString);
  launchJarAsync = readFromINI( "launchJarAsync", wxEmptyString);
  askForCheckingProcess = readFromINI( "askForCheckingProcess", wxEmptyString);
  doChecks = readFromINI( "doChecks",wxEmptyString);
  checksJREExecutionTerminationCode = readFromINI( "checksJREExecutionTerminationCode",wxEmptyString);
  paramsJreHome = readPathFromINI( "jre_home", wxEmptyString);
  paramsLaunchMode = readPathFromINI( "launchMode", wxEmptyString);

  
  //echo(paramsCommand);

  //procesamos el parametro booleano 'downloadEnabled'
  paramsEnabledDownload =string_to_bool(downloadEnabled,false);
  //procesamos el parametro booleano 'launchJarAsync'
  paramsLaunchJarAsync =string_to_bool(launchJarAsync,false);
  //procesamos el parametro booleano 'askForCheckingProcess'
  paramsAskForCheckingProcess=string_to_bool(askForCheckingProcess,true);
  //procesamos el parametro booleano 'askForCheckingProcess'
  paramsDoChecks=string_to_bool(doChecks,true);
  //procesamos el parametro booleano 'checksJREExecutionTerminationCode'
  paramsChecksJREExecutionTerminationCode=string_to_bool(checksJREExecutionTerminationCode,true);
  paramsCheckJai=string_to_bool(checksJai,true);
  paramsCheckJaiIo=string_to_bool(checksJaiIo,true);

  
  if (paramsCommand == wxEmptyString )
  {
    error(cfgName.Format(_("The configuration file '%s' does not contain a command entry."),cfgName.c_str()));
  }
  if (paramsLaunchMode == wxEmptyString )
  {
    error(cfgName.Format(_("The configuration file '%s' does not contain a mode entry."),cfgName.c_str()));
  }
  paramsLaunchMode = paramsLaunchMode.MakeUpper();
  if ((!paramsLaunchMode.IsSameAs("APPLICATION")) && (!paramsLaunchMode.IsSameAs("INSTALL"))) {
	error(cfgName.Format(_("The configuration file '%s' contains a invalid mode entry."),cfgName.c_str()));
  }
  
  if (paramsJreVersionPrefered == wxEmptyString ) {
     paramsJreVersionPrefered = wxString(paramsJreVersion);
  }
  if ((!paramsDoChecks) && paramsJreHome == wxEmptyString) {
     error(cfgName.Format(_("The file entry 'jre_home' can not be empty when 'doChecks = No'."),cfgName.c_str()));
  }



  variables = sectionKeysFromINI("Variables");

  variablesValues = sectionKeysValuesFromINI("Variables",variables);


  size_t i;
  environVariables = sectionKeysFromINI("EnvironVariables");
  environVariablesValues = sectionKeysValuesFromINI("EnvironVariables",environVariables);
  // Hacemos sustituciones a los valores
  for (i=0; i < environVariables.GetCount() ;i++) {
	 environVariablesValues[i]=parseString(environVariablesValues[i]); 
  }

  //Las cargamos como variables de entorno
  for (i=0; i < environVariables.GetCount() ;i++) {
	 wxSetEnv(environVariables[i], environVariablesValues[i]); 
  }


  classpathVariables = sectionKeysFromINI("classpath");
  classpathValues = sectionKeysValuesFromINI("classpath",classpathVariables);

  calculateClasspath(classpathValues);

}


void LauncherApp::calculateClasspath(wxArrayString ClassPathValues)
{
  wxDir dir;
  size_t i,j;
  wxString Pattern;
  wxString libPath;
  wxString newPath;
  wxString filename;
  bool cont;
 for(i=0 ; i<ClassPathValues.GetCount() ; i++){

	libPath = parseString(ClassPathValues[i]);


	if(dir.Exists(libPath)){
	
		dir.Open(libPath);
		if (!(dir.IsOpened())){
			// deal with the error here - wxDir would already log an error message
			// explaining the exact reason of the failure

			return;
			//Se encarga Automaticamente de avisarnos del error en caso de fallo
		}

		classpath = classpath + "\"" + libPath+ "\";" ; // Se van a�adiendo los .zip y .jar 	
		

	}

	else {
		for(j=libPath.Len()-1; j>=0 ; j--){
			if(libPath[j]==92 || libPath[j]==47 ) break;
		}
		if (j!=0){
			Pattern=libPath.Mid(j+1,wxSTRING_MAXLEN);
			newPath=libPath.Mid(0,j+1);
			dir.Open(newPath);
			if(!(dir.IsOpened())) {
				return;
			}
			cont = dir.GetFirst(&filename,Pattern,wxDIR_FILES);
			while (cont) {
				classpath = classpath + "\"" + newPath + filename.c_str() + "\";" ;
				cont = dir.GetNext(&filename);
			}
		}
	}	
	
 }
 classpath.RemoveLast(); //Se borra el ultimo ";"

}

void LauncherApp::error(const wxString &msg)
{
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxICON_ERROR);
  dlg.ShowModal();
  printf(msg.c_str());
  exit(1);
}

void LauncherApp::confirm(const wxString &msg)
{
  int response;
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME), wxOK | wxCANCEL | wxICON_QUESTION );
  response = dlg.ShowModal();
  if (response != wxID_OK) {
    notifyToUser(_("Canceled by user"));
    exit(1);
  }
}
bool LauncherApp::confirmYesNoCancel(const wxString &msg,const int yesDefault = true)
{
  int response;
  long maskDefault;
  if (yesDefault) {
     maskDefault = wxYES_DEFAULT | wxYES_NO | wxCANCEL | wxICON_QUESTION;
  } else {
     maskDefault = wxNO_DEFAULT | wxYES_NO | wxCANCEL | wxICON_QUESTION;       
  }
  wxMessageDialog dlg(0, msg, _(APPLICATION_NAME),  maskDefault);
  response = dlg.ShowModal();
  if (response != wxID_YES && response != wxID_NO) {
    notifyToUser(_("Canceled by user"));
    exit(1);
  }
  return (response == wxID_YES);
 
}

void LauncherApp::showStatusWindow(){
  if (isStatusDialogVisible) {
    return;
  }
  
  statusDialog = new StatusDialog( APPLICATION_NAME );

  statusDialog->Centre();
  statusDialog->Show(TRUE);
  isStatusDialogVisible = true;
}


bool LauncherApp::OnInit()
{
  isStatusDialogVisible = false;
  exeFileName = wxFileName(argv[0]);
  exePath = exeFileName.GetPath();
  
  wxSetWorkingDirectory(exePath.GetFullPath());
  
  locale.Init();
  locale.AddCatalog("launcher");
  loadParams();  
  
  
  if (paramsLaunchMode.IsSameAs("INSTALL")){  
	APPLICATION_NAME = wxString(APPLICATION_NAME.Format(_("%s Install-Launcher"),paramsApplicationName.c_str()));
	SetAppName(_(APPLICATION_NAME));
	showStatusWindow();
  } else {
	APPLICATION_NAME = wxString(APPLICATION_NAME.Format(_("%s Launcher"),paramsApplicationName.c_str()));
	SetAppName(_(APPLICATION_NAME));
  }
  
  showStatusMsg(_("Intializing..."));
  
  myTimer = new wxTimer(this,LAUNCHAPP_TIMER_ID);
  myTimer->Start(150,true);
  this->MainLoop();
  return true;
}

void LauncherApp::fixSystemJREConfig(){
  showStatusMsg(_("Updating the system..."));
  if (localVersionToUse.Len() <= 5){
	  // Estamos usando la rama x.x o x.x.x, no hace falta arreglar nada
	  return;
  }
  //actualizamos CurrentVersion
  wxString baseKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft"
                     "\\Java Runtime Environment\\";
                     
  wxString genericVersion = paramsJreVersion.Left(3);
  
  wxString baseKeyGeneric = baseKey + genericVersion + "\\";
  wxString baseKeyVersion = baseKey + localVersionToUse + "\\";

  wxRegKey *pRegKey = new wxRegKey(baseKey);
  if( !pRegKey->Exists() ) {
    error(_("JRE not found."));
  }
  
  // compiamos el contenido de la rama de la version
  // que queremos a la generica (1.4 o 1.5 o ...)
  wxRegKey *pRegKeyGeneric = new wxRegKey(baseKeyGeneric);
  wxRegKey *pRegKeyVersion = new wxRegKey(baseKeyVersion);
  
  if ( !pRegKeyGeneric->Exists() ){
    pRegKeyGeneric->Create();  
  }
  wxString tempKey;
  wxString tempValue;
  size_t nValues;
  
  pRegKeyVersion->GetKeyInfo(NULL,NULL,&nValues,NULL);
  long pos = 1;
  pRegKeyVersion->GetFirstValue(tempKey,pos);
  for(unsigned i=0;i<nValues;i++)	{
      //echo("copy " + tempKey);
      pRegKeyVersion->QueryValue(tempKey,tempValue);
      pRegKeyGeneric->SetValue(tempKey,tempValue);
      pRegKeyVersion->GetNextValue(tempKey,pos);
  }
    
  
}


bool LauncherApp::compareVersions(const wxString localVersion, const wxString requireVersion)
{
    bool result  = (localVersion.Cmp(requireVersion) == 0);
    /*
	if (requireVersion.Len() > localVersion.Len() )	{   
        
		result = (localVersion.Find(requireVersion) == 0);
	}
	else 
	{
		result = (requireVersion.Find(localVersion) == 0);
	}
	*/

    /*
	if (result) {
 	   echo("localversion = '" + localVersion +"' requireVersion = '"+ requireVersion +"' ==> true");
    } else {
       echo("localversion = '" + localVersion +"' requireVersion = '"+ requireVersion +"' ==> false");
    }
    */
     
	return result;
}


bool LauncherApp::searchJRE()
{
  showStatusMsg(_("Searching JRE's..."));
  wxString version("");

  // Windows[tm] registry lookup
  wxString baseKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft"
                     "\\Java Runtime Environment\\";

  wxRegKey bKey(baseKey);
  if (!bKey.Exists())  {
     return false;
  }

  if (!bKey.QueryValue("CurrentVersion", version)){
     return false;
  }
  
  if (version == "1.1") {
     return false;
  }

  wxString strTemp;
  wxRegKey sKey(baseKey);
  if (!compareVersions(version, paramsJreVersionPrefered)){
	//Nos recorremos las versiones instaladas
	version = "";
	if (sKey.HasSubKey(paramsJreVersionPrefered)) {
      version = wxString(paramsJreVersionPrefered);
    } else {              
      for(unsigned i=20;i>1;i--) {
        strTemp = wxString::Format(paramsJreVersion + "_%02d",i);
        if (sKey.HasSubKey(strTemp)) {
  	  version = strTemp;
	  break;
	}
      }               
    }
  }

  if (version == "") {
    // comprobando x.x
    strTemp = paramsJreVersion.Left(3);
    if (sKey.HasSubKey(strTemp)) {
      version = strTemp;
    } else {
      if (paramsJreVersion.Len() >= 5) {
        // comprobando x.x.x
        strTemp = paramsJreVersion.Left(5);
        if (sKey.HasSubKey(strTemp)) {
          version = strTemp;
        }
       } 

    }
  }
	
  if (version == "") {
    // comprobando x.x.0
    strTemp = paramsJreVersion.Left(3)+".0";
    if (sKey.HasSubKey(strTemp)) {
      version = strTemp;
    }
  }            

  if (version == "") {
     return false;
  }            
  localVersionToUse = version;
  wxRegKey vKey(baseKey + version);

  if (!vKey.QueryValue("JavaHome", javaHome)){
     return false;
  }
  
  
  calculateJavaExePath(javaHome);
  /*
  echo("paramsJreVersion=" + paramsJreVersion);
  echo("paramsJreVersionPrefered=" + paramsJreVersionPrefered);
  echo("localVersionToUse=" + localVersionToUse);
  echo("javaHome=" +javaHome);
  echo("javaExecPath=" +javaExecPath);
  */
  return true;
}

void LauncherApp::runJRE()
{
  if (paramsLaunchMode.IsSameAs("INSTALL"))	{
    showStatusMsg(_("Launching installation..."));
  } else {
    showStatusMsg(_("Launching application..."));
  }
  wxString cmd = parseCommand();
  //echo(cmd);
  bool isOk=true;
  if (paramsLaunchJarAsync) {
      if (!run_external_async(cmd))
      {
	isOk=false;
      }
  } else {
      if (!run_external(cmd))
      {
	isOk=false;
      }  
  }
  
  if (!isOk) {
      if (paramsChecksJREExecutionTerminationCode) {
        error(
	  cmd.Format(
	    _("The command\n%s\ncould not be executed."),
	    cmd.c_str()
	  )
	);
      }
  } 

  completed = true;
}

void LauncherApp::jreInstall()
{
  showStatusWindow();
  
  showStatusMsg(_("Preparing to install JRE..."));
  
  if (!checkInstallerFile(paramsJre, paramsJreDownload,_("Java JRE version ")+paramsJreVersion))
  {
    error(_("The JRE could not be setup."));  
  }

  showStatusMsg(_("Installing JRE..."));
  if (!run_external(paramsJre))
  {
    error(_("The JRE could not be setup."));
  }
  if (!searchJRE()) {
    error(_("The JRE could not be setup."));
  }    
}


// Not used
void LauncherApp::netDownload()
{
  wxString browser;

#ifdef __WINDOWS__
  // We use the default browser.
  browser = "rundll32 url.dll,FileProtocolHandler ";
#endif

/*
  if (run_external(browser + paramsDownload))
  {
    completed = true;
  }
  else
  {
    error(_("Could not find a web browser."));
  }
  */
}

IMPLEMENT_APP(LauncherApp)


void LauncherApp::jaiInstall()
{
  if (!paramsCheckJai){
	  return;
  }
  bool isOK;
  showStatusMsg(_("Checking JAI Library..."));
  
  wxString baseKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft"
                     "\\Java Runtime Environment\\";

  wxString currentVersion;
  wxRegKey *pRegKey = new wxRegKey(baseKey);
  if( !pRegKey->Exists() )
    error(_("JRE not found."));
  if (!pRegKey->QueryValue("CurrentVersion", currentVersion)) error(_("JRE not found."));
  
  isOK=true;  
  if (!checksJai()) {
      //confirm(_("JAI library is required, Install it?"));
      showStatusWindow();
      
      showStatusMsg(_("Preparing to install JAI Library..."));
      if (!checkInstallerFile(paramsJai, paramsJaiDownload, _("JAI Library")))
      {
        isOK=false;  
      } else {
        pRegKey->SetValue("CurrentVersion",localVersionToUse);
        showStatusMsg(_("Preparing to install JAI Library..."));
        if (run_external(paramsJai))
        {
          isOK=(checksJai());
        } else {
          isOK=false;
        }  
        pRegKey->SetValue("CurrentVersion",currentVersion);
      }
      
  }  
  if (!isOK) {
    error(_("The JAI could not be setup."));
  }
}

void LauncherApp::jaiIoInstall()
{
  if (!paramsCheckJaiIo){
	  return;
  }
  bool isOK;
  showStatusMsg(_("Checking JAI imageIO Library..."));
  
  wxString baseKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft"
                     "\\Java Runtime Environment\\";

  wxString currentVersion;
  wxRegKey *pRegKey = new wxRegKey(baseKey);
  if( !pRegKey->Exists() )
    error(_("JRE not found."));
  if (!pRegKey->QueryValue("CurrentVersion", currentVersion)) error(_("JRE not found."));
  

  isOK=true;  
  if (!checksJaiIo()) {
      //confirm(_("JAI ImageIO library is required, Install it?"));
      showStatusWindow();
      
      showStatusMsg(_("Preparing to install JAI imageIO Library..."));
      if (!checkInstallerFile(paramsJaiIo, paramsJaiIoDownload,"JAI ImageIO Library"))
      {
        isOK=false;  
      } else {
        pRegKey->SetValue("CurrentVersion",localVersionToUse);
        showStatusMsg(_("Installing JAI imageIO Library..."));
        if (run_external(paramsJaiIo))
        {                                    
          isOK=(checksJaiIo());
        } else {
          isOK=false;        
        }
         pRegKey->SetValue("CurrentVersion",currentVersion);
      }
     
  }  
  if (!isOK) {
             error(_("The JAI imageIO could not be setup."));
  }
}

bool LauncherApp::checksJai(){
   return (wxFileExists(javaHome + "\\lib\\ext\\jai_core.jar"));
}

bool LauncherApp::checksJaiIo(){
   return (wxFileExists(javaHome + "\\lib\\ext\\jai_imageio.jar"));     
}

bool LauncherApp::downloadFileHttp(const wxString urlOfFile, const wxString filePath, const wxString msg){

  bool isOk;
  //echo(urlOfFile);
  if (urlOfFile == wxEmptyString) {
     return false;
  }
  
  if (filePath == wxEmptyString) {
     return false;
  }
  
  showStatusMsg(msg);
  wxURL url(urlOfFile);
  //echo("url open");
  wxInputStream *in_stream;

  in_stream = url.GetInputStream();
  //echo("in_stream open");
  if (!in_stream->IsOk()) {
     //echo("in_stream.IsOk == false");
     return false;
  }
  //echo("in_stream.IsOk == true");
  //echo("filePath =" + filePath);
  wxFileName fileName(filePath);
  
  
  // Preparamos la ruta para el fichero destino
  wxArrayString dirs;
  
  dirs = fileName.GetDirs();
  wxString dir;
  wxFileName curDir(".");
  for (size_t i=0; i < dirs.GetCount(); i++) {
      dir = dirs.Item(i);
      curDir.AppendDir(dir);
      //echo("dir " + curDir.GetPath());
      if (!curDir.DirExists()) {
         //echo("creating dir");
         isOk = curDir.Mkdir();
         if (!isOk) {
            //echo("dir create no ok");
            return false;
         }
         
         //echo("dir create ok");
      }
  }
  
  
  wxFileOutputStream out_stream  = wxFileOutputStream(filePath);
  //echo("out_stream open");
  
  //in_stream->Read(out_stream);
  size_t nbytes = 10240;
  char buffer[nbytes];
  
  
  //while (!in_stream->Eof()) {
  while (!in_stream->Eof()) {
      in_stream->Read(&buffer,nbytes);  
      if (in_stream->LastError() != wxSTREAM_NO_ERROR && in_stream->LastError() != wxSTREAM_EOF) {
          return false;
      }
      out_stream.Write(&buffer,in_stream->LastRead());
      if (out_stream.LastError() != wxSTREAM_NO_ERROR) {       
         return false;                                                                   
      }
      int totalKb = out_stream.GetSize() / 1024;
      wxString totalMsg =msg.Format("%i",totalKb);
      showStatusMsg(msg+" "+ totalMsg + " Kb");
      
  }
 
  
  isOk = true;
  /*
  if (isOk) {
    echo("isOk = true");
  } else {
    echo("isOk = false");         
  }
  */
  delete in_stream;
  
  //echo("end");
  return isOk;

}

bool LauncherApp::checkInstallerFile(const wxString filePath, const wxString urlDownload, const wxString fileDescription) {
  if (!wxFile::Exists(filePath))
  {
    if (paramsEnabledDownload) 
    {
      wxString msg = _("Installation requires to download this file:\n") + 
                     fileDescription + "\n" +
                     _("Do you want to continue?");
      confirm(msg);
      //showStatusMsg(_("Downloading ") + fileDescription + "...");      
      if (!downloadFileHttp(urlDownload,filePath,_("Downloading ") + fileDescription + "..." )) 
      {
        //FIXME: Falta msgError
        return false;  
      }       
    }
    else
    {
      return false;  
    }

  }
  return true;
}

void LauncherApp::showStatusMsg(const wxString msg) {
  if (isStatusDialogVisible) {
     while (this->Pending()) {
           this->Dispatch();
     }
     if (statusDialog->IsCanceled()) {
        int response;
        wxMessageDialog dlg(0, _("Do you really want to cancel the installation process?"), _(APPLICATION_NAME), wxYES_NO | wxICON_QUESTION );
        response = dlg.ShowModal();
        if (response == wxID_YES) {
            notifyToUser(_("Canceled by user"));
            exit(1);
        }
        statusDialog->DoNotCancel();
     }
     statusDialog->showStatus(msg);
  }
}

bool LauncherApp::checkVersion(const wxString javaexe)
{
     wxString cmd;
     wxArrayString out;
     wxArrayString err;
     
     cmd = cmd.Format("%s -version", javaexe.c_str());
     if( wxExecute(cmd,out,err)!= 0 ) {
         notifyToUser(_("Failed checking JVM version."));      
         return false;
     }
     
     if (err.Count() == 0) {
         notifyToUser(_("Failed checking JVM version."));      
         return false;        
     }
     
     if (err[0].Contains(paramsJreVersion.c_str()) && err[0].Contains("version")) {
        return true; 
     }
     notifyToUser(cmd.Format(_("%s\nJava %s recommended"), err[0].c_str(),paramsJreVersion.c_str()));
     return false;


}

void LauncherApp::run(){
  bool doChecks =true;
  		
  if (!paramsDoChecks) {
    javaHome = paramsJreHome;
    calculateJavaExePath(javaHome);
  } else {
      if (paramsAskForCheckingProcess) {
         wxString msg = _("Do you want to check the application requirements? \nThis will install missing components (Recommended).");
         doChecks = confirmYesNoCancel(msg,true);
         if (!doChecks) {
	        // No quiere comprobacion, por lo que solicitamos 
	        wxString msgDir = _("Please, select the Java VM.");   
	        wxFileDialog dlg(
	            0,
	            msgDir,
	            wxEmptyString,
	            wxString("java.exe"),
	            wxString("Java VM executable file (java.exe)|java.exe"),
	            wxOPEN | wxFILE_MUST_EXIST,
	            wxDefaultPosition
	        );
	        int response = dlg.ShowModal();  
	        if (response != wxID_OK) {
	           notifyToUser(_("Canceled by user"));
	           exit(1);
	        }
	        //caragamos la variable con el eljecutable
	        javaExecPath = dlg.GetPath();
	        
	        checkVersion(javaExecPath);
	        
	        //generamos el path para el JavaHome (por si acaso)
	        wxFileName fileName(javaExecPath);
	        fileName.SetFullName("");
	        fileName.AppendDir("..");
	        fileName.Normalize();
	        javaHome = fileName.GetPath();         	
         	
         }
      }
         
    
      if (doChecks) {
    
          if (!searchJRE())
          {
            jreInstall();
          }
        
          fixSystemJREConfig();
        
          jaiInstall();
        
          jaiIoInstall();
      }
  }

  copyRequiredFiles();

  runJRE();

  exit(0);    
     
}

void LauncherApp::OnTimer(wxTimerEvent& event) {     
     run();
}

void LauncherApp::calculateJavaExePath(const wxString aJavaHome){
  int osVerMayor;
  int osVerMinor;

  wxGetOsVersion(&osVerMayor,&osVerMinor);
  if (osVerMayor < 5) {
    javaExecPath = aJavaHome + "\\bin\\java";
  }else{
    javaExecPath = aJavaHome + "\\bin\\javaw";
  }
    
}


wxString LauncherApp::parseCommand(){

  return parseString(paramsCommand);
}

wxString LauncherApp::parseString(const wxString theString){
  wxString resultCommand(theString);

  // #EXE_FILE_NAME#
  resultCommand.Replace("#EXE_FILE_NAME#",exeFileName.GetFullPath().c_str());
  // #EXE_PATH#
  resultCommand.Replace("#EXE_PATH#",exePath.GetFullPath().c_str());

  // #JAVA#
  resultCommand.Replace("#JAVA#",javaExecPath.c_str());
  
  // #JAVA_HOME#
  resultCommand.Replace("#JAVA_HOME#",javaHome.c_str());

  // #JAR#
  resultCommand.Replace("#JAR#",paramsJar.c_str());

  // #CLASSPAHT#
  resultCommand.Replace("#CLASSPATH#",classpath.c_str());


  // calculamos la cadena args y sustituimos los #ARG?#
  wxString theArgs("");
  wxString theArg("");
  int i;
  for (i=1;i<this->argc;i++) {
    theArg = argv[i];
    if (i!=1){
    	theArgs = theArgs + " \"" + theArg + "\"";
    }else{
    	theArgs = "\"" + theArg + "\"";
    }

    resultCommand.Replace(theArg.Format("\"#ARG%i#\"",i),theArg.c_str());
  }
  // Dejamos a blanco los argumento que no hemos recivido y existen en el comando
  theArg = "";
  for (i=i;i<10;i++) {
    resultCommand.Replace(theArg.Format("#ARG%i#",i),theArg.c_str());
  }
  //echo(theArgs);

  // #ARGS#
  resultCommand.Replace("#ARGS#",theArgs.c_str());
  
  // #ARG0#
  resultCommand.Replace("#ARG0#",argv[0]);

  // variables de la seccion variables

  //echo(resultCommand);
  
//  
  if (!variables.IsEmpty()){
	  //echo(theArg.Format("No empty: count =%i",keys.GetCount()));
	  unsigned int i;
	  for (i=0;i<variables.GetCount();i++){
		  //echo("#"+variables[i]+"#="+variablesValues[i]);
		  resultCommand.Replace("#"+variables[i]+"#",variablesValues[i]);
	  }

  //echo(resultCommand);

  }

 

  return parseEnviron(resultCommand);
}

wxString LauncherApp::parseEnviron(const wxString theString){
  //echo("in string:"+theString);
  wxString resultString("");
  wxString token("");
  size_t pos = 0;
  bool startedToken=false;
  char character;
  wxString env;
  while (pos < theString.Len()){
    character =theString.GetChar(pos);
    //echo(token.Format("%i=%c",pos,character));
    if (character == '%'){
      //echo("mark");
      if (startedToken){
	 if (wxGetEnv(token, &env)){
           //echo("token Found:"+env);
           resultString += env; 
	 }else{
           //echo("token not Found:"+token);
           resultString += "%" + token + "%"; 
	 }
	 token="";
	 startedToken = false;
      } else {
	 startedToken = true;
      }
    
    }else {
      //echo("no mark");
      if (startedToken){
        token += character;
        //echo("token:"+token);
      } else {
        resultString += character;
        //echo("result:"+resultString);
      }
    }
    pos++;
  }
  //echo("tempResult:"+resultString);
  if (!environVariables.IsEmpty()){
    for (size_t i=0; i < environVariables.GetCount(); i++){
      //echo("replace:%"+environVariables[i] + "%");
      //echo("for "+environVariablesValues[i]);
      //echo("in string "+ resultString);
      resultString.Replace("%"+environVariables[i]+"%",environVariablesValues[i]);
    }
  }
  //echo("Final Result:"+resultString);
  return resultString;
}


bool LauncherApp::copyRequiredFiles(){
  int i = 0;
  //wxFileInputStream in( cfgName );
  //wxFileConfig cfg( in );
  //cfg.SetPath("/CopyRequiredFiles");

  wxString source;
  wxString target;
  wxString msg;
  wxFileName fileName;

  while (true) {
	i++;

	source = readPathFromINI("CopyRequiredFiles",source.Format("source%i",i),wxEmptyString);
	target = readPathFromINI("CopyRequiredFiles",target.Format("target%i",i),wxEmptyString);
	if (source == wxEmptyString && target == wxEmptyString) {
		return true;
	}
	if (source == wxEmptyString || target == wxEmptyString) {
		error(source.Format(_("Error copying the file number %i:\n missing source or target entry"),i));
		return false;
	}
	source = parseString(source);
	target = parseString(target);
	if (wxFileExists(target)){
		continue;
	} else {
	    if (wxDirExists(target)) {
	        fileName = wxFileName(target);
		wxFileName tempFileName(source);
	        fileName.SetFullName(tempFileName.GetFullName());
		if (fileName.FileExists()) {
			continue;
		}
	    }
				
	}
	if (!wxFileExists(source)){
		error(source.Format(_("Error copying the file number %i:\n source file '%s' does not exists."),i,source.c_str()));
		return false;
	}

	fileName = wxFileName(source);
	msg = msg.Format(_("copying %s..."), fileName.GetFullName().c_str());
	//echo(msg);
	//echo(msg.Format("%s --> %s",source.c_str(),target.c_str()));
	if (!copyFile(source,target, msg)){
		error(source.Format(_("Error copying the file number %i:\n '%s' --> '%s'."),i,source.c_str(),target.c_str()));
		return false;
	}

  }
}

bool LauncherApp::copyFile(const wxString source, const wxString target, const wxString msg){

  bool isOk;
  //echo(source);
  if (source == wxEmptyString) {
     return false;
  }
  
  //echo(target);
  if (target == wxEmptyString) {
     return false;
  }
  
  showStatusMsg(msg);

  wxFileName targetFileName(target);
  
  
  // Preparamos la ruta para el fichero destino
  wxArrayString dirs;
  
  dirs = targetFileName.GetDirs();
  wxString dir;
  wxFileName curDir;
  if (targetFileName.IsAbsolute()) {
	curDir.Assign("\\");
	curDir.SetVolume(targetFileName.GetVolume());
  }else{
	curDir.Assign(".");
  }
  for (size_t i=0; i < dirs.GetCount(); i++) {
      dir = dirs.Item(i);
      curDir.AppendDir(dir);
      //echo("dir " + curDir.GetPath());
      if (!curDir.DirExists()) {
         //echo("creating dir");
         isOk = curDir.Mkdir();
         if (!isOk) {
            //echo("dir create no ok");
            return false;
         }
         
         //echo("dir create ok");
      }
  }

  wxString finalTarget;
  if (targetFileName.IsDir() || targetFileName.DirExists()) { 
      //echo("targetFileName.IsDir -> true  " + targetFileName.GetFullPath());
      wxFileName sourceFileName(source);
      targetFileName.SetFullName(sourceFileName.GetFullName());
      finalTarget = targetFileName.GetFullPath();
  } else {
      //echo("targetFileName.IsDir -> false  " + targetFileName.GetFullPath());
      finalTarget = target;
  }


  //echo(msg.Format("%s --> %s",source.c_str(),finalTarget.c_str()));
  isOk = wxCopyFile(source,finalTarget,false);
	

  return isOk;

}

wxString LauncherApp::readPathFromINI(wxString section, wxString key, const wxString defaultValue){
	char* charResult;

	charResult =myGetPrivateProfileString(section.c_str(),key.c_str(),defaultValue.c_str(),cfgName.c_str()); 

	//Remplazamos \ por /
	
	register char* s = charResult;
	for (;*s;s++) {
		if (*s == '\\') {
			*s = '/';
		}
	}

	wxString result(charResult);
	return result;


}



wxString LauncherApp::readPathFromINI(wxString key, const wxString defaultValue){
	return readPathFromINI("default",key,defaultValue);

}

wxString LauncherApp::readFromINI(wxString section, wxString key, const wxString defaultValue){
	char* charResult;

	charResult =myGetPrivateProfileString(section.c_str(),key.c_str(),defaultValue.c_str(),cfgName.c_str()); 

	wxString result(charResult);
	return result;
}

wxString LauncherApp::readFromINI(wxString key, const wxString defaultValue){
	return readFromINI("default",key,defaultValue);
}

wxArrayString LauncherApp::sectionKeysFromINI(const wxString section){
	char* charResult;

	charResult =myGetPrivateProfileString(section.c_str(),NULL,NULL,cfgName.c_str()); 


	wxArrayString rvalue;
	wxString token = wxString("");

	for (int i=0; i < 2048;i++){
	   if (charResult[i] == 0){
		if (token == ""){
		  return rvalue;
		}
		rvalue.Add(token);
		token = wxString("");
	   }else{
	     token.Append(charResult[i]);
	   }
	}
	return rvalue;
}


wxArrayString LauncherApp::sectionKeysValuesFromINI(const wxString section,wxArrayString keys) {
	wxArrayString rvalue;
	unsigned int i;
	wxString key;
	wxString value;
	for (i=0;i < keys.GetCount(); i++){
		rvalue.Add(readPathFromINI( section, keys[i], "")); 
	}	
	return rvalue;
}

