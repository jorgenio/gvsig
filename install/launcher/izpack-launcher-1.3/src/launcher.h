/* Copyright (c) 2004 Julien Ponge - All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <wx/wx.h>
#include <wx/config.h>
#include <wx/confbase.h>
#include <wx/fileconf.h>
#include <wx/wfstream.h>

#ifdef __WINDOWS__
  #include <wx/msw/registry.h>
#else
  #include <stdlib.h>
  #include <map>
  #include <list>
#endif



#include "failuredialog.h"
#include "statusdialog.h"

class LauncherApp : public wxApp
{
private:

  wxString APPLICATION_NAME;
  wxString paramsApplicationName;

  wxFileName exeFileName; 
  wxFileName exePath;
  
  wxString javaHome;
  wxString javaExecPath;
// std::map<wxString, wxString> params;
  wxString paramsJar;
  wxString paramsCommand;  
  bool paramsLaunchJarAsync;
  bool paramsAskForCheckingProcess;
  wxString paramsJre;
  wxString paramsJreDownload;
  bool paramsCheckJai;
  wxString paramsJai;
  wxString paramsJaiDownload;
  bool paramsCheckJaiIo;
  wxString paramsJaiIo;
  wxString paramsJaiIoDownload;
  bool paramsEnabledDownload;
  bool paramsDoChecks;
  bool paramsChecksJREExecutionTerminationCode;
  wxString paramsJreHome;
  StatusDialog* statusDialog;
  bool isStatusDialogVisible;
  wxTimer* myTimer;

  wxArrayString variables;
  wxArrayString variablesValues;

  wxArrayString environVariables;
  wxArrayString environVariablesValues;

  wxArrayString classpathValues;
  wxArrayString classpathVariables;

  wxString paramsLaunchMode;
  
  wxString paramsJreVersion;
  wxString paramsJreVersionPrefered;

  wxString localVersionToUse;

  wxString classpath;

  wxLocale locale;

  bool completed;

  wxString cfgName;

  void loadParams();

  void error(const wxString &msg);

  bool searchJRE();

  void runJRE();

  void manualLaunch();

  void jreInstall();

  void netDownload();

  void echo(const wxString &msg);
  
  void confirm(const wxString &msg);

  bool confirmYesNoCancel(const wxString &msg,const int yesDefault);
  
  void notifyToUser(const wxString &msg);
  
  bool compareVersions(const wxString localVersion, const wxString requireVersion);

  void jaiInstall();

  bool checkVersion(const wxString msg);
  
  void jaiIoInstall();

  bool checksJaiIo();
  
  bool checksJai();
  
  void fixSystemJREConfig();
  
  bool downloadFileHttp(const wxString urlOfFile, const wxString filePath, const wxString msg);

  bool copyFile(const wxString urlOfFile, const wxString filePath, const wxString msg);
  
  bool checkInstallerFile(const wxString filePath, const wxString urlDownload, const wxString fileDescription);
  
  void showStatusMsg(const wxString msg);
  
  void run();
  
  void calculateJavaExePath(const wxString aJavaHome);

  wxString parseCommand();

  wxString parseString(const wxString theString);

  wxString parseEnviron(const wxString theString);

  bool copyRequiredFiles();


  wxString readPathFromINI(wxString section, wxString key, const wxString defaultValue);

  wxString readPathFromINI(wxString key, const wxString defaultValue);

  wxString readFromINI(wxString section, wxString key, const wxString defaultValue);

  wxString readFromINI(wxString key, const wxString defaultValue);

  wxArrayString sectionKeysFromINI(const wxString section);

  wxArrayString sectionKeysValuesFromINI(const wxString section,wxArrayString keys);

  void showStatusWindow();

  
  void calculateClasspath(wxArrayString ClassPathValues);

 
public:

  LauncherApp();

  virtual ~LauncherApp();

  virtual bool OnInit();
  
  virtual void OnTimer(wxTimerEvent& event);

protected:
   DECLARE_EVENT_TABLE()

};

#endif
