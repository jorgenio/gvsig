#ifndef MYINIREADER_H
#define MYINIREADER_H




#ifdef __cplusplus
extern "C" {
#endif


	char* myGetPrivateProfileString(const char* section,const char* key, const char* defaultValue, const char* fileName);

#ifdef __cplusplus
}
#endif

#endif
