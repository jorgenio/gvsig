echo Unpacking...
export _POSIX2_VERSION=199209
set -e
tail +$lcount "$0" | tar -xz  --no-same-owner
echo Launching instalation program....
cd ./tmp_gvSIGInstall
set +e
org_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
#export LD_LIBRARY_PATH="$PWD:$PWD/lib:$LD_LIBRARY_PATH"
./launcher-Linux >launcher_out 2>&1
res=$?
runOK=true
if [ $res -eq 1 -o $res -eq 0 ]
then
	runOK=true
else
	runOK=false
	export LD_LIBRARY_PATH="$PWD:$PWD/lib:$LD_LIBRARY_PATH"
	./launcher-Linux > launcher_out2  2>&1
	res=$?
	if [  $res = 1 -o  $res = 0 ]
	then
		runOK=true
	fi

fi
LD_LIBRARY_PATH=$org_LD_LIBRARY_PATH
if [ $runOK = false ]
then
	cat launcher_out
	echo
	cat launcher_out2
	echo
	if grep -q 'libstdc++.so.6:' launcher_out ; then
		echo You need the libstdc++6 library correctly instaled in your system to use
		echo the installation program.
		echo
		echo "In a debian-based distribution (if you have root privileges)"
		echo you can run the command:
		echo '    apt-get install libstdc++6'

	fi
	if grep -q 'libc.so.6:' launcher_out ; then
		echo You need the libc6 library correctly instaled in your system to use
		echo the installation program.
		echo
		echo "In a debian-based distribution (if you have root privileges)"
		echo you can run the command:
		echo '    apt-get install libc6'
	fi
	echo
fi

cd ..
rm -r ./tmp_gvSIGInstall 2>&1 >/dev/null
exit 0
