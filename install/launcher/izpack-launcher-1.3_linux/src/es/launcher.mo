��    )      d  ;   �      �     �     �     �  $   �  -   �     %     E     c     j          �     �     �  I   �               &     5     G  &   _     �     �     �  @   �       $   6      [     |     �     �  L   �       '   (     P  )   g  )   �  =   �     �  (     [   7  ]  �     �	     
  
   
      
  8   ?
  *   x
  ,   �
     �
     �
     �
       	        $  K   6      �     �     �     �     �  .   �     -  "   K  "   n  x   �  !   
  (   ,  %   U     {      �     �  J   �       0   ,  &   ]  9   �  7   �  >   �     5  7   T  |   �                %   '          	                 (                     
                               )                       "      $                     !         &                                #           (Recommended).  could not be executed.  does not exist. ' does not contain a jar file entry. Are you sure you want to cancel installation? Can't identify java_home of %s. Can't identify version of %s. Cancel Canceled by the user Canceled by user Checking... Continue Downloading %s... Error executing this installation option. Do you want to try another one? File %s not found. Installing JRE... Intializing... Java JRE version  Java VM executable file Java version %s is not the required %s Launching installation... Manually select the Java VM Preparing to install JRE... Problems found in the selected JRE:
%s

Do you want to continue? Select one of these options: The JAI I/O library is not installed The JAI library is not installed The JRE could not be setup. The configuration file ' The file %s does not exist. The installation requires to download this file:
%s
Do you want to continue? The jar-file  The jar-file in the configuration file  gvSIG Install-Launcher gvSIG instalation program. Please wait... gvSIG requires a Java Runtime Environment install a JRE version ready to run in the user home directory select a JRE manualy use JRE used by a previous gvSIG version use the JRE defined in JAVA_HOME environment variable or finded in the shell execution path Project-Id-Version: launcher
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-02-19 08:38+0100
PO-Revision-Date: 2007-02-19 08:39+0100
Last-Translator: Jose Manuel VivÃ³ (Chema) <josemanuel.vivo@iver.es>
Language-Team:  <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10.2
  (Recomendado). no pudo ejecutarse. no existe. no contiene una entrada 'jar'. Â¿Esta seguro de que desea cancelar la instalaciÃ³n? No se pudo identificar el java_home de %s. No se pudo indentificar la versiÃ³n de %s. Cancelar Cancelado por el usuario Cancelado por el usuario Comprobando... Continuar Descargando %s... Error usando la opciÃ³n de instalaciÃ³n. Â¿Desea intentarlo con otra? No se encontrÃ³ el fichero %s. Instalando el JRE... Inicializando... VersiÃ³n de Java JRE Fichero ejecutable de Java VM La versiÃ³n %s de Java no es la requerida %s Lanzando la instalaciÃ³n... Seleccionar manualmente la Java VM Preparando para instalar el JRE... Se han encontrado los siguientes problemas en el JRE seleccionado: <br> %s<br> <br> Â¿Desea continuar de todas formas? Seleccione una de estas opciones: La librerÃ­a JAI I/O no esta instalada La librerÃ­a JAI no esta instalada. El JRE no pudo instalarse. El fichero de configuraciÃ³n ' El fichero %s no existe. La instalaciÃ³n requiere descargar este fichero:
%s
Â¿Desea continuar? El fichero 'jar' El fichero 'jar' del fichero de configuraciÃ³n Lanzador de la instalaciÃ³n de gvSIG Programa de instalaciÃ³n de gvSIG. Por favor, espere... gvSIG requiere un Entrono de ejecuciÃ³n de Java (JRE) Instalar un JRE listo para funcionar en directorio del usuario Selecionar un JRE manualmente. Usar un JRE usado por una versiÃ³n anteriror de gvSIG Usar el JRE definido en la variable de entorno JAVA_HOME o encontrada en la ruta de ejecuciÃ³n del interprete de comandos. 