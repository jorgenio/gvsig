/* Copyright (c) 2004 Julien Ponge - All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <wx/wx.h>
#include <wx/config.h>
#include <wx/confbase.h>
#include <wx/fileconf.h>
#include <wx/wfstream.h>


#include <stdlib.h>
#include <map>
#include <list>

#include "selectiondialog.h"
#include "statusdialog.h"

class LauncherApp : public wxApp
{
private:

  wxString APPLICATION_NAME;
  
  wxString javaHome;
  wxString javaExecPath;
// std::map<wxString, wxString> params;
  wxString paramsJar;
  wxString paramsJarParams;  
  bool paramsLaunchJarAsync;
  wxString paramsJre;
  wxString paramsJreDownload;
  wxString paramsJreInstallCmd;
  wxString paramsJreInstalledJavaFile;
  wxString paramsJreInstalledRootDirectory;
  bool paramsEnabledDownload;
  StatusDialog* statusDialog;
  bool isStatusDialogVisible;
  wxTimer* myTimer;
 
  
  wxString paramsJreVersion;
  wxString paramsJreVersionPrefered;

  wxLocale locale;

  wxString cfgName;

  wxFont font;

  void loadParams();

  void error(const wxString &msg);

  void runJRE();

  void echo(const wxString &msg);
  
  void confirm(const wxString &msg);

  bool confirmYesNoCancel(const wxString &msg,const int yesDefault);
  
  void notifyToUser(const wxString &msg);
  void notifyErrorToUser(const wxString &msg);
  
  bool compareVersions(const wxString localVersion, const wxString requireVersion);
  
  bool downloadFileHttp(const wxString urlOfFile, const wxString filePath, const wxString msg);
  
  bool checkInstallerFile(const wxString filePath, const wxString urlDownload, const wxString fileDescription);
  
  void showStatusMsg(const wxString msg);
  
  void run();
  
  wxString checkJava(const wxString javaFileName) ; 
  
  wxString previousJRE() ; //*
    
  wxString installJRE() ;
  
  wxString useEnvironJRE() ; //*
  
  wxString selectJavaExecFileManualy();
  
  bool checkPreviousJRE(); //*
  
  bool checkEnvironmentJRE();
  
  wxString getJavaExecFileHome(wxString javaFileName);
  
  wxString getJavaExecFileVersion(wxString javaFileName);
  
public:

  LauncherApp();

  virtual ~LauncherApp();

  virtual bool OnInit();
  
  virtual void OnTimer(wxTimerEvent& event);

protected:
   DECLARE_EVENT_TABLE()

};

#endif
