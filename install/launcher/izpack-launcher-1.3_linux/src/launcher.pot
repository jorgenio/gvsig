# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-02-19 08:38+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: launcher.cpp:150
msgid "gvSIG Install-Launcher"
msgstr ""

#: launcher.cpp:225
msgid "The configuration file '"
msgstr ""

#: launcher.cpp:225
msgid "' does not contain a jar file entry."
msgstr ""

#: launcher.cpp:247 launcher.cpp:264
msgid "Canceled by user"
msgstr ""

#: launcher.cpp:289
msgid "Intializing..."
msgstr ""

#: launcher.cpp:325
msgid "Launching installation..."
msgstr ""

#: launcher.cpp:328
msgid "The jar-file in the configuration file "
msgstr ""

#: launcher.cpp:328
msgid " does not exist."
msgstr ""

#: launcher.cpp:338 launcher.cpp:343
msgid "The jar-file "
msgstr ""

#: launcher.cpp:338 launcher.cpp:343
msgid " could not be executed."
msgstr ""

#: launcher.cpp:350
msgid "Preparing to install JRE..."
msgstr ""

#: launcher.cpp:352
msgid "Java JRE version "
msgstr ""

#: launcher.cpp:354 launcher.cpp:362
msgid "The JRE could not be setup."
msgstr ""

#: launcher.cpp:358
msgid "Installing JRE..."
msgstr ""

#: launcher.cpp:367
#, c-format
msgid "The file %s does not exist."
msgstr ""

#: launcher.cpp:481
#, c-format
msgid ""
"The installation requires to download this file:\n"
"%s\n"
"Do you want to continue?"
msgstr ""

#: launcher.cpp:483
#, c-format
msgid "Downloading %s..."
msgstr ""

#: launcher.cpp:506
msgid "Are you sure you want to cancel installation?"
msgstr ""

#: launcher.cpp:509 launcher.cpp:527 launcher.cpp:583
msgid "Canceled by the user"
msgstr ""

#: launcher.cpp:539
msgid ""
"Error executing this installation option. Do you want to try another one?"
msgstr ""

#: launcher.cpp:549
#, c-format
msgid ""
"Problems found in the selected JRE:\n"
"%s\n"
"\n"
"Do you want to continue?"
msgstr ""

#: launcher.cpp:568
msgid "Manually select the Java VM"
msgstr ""

#: launcher.cpp:574
msgid "Java VM executable file"
msgstr ""

#: launcher.cpp:625
#, c-format
msgid "File %s not found."
msgstr ""

#: launcher.cpp:632
#, c-format
msgid "Can't identify version of %s."
msgstr ""

#: launcher.cpp:637
#, c-format
msgid "Java version %s is not the required %s"
msgstr ""

#: launcher.cpp:642
#, c-format
msgid "Can't identify java_home of %s."
msgstr ""

#: launcher.cpp:646
msgid "The JAI library is not installed"
msgstr ""

#: launcher.cpp:650
msgid "The JAI I/O library is not installed"
msgstr ""

#: selectiondialog.cpp:50
msgid "gvSIG requires a Java Runtime Environment"
msgstr ""

#: selectiondialog.cpp:59
msgid "use JRE used by a previous gvSIG version"
msgstr ""

#: selectiondialog.cpp:60
msgid "install a JRE version ready to run in the user home directory"
msgstr ""

#: selectiondialog.cpp:61
msgid "select a JRE manualy"
msgstr ""

#: selectiondialog.cpp:62
msgid ""
"use the JRE defined in JAVA_HOME environment variable or finded in the shell "
"execution path"
msgstr ""

#: selectiondialog.cpp:67 selectiondialog.cpp:73
msgid " (Recommended)."
msgstr ""

#: selectiondialog.cpp:77
msgid "Select one of these options:"
msgstr ""

#: selectiondialog.cpp:97
msgid "Continue"
msgstr ""

#: statusdialog.cpp:46
msgid "gvSIG instalation program. Please wait..."
msgstr ""

#: statusdialog.cpp:47
msgid "Checking..."
msgstr ""

#: statusdialog.cpp:52
msgid "Cancel"
msgstr ""
