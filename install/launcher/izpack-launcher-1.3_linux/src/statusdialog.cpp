#include "statusdialog.h"

#ifndef __WINDOWS__
  #include "package.xpm"
#endif

void echo(const wxString &msg) 
{
  wxMessageDialog dlg(0, msg, "echo", wxOK);
  dlg.ShowModal();
}

long STATUSDIALOG_TIMER_ID = wxNewId();
long STATUSDIALOG_CANCELBUTTON_ID = wxNewId();

BEGIN_EVENT_TABLE(StatusDialog, wxFrame)
    EVT_TIMER(STATUSDIALOG_TIMER_ID, StatusDialog::OnTimer)
    EVT_BUTTON(STATUSDIALOG_CANCELBUTTON_ID,   StatusDialog::OnCancelButton)
    EVT_CLOSE(StatusDialog::OnCancel)
END_EVENT_TABLE()

StatusDialog::StatusDialog(const wxString &wxStringHeadline )
  : wxFrame(0, -1, _(wxStringHeadline), wxDefaultPosition, wxDefaultSize,
              wxSYSTEM_MENU | wxCAPTION )
{
 
  buildUI();
  SetIcon(wxICON(package));
}

StatusDialog::~StatusDialog()
{

}

void StatusDialog::buildUI()
{
  // Inits
  //wxFont font;
  //font.SetFamily(wxDECORATIVE);
  //font.SetPointSize(12);
  //this->SetFont(font);

  canceled = false;
  count = 0;
  wxString explanationMsg = _("gvSIG instalation program. Please wait...");
  statusMsg = _("Checking...");
  sizerDesc= new wxBoxSizer(wxHORIZONTAL);
  sizer = new wxBoxSizer(wxVERTICAL);
  // Widgets

  cancelButton= new wxButton(this, STATUSDIALOG_CANCELBUTTON_ID, _("Cancel"), wxDefaultPosition);
  //cancelButton->SetFont(font);
  icon = new wxStaticBitmap(this, -1,wxICON(package),wxDefaultPosition,wxSize(32,32));
  icon->SetBackgroundColour(cancelButton->GetBackgroundColour());
  sizerDesc->Add(icon,0, wxALIGN_LEFT | wxALL ,5);
  explanationText = new wxStaticText(this, -1, explanationMsg, wxDefaultPosition);
  explanationText->SetBackgroundColour(cancelButton->GetBackgroundColour());
  //explanationText->SetFont(font);
  sizerDesc->Add(explanationText, 1,wxALIGN_CENTRE  | wxEXPAND | wxALL,5);
  sizer->Add(sizerDesc,1,wxALIGN_LEFT | wxALIGN_TOP | wxEXPAND | wxALL,10);
  statusMsgText = new wxStaticText(this, -1, statusMsg, wxDefaultPosition);
  statusMsgText->SetBackgroundColour(cancelButton->GetBackgroundColour());
  //statusMsgText->SetFont(font);
  sizer->Add(statusMsgText, 0, wxALIGN_LEFT | wxALL, 5);
  
  //cancelButton->Hide();
  cancelButton->Enable(true);
  sizer->Add(cancelButton, 0, wxALIGN_RIGHT | wxALL, 5);
  
  this->SetBackgroundColour(cancelButton->GetBackgroundColour());
  
  
  
  
  // Sizer
  SetSizer(sizer);
  sizer->SetSizeHints(this);      
  
  //Timer
  myTimer = new wxTimer(this,STATUSDIALOG_TIMER_ID);

}

void StatusDialog::showStatus(const wxString statusString) {
     myTimer->Start(100,false);
     //echo(" StatusDialog::showStatus ");
     count = 0;
     statusMsg = statusString;
     statusMsgText->SetLabel(statusMsg);
     
     if (!this->myTimer->IsRunning()) {
       //echo(" timer not running ");
     } else {
       //echo(" timer running ");
     }
     
}

void StatusDialog::OnTimer(wxTimerEvent& event){
  //echo("StatusDialog::OnTimer");
  wxString cad;
  switch (count)
  {
   case 0: cad = " |"; count++; break;
   case 1: cad = " /"; count++; break;
   case 2: cad = " -"; count++; break;
   case 3: cad = " \\"; count = 0; break;
   default: count = 0;
  }  
  statusMsgText->SetLabel(statusMsg + cad);
}

void StatusDialog::OnCancel(wxEvent& event) {
  this->canceled = true;
}

void StatusDialog::OnCancelButton(wxMouseEvent& event) {
  this->canceled = true;
}


bool StatusDialog::IsCanceled() {
  return this->canceled;
}

void StatusDialog::DoNotCancel() {
  this->canceled = false;
}
