/////////////////////////////////////////////////////////////////////////////
// Name:        wx/dde.h
// Purpose:     DDE base header
// Author:      Julian Smart
// Modified by:
// Created:
// Copyright:   (c) Julian Smart
// RCS-ID:      $Id: dde.h 6834 2006-08-24 08:23:24Z jmvivo $
// Licence:     wxWindows Licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_DDE_H_BASE_
#define _WX_DDE_H_BASE_

#if defined(__WXMSW__)
#include "wx/msw/dde.h"
#elif defined(__WXMOTIF__)
#include "wx/motif/dde.h"
#elif defined(__WXGTK__)
#include "wx/gtk/dde.h"
#elif defined(__WXMAC__)
#include "wx/mac/dde.h"
#elif defined(__WXPM__)
#include "wx/os2/dde.h"
#elif defined(__WXSTUBS__)
#include "wx/stubs/dde.h"
#endif

#endif
    // _WX_DDE_H_BASE_
