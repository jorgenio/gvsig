/////////////////////////////////////////////////////////////////////////////
// Name:        helphtml.h
// Purpose:     Includes wx/generic/helphtml.h
// Author:      Julian Smart
// Modified by:
// Created:     2003-05-24
// RCS-ID:      $Id: helphtml.h 6834 2006-08-24 08:23:24Z jmvivo $
// Copyright:   (c) Julian Smart
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_HELPHTML_H_BASE_
#define _WX_HELPHTML_H_BASE_

#include "wx/generic/helphtml.h"

#endif
    // _WX_HELPHTML_H_BASE_
