/////////////////////////////////////////////////////////////////////////////
// Name:        wx/printdlg.h
// Purpose:     Base header for print dialogs
// Author:      Julian Smart
// Modified by:
// Created:
// RCS-ID:      $Id: printdlg.h 6834 2006-08-24 08:23:24Z jmvivo $
// Copyright:   (c) Julian Smart
// Licence:     wxWindows Licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_PRINTDLG_H_BASE_
#define _WX_PRINTDLG_H_BASE_

#if defined(__WXUNIVERSAL__)
#include "wx/generic/prntdlgg.h"
#elif defined(__WXMSW__)
#include "wx/msw/printdlg.h"
#elif defined(__WXMOTIF__)
#include "wx/generic/prntdlgg.h"
#elif defined(__WXGTK__)
#include "wx/generic/prntdlgg.h"
#elif defined(__WXX11__)
#include "wx/generic/prntdlgg.h"
#elif defined(__WXMGL__)
#include "wx/generic/prntdlgg.h"
#elif defined(__WXMAC__)
#include "wx/mac/printdlg.h"
#elif defined(__WXPM__)
#include "wx/generic/prntdlgg.h"
#elif defined(__WXSTUBS__)
#include "wx/generic/prntdlgg.h"
#endif

#if defined(__WXUNIVERSAL__) || (!defined(__WXMSW__) && !defined(__WXMAC__))
#define wxPrintDialog wxGenericPrintDialog
#define sm_classwxPrintDialog sm_classwxGenericPrintDialog

#define wxPrintSetupDialog wxGenericPrintSetupDialog
#define sm_classwxPrintSetupDialog sm_classwxGenericPrintSetupDialog

#define wxPageSetupDialog wxGenericPageSetupDialog
#define sm_classwxPageSetupDialog sm_classwxGenericPageSetupDialog
#endif

#endif
    // _WX_PRINTDLG_H_BASE_
