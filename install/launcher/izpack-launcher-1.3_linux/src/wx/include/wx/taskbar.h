/////////////////////////////////////////////////////////////////////////////
// Name:        wx/taskbar.h
// Purpose:     wxTaskBarIcon base header and class
// Author:      Julian Smart
// Modified by:
// Created:
// Copyright:   (c) Julian Smart
// RCS-ID:      $Id: taskbar.h 6834 2006-08-24 08:23:24Z jmvivo $
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_TASKBAR_H_BASE_
#define _WX_TASKBAR_H_BASE_

#if defined(__WXMSW__)
#include "wx/msw/taskbar.h"
#elif defined(__WXMOTIF__)
#include "wx/motif/taskbar.h"
#elif defined(__WXGTK__)
#elif defined(__WXMAC__)
#include "wx/mac/taskbar.h"
#elif defined(__WXPM__)
#include "wx/os2/taskbar.h"
#elif defined(__WXSTUBS__)
#include "wx/stubs/taskbar.h"
#endif

#endif
    // _WX_TASKBAR_H_BASE_
