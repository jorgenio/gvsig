/////////////////////////////////////////////////////////////////////////////
// Name:        wx/treelay.h
// Purpose:     wxTreeLayout base header
// Author:      Julian Smart
// Modified by:
// Created:
// Copyright:   (c) Julian Smart
// RCS-ID:      $Id: treelay.h 6834 2006-08-24 08:23:24Z jmvivo $
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_TREELAY_H_BASE_
#define _WX_TREELAY_H_BASE_

#include "wx/generic/treelay.h"

#endif
    // _WX_TREELAY_H_BASE_
