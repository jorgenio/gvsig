/////////////////////////////////////////////////////////////////////////////
// Name:        wx/wave.h
// Purpose:     wxWave base header
// Author:      Julian Smart
// Modified by:
// Created:
// Copyright:   (c) Julian Smart
// RCS-ID:      $Id: wave.h 6834 2006-08-24 08:23:24Z jmvivo $
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_WAVE_H_BASE_
#define _WX_WAVE_H_BASE_

#if defined(__WXMSW__)
#include "wx/msw/wave.h"
#elif defined(__WXGTK__)
#include "wx/gtk/wave.h"
#elif defined(__WXMAC__)
#include "wx/mac/wave.h"
#elif defined(__WXPM__)
#include "wx/os2/wave.h"
#elif defined(__WXMAC__)
#include "wx/mac/wave.h"
#endif

#endif
    // _WX_TREECTRL_H_BASE_
