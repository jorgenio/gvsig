#ifndef STATUSDIALOG_H
#define STATUSDIALOG_H

#include <wx/wx.h>
#include <wx/timer.h>

class StatusDialog : public wxFrame
{
private:

  wxBoxSizer* sizer;
  wxBoxSizer* sizerDesc;
  wxStaticBitmap* icon;
  wxStaticText* explanationText;
  wxStaticText* statusMsgText;
  wxString appName;
  wxString statusMsg;
  wxButton* cancelButton;
  wxTimer* myTimer;
  bool canceled;
  int count;
  void buildUI();

  
public:

  StatusDialog(const wxString &wxStringHeadline );

  virtual ~StatusDialog();

  virtual void showStatus(const wxString statusString);
  
  void OnTimer(wxTimerEvent& event);

  void OnCancel(wxEvent& event);
  
  void OnCancelButton(wxMouseEvent& event);

  bool IsCanceled();
  
  void DoNotCancel();
protected:
   DECLARE_EVENT_TABLE()

};



#endif
