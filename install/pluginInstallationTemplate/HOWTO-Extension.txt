{{{
#!rst
.. contents::

Cómo se genera un instalable para un Plugin de gvSIG
=======================================================

Número de  build
----------------

Un plugin debe añadir un panel (al que se accede en gvSIG por "ayuda->acerda de") con su correspondiente build.number para su seguimiento.
Para poder añadir dicho panel se seguirán los siguientes pasos:

+ En la carpeta config del plugin, se tiene que crear un fichero **"about.htm"**. Se puede ver un ejemplo de este fichero en el plugin **"extExpressionField"**. Lo más importante en este fichero es que debe tener la siguiente linea, que indica el build number: ::
   <p><br><br>
   <b> Build Number: #build.number#</b>
   </p>
 
+ Dentro de nuestro plugin, hay que crear dentro del build.xml un target **<buildNumber>**, que se encargará de aumentarlo: ::

	<target name="buildNumber">  
		<propertyfile	file="build.number"
				comment="Build Number for ANT. Do not edit!">
			<entry key="build.number" default="0" type="int" operation="+" />
		</propertyfile>
		<property file="build.number" />
	</target>

  **Nota**: Existe un *task* para el ant que hace una función parecida, el problema es que funciona de forma distinta
  a la que nosotros trabajamos (*Deja el siguiente número de build en el fichero, en vez de el actual*).

+ El fichero **"about.htm"** se tiene que copiar en una carpeta temporal y hacer el remplazo del #build.number# (el about.htm que se copiará al gvsig será el que está en dicha carpeta temporal). Aquí un ejemplo (extraído del *extExpressionField/build.xml*): ::
    
    <target name="copy-data-files">
		<copy file="config/config.xml" todir="${without_src}"/>
		<copy file="build.number" todir="${without_src}"/>
		<copy file="config/about.htm" todir="${without_src}"/>   <!-- AQUÍ SE HACE LA COPIA DEL about.htm A LA CARPETA TEMPORAL -->
		<loadproperties srcFile="build.number"/>
		<replace casesensitive="true"
  		file="${without_src}/about.htm"
  	  	token="#build.number#"
  		value="${build.number}"/>				<!-- AQUÍ SE HACE EL REMPLAZO DE #build.number# --> 
		<copy todir="${without_src}">
			<fileset dir="config" includes="text*.properties"/>
		</copy>
		<copy todir="${without_src}/images">
			<fileset dir="images/" includes="*"/>
		</copy>
    </target>

+ De nuevo en el build.xml de nuestro plugin, crear un target **<distribution>**, que llama al target *<buildNumber>* y luego al target por defecto. Este target por lo tanto hará lo mismo que el target por defecto, con la única diferencia de aumentar el buildnumber del plugin.

+ En los fuentes del plugin hay que crear una extensión (si ya la tenemos simplemente añadiremos dentro de ella) para registrar el panel de about.htm, un ejemplo (extraído nuevamente de *extExpressionField*) sería: ::

	public class AboutExpresionFieldExtension extends Extension {
	
		public void initialize() {
			// TODO Auto-generated method stub

		}

		public void postInitialize() {
			About about=(About)PluginServices.getExtension(About.class);
			FPanelAbout panelAbout=about.getAboutPanel();
			java.net.URL aboutURL = this.getClass().getResource(
		        "/about.htm");
		        panelAbout.addAboutUrl(PluginServices.getText(this,"calculate_expresion"),aboutURL);
		}
+ Para acabar, habrá que añadir en el fichero config/config.xml de nuestro plugin la extensión que acabamos de crear (en caso de que no existiera): ::
	
	<extension class-name="com.iver.cit.gvsig.AboutExpresionFieldExtension"
			description="Extension to add about panel."
			active="true"
			priority="1">
	</extension>    
  


Nombrado de los binarios
--------------------------

El nombre de los ficheros debe tener la siguiente estructura:

gvsig_[NOMBRE-EXT]-[VERSION_GVSIG]-[BUILD_NUMBER_GVSIG]-[BUILD_NUMBER_EXT]-[PLATAFORMA].[zip|exe|bin]

Donde:

* NOMBRE_EXT = Nombre de la extensión.

* VERSION_GVSIG = Número de versión de gvSIG sobre la que funciona el binario.

* BUILD_NUMBER_GVSIG = Número de build de gvSIG sobre el que funciona el binario.

* BUILD_NUMBER_EXT = Número de build propio de la extensión.

* PLATAFORMA = Sistema operativo (linux, windows, mac).

Por ejemplo: 

gvsig_sextante-1_1_0-1015-31-windows-i586.exe


Generación del instalable
-----------------------------

Vamos a describir los pasos que se siguen actualmente para generar una distribución de binarios de una extensíon para gvSIG.
Antes de empezar, hay que asegurarse de que en nuestro Workspace disponemos de la carpeta **"install"** y **"binaries"**, ya que estas dos carpetas son imprescindible para generar los binarios, y sin estas no se podría hacer.

Hay que descomprimir el fichero **"install/pluginInstallTemplate/install.zip**" dentro de nuestro proyecto. Este zip también se puede bajar de:

+ Si se está trabajando sobre el branch: `install_branch.zip <https://gvsig.org/svn/gvSIG/branches/v10/install/pluginInstallationTemplate/install.zip?format=raw>`_   
+ Si se está trabajando sobre el trunk: `install_trunk.zip <https://gvsig.org/svn/gvSIG/trunk/install/pluginInstallationTemplate/install.zip?format=raw>`_ 

Una vez descomprimido ya tenemos lo necesario (en forma de plantilla) para generar los binarios de cualquier extensión, y solo se deberán hacer algunos pequeños cambios para adaptarla a cada extensión...

Primero tenemos que modificar el fichero build.properties. Este fichero contiene el nombre de algunas variables y de la extensión. Tal y como está comentado en el propio fichero, hay que indicar el directorio de nuestra extensión en **MAIN_INSTALL_PLUGIN**, verificar que los números de versión sean los correctos, indicar el nombre del plugin (extensión) en **APPNAME**, y finalmente indicarle el directorio donde se dejaran caer los binarios en **OUTPUT_DIR**.

El siguiente fichero a modificar será el install.xml. Este fichero sirve de guía para el IzPAck a la hora de generar el instalable. Al principio del fichero se debe de indicar el nombre del plugin en **"<appname> </appname>"**.
Luego habrá que situarse en el final del fichero, en la zona de **<pack>**. En esta parte se definen qué ficheros tendrán que añadirse sobre la instalación de gvSIG ya existente. En **"name"** se indica el nombre del paquete tal y como se mostrará a la hora de la selección de paquetes durante la instalación, y en **"descripcion"** se indica la descripción que aparecerá al seleccionar dicha extensión. Posteriormente se definen los ficheros a añadir. Para ello disponemos de dos *tasks*:

**<file>**: copia un fichero o directorio. Tiene tres atributos a especificar:

- **targetdir**: Para especificar el destino de los ficheros que se copiarán.
- **src**: Para especificar el fichero/directorio que se copiará.
- **override**: Para especificar si se sobreescriben los ficheros a copiar en caso de que existan. Generalmente será **true**.

**<fileset>**: copia un conjunto de fichero o directorios. Tiene los siguientes campos:

- **targetdir**: Para especificar el destino, de igual manera que <file>.
- **dir**: El directorio de dónde se copiarán los ficheros.
- **includes**: Los ficheros a incluir en la copia. Lo interesante de este campo es que se pueden introducir patrones (como * o ?).
- **excludes**: Semejante a includes, sólo que aquí se especifican los ficheros que **no** se desean copiar.
- **override**: igual que en <file>.

*Para más información acerca de los tasks, hay un html de ayuda en la carpeta install del workspace: install/IzPack/doc/izpack/html/index.html*

Lo más normal será llevarse todo el directorio de nuestra extensión, y además, según que extensiones, se llevaran otros ficheros (ver ejemplos en el propio *install.xml*).

Ahora tendremos que modificar el **buildExt.xml** para prepara la instalación para **MAC OSX** cuyo proceso es distinto. Básicamente, en el *target* llamado **InstallMac** hay que copiar los fichero que hay que actualizar en la instalación (*igula que en el* **instal.xml** *pero usando los comando del* **ANT**). En el propio fichero hay comentados varios ejemplos. También existe el fichero **resources/configfile** que nos permite adaptar la configuración de la instalación de Mac (añadir un sufijo al nombre de la aplicación o comprobar el MD5 de andami), para mas información sobre este fichero se puede consultar el fichero **install/instalador-gvSIG-mac/HOWTO-UPDATER,EXTS.txt** (**CUIDADO CON LOS EL FORMATO DE LOS RETORNOS DE CARRO**).


Con esto ya tenemos todos los parámetros necesarios para generar los binarios, y simplemente habrá que lanzar el **buildExt.xml**, que en cuanto finalice, encontraremos los binarios de nuestro plugin en la ruta que indicamos en **OUTPUT_DIR** del fichero build.properties.

Dónde dejar los instalables
----------------------------

Los instalables de las versiones previas de gvSIG se dejan en el SVN asociado 
al TRAC de bugtracking, *https://gvsig.org/svn/bugtracking*, en *trunk/Versiones*.

En esa carpeta se crea para la extensión, algo como:

  [NOMBRE-EXT]-[MAJOR-VERSION]

Dentro de ésta se crearan dos carpetas, 
  
 * Documentacion-usuario
 * Versiones-previas

Y dentro de versiones previas se dejarán caer los instalables para todas las plataformas
soportadas.

En la carpeta de documentación de usuario, se dejarán caer las versiones en PDF de la
documentación que va a ser publicada como documentación de usuario junto con la extensión.


}}}
