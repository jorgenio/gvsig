#!/bin/bash
PREV_INSTALL=prev_install

# Si se produce un error, salimos inmediatamente
set -e

if [ ! -d "$PREV_INSTALL" ]; then
	echo "Can't find the directory \"$PREV_INSTALL\" ."
	exit
fi;

set +e
source build.properties 2>/dev/null

set -e
#echo $MANDATORY_EXTENSIONS


EXTENSIONS=",$EXTENSIONS,$MANDATORY_EXTENSIONS,"

echo $EXTENSIONS

xml=""

echo "Directorio actual: $PWD"
cd instalador-gvSIG-lin/extensiones
echo "EXTENSIONS=$EXTENSIONS"
for i in *
do 
	echo $i
	if ! expr  $EXTENSIONS : ".*,$i,.*" >/dev/null
	then 
		if [ -f $i/package.info ];then
			unset code
			unset name
			unset description
                        set +e
			source $i/package.info
                        set -e
		else 
			name="$i"
			description="$i"
			code="$i"
		fi
		xml="$xml

    <pack name=\"${name:-$i}\" required=\"no\" >
	  <description>${description:-$i}</description>
      <file targetdir=\"\$INSTALL_PATH/bin/gvSIG/extensiones/\" src=\"extensiones/${code:-$i}\" override=\"true\"/>
	</pack>
"	
	fi
done

echo "RESULTADO: $xml"
cd ..

while read line
do
	#if [  "$line" = "    <!-- OTRAS EXTENSIONES -->" ];then
	if [  "$line" = "<!-- OTRAS EXTENSIONES -->" ];then
		echo $xml
	else
		echo $line
	fi
done < install_template.xml >install.xml

cd ..
if [ -d "$PREV_INSTALL" ]; then
	rm -r $PREV_INSTALL;
	echo "Deleted \"$PREV_INSTALL\" ."
fi;
