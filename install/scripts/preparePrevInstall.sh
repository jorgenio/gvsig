#!/bin/bash
PREV_INSTALL=prev_install
currentDir=$PWD
# Si se produce un error, salimos inmediatamente
set -e

#cd ..
echo "Directorio actual: " $PWD

if [ -d "$PREV_INSTALL" ]; then
	echo "Found previous \"$PREV_INSTALL\" ."
	rm -r $PREV_INSTALL;
	echo "Deleted previous \"$PREV_INSTALL\" ."
fi;

mkdir $PREV_INSTALL
mkdir $PREV_INSTALL/bin
mkdir $PREV_INSTALL/libs
mkdir $PREV_INSTALL/Uninstaller

mkdir $PREV_INSTALL/bin/gvSIG

for i in instalador-gvSIG-lin/bin/gvSIG/extensiones/*; do
	ln -s $PWD/$i instalador-gvSIG-lin/extensiones
	echo "Linked the mandatory extension \"$i\" ."
done

ln -s $PWD/instalador-gvSIG-lin/extensiones $PREV_INSTALL/bin/gvSIG
echo "Linked the directory instalador-gvSIG-lin/extensiones ."

ln -s $PWD/instalador-gvSIG-lin/bin/lib $PREV_INSTALL/bin
echo "Linked the directory instalador-gvSIG-lin/bin/lib ."

ln -s $PWD/instalador-gvSIG-lin/bin/theme $PREV_INSTALL/bin
echo "Linked the directory instalador-gvSIG-lin/bin/theme ."

for i in $(ls instalador-gvSIG-lin/bin/); do
	if [ ! -d instalador-gvSIG-lin/bin/$i ]; then
		ln -s $PWD/instalador-gvSIG-lin/bin/$i $PREV_INSTALL/bin
		echo "Linked the file \"instalador-gvSIG-lin/bin/$i\" ."
	fi;
done

ln -s $PWD/instalador-gvSIG-lin/resources/gvSIG.sh $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-lin/resources/images/help.png $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-lin/resources/images/help16x16.png $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-lin/resources/images/ico-gvSIG.png $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-lin/resources/images/ico-gvSIG16x16.png $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-lin/resources/images/uninstall.png $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-lin/resources/images/uninstall16x16.png $PREV_INSTALL/bin

ln -s $PWD/instalador-gvSIG-lin/installer_files/install.sh $PREV_INSTALL
cd $currentDir
