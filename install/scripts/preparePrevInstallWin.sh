#!/bin/bash
PREV_INSTALL=prev_install
currentDir=$PWD
# Si se produce un error, salimos inmediatamente
set -e

#cd ..
echo "Directorio actual: " $PWD

if [ -d "$PREV_INSTALL" ]; then
	echo "Found previous \"$PREV_INSTALL\" ."
	rm -r $PREV_INSTALL;
	echo "Deleted previous \"$PREV_INSTALL\" ."
fi;

mkdir $PREV_INSTALL
mkdir $PREV_INSTALL/bin
mkdir $PREV_INSTALL/libs
mkdir $PREV_INSTALL/Uninstaller

mkdir $PREV_INSTALL/bin/gvSIG

for i in instalador-gvSIG-win/bin/gvSIG/extensiones/*; do
	ln -s $PWD/$i instalador-gvSIG-win/extensiones
	echo "Linked the mandatory extension \"$i\" ."
done

ln -s $PWD/instalador-gvSIG-win/extensiones $PREV_INSTALL/bin/gvSIG
echo "Linked the directory instalador-gvSIG-win/extensiones ."

ln -s $PWD/instalador-gvSIG-win/bin/lib $PREV_INSTALL/bin
echo "Linked the directory instalador-gvSIG-win/bin/lib ."

ln -s $PWD/instalador-gvSIG-win/bin/theme $PREV_INSTALL/bin
echo "Linked the directory instalador-gvSIG-win/bin/theme ."

for i in $(ls instalador-gvSIG-win/bin/); do
	if [ ! -d instalador-gvSIG-win/bin/$i ]; then
		ln -s $PWD/instalador-gvSIG-win/bin/$i $PREV_INSTALL/bin
		echo "Linked the file \"instalador-gvSIG-win/bin/$i\" ."
	fi;
done

ln -s $PWD/instalador-gvSIG-win/resources/gvSIG.bat $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-win/resources/gvSIG.ini $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-win/resources/uninstall.bat $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-win/resources/andami-config.xml $PREV_INSTALL/bin
#ln -s $PWD/instalador-gvSIG-win/resources/gpl.txt $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-win/resources/images/help.ico $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-win/resources/images/ico-gvSIG.ico $PREV_INSTALL/bin
ln -s $PWD/instalador-gvSIG-win/resources/images/uninstall.ico $PREV_INSTALL/bin

ln -s $PWD/instalador-gvSIG-win/installer_files/install.bat $PREV_INSTALL
ln -s $PWD/instalador-gvSIG-win/installer_files/LEEME.TXT $PREV_INSTALL
ln -s $PWD/instalador-gvSIG-win/installer_files/README.TXT $PREV_INSTALL
cd $currentDir
