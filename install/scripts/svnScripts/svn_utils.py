import pysvn
import os
from StringIO import StringIO
import time

trunk_type='trunk'
branches_type='branches'
tags_type='tags'
branches_types=(trunk_type,branches_type,tags_type)

gvSIG_svn_default_root="https://svn.forge.osor.eu/svn/gvsig-desktop"
gvSIG_svn_root_env_name="GVSIG_SVN_ROOT"

svn_client=None
svn_notify_callback=None

def notify(event):
  print "svn: %(action)s: %(path)s" % event

def getNotify():
  global svn_notify_callback
  if svn_notify_callback == None:
    svn_notify_callback = notify
  return svn_notify_callback

def setNotify(aNorify):
  global svn_notify_callback
  svn_notify_callback = aNorify
  getSVNClient().callback_notify=svn_notify_callback

def login(*args):
    return True, 'USUARIO', 'PASSWORD', False
def logMsgCallback():
    return True,str(logMsg)


def getSVNClient():
  global svn_client
  if svn_client == None:
    svn_client = pysvn.Client()
    svn_client.callback_get_log_message = logMsgCallback
    svn_client.callback_notify = getNotify()
    svn_client.callback_get_login = login
    svn_client.callback_ssl_server_trust_prompt = ssl_server_trust_prompt
  return svn_client

def ssl_server_trust_prompt( trust_dict ):
    print "Validating ssl certificate"
    return True, trust_dict.get("failures"), True

def relativeURL(base,url):
  if base == url:
    raise Exception("Url error: %s == %s" % (base,url))
  if url[:len(base)] != base:
    raise Exception("Base error: %s != %s" % (base,url))
  return url[len(base)+1:]

def branchTypeAndName(base,url):
  relative = relativeURL(base,url).split("/")
  ltype=relative[0]
  if ltype not in branches_types:
    raise Exception("Branch type %s not in typeList: '%s'" % (ltype,branches_types))
  if ltype==trunk_type:
    return ltype,None
  if len(relative) < 2:
    raise Exception("Bad URL branch type '%s' without name: %s" % (ltype,url))

  return ltype,relative[1]

def branchType(base,url):
  return branchTypeAndName(base,url)[0]

def branchName(base,url):
  return branchTypeAndName(base,url)[1]

def switchURL(base,currentURL,targetType,targetName=None):
  if targetType == "":
    raise Exception("Target type empty")
  targetType = targetType.lower()
  if targetType not in branches_types:
    raise Exception("Target type '%s' not in typeList: '%s'" % (targetType,branches_types))
  if targetType != trunk_type and targetName==None:
    raise Exception("Target type '%s' needs a name" % targetType)

  srcType,srcName = branchTypeAndName(base,currentURL)
  if srcType == trunk_type:
    srcToReplace = srcType
  else:
    srcToReplace = "/".join((srcType,srcName))
  if targetType == trunk_type:
    trgToReplace = targetType
  else:
    trgToReplace = "/".join((targetType,targetName))

  return currentURL.replace(srcToReplace,trgToReplace)

def get_gvSIG_SVN_root():
  if gvSIG_svn_root_env_name == None:
    return gvSIG_svn_default_root
  try:
    return os.environ[gvSIG_svn_root_env_name]
  except KeyError:
    return gvSIG_svn_default_root
    
def set_gvSIG_SVN_default_root(newDefRoot):
  global gvSIG_svn_default_root 
  gvSIG_svn_default_root = newDefRoot
   
def set_gvSIG_SVN_root_env_name(newName):
  global gvSIG_svn_root_env_name
  gvSIG_svn_root_env_name = newName

def getFirstCopyRevision(url):
  client = getSVNClient()
  msgs = client.log(url,strict_node_history=True,limit=0)
  if msgs == None:
    raise Exception("Can't get log: %s" % url)
  if len(msgs) == 0:
    raise Exception("log empty: %s" % url)
  return msgs[-1]["revision"]
    

def formatSvnLogForPrint(snvLog):
  buff = StringIO()
  buff.write("""
=======================================================================
   Date         |  Revision   |   Author
=======================================================================
""")
  for oneLog in snvLog:
    buff.write(
      " %s | %s       | %s " % (
        time.strftime( "%d/%m/%y %H:%M", time.localtime( oneLog["date"])),
        oneLog["revision"].number,
        oneLog["author"]
      )
    )
    buff.write("""\nMessage:

%s
************************************************************************
""" % (oneLog["message"]) )
  return buff.getvalue()

    
def formatElapseTime(seconds):
  seconds = int(seconds)
  hours = seconds / 3600
  seconds = seconds % 3600
  minutes = seconds /60
  seconds = seconds % 60
  formatstr = "%(seconds)02i sec"
  if minutes > 0:
    formatstr = "%(minutes)02i min " + formatstr
  if hours > 0:
    formatstr = "%(hours)i h " + formatstr
  return formatstr % {"seconds": seconds, "minutes": minutes, "hours": hours}
  
  
