/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
 * MA  02110-1301, USA.
 * 
 */

/*
 * AUTHORS (In addition to CIT):
 * 2008 PRODEVELOP		Main development
 */

package org.gvsig.normalization.persistence;

import com.iver.cit.gvsig.fmap.layers.XMLException;
import com.iver.utiles.XMLEntity;




/**
 * Geocoding persistence interface
 * 
 * @author <a href="mailto:jsanz@prodevelop.es"> Jorge Gaspar Sanz Salinas</a>
 * @author <a href="mailto:vsanjaime@prodevelop.es"> Vicent Sanjaime Calvet</a>
 * 
 */
public interface GeocodingPersistence {

	/**
	 * Gets the class name that allows create objects via introspection.
	 * 
	 * @return the name of the class that persists
	 */
    public String getClassName();
	
	/**
	 * Save the pattern to XML
	 * 
	 * @return
	 * @throws XMLExceptions
	 */
	public XMLEntity getXMLEntity() throws XMLException;

	/**
	 * Load the pattern from XML
	 * 
	 * @param xml
	 * @throws XMLException
	 */
	public void setXMLEntity(XMLEntity xml) throws XMLException;
}
