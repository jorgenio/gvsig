/* gvSIG. Geographic Information System of the Valencian Government
 *
 * Copyright (C) 2007-2008 Infrastructures and Transports Department
 * of the Valencian Government (CIT)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
 * MA  02110-1301, USA.
 * 
 */

/*
 * AUTHORS (In addition to CIT):
 * 2008 PRODEVELOP		Main development
 */

package org.gvsig.normalization.persistence;

public class GeocodingTags {
	
	// persistence geocoding
	public static final String PROJECTENCODING = "UTF-8";
	
	public static final String PATTERN = "pattern";
	public static final String PATTERNNAME = "patternname";
	
	public static final String SCORE = "score";
	public static final String RESULTSNUMBER = "resultsnumber";
	
	public static final String LAYERNAME = "layername";
	public static final String LAYERPROVIDER = "layerprovider";
	
	public static final String MAINLITERAL = "mainliteral";
	
	public static final String INTERSECTIONLITERAL = "intersectionLiteral";
	
	public static final String ADDRESSNUMBER = "addressnumber";
	
	public static final String FIRSTLEFTNUMBER = "firstleftnumber";
	public static final String FIRSTRIGHTNUMBER = "firstrightnumber";
	public static final String LASTLEFTNUMBER = "lastleftnumber";
	public static final String LASTRIGHTNUMBER = "lastrightnumber";
	
	public static final String FIRSTNUMBER = "firstnumber";
	public static final String LASTNUMBER = "lastnumber";
	
	public static final String STYLE = "style";
	public static final String SIMPLECENTROID = "simplecentroid";
	public static final String SIMPLERANGE = "simplerange";
	public static final String DOUBLERANGE = "doublerange";
	public static final String COMPOSED = "composed";
	
	// persistence normalization
	
	public static final String PATTERNz = "pattern";
	public static final String NOFIRSTROWS = "nofirstrows";
	public static final String STRINGVALUEWIDTH = "stringvaluewidth";
	public static final String INTEGERVALUEWIDTH = "integervaluewidth";
	public static final String THOUSANDSEPARATOR = "thousandseparator";
	public static final String DECIMALSEPARATOR = "decimalseparator";
	public static final String TEXTSEPARATOR = "textseparator";
	
	public static final String STRINGVALUE = "STRING VALUE";
	public static final String DATEVALUE = "DATE VALUE";
	public static final String DECIMALVALUE = "DECIMAL VALUE";
	public static final String INTEGERVALUE = "INTEGER VALUE";

	public static final String DATEVALUEFORMAT = "datevalueformat";

	public static final String DECIMALVALUEINT = "decimalvalueint";

	public static final String DECIMALVALUEDEC = "decimalvaluedec";

	public static final String FIELDNAME = "fieldname";
	public static final String FIELDTYPE = "FIELD TYPE";
	public static final String FIELDWIDTH = "fieldwidth";
	public static final String FIELDSEPARATOR = "FIELD SEPARATORS";
	public static final String INFIELDSEPARATORS = "IN FIELD SEPARATORS";
	public static final String IMPORTFIELD = "importfield";

	public static final String TABSEP = "tabsep";
	public static final String SPACESEP = "spacesep";
	public static final String COLONSEP = "colonsep";
	public static final String SEMICOLONSEP = "semicolonsep";
	public static final String OTHERSEP = "othersep";
	public static final String JOINSEP = "joinsep";
	
	public static final String ELEMENT = "ELEMENT";
	
	public static final String NPATTERN = "NORMALIZATION PATTERN";
	
	
}
