package com.iver.cit.gvsig.fmap.core;


/**
 * The interface <code>FShape3D</code> extends <code>FShape</code> adding support for 3D.
 * 
 * @see FShape
 *
 * @author Vicente Caballero Navarro
 */
public interface FShape3D {

}
