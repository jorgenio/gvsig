package org.gvsig.topology;

public interface ITopologyStatus {
 
	public static final byte VALIDATED = 0;
	 
	public static final byte VALIDATED_WITH_ERRORS = 1;
	 
	public static final byte NON_VALIDATED = 2;
	 
}
 
