package org.gvsig.topology.topologyrules;

import org.gvsig.topology.AbstractTopologyRule;
import org.gvsig.topology.IOneLyrRule;


/**
 *Any validated geometry must be a right JTS geometry.
 */
public class JtsValidRule extends AbstractTopologyRule implements IOneLyrRule {
 
}
 
