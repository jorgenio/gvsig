package org.gvsig.topology.topologyrules;

import org.gvsig.topology.AbstractTopologyRule;
import org.gvsig.topology.IOneLyrRule;


/**
 *This rule checks that lines of a line layer dont have
 *pseudonodes ends (a point that only touch another line)
 */
public class LinesMustNotHavePseudonodes extends AbstractTopologyRule implements IOneLyrRule {
 
}
 
