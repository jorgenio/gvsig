package org.gvsig.topology.topologyrules;

import org.gvsig.topology.AbstractTopologyRule;
import org.gvsig.topology.IOneLyrRule;


/**
 *The polygons of a layer must not have self intersections
 *
 */
public class PolygonMustNotSelfIntersect extends AbstractTopologyRule implements IOneLyrRule {
 
}
 
