set(LIB_NAME jgdal)

FILE(GLOB LIB_PUBLIC_HEADERS "${HEADER_PATH}/*.h")
FILE(GLOB LIB_COMMON_FILES "*.c*")

include_directories(
	${CMAKE_SOURCE_DIR}/include
	${JAVA_INCLUDE_PATH}
	${JAVA_INCLUDE_PATH2}
	${GDAL_INCLUDE_DIR}
	${GDAL_INCLUDE_DIR2}
	${GDAL_INCLUDE_DIR3}
	${GDAL_INCLUDE_DIR4}
	${GDAL_INCLUDE_DIR5}
)

add_library(${LIB_NAME} SHARED 
	${LIB_PUBLIC_HEADERS}
	${LIB_COMMON_FILES}
)


target_link_libraries(${LIB_NAME}
	${GDAL_LIBRARY}
)

IF(HDF4)
	target_link_libraries(${LIB_NAME}
		${HDF4HMM_LIBRARY}
		${HDF4HDM_LIBRARY}
	)
ENDIF(HDF4)

IF(HDF5)
	target_link_libraries(${LIB_NAME}
		${HDF5_LIBRARY}
	)
ENDIF(HDF5)

if(APPLE)
SET_TARGET_PROPERTIES(${LIB_NAME}
	PROPERTIES
	SUFFIX .jnilib)
endif(APPLE)

INCLUDE(ModuleInstall OPTIONAL)

SET_TARGET_PROPERTIES(jgdal PROPERTIES VERSION "${JGDAL_VERSION}")
IF(WIN32)
	SET_TARGET_PROPERTIES(jgdal PROPERTIES OUTPUT_NAME "jgdal${JGDAL_VERSION}")
ENDIF(WIN32)
IF(UNIX)
	SET_TARGET_PROPERTIES(jgdal PROPERTIES OUTPUT_NAME "jgdal${VERSION}")
ENDIF(UNIX)

IF(UNIX AND NOT APPLE)
	SET_TARGET_PROPERTIES ( ${LIB_NAME} PROPERTIES LINK_FLAGS "-Wl,-E")
ENDIF(UNIX AND NOT APPLE)

