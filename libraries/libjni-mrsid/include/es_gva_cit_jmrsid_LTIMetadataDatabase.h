/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class es_gva_cit_jmrsid_LTIMetadataDatabase */

#ifndef _Included_es_gva_cit_jmrsid_LTIMetadataDatabase
#define _Included_es_gva_cit_jmrsid_LTIMetadataDatabase
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     es_gva_cit_jmrsid_LTIMetadataDatabase
 * Method:    getDataByIndexNat
 * Signature: (JI)J
 */
JNIEXPORT jlong JNICALL Java_es_gva_cit_jmrsid_LTIMetadataDatabase_getDataByIndexNat
  (JNIEnv *, jobject, jlong, jint);

/*
 * Class:     es_gva_cit_jmrsid_LTIMetadataDatabase
 * Method:    FreeLTIMetadataDatabaseNat
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_es_gva_cit_jmrsid_LTIMetadataDatabase_FreeLTIMetadataDatabaseNat
  (JNIEnv *, jobject, jlong);

#ifdef __cplusplus
}
#endif
#endif
