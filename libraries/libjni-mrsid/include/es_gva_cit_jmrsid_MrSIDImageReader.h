/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class es_gva_cit_jmrsid_MrSIDImageReader */

#ifndef _Included_es_gva_cit_jmrsid_MrSIDImageReader
#define _Included_es_gva_cit_jmrsid_MrSIDImageReader
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     es_gva_cit_jmrsid_MrSIDImageReader
 * Method:    MrSIDImageReaderNat
 * Signature: (Ljava/lang/String;)J
 */
JNIEXPORT jlong JNICALL Java_es_gva_cit_jmrsid_MrSIDImageReader_MrSIDImageReaderNat
  (JNIEnv *, jobject, jstring);

/*
 * Class:     es_gva_cit_jmrsid_MrSIDImageReader
 * Method:    MrSIDImageReaderArrayNat
 * Signature: ([B)J
 */
JNIEXPORT jlong JNICALL Java_es_gva_cit_jmrsid_MrSIDImageReader_MrSIDImageReaderArrayNat
  (JNIEnv *, jobject, jbyteArray);

/*
 * Class:     es_gva_cit_jmrsid_MrSIDImageReader
 * Method:    FreeMrSIDImageReaderNat
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_es_gva_cit_jmrsid_MrSIDImageReader_FreeMrSIDImageReaderNat
  (JNIEnv *, jobject, jlong);

#ifdef __cplusplus
}
#endif
#endif
