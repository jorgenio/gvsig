#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include "libcs2cs.h"

int main(void) 
{
	//char argv[][]="cs2cs +proj=latlong +ellps=intl +nadgrids=sped2et.gsb +to +proj=latlong +ellps=GRS80 -w6 -v";
	char* arg1="cs2cs";
	char* arg2="+proj=latlong";
	char* arg3="+ellps=intl";
	char* arg4="+nadgrids=sped2et.gsb";
	char* arg5="+to";
	char* arg6="+proj=latlong";
	char* arg7="+ellps=GRS80";
	char* arg8="-w6";
	char* arg9="-v";
	char* argv[]={arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9};
	
	int nargv=9;
	double pi=4*atan(1.0);
	double latitude=40.4083676250000*pi/180.0;
	double longitude=-3.4083676250000*pi/180.0;
	double z=0.0;
	int libcs2csError;
	libcs2csError=libcs2cs(nargv,argv,&longitude,&latitude,&z);
	if(libcs2csError==0)
	{
		printf("latitud  = %10.8f\n",latitude*180.0/pi);
		printf("longitud = %10.8f\n",longitude*180.0/pi);
		printf("altitud  = %10.3f\n",z);
	}
	exit(0);
}
