#!/bin/bash

# Si se produce un error, salimos inmediatamente
set -e

DIR_BASE=`pwd`

source variables.sh
# Get a version with format 1_0_alpha
UNDERSCORE_VERSION=`echo $FULLVERSION | sed 'y/./_/'`
# Get a version with format 10_alpha
BRIEFVERSION=`echo $FULLVERSION | sed 's/\.//'`
# The name of the dir which will be ZIPed, containing the resulting installer
INSTALLER_DIR="$APPNAME"-$FULLVERSION\_installer
JARNAME="$APPNAME"-"$FULLVERSION".jar
# The extension targets on the this version of gvSIG:
GVSIG_VERSION=1.0

GVSIG_BUILDNUMBER=`sed -n 's/build.number=//p' ../../_fwAndami/gvSIG/extensiones/com.iver.cit.gvsig/build.number`

GVSIG_FULLVERSION=gvSIG-$GVSIG_VERSION-$GVSIG_BUILDNUMBER

cd ../../_fwAndami
ANDAMI_PATH=`pwd`

cd ../install
INSTALL_GVSIG_PATH=`pwd`
INSTALL_GVSIG_LINUX_PATH="$INSTALL_GVSIG_PATH"/instalador-gvSIG-lin
INSTALL_GVSIG_WINDOWS_PATH="$INSTALL_GVSIG_PATH"/instalador-gvSIG-win

cd "$DIR_BASE"

DIR_LIN=linux
DIR_WIN=windows
WINDOWSZIP="$APPNAME"-"$BRIEFVERSION"\_"$GVSIG_FULLVERSION"-windows-i586.7z
LINUXZIP="$APPNAME"-"$BRIEFVERSION"\_"$GVSIG_FULLVERSION"-linux-i586.tgz

LINUXBIN="$APPNAME"-"$BRIEFVERSION"\_"$GVSIG_FULLVERSION"-linux-i586.bin
LINUXBINWITHJRE="$APPNAME"-"$BRIEFVERSION"\_"$GVSIG_FULLVERSION"-linux-i586-withjre.bin

WINDOWSEXE="$APPNAME"-"$BRIEFVERSION"\_"$GVSIG_FULLVERSION"-windows-i586.exe
WINDOWSEXEWITHJRE="$APPNAME"-"$BRIEFVERSION"\_"$GVSIG_FULLVERSION"-windows-i586-withjre.exe

#Directorios
OUTPUT_DIR="$TARGET_DIR"/v"$FULLVERSION"\_"$GVSIG_FULLVERSION"
PATH_SOURCE_EXT="$ANDAMI_PATH"/gvSIG/extensiones
PATH_SOURCE_MANDAT_EXT="$ANDAMI_PATH"/gvSIG/extensiones


echo "*****************"
echo "      BASE       "
echo "*****************"
# Para Linux
echo -n "$DIR_BASE"/"$DIR_LIN" "-- "
cd "$DIR_BASE"/"$DIR_LIN"
rm bin -rf
if [ ! -d libs ] ; then
   ln -s "$INSTALL_GVSIG_LINUX_PATH"/libs
fi
mkdir -p bin/gvSIG/extensiones
cp resources/gpl.txt bin
# No enlazamos directamente el directorio lib para no incluir el directorio CVS
mkdir -p bin/lib
cd "$DIR_BASE"/"$DIR_LIN"/bin/lib
for i in "$ANDAMI_PATH"/lib/*.jar ; do
  ln -s "$i" .
done
for i in "$ANDAMI_PATH"/lib/*.zip ; do
  ln -s "$i" .
done
cd "$DIR_BASE"/"$DIR_LIN"
ln -s "$ANDAMI_PATH"/andami.jar bin/
ln -s "$ANDAMI_PATH"/castor.properties bin/
#cp resources/andami-config.xml bin
echo OK.


# Para Windows
echo -n "$DIR_BASE"/"$DIR_WIN" "-- "
cd "$DIR_BASE"/"$DIR_WIN"
rm bin -rf
if [ ! -d libs ] ; then
   ln -s "$INSTALL_GVSIG_WINDOWS_PATH"/libs
fi
mkdir -p bin/gvSIG/extensiones
cp resources/gpl.txt bin
# No enlazamos directamente el directorio lib para no incluir el directorio CVS
mkdir -p bin/lib
cd "$DIR_BASE"/"$DIR_WIN"/bin/lib
for i in "$ANDAMI_PATH"/lib/*.jar ; do
  ln -s "$i" .
done
for i in "$ANDAMI_PATH"/lib/*.zip ; do
  ln -s "$i" .
done
cd "$DIR_BASE"/"$DIR_WIN"
ln -s "$ANDAMI_PATH"/andami.jar bin
ln -s "$ANDAMI_PATH"/castor.properties bin/
#cp resources/andami-config.xml bin

#Copiamos el lanzador y sus tracuciones al tmpResources
rm -Rf tmpResources
mkdir tmpResources
cp -R $INSTALL_GVSIG_PATH/launcher/izpack-launcher-1.3/dist/* ./tmpResources
mv ./tmpResources/launcher-Win32.exe ./tmpResources/gvSIG.exe
#Quitamos el ini, manifest y los CVS
rm ./tmpResources/*.ini ./tmpResources/*.manifest
find ./tmpResources -name CVS -type d -exec rm -rf {} 2> /dev/null ';' || true
echo OK.

# Estas extensiones se copian directamente al directorio destino, ya que no vamos a dar
# opcion a no instalarlas, son obligatorias
cd "$DIR_BASE"
i=0
while [ ! -z ${MANDATORY_EXTENSIONS[$i]} ]
do
  echo ln -s "$PATH_SOURCE_MANDAT_EXT"/${MANDATORY_EXTENSIONS[$i]} "$DIR_LIN"/bin/gvSIG/extensiones
  ln -s "$PATH_SOURCE_MANDAT_EXT"/${MANDATORY_EXTENSIONS[$i]} "$DIR_LIN"/bin/gvSIG/extensiones
  echo ln -s "$PATH_SOURCE_MANDAT_EXT"/${MANDATORY_EXTENSIONS[$i]} "$DIR_WIN"/bin/gvSIG/extensiones
  ln -s "$PATH_SOURCE_MANDAT_EXT"/${MANDATORY_EXTENSIONS[$i]} "$DIR_WIN"/bin/gvSIG/extensiones
  i=`expr $i + 1`
done


echo "*****************"
echo "   EXTENSIONES   "
echo "*****************"

rm -rf $DIR_LIN/extensiones
mkdir $DIR_LIN/extensiones
rm -rf $DIR_WIN/extensiones
mkdir $DIR_WIN/extensiones

i=0
while [ ! -z ${EXTENSIONS[$i]} ]
do
  echo "Copiando "${EXTENSIONS[$i]}
  echo cp "$PATH_SOURCE_EXT"/${EXTENSIONS[$i]} "$DIR_LIN"/extensiones -rf
  cp "$PATH_SOURCE_EXT"/${EXTENSIONS[$i]} "$DIR_LIN"/extensiones -rf
  echo cp "$PATH_SOURCE_EXT"/${EXTENSIONS[$i]} "$DIR_WIN"/extensiones -rf
  cp "$PATH_SOURCE_EXT"/${EXTENSIONS[$i]} "$DIR_WIN"/extensiones -rf
  i=`expr $i + 1`
done


echo "*****************"
echo "    INST-WIN     "
echo "*****************"
# Generar el instalador (jar) para windows
cd "$DIR_BASE"/"$DIR_WIN"
rm "$JARNAME" -f
ant -DJARNAME="$JARNAME" -DGVSIG_VERSION="$GVSIG_VERSION" -DAPPNAME="$APPNAME" -DINSTALL_GVSIG_PATH="$INSTALL_GVSIG_PATH"

echo "*****************"
echo "    INST-LIN     "
echo "*****************"
# Generar el instalador (jar) para Linux
cd "$DIR_BASE"/"$DIR_LIN"
rm "$JARNAME" -f
ant -DJARNAME="$JARNAME" -DGVSIG_VERSION="$GVSIG_VERSION" -DAPPNAME="$APPNAME" -DINSTALL_GVSIG_PATH="$INSTALL_GVSIG_PATH"


echo "******************"
echo " GENERAR DISTRIB "
echo "******************"
# Generar el .bin para Linux y el .EXE para Windows

mkdir -p "$OUTPUT_DIR"
echo "- Linux"
## Nueva instalacion para linux
cd "$DIR_BASE"/"$DIR_LIN"
rm -Rf "$INSTALLER_DIR"
mkdir -p "$INSTALLER_DIR"/_tmp_install
cp "$JARNAME" "$INSTALLER_DIR"/_tmp_install
cd "$INSTALLER_DIR"
cp -R "$INSTALL_GVSIG_PATH"/launcher/izpack-launcher-1.3_linux/dist/* ./_tmp_install
find . -name CVS -type d -exec rm -rf {} 2> /dev/null ';' || true

rm ./_tmp_install/launcher.ini
cp "$DIR_BASE"/"$DIR_LIN"/resources/launcher.ini ./_tmp_install/launcher.ini
sed "s/%JARNAME%/$JARNAME/" ./_tmp_install/launcher.ini > ./_tmp_install/launcher.ini.bak
mv ./_tmp_install/launcher.ini.bak ./_tmp_install/launcher.ini

tar -cvzf ./tmp.tgz ./_tmp_install
cp "$INSTALL_GVSIG_LINUX_PATH"/jre/*.gz ./_tmp_install
tar -cvzf ./tmp_wjre.tgz ./_tmp_install
echo '#!/bin/sh' > xx.tmp
lcount=`cat xx.tmp "$DIR_BASE"/"$DIR_LIN"/resources/h_gvSIG-install.sh | wc -l`
lcount=$(($lcount+2)) # sumamos dos: uno por la linea siguiente y otro para el inicio
echo "lcount=$lcount" >> xx.tmp
cat xx.tmp "$DIR_BASE"/"$DIR_LIN"/resources/h_gvSIG-install.sh ./tmp.tgz  > "$LINUXBIN"
cat xx.tmp "$DIR_BASE"/"$DIR_LIN"/resources/h_gvSIG-install.sh ./tmp_wjre.tgz  > "$LINUXBINWITHJRE"
rm xx.tmp
chmod a+x "$LINUXBIN" "$LINUXBINWITHJRE"
mv "$LINUXBINWITHJRE" "$LINUXBIN" "$OUTPUT_DIR"
cd "$DIR_BASE"/"$DIR_LIN"
rm -Rf "$INSTALLER_DIR"
## Fin Nueva instalacion para linux
rm "$JARNAME"




## Para Windows
echo "- Windows"
#set -x
cd "$DIR_BASE"/"$DIR_WIN"
rm -Rf "$INSTALLER_DIR"
mkdir "$INSTALLER_DIR"
#cp -a installer_files "$INSTALLER_DIR"

cp -aR "$INSTALL_GVSIG_PATH"/launcher/izpack-launcher-1.3/dist/* "$INSTALLER_DIR"
rm -f "$INSTALLER_DIR"/install.bat

find "$INSTALLER_DIR" -name CVS -type d -exec rm -rf {} 2> /dev/null ';' || true

## hacemos las sustituciones de la variable
rm "$INSTALLER_DIR"/launcher-Win32.ini
cp resources/launcher-Win32.ini "$INSTALLER_DIR"/launcher-Win32.ini
sed "s/%JARNAME%/$JARNAME/" "$INSTALLER_DIR"/launcher-Win32.ini > "$INSTALLER_DIR"/launcher-Win32.ini.bak
mv "$INSTALLER_DIR"/launcher-Win32.ini.bak "$INSTALLER_DIR"/launcher-Win32.ini;


mv "$JARNAME" "$INSTALLER_DIR"
cd "$INSTALLER_DIR"

if [ -f  "$DIR_BASE"/"$DIR_WIN"/"$WINDOWSZIP" ] ; then
	rm -f "$DIR_BASE"/"$DIR_WIN"/"$WINDOWSZIP"
fi
"$INSTALL_GVSIG_PATH"/launcher/7z/7za a -t7z -r "$DIR_BASE"/"$DIR_WIN"/"$WINDOWSZIP" * -mx0
cd -
sed "s/%TITLE%/$APPNAME-$BRIEFVERSION\_$BUILDNUMBER/" "$INSTALL_GVSIG_PATH"/launcher/7z/dist_config.txt > dist_config.txt
cat "$INSTALL_GVSIG_PATH"/launcher/7z/7zS.sfx  dist_config.txt "$WINDOWSZIP" > "$WINDOWSEXE"

### paquete con los instalables de JRE, Jai y Jai i/o
"$INSTALL_GVSIG_PATH"/launcher/7z/7za a -t7z "$DIR_BASE"/"$DIR_WIN"/"$WINDOWSZIP"  "$INSTALL_GVSIG_WINDOWS_PATH"/jre_installers/*.exe -mx0
cat "$INSTALL_GVSIG_PATH"/launcher/7z/7zS.sfx  dist_config.txt "$WINDOWSZIP" > "$WINDOWSEXEWITHJRE"

###


rm dist_config.txt
rm "$WINDOWSZIP"

rm -Rf "$INSTALLER_DIR"

mv "$WINDOWSEXE" "$OUTPUT_DIR"
mv "$WINDOWSEXEWITHJRE" "$OUTPUT_DIR"

# Limpiamos tmpResources
cd "$DIR_BASE"/"$DIR_WIN"
rm -r ./tmpResources
cd -

cd "$DIR_BASE"

#limpiamos
rm -r "$DIR_BASE"/"$DIR_WIN"/bin "$DIR_BASE"/"$DIR_WIN"/extensiones "$DIR_BASE"/"$DIR_WIN"/libs "$DIR_BASE"/"$DIR_WIN"/"$JARNAME" 2> /dev/null || true
rm -r "$DIR_BASE"/"$DIR_LIN"/bin "$DIR_BASE"/"$DIR_LIN"/extensiones "$DIR_BASE"/"$DIR_LIN"/libs "$DIR_BASE"/"$DIR_WIN"/"$JARNAME" 2> /dev/null || true
