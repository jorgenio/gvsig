#!/bin/sh

export APPNAME=gvSIG_pilot_redes
export FULLVERSION='1.0'
export TARGET_DIR=/tmp/versiones/


## estas extensiones se muestran en los packs, y se puede elegir instalarlos o no
## (el fichero install.xml también debe estar actualizado para reflejar esto)
EXTENSIONS=(
com.iver.cit.gvsig.graph
)

## estas extensiones se instalan pero no se muestran en los packs
MANDATORY_EXTENSIONS=(
com.iver.cit.gvsig.graph
)

