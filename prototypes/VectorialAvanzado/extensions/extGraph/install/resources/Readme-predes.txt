Network Extension Pilot version 1.0 (Build number #build.number#):

Before starting, check the following points:

1.- Download the sources for gvSIG v1.0.2. Follow the procedure described in the �Readme.txt� file to setup a working development environment.

*** Compilation

1.- Uncompress the pilot package into the source folder just created (gvSIG-1_0_2-src). Note: some files in the  'binaries' folder must be overwritten.

2.- (Only if using Eclipse) Import the projects:
  *1: Pick the the File/Import menu command.
  *2: Choose 'Existing Projects into workspace' and hit 'Next'
  *3: With the option 'Select root directory' press 'Browse...'
  *4: Locate the workspace folder (normally, this should be the default) and hit 'Accept'.
  *5: Click on 'Deselect All'
  *6: Mark the following project from the list: extGraph_predes.
  *7: Click on 'Finish'.

3.- Run the 'build.xml' in the project 'extGraph_predes' (using  Ant or Eclipse).

*** Execution

After the projects have been compiled, new menu options will appear to generate network topologies and run Shortest Path calculationss.

Also, in the property panel for vectorial layers, a new Dot Density symbology type will appear.

*** Building after changes

After modifying the pilot sources, the 'build.xml' within the projects must be run in the following order, using Ant or Eclipse:

  *1 libFMap
  *2 appgvSIG
  *3 extGraph_predes

For further assistance, please subscribe to the gvSIG developer list and send your questions there (see in http://www.gvsig.gva.es).

Thanks for developing on gvSIG.

Regards,

The gvSIG Team