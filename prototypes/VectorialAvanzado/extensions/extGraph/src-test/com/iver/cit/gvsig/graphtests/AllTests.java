package com.iver.cit.gvsig.graphtests;


import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(
				"Test for com.iver.cit.gvsig.graph.test");
		//$JUnit-BEGIN$
		suite.addTestSuite(TestLoader.class);
		//$JUnit-END$
		return suite;
	}

}


