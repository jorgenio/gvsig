package com.iver.cit.gvsig.graphtests;


import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import junit.framework.TestCase;

import org.cresques.cts.IProjection;

import com.hardcode.gdbms.driver.mysql.MySQLDriver;
import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.crs.CRSFactory;
import com.iver.cit.gvsig.fmap.edition.EditionException;
import com.iver.cit.gvsig.fmap.edition.writers.JdbcWriter;
import com.iver.cit.gvsig.fmap.edition.writers.dbf.DbfWriter;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.graph.GraphException;
import com.iver.cit.gvsig.graph.core.writers.NetworkFileRedWriter;
import com.iver.cit.gvsig.graph.core.writers.NetworkGvTableWriter;

public class TestNetworkGvTableWriter extends TestCase {
	NetworkGvTableWriter netBuilder = new NetworkGvTableWriter();
	FLyrVect lyr;
	/*
	 * Test method for
	 * 'com.iver.cit.gvsig.graph.core.NetworkWriter.writeNetwork()'
	 */
	public void testWriteNetwork() {
		try {
			long t1 = System.currentTimeMillis();
			netBuilder.writeNetwork();
			long t2 = System.currentTimeMillis();
			System.out.println("Building DBF time:" + (t2-t1) + " msecs.");
		} catch (EditionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	protected void setUp() throws Exception {
		LayerFactory
				.setDriversPath("../_fwAndami/gvSIG/extensiones/com.iver.cit.gvsig/drivers");
		IProjection prj = CRSFactory.getCRS("EPSG:23030");
		File shpFile = new File("c:/ejes.shp");
		lyr = (FLyrVect) LayerFactory.createLayer("Ejes",
				"gvSIG shp driver", shpFile, prj);

		String fieldType = "tipored";
		String fieldDist = "length";
		String fieldCost = "cost";
		String fieldSense = "";

		netBuilder.setLayer(lyr);
		netBuilder.setFieldCost(fieldCost);
		netBuilder.setFieldType(fieldType);
		netBuilder.setFieldDist(fieldDist);
		netBuilder.setFieldSense(fieldSense);
		DbfWriter nodeWriter = new DbfWriter();
		File nodeFile = new File("c:/nodes.dbf");
		nodeFile.delete();
		nodeWriter.setFile(nodeFile);

		File edgeFile = new File("c:/edges.dbf");
		DbfWriter edgeWriter = new DbfWriter();
		edgeWriter.setFile(edgeFile);
		
		
		netBuilder.setEdgeWriter(edgeWriter);
		netBuilder.setNodeWriter(nodeWriter);
		
		
		
	}

}
