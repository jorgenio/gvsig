/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.cit.gvsig;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.andami.preferences.IPreference;
import com.iver.andami.preferences.IPreferenceExtension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.cit.gvsig.fmap.drivers.DriverIOException;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.GraphicLayer;
import com.iver.cit.gvsig.fmap.layers.SelectableDataSource;
import com.iver.cit.gvsig.fmap.rendering.FGraphic;
import com.iver.cit.gvsig.graph.preferences.RoutePage;
import com.iver.cit.gvsig.gvsig.gui.styling.SymbolSelector;
import com.iver.cit.gvsig.project.documents.view.gui.View;

public class SymbolTest extends Extension implements IPreferenceExtension{
	private static final IPreference thePreferencePage = new RoutePage();

	public void initialize() {
		// TODO Auto-generated method stub

	}

	public void execute(String actionCommand) {
		IWindow w = PluginServices.getMDIManager().getActiveWindow();

		View v = (View) w;
		MapControl mCtrl = v.getMapControl();
		MapContext mc = mCtrl.getMapContext();

		GraphicLayer graphicLayer = mc.getGraphicsLayer();


		if (actionCommand.equals("DALE!")) {
			FLayer lyr = null;

			SymbolSelector symSel = new SymbolSelector(null, FShape.POLYGON);
			PluginServices.getMDIManager().addWindow(symSel);
			ISymbol sym = symSel.getSymbol();
//			DotDensityFillSymbol sym = new DotDensityFillSymbol();
//			sym.setDotColor(Color.RED);
//			sym.setDotCount(20);
//			sym.setDotSize(2);

			FLayer[] actives = mc.getLayers().getActives();
			for (int i = 0; i < actives.length; i++) {
				if (actives[i] instanceof FLyrVect)
					lyr = actives[i];
			}
			if (lyr == null) {
				System.err.println("no hay capas");
				return;
			}
			FLyrVect l = (FLyrVect) lyr;
			int idSymbol = graphicLayer.addSymbol(sym);
			graphicLayer.clearAllGraphics();
			try {
				SelectableDataSource sds = l.getRecordset();
				long rows = sds.getRowCount();
				for (int i = 0; i < rows; i++) {
					IGeometry geom = l.getSource().getShape(i);
					System.err.println(i+") "+geom.toString());
					FGraphic gra = new FGraphic(geom, idSymbol);
					graphicLayer.addGraphic(gra);
				}


			} catch (DriverException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (com.hardcode.gdbms.engine.data.driver.DriverException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DriverIOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mCtrl.drawGraphics();
		} else if (actionCommand.equals("CLEAN_GRAPHICS")) {
			graphicLayer.clearAllGraphics();
		}
	}

	public boolean isEnabled() {
		return true;
	}

	public boolean isVisible() {
		IWindow w = PluginServices.getMDIManager().getActiveWindow();
		try {
			View v = (View) w;
			MapContext mc = v.getMapControl().getMapContext();
			FLayer[] actives = mc.getLayers().getActives();
			for (int i = 0; i < actives.length; i++) {
				if (actives[i] instanceof FLyrVect)
					return true;
			}
		} catch (Exception e) {}
		return false;
	}

	public IPreference getPreferencesPage() {
		return thePreferencePage;
	}

}
