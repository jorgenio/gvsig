/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: AbstractFillSymbol.java 9299 2006-12-13 17:45:38Z  $
* $Log$
* Revision 1.6  2006-12-04 17:13:39  fjp
* *** empty log message ***
*
* Revision 1.5  2006/11/14 11:10:27  jaume
* *** empty log message ***
*
* Revision 1.4  2006/11/09 18:39:05  jaume
* *** empty log message ***
*
* Revision 1.3  2006/11/06 07:33:54  jaume
* javadoc, source style
*
* Revision 1.2  2006/10/31 16:16:34  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.fmap.core.symbols;

import java.awt.Color;

import javax.print.attribute.PrintRequestAttributeSet;

import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.core.ISymbol;

/**
 * Abstract class that any FILL SYMBOL should extend
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public abstract class AbstractFillSymbol implements ISymbol {
	Color color = Color.WHITE;
	Color outline = Color.BLACK; // TODO aix� canviar� a AbsrtactLineSymbol quan existisca
	protected boolean isShapeVisible;
	protected String desc;
	protected ISymbol[] layers;

	public boolean isShapeVisible() {
		return isShapeVisible;
	}

	public void setDescription(String desc) {
		this.desc = desc;
	}

	public String getDescription() {
		return desc;
	}

	public boolean isSuitableFor(IGeometry geom) {
		return geom.getGeometryType() == FShape.POLYGON;
	}

	public ISymbol getLayer(int layerIndex) {
		try{
			return layers[layerIndex];
		} catch (Exception e) {
			return null;
		}
	}

	public int getOnePointRgb() {
		return outline.getRGB();
	}

	public ISymbol getLayerByIndex(int i) {
		return layers[i];
	}
	
	public void setPrintingProperties(PrintRequestAttributeSet printProperties) {
		// TODO Auto-generated method stub
		
	}

}
