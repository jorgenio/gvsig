/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: AbstractMarkerSymbol.java 9299 2006-12-13 17:45:38Z  $
* $Log$
* Revision 1.4  2006-12-04 17:13:39  fjp
* *** empty log message ***
*
* Revision 1.3  2006/11/14 11:10:27  jaume
* *** empty log message ***
*
* Revision 1.2  2006/11/09 18:39:05  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/31 16:16:34  jaume
* *** empty log message ***
*
* Revision 1.4  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
* Revision 1.3  2006/10/26 16:27:33  jaume
* support for composite marker symbols (not tested)
*
* Revision 1.2  2006/10/26 07:46:58  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/25 10:50:41  jaume
* movement of classes and gui stuff
*
* Revision 1.1  2006/10/18 07:54:06  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.fmap.core.symbols;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.print.attribute.PrintRequestAttributeSet;

import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.core.ISymbol;

/**
 * Abstract class that any MARKER SYMBOL should extend.
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public abstract class AbstractMarkerSymbol implements ISymbol {
	protected Color color = Color.BLACK;
	protected boolean isShapeVisible = true;
	protected String desc;
	protected double rotation;
	protected Point2D offset;
	protected ISymbol[] subSymbols; // TODO maybe push it up to ISymbol

	public String getDescription() {
		return desc;
	}

	public boolean isShapeVisible() {
		return isShapeVisible;
	}
	public void setDescription(String desc) {
		this.desc = desc;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double r) {
		this.rotation = r;
	}
	public Point2D getOffset() {
		if (offset == null) {
			offset = new Point();
		}
		return offset;
	}

	public void setOffset(Point offset) {
		this.offset = offset;
	}

	/**
	 * TODO maybe push it up to ISymbol
	 * @param sym
	 */
	public void addSymbol(ISymbol sym) {
		int capacity = 0;
		if (subSymbols != null)
			capacity = subSymbols.length;
		ArrayList lst = new ArrayList(capacity);
		for (int i = 0; i < capacity; i++) {
			lst.add(subSymbols[i]);
		}
		subSymbols = (ISymbol[])lst.toArray(new ISymbol[0]);
	}

	/**
	 * TODO maybe push it up to ISymbol
	 * @param sym
	 * @return true if this symbol contains the removed one
	 */
	public boolean removeSymbol(ISymbol sym) {

		int capacity = 0;
		if (subSymbols != null)
			capacity = subSymbols.length;
		ArrayList lst = new ArrayList(capacity);
		for (int i = 0; i < capacity; i++) {
			lst.add(subSymbols[i]);
		}
		boolean contains = lst.remove(sym);
		subSymbols = (ISymbol[])lst.toArray(new ISymbol[0]);
		return contains;
	}

	public boolean isSuitableFor(IGeometry geom) {
		return geom.getGeometryType() == FShape.POINT;
	}

	public int getOnePointRgb() {
		return color.getRGB();
	}
	
	public void setPrintingProperties(PrintRequestAttributeSet printProperties) {
		// TODO Auto-generated method stub
		
	}

}
