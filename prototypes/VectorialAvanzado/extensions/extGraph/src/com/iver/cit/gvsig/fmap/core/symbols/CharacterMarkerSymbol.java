/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
 *
 * $Id: CharacterMarkerSymbol.java 9299 2006-12-13 17:45:38Z  $
 * $Log$
 * Revision 1.6  2006-12-04 17:13:39  fjp
 * *** empty log message ***
 *
 * Revision 1.5  2006/11/14 11:10:27  jaume
 * *** empty log message ***
 *
 * Revision 1.4  2006/11/09 18:39:05  jaume
 * *** empty log message ***
 *
 * Revision 1.3  2006/11/08 10:56:47  jaume
 * *** empty log message ***
 *
 * Revision 1.2  2006/11/06 17:08:45  jaume
 * *** empty log message ***
 *
 * Revision 1.1  2006/10/31 16:16:34  jaume
 * *** empty log message ***
 *
 * Revision 1.4  2006/10/30 19:30:35  jaume
 * *** empty log message ***
 *
 * Revision 1.3  2006/10/29 23:53:49  jaume
 * *** empty log message ***
 *
 * Revision 1.2  2006/10/26 16:27:33  jaume
 * support for composite marker symbols (not tested)
 *
 * Revision 1.1  2006/10/25 10:50:41  jaume
 * movement of classes and gui stuff
 *
 * Revision 1.3  2006/10/24 19:54:16  jaume
 * added IPersistence
 *
 * Revision 1.2  2006/10/24 08:02:51  jaume
 * *** empty log message ***
 *
 * Revision 1.1  2006/10/18 07:54:06  jaume
 * *** empty log message ***
 *
 *
 */
package com.iver.cit.gvsig.fmap.core.symbols;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import javax.print.attribute.PrintRequestAttributeSet;

import org.apache.batik.ext.awt.geom.PathLength;

import com.iver.cit.gvsig.fmap.core.FPoint2D;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.cit.gvsig.fmap.core.v02.FConstant;
import com.iver.cit.gvsig.fmap.core.v02.FConverter;
import com.iver.utiles.StringUtilities;
import com.iver.utiles.XMLEntity;
import com.vividsolutions.jts.geom.Geometry;


// TODO quitar batik-util.jar del classpath de extGraph cuando movamos esto a FMap
/**
 * Symbol that manages symbols from a TrueType font source
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class CharacterMarkerSymbol extends AbstractMarkerSymbol {
	private Font font = new Font("Arial", Font.PLAIN, 20);
	private int symbol;
	private ISymbol selectionSymbol;

	/**
	 * Creates a new instance of CharacterMarker with default values
	 *
	 */
	public CharacterMarkerSymbol() {
		super();
	}

	/**
	 * Creates a new instance of CharacterMarker specifying the marker source
	 * font, the character code corresponding to the symbol, and the color that
	 * will be used in rendering time.
	 *
	 * @param font -
	 *            src Font
	 * @param charCode -
	 *            character code of the symbol for this font
	 * @param color -
	 *            color to be used in when rendering.
	 */
	public CharacterMarkerSymbol(Font font, int charCode, Color color) {
		super();
		this.font = font;
		symbol = charCode;
		this.color = color;
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}

	public ISymbol getSymbolForSelection() {
		if (selectionSymbol == null) {
			XMLEntity xml = getXMLEntity();
			xml.putProperty("color", StringUtilities.color2String(Color.YELLOW));
			selectionSymbol = SymbolFactory.createFromXML(xml, getDescription() + " version for selection.");
		}
		return selectionSymbol;
	}

	public void draw(Graphics2D g, AffineTransform affineTransform, FShape shp) {
		g.setFont(getFont());
		g.setColor(color);
		g.rotate(getRotation());
		Point2D p = null;
		switch (shp.getShapeType()) {
		case FShape.POINT:
			p = new Point2D.Double(((FPoint2D) shp).getX(), ((FPoint2D) shp)
					.getY());
			break;
		case FShape.LINE:
			PathLength pathLen = new PathLength(shp);
			float midDistance = pathLen.lengthOfPath() / 2;
			p = pathLen.pointAtLength(midDistance);
			break;
		case FShape.POLYGON:
			Geometry geom = FConverter.java2d_to_jts(shp);
			com.vividsolutions.jts.geom.Point centroid = geom.getCentroid();
			p = new Point2D.Double(centroid.getX(), centroid.getY());
		}
		// p = affineTransform.transform(p, null);

		char[] text = new char[] { (char) symbol };
		g.drawChars(text, 0, text.length, (int) p.getX(), (int) p.getY());

		if (subSymbols!=null) {
			for (int i = 0; i < subSymbols.length; i++) {
				subSymbols[i].draw(g, affineTransform, shp);
			}
		}
	}

	public int getPixExtentPlus(Graphics2D g, AffineTransform affineTransform,
			Shape shp) {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public XMLEntity getXMLEntity() {
		XMLEntity xml = new XMLEntity();

		// the class name
		xml.putProperty("className", getClassName());

		// color
		xml.putProperty("color", StringUtilities.color2String(color));

		// font
		xml.putProperty("font", font.getFontName());

		// font style
		xml.putProperty("fontStyle", font.getStyle());

		// symbol code
		xml.putProperty("symbolCode", symbol);

		// description
		xml.putProperty("desc", desc);

		// is shape visible
		xml.putProperty("isShapeVisible", isShapeVisible);

		// x offset
		xml.putProperty("xOffset", getOffset().getX());

		// y offset
		xml.putProperty("yOffset", getOffset().getY());

		// rotation
		xml.putProperty("rotation", rotation);
		return xml;
	}

	public int getSymbolType() {
		return CHARACTER_MARKER;
	}

	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r) {
		g.rotate(getRotation());
		// Sirve de algo el scaleInstance???
		// g.setFont(font.deriveFont(scaleInstance));
		double h = r.getHeight();
		Font myFont = font.deriveFont(
				(float) (h * FConstant.FONT_HEIGHT_SCALE_FACTOR)).deriveFont(
						scaleInstance);
		g.setFont(myFont);
		g.setColor(color);
		r.y = (int) h;

		// center character in case the rectangle is not square
		int x = r.x;
		int y = r.y;
		int width = (int) r.getHeight();
		int height = (int) r.getWidth();
		int diff = (width-height);
		if (diff<0) {
			x -= diff /2;
		} else {
			y += diff /2;
		}

		// and apply the offset
		x = x + (int) offset.getX(); // TODO scale offset
		y = y + (int) offset.getY(); // TODO scale offset
		char[] text = new char[] { (char) symbol };
		g.drawChars(text, 0, text.length, x, y);

		// finally, draw the subsymbols if any
		if (subSymbols!=null) {
			for (int i = 0; i < subSymbols.length; i++) {
				subSymbols[i].drawInsideRectangle(g, scaleInstance, r);
			}
		}
	}

	public void setCharacter(int symbol) {
		this.symbol = symbol;
	}

	public String getClassName() {
		return this.getClass().getName();
	}

	public void setXMLEntity(XMLEntity xml) {
		color = StringUtilities.string2Color(xml.getStringProperty("color"));
		Point p = new Point();
		p.setLocation(xml.getDoubleProperty("xOffset"), xml.getDoubleProperty("yOffset"));

		desc = xml.getStringProperty("desc");
		font = new Font(xml.getStringProperty("font"),
				xml.getIntProperty("fontStyle"),
				100 /* or whatever*/);
		isShapeVisible = xml.getBooleanProperty("isShapeVisible");
		symbol = xml.getIntProperty("symbolCode");
		offset = p;
		rotation = xml.getDoubleProperty("rotation");

	}

}
