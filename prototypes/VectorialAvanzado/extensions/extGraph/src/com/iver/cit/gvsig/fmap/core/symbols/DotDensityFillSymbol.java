/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: DotDensityFillSymbol.java 8729 2006-11-14 11:12:48Z jaume $
* $Log$
* Revision 1.4  2006-11-14 11:10:27  jaume
* *** empty log message ***
*
* Revision 1.3  2006/11/13 09:15:23  jaume
* javadoc and some clean-up
*
* Revision 1.2  2006/11/09 18:39:05  jaume
* *** empty log message ***
*
* Revision 1.1  2006/11/09 10:22:50  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.fmap.core.symbols;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.Random;

import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.utiles.StringUtilities;
import com.iver.utiles.XMLEntity;

/**
 * <p>
 * Symbol that draws a set of points within a polygon. The amount of points is
 * defined by the field dotCount.<br>
 * </p>
 * <p>
 * This symbol only draws the points. The outline and the fill of the polygon is
 * handled by a SimpleFillSymboll where a DotDensityFillSymbol should be
 * embedded.<br>
 * </p>
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class DotDensityFillSymbol extends AbstractFillSymbol {
	private int  dotCount;
	private double dotSize;
	private double dotSpacing;
	private boolean fixedPlacement;
	public DotDensityFillSymbol() {
		super();
		isShapeVisible = true;
	}

	public ISymbol getSymbolForSelection() {
		return this; // the selection color is applied in the SimpleFillSymbol
	}

	public void draw(Graphics2D g, AffineTransform affineTransform, FShape shp) {
		int width = shp.getBounds().width;
		int height = shp.getBounds().height;
		int minx = shp.getBounds().x;
		int miny = shp.getBounds().y;
		Random random = new Random();
		g.setClip(shp);
		g.setColor(color);
		g.setBackground(null);
		int size = (int) dotSize;
		for (int i = 0; i < dotCount; i++) {
			int x = (int) Math.abs(random.nextDouble() * width);
			int y = (int) Math.abs(random.nextDouble() * height);
			x = x + minx;
			y = y + miny;
			g.fillRect(x, y, size, size);
		}
		g.setClip(null);
	}

	public int getPixExtentPlus(Graphics2D g, AffineTransform affineTransform,
			Shape shp) {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public XMLEntity getXMLEntity() {
		XMLEntity xml = new XMLEntity();
		xml.putProperty("className", getClassName());

		// color
		xml.putProperty("color", StringUtilities.color2String(color));

		// description
		xml.putProperty("desc", desc);

		// is shape visible
		xml.putProperty("isShapeVisible", isShapeVisible);

		// dot count
		xml.putProperty("dotCount", dotCount);

		// dot size
		xml.putProperty("dotSize", dotSize);

		// dot spacing
		xml.putProperty("dotSpacing", dotSpacing);

		return xml;
	}

	public int getSymbolType() {
		return FShape.POLYGON;
	}

	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r) {
		int x = r.x;
		int y = r.y;
		int width = r.width;
		int height= r.height;
		int size = height / 8;
		g.setColor(color);
		g.setBackground(null);
		g.fillRect((int) (x+height*0.2), (int) (y+width*0.2), size, size);
		g.fillRect((int) (x+height*0.25), (int) (y+width*0.7), size, size);
		g.fillRect((int) (x+height*0.65), (int) (y+width*0.5), size, size);
		g.fillRect((int) (x+height*0.8), (int) (y+width*0.1), size, size);
		g.fillRect((int) (x+height*0.8), (int) (y+width*0.8), size, size);
		g.fillRect((int) (x+height*0.9), (int) (y+width*0.3), size, size);
		g.fillRect((int) (x+height*1.1), (int) (y+width*0.8), size, size);
	}

	public String getClassName() {
		return getClass().getName();
	}

	public void setXMLEntity(XMLEntity xml) {
		// color
		color = StringUtilities.string2Color(xml.getStringProperty("color"));

		// description
		desc = xml.getStringProperty("desc");

		// is shape visible
		isShapeVisible = xml.getBooleanProperty("isShapeVisible");

		// dot count
		dotCount = xml.getIntProperty("dotCount");

		// dot size
		dotSize = xml.getDoubleProperty("dotSize");

		// dot spacing
		dotSpacing = xml.getDoubleProperty("dotSpacing");

		layers = new ISymbol[xml.getChildrenCount()];

	}

	public int getDotCount() {
		return dotCount;
	}

	public void setDotCount(int dotCount) {
		this.dotCount = dotCount;
	}

	public double getDotSize() {
		return dotSize;
	}

	public void setDotSize(double dotSize) {
		this.dotSize = dotSize;
	}

	public double getDotSpacing() {
		return dotSpacing;
	}

	public void setDotSpacing(double dotSpacing) {
		this.dotSpacing = dotSpacing;
	}

	public Color getDotColor() {
		return color;
	}

	public void setDotColor(Color dotColor) {
		this.color = dotColor;
	}

}
