/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: MarkerFillSymbol.java 8658 2006-11-09 18:39:05Z jaume $
* $Log$
* Revision 1.10  2006-11-09 18:39:05  jaume
* *** empty log message ***
*
* Revision 1.9  2006/11/09 10:22:50  jaume
* *** empty log message ***
*
* Revision 1.8  2006/11/08 13:05:51  jaume
* *** empty log message ***
*
* Revision 1.7  2006/11/08 10:56:47  jaume
* *** empty log message ***
*
* Revision 1.6  2006/11/07 08:52:30  jaume
* *** empty log message ***
*
* Revision 1.5  2006/11/06 17:08:45  jaume
* *** empty log message ***
*
* Revision 1.4  2006/11/06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.3  2006/11/06 07:33:54  jaume
* javadoc, source style
*
* Revision 1.2  2006/10/31 16:16:34  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.fmap.core.symbols;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.utiles.StringUtilities;
import com.iver.utiles.XMLEntity;

public class MarkerFillSymbol extends AbstractFillSymbol {
	private double rotation;
	private AbstractMarkerSymbol markerSymbol = new CharacterMarkerSymbol();
	private boolean grid = true;
	private double xOffset = 0;
	private double yOffset = 0;
	private double xSeparation = 20;
	private double ySeparation = 20;

	public boolean isGrid() {
		return grid;
	}

	public void setGrid(boolean grid) {
		this.grid = grid;
	}

	public AbstractMarkerSymbol getMarkerSymbol() {
		return markerSymbol;
	}

	public void setMarkerSymbol(AbstractMarkerSymbol markerSymbol) {
		this.markerSymbol = markerSymbol;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public double getXOffset() {
		return xOffset;
	}

	public void setXOffset(double offset) {
		xOffset = offset;
	}

	public double getXSeparation() {
		return xSeparation;
	}

	public void setXSeparation(double separation) {
		xSeparation = separation;
	}

	public double getYOffset() {
		return yOffset;
	}

	public void setYOffset(double offset) {
		yOffset = offset;
	}

	public double getYSeparation() {
		return ySeparation;
	}

	public void setYSeparation(double separation) {
		ySeparation = separation;
	}

	public ISymbol getSymbolForSelection() {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public void draw(Graphics2D g, AffineTransform affineTransform, FShape shp) {
		g.setColor(null);
		g.setStroke(new BasicStroke(0));
		int w=7;
		int h=7;

		BufferedImage bi = null;


		Rectangle rProv = new Rectangle();
		rProv.setFrame(0, 0,w,h);

		Paint resulPatternFill = null;
		bi= new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D gAux = bi.createGraphics();
		markerSymbol.drawInsideRectangle(gAux, new AffineTransform(), rProv);
		resulPatternFill = new TexturePaint(bi,rProv);
		g.setColor(null);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		g.setPaint(resulPatternFill);
		g.fill(shp);
		//g.draw(shp);
		if (layers!=null)  {
			for (int i = 0; i < layers.length; i++) {
				layers[i].draw(g, affineTransform, shp);
			}
		}
	}

	public int getPixExtentPlus(Graphics2D g, AffineTransform affineTransform, Shape shp) {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public XMLEntity getXMLEntity() {
		XMLEntity xml = new XMLEntity();
		xml.putProperty("className", getClassName());
		// color
		xml.putProperty("color", StringUtilities.color2String(color));

		xml.putProperty("rotation", rotation);
		xml.putProperty("grid", grid);
		xml.putProperty("xOffset", xOffset);
		xml.putProperty("yOffset", yOffset);
		xml.putProperty("xSeparation", xSeparation);
		xml.putProperty("ySeparation", ySeparation);

		xml.addChild(markerSymbol.getXMLEntity());
		return xml;
	}

	public int getSymbolType() {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public void drawInsideRectangle(Graphics2D g, AffineTransform scaleInstance, Rectangle r) {
		if (isGrid()) {
			// Use a TexturePaint
			int w=7;
			int h=7;

			BufferedImage bi = null;

			Rectangle rProv = new Rectangle();
			rProv.setFrame(0, 0,w,h);

			Paint resulPatternFill = null;
			bi= new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
			Graphics2D gAux = bi.createGraphics();
			markerSymbol.drawInsideRectangle(gAux, new AffineTransform(), rProv);
			resulPatternFill = new TexturePaint(bi,rProv);
			g.setColor(null);
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);

			g.setPaint(resulPatternFill);
			g.fill(r);
			g.drawRect(0, 0, r.width, r.height);
		} else {
			Point2D[] points = ramdonizePoints(r);
			for (int i = 0; i < points.length; i++) {

			}
		}
	}

	private Point2D[] ramdonizePoints(Rectangle r) {
		ArrayList points = new ArrayList();
			throw new Error("Not yet implemented!");
//		return (Point2D[]) points.toArray(new Point2D[0]);
	}

	public String getClassName() {
		return getClass().getName();
	}

	public void setXMLEntity(XMLEntity xml) {
		rotation = xml.getDoubleProperty("rotation");
		grid = xml.getBooleanProperty("grid");
		xOffset = xml.getDoubleProperty("xOffset");
		yOffset = xml.getDoubleProperty("yOffset");
		xSeparation = xml.getDoubleProperty("xSeparation");
		ySeparation = xml.getDoubleProperty("ySeparation");
		markerSymbol = (AbstractMarkerSymbol) SymbolFactory.
							createFromXML(xml.getChild(0), null);
	}

	public void setMarker(AbstractMarkerSymbol marker) {
		this.markerSymbol = marker;
	}

}
