/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: SimpleFillSymbol.java 8729 2006-11-14 11:12:48Z jaume $
* $Log$
* Revision 1.7  2006-11-14 11:10:27  jaume
* *** empty log message ***
*
* Revision 1.6  2006/11/09 18:39:05  jaume
* *** empty log message ***
*
* Revision 1.5  2006/11/09 10:22:50  jaume
* *** empty log message ***
*
* Revision 1.4  2006/11/08 13:05:51  jaume
* *** empty log message ***
*
* Revision 1.3  2006/11/08 10:56:47  jaume
* *** empty log message ***
*
* Revision 1.2  2006/11/06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.fmap.core.symbols;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.utiles.StringUtilities;
import com.iver.utiles.XMLEntity;

/**
 * Basic fill symbol. It will allow to paint a shape with its filling color (and transparency)
 * and the outline.
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class SimpleFillSymbol extends AbstractFillSymbol {
	private SimpleFillSymbol symbolForSelection;

	public ISymbol getSymbolForSelection() {
		if (symbolForSelection == null) {
			XMLEntity xml = getXMLEntity();
			xml.putProperty("color", StringUtilities.color2String(Color.YELLOW));
			symbolForSelection = (SimpleFillSymbol) SymbolFactory.
					createFromXML(xml, getDescription()+" version for selection");
		}
		return symbolForSelection;
	}

	public void draw(Graphics2D g, AffineTransform affineTransform, FShape shp) {
		g.setColor(color);
		g.fill(shp);
		g.setColor(outline);
		g.setStroke(new BasicStroke(1));
		g.draw(shp);
		for (int i = 0; i < layers.length; i++) {
			layers[i].draw(g, affineTransform, shp);
		}
	}

	public int getPixExtentPlus(Graphics2D g, AffineTransform affineTransform,
			Shape shp) {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public XMLEntity getXMLEntity() {
		XMLEntity xml = new XMLEntity();

		// the class name
		xml.putProperty("className", getClassName());

		// color
		xml.putProperty("color", StringUtilities.color2String(color));

		// outline
		xml.putProperty("outline", StringUtilities.color2String(outline));

		// description
		xml.putProperty("desc", desc);

		// is shape visible
		xml.putProperty("isShapeVisible", isShapeVisible);

		// layers
		if (layers!=null)
			for (int i = 0; i < layers.length; i++) {
				xml.addChild(layers[i].getXMLEntity());
			}
		return xml;
	}

	public int getSymbolType() {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public void drawInsideRectangle(Graphics2D g,
			AffineTransform scaleInstance, Rectangle r) {
		int height = (int) r.getHeight();
		int width = (int) r.getWidth();
		g.setColor(color);
		for (int i = 0; i < height; i++) {
			g.drawLine(0, i, width, i);
		}
		g.setColor(outline);
		// TODO canviar esto per draw de AbstractLineSymbol
		g.drawRect(0, 0, width, height);


		r.setFrame(((int) r.getMinX())+1, ((int) r.getMinY())+1, ((int) r.getMaxX())-1, ((int) r.getMaxY())-1);
		for (int i = 0; layers != null && i < layers.length; i++) {
			layers[i].drawInsideRectangle(g, scaleInstance, r);
		}
	}

	public String getClassName() {
		return getClass().getName();
	}

	public void setXMLEntity(XMLEntity xml) {
		// color
		color = StringUtilities.string2Color(xml.getStringProperty("color"));

		// outline
		outline = StringUtilities.string2Color(xml.getStringProperty("outline"));

		// description
		desc = xml.getStringProperty("desc");

		// is shape visible
		isShapeVisible = xml.getBooleanProperty("isShapeVisible");

		// layers
		layers = new ISymbol[xml.getChildrenCount()];
		for (int i = 0; i < layers.length; i++) {
			layers[i] = SymbolFactory.createFromXML(xml.getChild(i), null);
		}
	}
}
