/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: SimpleMarkerSymbol.java 8729 2006-11-14 11:12:48Z jaume $
* $Log$
* Revision 1.1  2006-11-14 11:10:27  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.fmap.core.symbols;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.utiles.StringUtilities;
import com.iver.utiles.XMLEntity;

public class SimpleMarkerSymbol extends AbstractMarkerSymbol {
	private boolean outline;
	private Color outlineColor;
	private double outlineSize;


	private ISymbol selectionSymbol;

	public ISymbol getSymbolForSelection() {
		if (selectionSymbol == null) {
			XMLEntity xml = getXMLEntity();
			xml.putProperty("color", StringUtilities.color2String(Color.YELLOW));
			selectionSymbol = SymbolFactory.createFromXML(xml, getDescription() + " version for selection.");
		}
		return selectionSymbol;
	}

	public void draw(Graphics2D g, AffineTransform affineTransform, FShape shp) {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");

	}

	public int getPixExtentPlus(Graphics2D g, AffineTransform affineTransform, Shape shp) {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");

	}

	public XMLEntity getXMLEntity() {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");

	}

	public int getSymbolType() {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");

	}

	public void drawInsideRectangle(Graphics2D g, AffineTransform scaleInstance, Rectangle r) {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");

	}

	public String getClassName() {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");

	}

	public void setXMLEntity(XMLEntity xml) {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");

	}

}