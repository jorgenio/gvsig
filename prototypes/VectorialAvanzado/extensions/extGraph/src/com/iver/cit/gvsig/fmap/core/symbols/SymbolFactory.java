/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: SymbolFactory.java 8537 2006-11-06 16:06:52Z jaume $
* $Log$
* Revision 1.3  2006-11-06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.2  2006/11/06 07:33:54  jaume
* javadoc, source style
*
* Revision 1.1  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.fmap.core.symbols;

import com.iver.andami.messages.NotificationManager;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.utiles.NotExistInXMLEntity;
import com.iver.utiles.XMLEntity;

/**
 * Factory for obtaining ISymbols of any kind from several sources like.
 * <ol>
 * 	<li>
 * 		<b>XMLEntity's</b> that, at least, contains a full class name
 * 			string property that defines which class handles such symbol.
 *  </li>
 * </ol>
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class SymbolFactory {
	/**
	 * Factory that allows to create ISymbol from an ISymbol xml
	 * descriptor. A barely specific XMLEntity object. The string passed in the
	 * second argument is the description text that will be used in case no description
	 * is supplied by the symbol
	 */
	public static ISymbol createFromXML(XMLEntity xml, String defaultDescription) {
		String className = null;
		try {
			className = xml.getStringProperty("className");
		} catch (NotExistInXMLEntity e) {
			NotificationManager.
				addError("Symbol class name not set.\n" +
						" Maybe you forgot to add the" +
						" putProperty(\"className\", yourClassName)" +
						" call in the getXMLEntity method of your symbol", e);
		}

		if (!xml.contains("desc"))
			xml.putProperty("desc", defaultDescription);

		Class clazz = null;
		ISymbol sym = null;

		try {
			clazz = Class.forName(className);
			sym = (ISymbol) clazz.newInstance();
			sym.setXMLEntity(xml);
		} catch (InstantiationException e) {
			NotificationManager.
				addError("Trying to instantiate an interface" +
						" or abstract class + "+className, e);
		} catch (IllegalAccessException e) {
			NotificationManager.addError(null, e);
		} catch (ClassNotFoundException e) {
			NotificationManager.addError("No class called " + className +
					" was found.\nCheck the following.\n<br>" +
					"\t- The fullname of the class you're looking " +
						"for matches the value in the className " +
						"property of the XMLEntity ("+className+").\n<br>" +
					"\t- The jar file containing your symbol class is in" +
						"the application classpath<br>", e);
		}
		return sym;
	}
}