/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: DotDensityLegend.java 8852 2006-11-17 12:49:58Z jaume $
* $Log$
* Revision 1.2  2006-11-17 12:49:58  jaume
* *** empty log message ***
*
* Revision 1.1  2006/11/14 12:30:26  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.fmap.rendering;

import java.awt.Color;

import com.hardcode.gdbms.engine.values.NumericValue;
import com.hardcode.gdbms.engine.values.Value;
import com.iver.cit.gvsig.fmap.core.IFeature;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.cit.gvsig.fmap.core.symbols.DotDensityFillSymbol;
import com.iver.cit.gvsig.fmap.core.symbols.SimpleFillSymbol;
import com.iver.cit.gvsig.fmap.core.symbols.SymbolFactory;
import com.iver.utiles.StringUtilities;
import com.iver.utiles.XMLEntity;


public class DotDensityLegend extends VectorialUniqueValueLegend {


    private double dotValue;

    public ISymbol getSymbolByValue(Value key) {
        SimpleFillSymbol sym = (SimpleFillSymbol) getDefaultSymbol();
        ((DotDensityFillSymbol) sym.getLayerByIndex(0)).
            setDotCount((int) (((NumericValue) key).doubleValue()/dotValue));
        return sym;
    }

    public ISymbol getSymbolByFeature(IFeature feat) {
        return getSymbolByValue(feat.getAttribute(fieldId));
    }

    public void setDotValue(double dotValue) {
        this.dotValue = dotValue;
    }

    public XMLEntity getXMLEntity() {
        XMLEntity xml = new XMLEntity();
        xml.putProperty("className", getClass().getName());
        xml.putProperty("dotValue", dotValue);
        xml.putProperty("fieldName", getFieldName());
        xml.addChild(getDefaultSymbol().getXMLEntity());
        return xml;
    }

    public void setXMLEntity(XMLEntity xml) {
        dotValue = xml.getDoubleProperty("dotValue");
        setFieldName(xml.getStringProperty("fieldName"));
        setDefaultSymbol(SymbolFactory.createFromXML(xml.getChild(0), null));
    }

    public Color getOutlineColor() {
    	try {
            XMLEntity xml = getDefaultSymbol().getXMLEntity();
        	return StringUtilities.string2Color(xml.getStringProperty("outline"));
        } catch (NullPointerException npE) {
        	return null;
        }
    }

    public Color getDotColor() {
    	try {
            XMLEntity xml = getDefaultSymbol().getXMLEntity().getChild(0);
        	return StringUtilities.string2Color(xml.getStringProperty("color"));
        } catch (NullPointerException npE) {
        	return null;
        }
    }

    public Color getBGColor() {
    	try {
            XMLEntity xml = getDefaultSymbol().getXMLEntity();
        	return StringUtilities.string2Color(xml.getStringProperty("color"));
        } catch (NullPointerException npE) {
        	return null;
        }
    }

    public double getDotValue() {
    	try {
        	XMLEntity xml = getXMLEntity();
    		return xml.getDoubleProperty("dotValue");
    	} catch (NullPointerException npE) {
    		return 0;
    	}
    }

    public double getDotSize() {
    	try {
    		XMLEntity xml = getDefaultSymbol().getXMLEntity().getChild(0);
    		return xml.getDoubleProperty("dotSize");
    	} catch (NullPointerException npE) {
    		return -1; // TODO define default
    	}
    }
}
