/*
 * Created on 20-jun-2006 by azabala
 *
 */
package com.iver.cit.gvsig.graph;

/**
 * Exception throwed by any class of the Graph package.
 * @author alzabord
 *
 */
public class GraphException extends Exception {
	public GraphException(Exception e){
		super(e);
	}
	
	public GraphException(){
		super();
	}
	
	public GraphException(String msg){
		super(msg);
	}
	
	public GraphException(String msg, Exception e){
		super(msg, e);
	}
	
	
}
