/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.cit.gvsig.graph;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.SingleLayerIterator;
import com.iver.cit.gvsig.fmap.tools.Behavior.PointBehavior;
import com.iver.cit.gvsig.graph.core.Network;
import com.iver.cit.gvsig.graph.tools.BarrierListener;
import com.iver.cit.gvsig.graph.tools.FlagListener;
import com.iver.cit.gvsig.project.documents.view.gui.View;

public class NetworkAddFlag extends Extension {		
    FlagListener flagListener = null;
    BarrierListener barrierListener = null;
	public void initialize() {
	}

	public void execute(String actionCommand) {
		View v = (View) PluginServices.getMDIManager().getActiveWindow();
        MapControl mapCtrl = v.getMapControl();

        if (!mapCtrl.hasTool("addFlag")) // We create it for the first time.
        {
        	flagListener = new FlagListener(mapCtrl);
            mapCtrl.addMapTool("addFlag", new PointBehavior(flagListener));
        }

        if (!mapCtrl.hasTool("addBarrier")) // We create it for the first time.
        {
        	barrierListener = new BarrierListener(mapCtrl);
            mapCtrl.addMapTool("addBarrier", new PointBehavior(barrierListener));
        }
        
        if (actionCommand.compareTo("ADD_FLAG_TO_NETWORK") == 0)
        {
        	flagListener.setMode(FlagListener.TO_ARC);
            mapCtrl.setTool("addFlag");
        }
        if (actionCommand.compareTo("ADD_FLAG_TO_NODE") == 0)
        {
        	flagListener.setMode(FlagListener.TO_NODE);
            mapCtrl.setTool("addFlag");
        }
        if (actionCommand.compareTo("ADD_BARRIER") == 0)
        {
            mapCtrl.setTool("addBarrier");
        }        
//        else
//        {
//        	JOptionPane.showMessageDialog((JComponent) PluginServices.getMDIManager().getActiveWindow(), 
//        			"Not implemented yet");
//        }

		
	}

	public boolean isEnabled() {
		IWindow window = PluginServices.getMDIManager().getActiveWindow();
		if (window instanceof View)
		{
			View v = (View) window;
	        MapControl mapCtrl = v.getMapControl();
			MapContext map = mapCtrl.getMapContext();
			
			SingleLayerIterator it = new SingleLayerIterator(map.getLayers());
			while (it.hasNext())
			{
				FLayer aux = it.next();
				if (!aux.isActive())
					continue;
				Network net = (Network) aux.getProperty("network");
				
				if ( net != null)
				{
					return true;
				}
			}
		}
		return false;

	}

	public boolean isVisible() {
		IWindow window = PluginServices.getMDIManager().getActiveWindow();
		if (window instanceof View)
		{
			View v = (View) window;
	        MapControl mapCtrl = v.getMapControl();
			MapContext map = mapCtrl.getMapContext();
			
			SingleLayerIterator it = new SingleLayerIterator(map.getLayers());
			while (it.hasNext())
			{
				FLayer aux = it.next();
				if (!aux.isActive())
					continue;
				Network net = (Network) aux.getProperty("network");
				
				if ( net != null)
				{
					return true;
				}
			}
		}
		return false;

	}

}


