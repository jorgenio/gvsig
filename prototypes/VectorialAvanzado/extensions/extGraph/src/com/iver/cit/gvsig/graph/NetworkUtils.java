/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.cit.gvsig.graph;

import java.awt.Color;
import java.io.File;

import javax.swing.ImageIcon;

import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.core.ShapeFactory;
import com.iver.cit.gvsig.fmap.core.v02.FConstant;
import com.iver.cit.gvsig.fmap.core.v02.FSymbol;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.GraphicLayer;
import com.iver.cit.gvsig.fmap.rendering.FGraphic;
import com.iver.cit.gvsig.graph.core.GvFlag;
import com.iver.cit.gvsig.util.GvSession;

public class NetworkUtils {
	static int idSymbolFlag = -1;
	public static void clearBarriersFromGraphics(MapControl mc) {
		GraphicLayer graphics = mc.getMapContext().getGraphicsLayer();
		for (int i = graphics.getNumGraphics() - 1; i >= 0; i--) {
			FGraphic aux = graphics.getGraphic(i);
			if (aux.getTag() != null)
				if (aux.getTag().equalsIgnoreCase("BARRIER"))
					graphics.removeGraphic(aux);
		}
	}

	public static void clearFlagsFromGraphics(MapControl mc) {
		GraphicLayer graphics = mc.getMapContext().getGraphicsLayer();
		for (int i = graphics.getNumGraphics() - 1; i >= 0; i--) {
			FGraphic aux = graphics.getGraphic(i);
			if (aux.getTag() != null)
				if (aux.getTag().equalsIgnoreCase("FLAG"))
					graphics.removeGraphic(aux);
		}
	}

	public static void clearFlagFromGraphics(MapControl mc, GvFlag flag) {
		GraphicLayer graphics = mc.getMapContext().getGraphicsLayer();
		FGraphic graphic = graphics.getGraphicByObjectTag(flag);
		graphics.removeGraphic(graphic);
	}

	public static void clearRouteFromGraphics(MapControl mc) {
		GraphicLayer graphics = mc.getMapContext().getGraphicsLayer();
		for (int i = graphics.getNumGraphics() - 1; i >= 0; i--) {
			FGraphic aux = graphics.getGraphic(i);
			if (aux.getTag() != null)
				if (aux.getTag().equalsIgnoreCase("ROUTE"))
					graphics.removeGraphic(aux);
		}
	}
	
	public static File getNetworkFile(FLayer lyr) {
			String directoryName = System.getProperty("java.io.tmpdir");
			String aux = lyr.getName().replaceAll("\\Q.shp\\E", ".net");
			File newFile = new File(directoryName +
					File.separator +
					aux);
			return newFile;

	}

	public static void addGraphicFlag(MapControl mapControl, GvFlag flag) {
		GraphicLayer graphicLayer = mapControl.getMapContext().getGraphicsLayer();
		if (idSymbolFlag == -1)
		{
			FSymbol simFlag = new FSymbol(FShape.POINT, Color.BLUE);
			simFlag.setSize(24);
			simFlag.setStyle(FConstant.SYMBOL_STYLE_MARKER_IMAGEN);
			ImageIcon icon = new ImageIcon(NetworkUtils.class.getClassLoader()
					.getResource("images/pushpin.png"));
			simFlag.setIcon(icon.getImage());
			idSymbolFlag = graphicLayer.addSymbol(simFlag);
		}
		IGeometry gAux = ShapeFactory.createPoint2D(
				flag.getOriginalPoint().getX(), flag.getOriginalPoint().getY());
		FGraphic graphic = new FGraphic(gAux, idSymbolFlag);
		graphic.setTag("FLAG");
		graphic.setObjectTag(flag);
		graphicLayer.addGraphic(graphic);

	}
}
