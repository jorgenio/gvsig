/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.cit.gvsig.graph;

import java.awt.Component;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.SingleLayerIterator;
import com.iver.cit.gvsig.graph.core.GvFlag;
import com.iver.cit.gvsig.graph.core.Network;
import com.iver.cit.gvsig.graph.solvers.OneToManySolver;
import com.iver.cit.gvsig.project.documents.view.gui.View;

public class ODMatrixExtension extends Extension {



	public void initialize() {
	}

	public void execute(String actionCommand) {
		
		View v = (View) PluginServices.getMDIManager().getActiveWindow();
		MapContext map = v.getMapControl().getMapContext();
		SingleLayerIterator it = new SingleLayerIterator(map.getLayers());
		
		if (actionCommand.equals("ODMATRIX")) {
			while (it.hasNext())
			{
				FLayer aux = it.next();
				if (!aux.isActive())
					continue;
				Network net = (Network) aux.getProperty("network");

				if ( net != null)
				{
					GvFlag[] flags = net.getFlags();
					if(flags.length == 0)
					{
						JOptionPane.showMessageDialog(null, "Primero carga las paradas.");
						return;
					}

//					PluginServices.getMDIManager().addWindow(new RouteControlPanel(net));
					JFileChooser dlg = new JFileChooser();
					if (dlg.showSaveDialog((Component) PluginServices.getMainFrame()) == JFileChooser.APPROVE_OPTION)
					{
//						RandomAccessFile file = null;
//						try {
//							file = new RandomAccessFile(dlg.getSelectedFile(), "rw");
//						} catch (FileNotFoundException e1) {
//							// TODO Auto-generated catch block
//							e1.printStackTrace();
//						}
						BufferedWriter output;
						try {
							output = new BufferedWriter(new FileWriter(dlg.getSelectedFile()));
	//						output.setByteOrder(ByteOrder.LITTLE_ENDIAN);
	
							OneToManySolver solver = new OneToManySolver();
							solver.setNetwork(net);
							solver.putDestinationsOnNetwork();
							for (int i=0; i < flags.length; i++)
							{
								
								solver.setSourceFlag(flags[i]);
								long t1 = System.currentTimeMillis();
								
								solver.calculate();
								long t2 = System.currentTimeMillis();
								System.out.println("Punto " + i + " de " + flags.length + ". " + (t2-t1) + " msecs.");
								// Escribe el resultado
								// idNodo1 idNodo2 tiempo distancia
								
								for (int j=0; j < flags.length; j++)
								{
									long secs = Math.round(flags[j].getCost());
									long meters = Math.round(flags[j].getAccumulatedLength());
									String strAux = i + "\t" + j + "\t" + secs + "\t" + meters;
									output.write(strAux);
									output.newLine();
									
								}
								long t3 = System.currentTimeMillis();
								System.out.println("T. de escritura: " + (t3-t2) + " msecs.");
								
							}
							solver.removeDestinationsFromNetwork();
							output.flush();
							output.close();
							JOptionPane.showMessageDialog((Component) PluginServices.getMainFrame(),
									PluginServices.getText(this,"fichero_generado"));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (GraphException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} // if
					
					return;
				}
			}
		}
		

	}

	
	public boolean isEnabled() {
		IWindow window = PluginServices.getMDIManager().getActiveWindow();
		if (window instanceof View)
		{
			View v = (View) window;
	        MapControl mapCtrl = v.getMapControl();
			MapContext map = mapCtrl.getMapContext();
			
			SingleLayerIterator it = new SingleLayerIterator(map.getLayers());
			while (it.hasNext())
			{
				FLayer aux = it.next();
				if (!aux.isActive())
					continue;
				Network net = (Network) aux.getProperty("network");
				
				if ( net != null)
				{
					return true;
				}
			}
		}
		return false;
	}

	public boolean isVisible() {
		IWindow f = PluginServices.getMDIManager()
		 .getActiveWindow();
		if (f == null) {
		    return false;
		}
		if (f instanceof View) {
			return true;
		}
		return false;

	}


}


