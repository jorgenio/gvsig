/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.cit.gvsig.graph;

import java.awt.Color;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.core.ShapeFactory;
import com.iver.cit.gvsig.fmap.core.v02.FConstant;
import com.iver.cit.gvsig.fmap.core.v02.FLabel;
import com.iver.cit.gvsig.fmap.core.v02.FSymbol;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.GraphicLayer;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.fmap.layers.SingleLayerIterator;
import com.iver.cit.gvsig.fmap.rendering.FGraphic;
import com.iver.cit.gvsig.fmap.rendering.FGraphicLabel;
import com.iver.cit.gvsig.graph.core.GvEdge;
import com.iver.cit.gvsig.graph.core.GvFlag;
import com.iver.cit.gvsig.graph.core.GvNode;
import com.iver.cit.gvsig.graph.core.IGraph;
import com.iver.cit.gvsig.graph.core.Network;
import com.iver.cit.gvsig.graph.solvers.EdgesMemoryDriver;
import com.iver.cit.gvsig.graph.solvers.OneToManySolver;
import com.iver.cit.gvsig.project.documents.view.gui.View;

/**
 * @author fjp
 * 
 * Extension to perform ServiceArea calculations. Here you will find code to:
 * 1.- See the distances to every node on the network to one or many point
 * sources. 2.- TODO: Calculate a polyline layer with costs and length
 * calculated to nearest source point. 3.- TODO: Calculate polygons covering
 * those service areas.
 */
public class ServiceAreaExtension extends Extension {

	private int idSymbolLine = -1;

	public void initialize() {
	}

	public void execute(String actionCommand) {

		View v = (View) PluginServices.getMDIManager().getActiveWindow();
		MapControl mapCtrl = v.getMapControl();
		MapContext map = mapCtrl.getMapContext();
		SingleLayerIterator it = new SingleLayerIterator(map.getLayers());

		if (actionCommand.equals("LABEL_NODE_DISTANCES")) {
			while (it.hasNext()) {
				FLayer aux = it.next();
				if (!aux.isActive())
					continue;
				Network net = (Network) aux.getProperty("network");

				if (net != null) {
					GvFlag[] flags = net.getFlags();
					if (flags.length == 0) {
						JOptionPane.showMessageDialog(null,
								"Primero carga las paradas.");
						return;
					}
//					setVelocities(net);
					try {
						OneToManySolver solver = new OneToManySolver();
						solver.setNetwork(net);
						solver.putDestinationsOnNetwork();
						GraphicLayer graphicLayer = mapCtrl.getMapContext()
								.getGraphicsLayer();
						removeOldLabels(graphicLayer);
						for (int i = 0; i < flags.length; i++) {

							solver.setSourceFlag(flags[i]);
							long t1 = System.currentTimeMillis();
							solver.setExploreAllNetwork(true);
							solver.calculate();
							long t2 = System.currentTimeMillis();
							System.out.println("Punto " + i + " de "
									+ flags.length + ". " + (t2 - t1)
									+ " msecs.");
							// Despu�s de esto, los nodos de la red est�n
							// etiquetados con los costes al nodo or�gen
							EdgesMemoryDriver driver = new EdgesMemoryDriver(net);
							FLayer lyr = LayerFactory.createLayer("Edges", driver, null);
							map.getLayers().addLayer(lyr);
							// doLabeling(mapCtrl, net, flags[i]);

						}

						solver.removeDestinationsFromNetwork();
					} catch (GraphException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					return;
				}
			}
		}

	}

	private FSymbol getTextSymbol() {
		FSymbol theSymbol = new FSymbol(FConstant.SYMBOL_TYPE_TEXT);
		theSymbol.setColor(Color.RED);
		theSymbol.setStyle(FConstant.SYMBOL_STYLE_MARKER_CIRCLE);
		theSymbol.setFontColor(Color.BLACK);
		theSymbol.setSizeInPixels(true);
		theSymbol.setSize(9);
		return theSymbol;
	}

	private void removeOldLabels(GraphicLayer gLyr) {
		for (int i = gLyr.getNumGraphics() - 1; i >= 0; i--) {
			FGraphic gr = gLyr.getGraphic(i);
			if (gr.equals("N"))
				gLyr.removeGraphic(i);

		}
	}

	private void doLabeling(MapControl mapControl, Network net, GvFlag flag) {
		GraphicLayer graphicLayer = mapControl.getMapContext()
				.getGraphicsLayer();
		IGraph g = net.getGraph();
		int idSymbol = graphicLayer.addSymbol(getTextSymbol());
		String tag = "N";
		for (int i = 0; i < g.numVertices(); i++) {
			GvNode node = g.getNodeByID(i);
			IGeometry geom = ShapeFactory.createPoint2D(node.getX(), node
					.getY());
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(1);
			String aux = "\u221E"; // infinito
			if (node.getBestCost() < Double.MAX_VALUE)
				aux = nf.format(node.getBestCost()) + " - " + nf.format(node.getAccumulatedLength());
			FGraphicLabel theGLabel = new FGraphicLabel(geom, idSymbol, aux);
			theGLabel.setObjectTag(tag);
			theGLabel.getLabel().setJustification(FLabel.CENTER_TOP);
			graphicLayer.addGraphic(theGLabel);
		}
		mapControl.drawGraphics();

	}

	public boolean isEnabled() {
		IWindow window = PluginServices.getMDIManager().getActiveWindow();
		if (window instanceof View)
		{
			View v = (View) window;
	        MapControl mapCtrl = v.getMapControl();
			MapContext map = mapCtrl.getMapContext();
			
			SingleLayerIterator it = new SingleLayerIterator(map.getLayers());
			while (it.hasNext())
			{
				FLayer aux = it.next();
				if (!aux.isActive())
					continue;
				Network net = (Network) aux.getProperty("network");
				
				if ( net != null)
				{
					return true;
				}
			}
		}
		return false;
	}

	public boolean isVisible() {
		IWindow f = PluginServices.getMDIManager()
		 .getActiveWindow();
		if (f == null) {
		    return false;
		}
		if (f instanceof View) {
			return true;
		}
		return false;

	}

}
