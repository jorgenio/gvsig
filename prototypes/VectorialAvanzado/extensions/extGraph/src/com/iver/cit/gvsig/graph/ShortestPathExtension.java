/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.cit.gvsig.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.core.IFeature;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.core.v02.FArrowSymbol;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.GraphicLayer;
import com.iver.cit.gvsig.fmap.layers.SingleLayerIterator;
import com.iver.cit.gvsig.fmap.rendering.FGraphic;
import com.iver.cit.gvsig.graph.core.GvFlag;
import com.iver.cit.gvsig.graph.core.IFlagListener;
import com.iver.cit.gvsig.graph.core.Network;
import com.iver.cit.gvsig.graph.gui.RouteControlPanel;
import com.iver.cit.gvsig.graph.gui.RouteReportPanel;
import com.iver.cit.gvsig.graph.solvers.Route;
import com.iver.cit.gvsig.graph.solvers.ShortestPathSolverAStar;
import com.iver.cit.gvsig.project.documents.view.gui.View;
import com.iver.cit.gvsig.util.GvSession;

public class ShortestPathExtension extends Extension {

	public static ShortestPathSolverAStar solver = new ShortestPathSolverAStar();
	private int idSymbolLine = -1;

	public void initialize() {
	}

	public void execute(String actionCommand) {
		View v = (View) PluginServices.getMDIManager().getActiveWindow();
		MapControl mapCtrl = v.getMapControl();
		MapContext map = mapCtrl.getMapContext();
		SingleLayerIterator it = new SingleLayerIterator(map.getLayers());
		while (it.hasNext())
		{
			FLayer aux = it.next();
			if (!aux.isActive())
				continue;
			Network net = (Network) aux.getProperty("network");

			if ( net != null)
			{
				Route route;
				try {
					solver.setNetwork(net);
//					solver.setFielStreetName("STREET_NAM");
					route = solver.calculateRoute();
					if (route.getFeatureList().size() == 0)
					{
						JOptionPane.showMessageDialog((JComponent) PluginServices.getMDIManager().getActiveWindow(),
								PluginServices.getText(this, "shortest_path_not_found"));
						return;
					}
					List routes = (List) GvSession.getInstance().get(mapCtrl, "Route");
					if(routes == null){
						routes = new ArrayList();
						GvSession.getInstance().put(mapCtrl, "Route", routes);
					}
					routes.add(route);
					
					createGraphicsFrom(route.getFeatureList(), v.getMapControl());
					RouteReportPanel routeReport = new RouteReportPanel(route, v.getMapControl());
					PluginServices.getMDIManager().addWindow(routeReport);
					List reportsPanels = (List) GvSession.getInstance().get(mapCtrl, "RouteReport");
					if(reportsPanels == null){
						reportsPanels = new ArrayList();
						GvSession.getInstance().put(mapCtrl, "RouteReport", reportsPanels);
					}	
					reportsPanels.add(routeReport);
					RouteControlPanel controlPanel = (RouteControlPanel) GvSession.getInstance().get(mapCtrl, "RouteControlPanel");
					if (controlPanel != null)
						controlPanel.refresh();
					
				} catch (GraphException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}



	}

	private void createGraphicsFrom(Collection featureList, MapControl mapControl) {
		Iterator it = featureList.iterator();
		GraphicLayer graphicLayer = mapControl.getMapContext().getGraphicsLayer();
//		if (idSymbolLine == -1)
		{
			FArrowSymbol arrowSymbol = new FArrowSymbol(Color.RED);
			// FSymbol arrowSymbol = new FSymbol(FConstant.SYMBOL_TYPE_LINE);
			arrowSymbol.setStroke(new BasicStroke(3.0f));
			idSymbolLine = graphicLayer.addSymbol(arrowSymbol);
			
		}
		// Para evitar hacer reallocate de los elementos de la
		// graphicList cada vez, creamos primero la lista
		// y la insertamos toda de una vez.
		ArrayList graphicsRoute = new ArrayList();
		while (it.hasNext()) {
			IFeature feat = (IFeature) it.next();
			IGeometry gAux = feat.getGeometry();
			FGraphic graphic = new FGraphic(gAux, idSymbolLine);
			graphic.setTag("ROUTE");
			graphicsRoute.add(graphic);
//			graphicLayer.insertGraphic(0, graphic);
		}
		// Lo insertamos al principio de la lista para que los
		// pushpins se dibujen despu�s.

		graphicLayer.inserGraphics(0, graphicsRoute);
		mapControl.drawGraphics();

	}

	public boolean isEnabled() {
		com.iver.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
		 .getActiveWindow();
		if (f == null) {
		    return false;
		}
		if (f instanceof View) {
		    View v = (View) f;
			MapContext map = v.getMapControl().getMapContext();
			SingleLayerIterator it = new SingleLayerIterator(map.getLayers());
			while (it.hasNext())
			{
				FLayer aux = it.next();
				if (!aux.isActive())
					continue;
				Network net = (Network) aux.getProperty("network");

				if ( net != null)
				{
					int count = 0;
					for (int i=0; i < net.getOriginaFlags().size(); i++)
					{
						GvFlag flag = (GvFlag) net.getOriginaFlags().get(i);
						if (flag.isEnabled()) count++;
					}
					if (count > 1) return true;
				}
			}
			return false;

		}
		return false;
	}

	public boolean isVisible() {
		IWindow f = PluginServices.getMDIManager()
		 .getActiveWindow();
		if (f == null) {
		    return false;
		}
		if (f instanceof View) {
			return true;
		}
		return false;

	}

}


