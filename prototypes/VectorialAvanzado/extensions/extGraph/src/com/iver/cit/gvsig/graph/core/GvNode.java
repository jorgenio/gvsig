/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.cit.gvsig.graph.core;

import java.util.ArrayList;

public class GvNode {
	public final static int statNotInList = 0;
	public final static int statNowInList = 1;
	public final static int statWasInList = 2;
	private int idNode;
	private double x;
	private double y;
	
	int from_link = -1; // id del Arco desde el que hemos llegado
	int   numSoluc = 0; // Empezamos con esto a cero en toda la red. 
					// De manera global, habr� una variable numSolucGlobal que indica el n� de cada petici�n de ruta.
					// Sirve para no tener que inicializar siempre tooooda la red. Lo que hacemos es comparar el 
					// n� de petici�n global con este. Si no coinciden, antes de hacer nada hay que inicializar su
					// best_cost a infinito.

	double best_cost = Double.MAX_VALUE;
	double accumulatedLength = 0;
	double stimation = Double.MAX_VALUE; // bestCost + algo relacionado con la distancia al destino
	int status;
	ArrayList enlaces  = new ArrayList(); // Enlaces con los vecinos
	ArrayList giros = new ArrayList(); // Costes de los giros. Si existe un CGiro, miraremos su coste y lo a�adimos.
						  // Si es negativo, suponemos que es un giro prohibido.

	public GvNode() {
		initialize();
	}
	
	public void initialize() {
		numSoluc = AbstractNetSolver.numSolucGlobal;
		from_link = -1;
		best_cost = Double.MAX_VALUE;
		stimation = Double.MAX_VALUE;
		accumulatedLength = 0;
		status = statNotInList;

	}
	
	public int getIdNode() {
		return idNode;
	}
	public void setIdNode(int idNode) {
		this.idNode = idNode;
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getBestCost() {
		return best_cost;
	}
	public void setBestCost(double best_cost) {
		this.best_cost = best_cost;
	}
	public ArrayList getEnlaces() {
		return enlaces;
	}
	public void setEnlaces(ArrayList enlaces) {
		this.enlaces = enlaces;
	}
	public double getStimation() {
		return stimation;
	}
	public void setStimation(double estimacion) {
		this.stimation = estimacion;
	}
	public int getFromLink() {
		return from_link;
	}
	public void setFromLink(int from_link) {
		this.from_link = from_link;
	}
	public ArrayList getTurns() {
		return giros;
	}
	public void setTurns(ArrayList turns) {
		this.giros = turns;
	}
	public int getNumSoluc() {
		return numSoluc;
	}
	public void setNumSoluc(int numSoluc) {
		this.numSoluc = numSoluc;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public void calculateStimation(GvNode finalNode, double newCost) {
		double DeltaX = ((finalNode.getX() - x)/1000.0)*((finalNode.getX() - x)/1000.0);
		double DeltaY = ((finalNode.getY() - y)/1000.0)*((finalNode.getY() - y)/1000.0);
		double distLineaRecta = Math.sqrt(DeltaX + DeltaY); // En Km
		stimation = newCost + (distLineaRecta* 30.0);  // Segundos que tardamos en recorrer esos Km a 120 Km/hora

		
	}

	public double getAccumulatedLength() {
		return accumulatedLength;
	}

	public void setAccumulatedLength(double accumulatedLength) {
		this.accumulatedLength = accumulatedLength;
	}

}


