package com.iver.cit.gvsig.graph.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import org.gvsig.gui.beans.AcceptCancelPanel;
import java.awt.SystemColor;

public class MultiInputDlg extends JDialog {
	private boolean canceled = false;
	private class MyListener implements ActionListener {

		private MultiInputDlg dlg;
		public MyListener(MultiInputDlg dlg)
		{
			this.dlg = dlg;
		}
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equalsIgnoreCase("OK"))
			{
				dlg.dispose();
			}
			if (e.getActionCommand().equalsIgnoreCase("CANCEL"))
			{
				dlg.canceled = true;
				dlg.dispose();
				
			}			
		}
		
	}

	private class MyModel extends DefaultTableModel {

		private ArrayList l;
		private ArrayList r;

		MyModel(ArrayList leftList, ArrayList rightList) {
			l = leftList;
			r = rightList;
		}
		
		public ArrayList getRigthValues()
		{
			return r;
		}
		public int getColumnCount() {
			return 2;
		}

		public int getRowCount() {
			if (l == null) return 0;
			return l.size();
		}

		public Object getValueAt(int row, int column) {
			if (column == 0)
				return l.get(row);
			return r.get(row);
		}

		public boolean isCellEditable(int row, int column) {
			if (column == 1)
				return true;
			else
				return false;
		}

		public void setValueAt(Object aValue, int row, int column) {
			r.set(row, aValue);
		}
		
	}
	private ArrayList firstList;
	private ArrayList secondList;
	
	private String msg;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1914037292995440998L;
	private JTextArea jLblMsg = null;  //  @jve:decl-index=0:visual-constraint="220,10"
	private JScrollPane jScrollPane = null;  //  @jve:decl-index=0:visual-constraint="183,139"
	private JTable jTable = null;
	private String colName1;
	private String colName2;

	public MultiInputDlg(String msg, ArrayList leftValues, ArrayList veloKm) {
		firstList = leftValues;
		secondList = veloKm;
		this.msg = msg;
		initialize();
	}
	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
		jLblMsg = new JTextArea();
		jLblMsg.setText(msg);
		jLblMsg.setBackground(SystemColor.control);
		jLblMsg.setLineWrap(true);
		jLblMsg.setPreferredSize(new Dimension(200, 100));
		
		BorderLayout layout = new BorderLayout();
		layout.setHgap(30);
		layout.setVgap(10);
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(jLblMsg, BorderLayout.NORTH);
		this.setSize(new Dimension(300, 350));
		this.getContentPane().add(getJScrollPane(), BorderLayout.CENTER);
		MyListener myListener = new MyListener(this);
		AcceptCancelPanel okCancelPanel = new AcceptCancelPanel(myListener, myListener);
		this.getContentPane().add(okCancelPanel, BorderLayout.SOUTH);
	
	}

	public ArrayList getRightValues() {
		return secondList;
	}
	/**
	 * This method initializes jScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getJTable());
		}
		return jScrollPane;
	}
	/**
	 * This method initializes jTable	
	 * 	
	 * @return javax.swing.JTable	
	 */
	private JTable getJTable() {
		if (jTable == null) {
			jTable = new JTable();
			MyModel myModel = new MyModel(firstList, secondList);
			jTable.setModel(myModel);
		}
		return jTable;
	}
	public void actionPerformed(ActionEvent e) {
		
		
	}
	public boolean isCanceled() {
		return canceled;
	}
	public void setColumnNames(String col1, String col2) {
		this.colName1 = col1;
		this.colName2 = col2;
		jTable.getColumnModel().getColumn(0).setHeaderValue(col1);
		jTable.getColumnModel().getColumn(1).setHeaderValue(col2);
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
