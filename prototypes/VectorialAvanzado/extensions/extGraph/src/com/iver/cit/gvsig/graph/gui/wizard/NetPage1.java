/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: NetPage1.java 9299 2006-12-13 17:45:38Z  $
* $Log$
* Revision 1.5  2006-12-04 17:13:39  fjp
* *** empty log message ***
*
* Revision 1.4  2006/11/20 08:44:55  fjp
* borrar tramos amarillos, seleccionar solo campos num�ricos y situar las barreras encima del tramo invalidado
*
* Revision 1.3  2006/11/09 10:12:24  fjp
* Traducciones
*
* Revision 1.2  2006/10/24 08:04:41  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/19 15:12:10  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.graph.gui.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import jwizardcomponent.JWizardPanel;

import org.gvsig.gui.beans.swing.GridBagLayoutPanel;

import com.iver.andami.PluginServices;
/**
 * Configures the length
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
class NetPage1 extends JWizardPanel implements ActionListener {
	private NetWizard owner;
	private JComboBox cmbLengthField;
	// TODO comprobar si  es necesario
	private Hashtable exceptions;
	private JComboBox cmbTypeField;
	private JComboBox cmbSenseField;
	private JComboBox cmbCostUnits;
	private JComboBox cmbCostField;

	private Object useLineLengthItem = "< "+PluginServices.getText(this, "use_line_length")+" >";

	private Object nullValue = "- "+PluginServices.getText(this, "none")+" -";
	private JCheckBox chkTypeField;
	private JCheckBox chkLengthField;
	private JCheckBox chkCostUnits;
	private JCheckBox chkSenseField;
	private JCheckBox chkCostField;

	NetPage1(NetWizard wizard) {
		super(wizard.getWizardComponents());
		this.owner = wizard;
		initialize();
	}

	private void initialize() {
		GridBagLayoutPanel contentPane = new GridBagLayoutPanel();
		contentPane.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				PluginServices.getText(this, "field_configuration"),
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null));

		// Way type
		cmbTypeField = new JComboBox();
		cmbTypeField.addItem(nullValue);
		cmbTypeField.setToolTipText(PluginServices.getText(this, "type_field_text"));
		String[] ss = owner.getNumericLayerFieldNames();
		for (int i = 0; i < ss.length; i++) {
			cmbTypeField.addItem(ss[i]);
		}
		chkTypeField = new JCheckBox(PluginServices.getText(this, "select_type_field")+":");
		chkTypeField.addActionListener(this);
		contentPane.addComponent(chkTypeField, cmbTypeField);

		// Length
		cmbLengthField = new JComboBox();
		cmbLengthField.addItem(nullValue);
		cmbLengthField.setToolTipText(PluginServices.getText(this, "length_field_text"));
		ss = owner.getNumericLayerFieldNames();
		for (int i = 0; i < ss.length; i++) {
			cmbLengthField.addItem(ss[i]);
		}
		chkLengthField = new JCheckBox(PluginServices.getText(this, "select_length_field")+":");
		chkLengthField.addActionListener(this);
		contentPane.addComponent(chkLengthField, cmbLengthField);

		// Cost
		chkCostField = new JCheckBox(PluginServices.getText(this, "cost_field")+":");
		chkCostField.addActionListener(this);
		contentPane.addComponent(chkCostField, cmbCostField = new JComboBox());

		cmbCostField.addItem(useLineLengthItem );
		cmbCostField.setToolTipText(PluginServices.getText(this, "cost_field_text"));
		cmbCostField.addActionListener(this);
		String[] numericFields = owner.getNumericLayerFieldNames();

		for (int i = 0; i < numericFields.length; i++)
			cmbCostField.addItem(numericFields[i]);

		// Costs units
		cmbCostUnits = new JComboBox(NetWizard.COST_UNITS);
		chkCostUnits = new JCheckBox(PluginServices.getText(this, "cost_units")+":");
		chkCostUnits.addActionListener(this);
		contentPane.addComponent(chkCostUnits, cmbCostUnits);
		cmbCostUnits.setToolTipText(PluginServices.getText(this, "cost_units_text"));

		// Sense
		cmbSenseField = new JComboBox();
		cmbSenseField.addItem(nullValue);
		String[] fieldNames = owner.getNumericLayerFieldNames();
		for (int i = 0; i < fieldNames.length; i++) {
			cmbSenseField.addItem(fieldNames[i]);
		}
		cmbSenseField.addActionListener(this);
		cmbSenseField.setToolTipText(PluginServices.getText(this, "sense_field_text"));
		chkSenseField = new JCheckBox(PluginServices.getText(this, "select_sense_field")+":");
		chkSenseField.addActionListener(this);
		contentPane.addComponent(chkSenseField, cmbSenseField);
		contentPane.addComponent(new JLabel(PluginServices.getText(this,
				"explanation_sense_field")));
		actionPerformed(null);
		this.add(contentPane);
	}

	public void actionPerformed(ActionEvent e) {

		cmbCostUnits.setEnabled(
				!cmbCostField.getSelectedItem().equals(useLineLengthItem));
		if (chkTypeField.isSelected()) {
			owner.setTypeField(cmbTypeField.getSelectedItem().equals(nullValue)?
					null : (String) cmbTypeField.getSelectedItem());
			cmbTypeField.setEnabled(true);
		} else {
			owner.setTypeField(null);
			cmbTypeField.setEnabled(false);
		}

		if (chkLengthField.isSelected()) {
			owner.setLengthField(cmbLengthField.getSelectedItem().equals(nullValue)?
				null : (String) cmbLengthField.getSelectedItem());
			cmbLengthField.setEnabled(true);
		} else {
			owner.setLengthField(null);
			cmbLengthField.setEnabled(false);
		}

		if (chkSenseField.isSelected()) {
			owner.setSenseField(cmbSenseField.getSelectedItem().equals(nullValue)?
					null : (String) cmbSenseField.getSelectedItem());
			cmbSenseField.setEnabled(true);
		} else {
			owner.setSenseField(null);
			cmbSenseField.setEnabled(false);
		}

		if (chkCostField.isSelected()) {
			owner.setCostField(cmbCostField.getSelectedItem().equals(useLineLengthItem)?
				null : (String) cmbCostField.getSelectedItem());
			cmbCostField.setEnabled(true);
		} else {
			owner.setCostField(null);
			cmbCostField.setEnabled(false);
		}

		if (chkCostUnits.isSelected()) {
			owner.setCostsUnit((String) cmbCostUnits.getSelectedItem());
			cmbCostUnits.setEnabled(true);
		} else {
			owner.setCostsUnit(null); // TODO use unknown?
			cmbCostUnits.setEnabled(false);
		}
	}
}
