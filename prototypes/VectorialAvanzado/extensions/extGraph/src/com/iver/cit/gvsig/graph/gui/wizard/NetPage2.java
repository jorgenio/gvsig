/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: NetPage2.java 9299 2006-12-13 17:45:38Z  $
* $Log$
* Revision 1.4  2006-12-04 17:13:39  fjp
* *** empty log message ***
*
* Revision 1.3  2006/11/14 11:10:27  jaume
* *** empty log message ***
*
* Revision 1.2  2006/10/19 15:15:27  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/19 15:12:10  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.graph.gui.wizard;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import jwizardcomponent.JWizardPanel;

import org.gvsig.gui.beans.swing.GridBagLayoutPanel;

import com.iver.andami.PluginServices;
/**
 * Select costs or way type.
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 * @deprecated
 */
public class NetPage2 extends JWizardPanel {

	private NetWizard owner;
	private JRadioButton rdBtnCosts;
	private JRadioButton rdBtnWayType;

	NetPage2(NetWizard wizard) {
		super(wizard.getWizardComponents());
		this.owner = wizard;
		initialize();
	}

	private void initialize() {
		GridBagLayoutPanel contentPane = new GridBagLayoutPanel();
		contentPane.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				PluginServices.getText(this, "criterium"),
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null));

		contentPane.addComponent(
				rdBtnCosts = new JRadioButton(
						PluginServices.getText(this, "cost_from_a_table_field")));
		contentPane.addComponent(
				rdBtnWayType = new JRadioButton(
						PluginServices.getText(this, "based_upon_the_road_type")));

		ButtonGroup group = new ButtonGroup();
		group.add(rdBtnCosts);
		group.add(rdBtnWayType);
		this.add(contentPane);
	}

	public void next() {
		Boolean costs = null;
		if (rdBtnCosts.isSelected())
			costs = new Boolean(true);
		if (rdBtnWayType.isSelected())
			costs = new Boolean(false);

		if (costs!=null) {
			owner.setUsingCosts(costs);
			((NetPage3) owner.getWizardComponents().getWizardPanel(2))
				.refresh();
			goNext();
		}

	}


}
