/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: NetPage3.java 8280 2006-10-24 08:04:41Z jaume $
* $Log$
* Revision 1.3  2006-10-24 08:04:41  jaume
* *** empty log message ***
*
* Revision 1.2  2006/10/19 15:15:27  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/19 15:12:10  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.graph.gui.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jwizardcomponent.JWizardPanel;

import org.gvsig.gui.beans.swing.GridBagLayoutPanel;

import com.iver.andami.PluginServices;
import com.iver.utiles.swing.JComboBox;
/**
 * Configure costs or way types
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 * @deprecated
 */
class NetPage3 extends JWizardPanel implements ActionListener {

	private NetWizard owner;
	private JComboBox cmbCostField;
	private JComboBox cmbCostUnits;
	private Boolean usingCosts;
	private Object useLineLengthItem = "< "+PluginServices.getText(this, "use_line_length")+" >";

	NetPage3(NetWizard wizard) {
		super(wizard.getWizardComponents());
		this.owner = wizard;
	}

	public void refresh() {
		removeAll();
		GridBagLayoutPanel contentPane = new GridBagLayoutPanel();
		usingCosts = owner.isUsingCosts();
		if (usingCosts == null) {
			goBack();
		} else if (usingCosts.booleanValue() == true) {
			contentPane.addComponent(
					PluginServices.getText(this, "cost_field"),
					cmbCostField = new JComboBox());

			cmbCostField.addItem(useLineLengthItem );
			cmbCostField.addActionListener(this);
			String[] numericFields = owner.getNumericLayerFieldNames();

			for (int i = 0; i < numericFields.length; i++)
				cmbCostField.addItem(numericFields[i]);

			contentPane.addComponent(
					PluginServices.getText(this, "cost_units"),
					cmbCostUnits = new JComboBox(NetWizard.COST_UNITS));
		} else {
			// TODO missing configure tipo de via
		}
		this.add(contentPane);
		actionPerformed(null);
	}

	public void next() {
		if (usingCosts==null) return;

		if (usingCosts.equals(Boolean.TRUE)) {
			owner.setCostField(
					cmbCostField.getSelectedIndex() == 0 ?
							"" : (String)cmbCostField.getSelectedItem());
			owner.setCostsUnit(
					(String) NetWizard.COST_UNITS[cmbCostUnits.getSelectedIndex()]);
		} else {

		}
		goNext();
	}

	public void actionPerformed(ActionEvent e) {
		cmbCostUnits.setEnabled(
			!cmbCostField.getSelectedItem().equals(useLineLengthItem));
	}

}
