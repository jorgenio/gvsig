/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: NetPage4.java 9299 2006-12-13 17:45:38Z  $
* $Log$
* Revision 1.4  2006-12-04 17:13:39  fjp
* *** empty log message ***
*
* Revision 1.3  2006/10/24 08:04:41  jaume
* *** empty log message ***
*
* Revision 1.2  2006/10/19 15:15:27  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/19 15:12:10  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.graph.gui.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jwizardcomponent.JWizardPanel;

import org.gvsig.gui.beans.swing.GridBagLayoutPanel;

import com.iver.andami.PluginServices;
import com.iver.utiles.swing.JComboBox;

/**
 * Configuring sense
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 * @deprecated
 */
class NetPage4 extends JWizardPanel implements ActionListener {
	private NetWizard owner;
	private JComboBox cmbSenseField;
	private Object nullValue = "- "+PluginServices.getText(this, "none")+" -";

	NetPage4(NetWizard wizard) {
		super(wizard.getWizardComponents());
		this.owner = wizard;
		initialize();
	}

	private void initialize() {
		GridBagLayoutPanel contentPane = new GridBagLayoutPanel();

		cmbSenseField = new JComboBox();
		cmbSenseField.addItem(nullValue);
		String[] fieldNames = owner.getLayerFieldNames();
		for (int i = 0; i < fieldNames.length; i++) {
			cmbSenseField.addItem(fieldNames[i]);
		}
		cmbSenseField.addActionListener(this);
		contentPane.addComponent(PluginServices.getText(this, "select_sense_field"),cmbSenseField);
		add(contentPane);
		actionPerformed(null);
	}

	public void actionPerformed(ActionEvent e) {
		// refresh table
		owner.setSenseField(cmbSenseField.getSelectedItem().equals(nullValue)?
				"" : (String) cmbSenseField.getSelectedItem());
	}


}
