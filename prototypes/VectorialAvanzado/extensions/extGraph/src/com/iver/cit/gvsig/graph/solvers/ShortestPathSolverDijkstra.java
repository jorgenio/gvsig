/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.cit.gvsig.graph.solvers;

import java.util.ArrayList;
import java.util.Stack;

import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.core.IFeature;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.core.v02.FConverter;
import com.iver.cit.gvsig.fmap.layers.VectorialAdapter;
import com.iver.cit.gvsig.graph.GraphException;
import com.iver.cit.gvsig.graph.core.AbstractNetSolver;
import com.iver.cit.gvsig.graph.core.GvEdge;
import com.iver.cit.gvsig.graph.core.GvFlag;
import com.iver.cit.gvsig.graph.core.GvNode;
import com.iver.cit.gvsig.graph.core.GvTurn;
import com.iver.cit.gvsig.graph.core.IGraph;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;

public class ShortestPathSolverDijkstra extends AbstractNetSolver {
	
	private GeometryFactory geomFactory = new GeometryFactory();
	
	protected Route route = new Route();
	private int fieldIndexStreetName;
	
	private class InfoShp {
		public int idArc;
		public double pct;
		public double cost;
		public double distance;
		public int direction;
		public boolean bFlip;

	}
	
	public void setFielStreetName(String name) {
		try {
			int aux = net.getLayer().getRecordset().getFieldIndexByName(name);
			if (aux == -1)
				throw new RuntimeException("Field " + name + " not found.");
			fieldIndexStreetName = aux;
		} catch (com.hardcode.gdbms.engine.data.driver.DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * @return a list of features
	 * @throws GraphException 
	 */
	public Route calculateRoute() throws GraphException {
		GvFlag[] flags = net.getFlags();
		if (flags.length == 0)
			throw new RuntimeException("Please, add flags before");
		int desde = 0;
		int hasta = 1;
		double elCoste1 = 0;
		route = new Route();
		for (int i = 0; i < flags.length - 1; i++) {
			GvFlag fFrom = flags[desde];
			GvFlag fTo = flags[hasta];

			if (fFrom != fTo) {
				int idStart = net.creaArcosVirtuales(fFrom);
				int idStop = net.creaArcosVirtuales(fTo);

				double newCost = dijkstra(idStart, idStop);
				elCoste1 += newCost;

				if (newCost != Double.MAX_VALUE)
				{
					try {
						populateRoute(fFrom, fTo, idStart, idStop);
					} catch (DriverException e) {
						e.printStackTrace();
						throw new GraphException(e);
					}
				}
				else
				{
					// No way
				}

				net.reconstruyeTramo(fFrom.getIdArc());
				net.reconstruyeTramo(fTo.getIdArc());
				desde = hasta;
			} // if son puntos distintos
			hasta++;
		}

		return route;
	}

	private void populateRouteSimple(int idStart, int idEnd) throws DriverException {
		int idEnlace;
		GvNode node;
		GvEdge link;
		double costeEntrada;

		// Trazar el camino desde idEnd hasta idStart hacia atr�s marcando los Enlaces
		double Coste = 0;
		double Coste2 = 0;
		IGraph graph = net.getGraph();
		node = graph.getNodeByID(idEnd);
		int from_link = node.getFromLink();
		VectorialAdapter va = (VectorialAdapter) net.getLayer().getSource();
		while (node.getIdNode() != idStart)
		{
			if (from_link == -1)
			{
				throw new RuntimeException("Fallo al recorrer de manera inversa la soluci�n. Encontrado arco con -1");
				// break; // FALLO!!
			} 
			link = graph.getEdgeByID(from_link);
			IFeature feat = va.getFeature(link.getIdArc());
			route.addRouteFeature(feat.getGeometry(), link.getIdArc(), 
					link.getWeight(), link.getDistance(), feat.getAttribute(getFieldIndexStreetName()).toString());

			node = graph.getNodeByID(link.getIdNodeOrig());

			Coste = Coste + link.getWeight();
			Coste2 = Coste2 + link.getDistance();

			// TODO:
			// from_link = node.get_from_link(idEnlace, &costeEntrada);
			from_link = node.getFromLink();
			
	    }	
		System.out.println("Salgo con node = " + node.getIdNode());
	}

	private void populateRoute(GvFlag origin, GvFlag dest, int idStart, int idEnd) throws DriverException {
		int idEnlace;
		GvNode node;
		GvEdge link;
		double costeEntrada;

		// Trazar el camino desde idEnd hasta idStart hacia atr�s marcando los Enlaces
		IGraph graph = net.getGraph();
		node = graph.getNodeByID(idEnd);
		int from_link = node.getFromLink();
		VectorialAdapter va = (VectorialAdapter) net.getLayer().getSource();
		
		/*	Miramos los nodos de los tramos inicio y final, y cogemos el nodo que tenga el from_link rellenado. E IGUAL A NUMSOLUCGLOBAL!!!!
			A partir de ah�, recorremos hacia atr�s y tenemos el cuerpo principal del shape. Luego, para
			las puntas hay que tener en cuenta los porcentajes, viendo el trozo que est� pegando a los nodos
			que sabemos que est�n en el path
		*/

		/* 22/9/2003 Corregimos el fallo que hab�a de escribir el shape de atr�s adelante.
		*  Guardamos lo que necesitamos en listaShapes y recorremos esa lista guardando los tramos
		*	con el sentido adecuado.
		*/

		/* 3/Febrero/2005 Limpieza de c�digo superfluo y correci�n de un fallo raro. Ahora es m�s simple y parece
		* que no falla. A ver si dura. IDEA: quiz�s se necesite meter el porcentaje en los arcos partidos.
		*/	

		// Define a template class for a vector of IDs.
		InfoShp infoShp;
		Stack pilaShapes = new Stack();
//		typedef stack<CInfoShp> PILAINFO;
//		PILAINFO pilaShapes;

		double costeTramoFinal=-1;
		GvNode nodeEnd = graph.getNodeByID(idEnd);
		GvEdge finalEdge = graph.getEdgeByID(nodeEnd.getFromLink());
		costeTramoFinal = finalEdge.getWeight();

		GvNode pNodo;
		GvEdge pEnlace;
		
		boolean bFlipearShape = false;

		double pctMax, pctMin;



		//////////////////////////////////////////////////////////////////////////////////////
		// Trozo del final
		// El shape va de idStop1 a idStop2, y el porcentaje viene en ese sentido.
		// Si el idEnd es idStop1, quiere decir que el tramo que falta va en ese sentido tambi�n,
		// as� que recorremos ese shape desde idStop1 hasta que rebasemos o igualemos ese porcentaje.
		// Si no, hemos pasado por idStop2 y la parte que hay que meter es desde el pto interior a idStop2
		///////////////////////////////////////////////////////////////////////////////////////
		IFeature feat = va.getFeature(finalEdge.getIdArc());
		MultiLineString jtsGeom = (MultiLineString) feat.getGeometry().toJTSGeometry();
//		CoordinateFilter removeDuplicates = new UniqueCoordinateArrayFilter(); 
//		jtsGeom.apply(removeDuplicates);
//		jtsGeom.geometryChanged();
		
		
		
		// SI ESTAMOS SOBRE EL MISMO TRAMO, CASO PARTICULAR
		// y el sentido de circulaci�n es correcto
		if ((origin.getIdArc() == dest.getIdArc()) && (nodeEnd.getBestCost() <= costeTramoFinal))
		{
			if (dest.getPct() > origin.getPct())
			{
				pctMax = dest.getPct();
				pctMin = origin.getPct(); 
			}
			else
			{
				pctMax = origin.getPct();
				pctMin = dest.getPct();
				bFlipearShape = true;
			}

			LineString line = SituaSobreTramo(jtsGeom,dest.getIdArc(), dest.getPct(),1);
			
			pctMin = pctMin / pctMax; // Porque ha cambiado la longitud del shape

			line = SituaSobreTramo(line,dest.getIdArc(), pctMin,0);

			if (bFlipearShape)
				line = line.reverse();
			
			IGeometry geom = FConverter.jts_to_igeometry(line);
			// TODO: Calcular bien el length de este arco, 
			// basandonos en el porcentaje costeTramoFinal / costeOriginal
			route.addRouteFeature(geom, origin.getIdArc(), 
					costeTramoFinal, line.getLength(), feat.getAttribute(getFieldIndexStreetName()).toString());


			return ; // Deber�a sacar el coste
		}

		

		// Trazar el camino desde idEnd hasta idStart hacia atr�s marcando los Enlaces
		pNodo = graph.getNodeByID(idEnd);

		while ((pNodo.getIdNode() != idStart)) 
		{
			idEnlace = pNodo.getFromLink();
			pEnlace = graph.getEdgeByID(idEnlace);

			pNodo = graph.getNodeByID(pEnlace.getIdNodeOrig());
			
			infoShp = new InfoShp();
			infoShp.distance = pEnlace.getDistance();
			infoShp.cost = pEnlace.getWeight();

			if ((pEnlace.getIdArc() == origin.getIdArc()) || (pEnlace.getIdArc() == dest.getIdArc()))
			{
				if (pEnlace.getIdArc() == origin.getIdArc())
				{
					infoShp.pct = origin.getPct();
					infoShp.idArc = origin.getIdArc();
					if (pEnlace.getDirec() == 0) 
					{
						infoShp.direction = 1;
						infoShp.bFlip = true;
					}
					else // Hemos pasado por el 2
					{
						infoShp.direction = 0;
						infoShp.bFlip = false;
					} // if else */
				}
				else
				{
					infoShp.pct = dest.getPct();
					infoShp.idArc = dest.getIdArc();
					if (pEnlace.getDirec() == 0)
					{
						infoShp.direction = 0;
						infoShp.bFlip = true;
					}
					else
					{
						infoShp.direction = 1;
						infoShp.bFlip = false;
					} // if else */
				}
			}
			else
			{
				infoShp.pct = 1.0;
				infoShp.idArc = pEnlace.getIdArc();
				
				infoShp.direction =1;
				if (pEnlace.getDirec() == 1)
					infoShp.bFlip = false;
				else
					infoShp.bFlip = true;
			}

			pilaShapes.push(infoShp);
		}

		// Y ahora recorremos hacia atr�s el vector y escribimos los shapes.
		// VECTORINFO::iterator theIterator;
		int auxC = 0;
		
		while (!pilaShapes.empty())  
		{
			infoShp = (InfoShp) pilaShapes.peek();
			feat = va.getFeature(infoShp.idArc);
			MultiLineString line = (MultiLineString) feat.getGeometry().toJTSGeometry();
//			line.apply(removeDuplicates);
//			line.geometryChanged();
			
			LineString aux = null;
			if (infoShp.pct < 1.0)
				aux = SituaSobreTramo(line,infoShp.idArc, infoShp.pct, infoShp.direction);

			IGeometry geom = null;
			if (aux == null)
			{
				if (infoShp.bFlip)
					line = line.reverse();
				geom = FConverter.jts_to_igeometry(line);
			}	
			else
			{
				if (infoShp.bFlip)
					aux = aux.reverse();
				geom = FConverter.jts_to_igeometry(aux);
			}	


			route.addRouteFeature(geom, infoShp.idArc, 
					infoShp.cost, infoShp.distance, feat.getAttribute(getFieldIndexStreetName()).toString());


			pilaShapes.pop();
			auxC++;
			
		}

		return;

		
	}

	LineString SituaSobreTramo(Geometry geom, int idArc, double pct, int parte)
//	 Si parte vale cero, los v�lidos son los primeros. Si no, los segundos.
	{
		int j, numVertices;
		double longAcum, longReal, longBuscada, distSobre, miniPorcentaje;
		double nuevaX, nuevaY; // Por cuestiones de claridad al programar
		double dist=0;

		longAcum = 0;
		longReal = geom.getLength();
		longBuscada = longReal * pct;
		Coordinate[] coords = geom.getCoordinates();
		Coordinate c1 = null, c2 = null;
		ArrayList savedCoords = new ArrayList();

	 	if (parte > 0) // Hemos entrado por el 1 hacia el 2 (al 2 no llegamos)
		{
			for( j = 0; j < coords.length-1; j++ ) 
			{
				c1 = coords[j];
				c2 = coords[j+1];
				dist = c1.distance(c2);
				longAcum += dist;
				savedCoords.add(c1);
				if (longAcum >= longBuscada)
				{
					// Hasta aqu�. Ahora ahi que poner el punto sobre el tramo
					distSobre = dist - (longAcum - longBuscada);
					miniPorcentaje = distSobre/dist;

					nuevaX = c1.x + (c2.x-c1.x) * miniPorcentaje;
					nuevaY = c1.y + (c2.y-c1.y) * miniPorcentaje;
		
					savedCoords.add(new Coordinate(nuevaX, nuevaY));
					break;
				} // if longAcum >= longBuscada
			} // for j
			
		}
		else // Hemos entrado por el 2 hacia el 1
		{
			numVertices = 0;
			for( j = 0; j < coords.length; j++ ) 
			{
				////////////////////////////////////////////////////////////////
				// 13_ene_2005: Si el �ltimo punto es el �ltimo punto no
				// podemos acceder al elemento j+1 porque nos salimos del shape
				/////////////////////////////////////////////////////////////////
				c1 = coords[j];
				if (j < coords.length -1)
				{					
					c2 = coords[j+1];

					dist = c1.distance(c2);
					longAcum += dist;
				}

				if (longAcum >= longBuscada)
				{
					// Hasta aqu�. Empezamos a meter puntos

					if (numVertices == 0) 
					{
						distSobre = dist - (longAcum - longBuscada);
						miniPorcentaje = distSobre/dist;
						nuevaX = c1.x + (c2.x-c1.x) * miniPorcentaje;
						nuevaY = c1.y + (c2.y-c1.y) * miniPorcentaje;
						
						savedCoords.add(new Coordinate(nuevaX, nuevaY));
					}
					else
					{
						savedCoords.add(c2);
					}
					numVertices ++;
					// break;
				} // if longAcum >= longBuscada
			} // for j
			
			// savedCoords.add(c2);
			
		} // if else

	 	
	 	return geomFactory.createLineString((Coordinate[] )savedCoords.toArray(new Coordinate[0]));
	}

	private int getFieldIndexStreetName() {
		return fieldIndexStreetName;
	}

	private double dijkstra(int idStart, int idStop) {
		int nodeNum;
		int linkNum;
		double newCost;
		int idSiguienteNodo;
		GvNode node, toNode, finalNode, bestNode; // , *pNodoProv;
		GvEdge link;
		boolean bExit = false;
		double bestCost;

		boolean bGiroProhibido;
		ArrayList candidatos = new ArrayList();

		GvTurn theTurn;
		// char Mensaje[200];
		
		IGraph graph = net.getGraph();

		// NUEVO: 27-6-2003
		// Cada nodo y cada arco llevan un numero de soluci�n. Se supone
		// que si lo del nodo y el arco no coincide con
		// este numero, todav�a no ha sido inicializado y hay que hacerlo.
		// Para evitar coincidencias cuando de la vuelta el contador, cada
		// 65000 peticiones (por ejemplo), repasamos toda
		// la red y ponemos numSolucGlobal a -1
		if (numSolucGlobal > 65000) {
			numSolucGlobal = -1;

			for (nodeNum = 0; nodeNum < graph.numVertices(); nodeNum++) {
				node = graph.getNodeByID(nodeNum);
				node.initialize();
			} // for nodeNum */

		}
		numSolucGlobal++;

		candidatos.clear();
		// A�adimos el Start Node a la lista de candidatosSTL
		// Nodo final
		finalNode = graph.getNodeByID(idStop);
		finalNode.initialize();

		node = graph.getNodeByID(idStart);
		node.initialize();
		bestNode = node;

		candidatos.add(node);
		node.setBestCost(0);
		node.setStatus(GvNode.statNowInList);
		bestCost = Double.MAX_VALUE;

		// Mientras que la lista de candidatosSTL no est� vac�a, procesamos
		// Nodos

		while ((!bExit) && (candidatos.size() > 0)) {
			// Buscamos el nodo con m�nimo coste
			node = (GvNode) candidatos.get(0);
			bestNode = node;
			bestCost = node.getBestCost();
			for (nodeNum = 1; nodeNum < candidatos.size(); nodeNum++) {
				node = (GvNode) candidatos.get(nodeNum);
				if (node.getBestCost() < bestCost) {
					bestCost = node.getBestCost();
					bestNode = node;
				}
			} // for nodeNum candidatosSTL

			node = bestNode;
			// Borramos el mejor nodo de la lista de candidatosSTL
			node.setStatus(GvNode.statWasInList);
			// TODO: BORRAR POR INDEX, NO AS�. ES M�S LENTO QUE SI BORRAMOS EL i-�simo.
			candidatos.remove(node);
			// System.out.println("LINK " + link.getIdArc() + " from ");
			// System.out.println("from " + idStart + " to " + finalNode.getIdNode() + ". node=" + node.getIdNode());
			// Miramos si hemos llegado donde quer�amos
			if (bestNode.getIdNode() == idStop) {
				bExit = true;
				break;
			}

			// sprintf(Mensaje,"Enlaces en el nodo %ld:
			// %ld.",pNodo->idNodo,pNodo->Enlaces.GetSize());
			// AfxMessageBox(Mensaje);

			// A�adimos a la lista de candidatosSTL los vecinos del nodo que
			// acabamos de borrar
			// HAY Arcos QUE SALEN Y Arcos QUE LLEGAN. SOLO MIRAMOS LOS QUE
			// SALEN.
			for (linkNum = 0; linkNum < node.getEnlaces().size(); linkNum++) {
				// Pillamos el nodo vecino
				link = (GvEdge) node.getEnlaces().get(linkNum);
				idSiguienteNodo = link.getIdNodeEnd();

				toNode = graph.getNodeByID(idSiguienteNodo);

				// 27_5_2004
				// Si un arco tiene coste negativo, lo ignoramos
				if (link.getWeight() < 0)
					continue;

				// Fin arco con coste negativo

				// NUEVO: 26-7-2003: Comprobamos si est� inicializado
				if (toNode.getNumSoluc() != numSolucGlobal) {
					toNode.initialize();
				}
				else
				{
					// System.out.println("Nodo ya inicializado");
				}

				// Miramos si ese nodo ya ha estado antes en la lista de
				// candidatos
				if (toNode.getStatus() != GvNode.statWasInList) {
					// Miramos a ver si podemos mejorar su best_cost
					newCost = node.getBestCost() + link.getWeight();

					// Miramos la lista de Giros de ese nodo
					bGiroProhibido = false;
					for (int idGiro = 0; idGiro < node.getTurns().size(); idGiro++) {
						// Si est� prohibido, a por otro
						theTurn = (GvTurn) node.getTurns().get(idGiro);
						 if ((theTurn.getIdArcFrom() ==	 graph.getEdgeByID(node.getFromLink()).getIdArc()) &&
								 (theTurn.getIdArcTo() == link.getIdArc()))
						 {
							 if (theTurn.getCost() < 0)
							 {
								 bGiroProhibido = true;
								 // No podemos inicializar por completo el nodo porque entonces
								 // perdemos el fromLink (el arco desde donde hemos llegado hasta
								 // este nodo, que es necesario para calcular los costes y generar
								 // el shape recorriendo hacia atr�s el camino realizado.
								 // Si hab�a m�s de un nodo con costes prohibidos, cab�a la posibilidad
								 // de que perdieramos este enlace.
																	
								 // Para que pueda volver a entrar en c�lculos
								 node.setStatus(GvNode.statNotInList);
								 node.setBestCost(Double.MAX_VALUE);						
							 }
							 else
								 newCost += theTurn.getCost();
							 break; // Salimos del for porque ya hemos encontrado el giro
						 }
					}
					// Si est� prohibido, vamos a por otro enlace, PERO
					// MARCAMOS EL NODO
					// COMO NO VISITADO PARA QUE PUEDA VOLVER A ENTRAR EN LA
					// LISTA DE CANDIDATOS
					// SI VENIMOS POR OTRO LADO.
					if (bGiroProhibido) {
						continue;
					}

					if (newCost < toNode.getBestCost()) {
						// Es una mejora, as� que actualizamos el vecino y
						// lo a�adimos a los candidatosSTL
						toNode.setBestCost(newCost);
						 
						toNode.setFromLink(link.getIdEdge());

						if (toNode.getStatus() != GvNode.statNowInList) {
							toNode.setStatus(GvNode.statNowInList);
							candidatos.add(toNode);
						}
					} // Si hay mejora
				} // if ese nodo no ha estado en la lista de candidatosSTL

			} // for linkNum
		} // while candidatosSTL

		newCost = finalNode.getBestCost();

		return newCost;
	}

}
