/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: AbstractTypeSymbolEditorPanel.java 8687 2006-11-13 09:15:23Z jaume $
* $Log$
* Revision 1.6  2006-11-13 09:15:23  jaume
* javadoc and some clean-up
*
* Revision 1.5  2006/11/06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.4  2006/10/31 16:16:34  jaume
* *** empty log message ***
*
* Revision 1.3  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
* Revision 1.2  2006/10/29 23:53:49  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/27 12:41:09  jaume
* GUI
*
*
*/
package com.iver.cit.gvsig.gvsig.gui.styling;

import javax.swing.JPanel;

import com.iver.utiles.XMLEntity;
/**
 * Abstract class that all the Symbol settings interface must extend.
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public abstract class AbstractTypeSymbolEditorPanel {

	protected SymbolEditor owner;
	protected int layerIndex = 0;

	/**
	 * Produces and returns an XMLEntity object describing the ISymbol according with
	 * the user settings. This XMLEntity must be self-defining far enough to be directly
	 * passed to the SymbolFactory.createFromXML() in order to produce correctly a new
	 * ISymbol instance of the class of this panel's ISymbol denoted by the getSymbolClass()
	 * method.
	 * @return XMLEntity defining the ISymbol.
	 * @see getSymbolClass() method
	 */
	public abstract XMLEntity getXMLEntity();

	/**
	 * Creates a new instance of this type symbol editor panel associated to
	 * its parent SymbolEditor which will be notified of the changes in
	 * order to keep the symbol preview in sync with the user settings.
	 * @param owner, the SymbolEditor which created this.
	 */
	public AbstractTypeSymbolEditorPanel(SymbolEditor owner) {
		this.owner = owner;
	}

	/**
	 * <p>Returns the name of the config tabs that will be shown in the selector combo box.
	 * This is typically a human-readable (and also translatable) name for the symbol that
	 * this TypeEditorPanel deals with, but maybe you prefer to use any other one.<br>
	 * </p>
	 * <p>
	 * The order of the entries in the combo is alphabetically-based. So you can force a
	 * position by defining a name that suits your needs.
	 * </p>
	 * @return A human-readable text naming this panel
	 */
	public abstract String getName();

	/**
	 * <p>Due to the complexity that many symbols settings can reach, the SymbolEditorPanel is
	 * designed in a tabbed-based fashion. So, you can use as many of pages you want to put
	 * your components. This pages are regular JPanels that will be automatically added to
	 * the SymbolEditor dialog.<br>
	 * </p>
	 * <p>
	 * In case you need only one page, just return a JPanel array with a length of 1.
	 * </p>
	 * @return An array of JPanel containing the setting's interface.
	 */
	public abstract JPanel[] getTabs();

	/**
	 * Invoked when the user selects or adds a new layer. This method fills up
	 * the components on the right according on the layer properties
	 * @param <b>int</b> layerIndex
	 */
	public abstract void refreshControls(int layerIndex);

	public final String toString() {
		return getName();
	}

	protected final void fireSymbolChangedEvent() {
		owner.setLayerToSymbol(owner.getSelectedLayerIndex(), getXMLEntity());
		owner.refresh();
	}

	/**
	 * Returns the symbol class that is handled by this configuration panel.
	 * @return <b>Class</b> (of the concrete ISymbol that this configuration panel deals with)
	 */
	public abstract Class getSymbolClass();
}

