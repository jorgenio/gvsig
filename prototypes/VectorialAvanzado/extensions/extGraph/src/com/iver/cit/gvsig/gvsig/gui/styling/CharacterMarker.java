/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: CharacterMarker.java 8539 2006-11-06 17:09:26Z jaume $
* $Log$
* Revision 1.3  2006-11-06 17:08:45  jaume
* *** empty log message ***
*
* Revision 1.2  2006/11/06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/31 16:16:34  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.gvsig.gui.styling;

import java.util.ArrayList;

import javax.swing.JPanel;

import com.iver.andami.PluginServices;
import com.iver.utiles.XMLEntity;

public class CharacterMarker extends AbstractTypeSymbolEditorPanel{

	private ArrayList tabs = new ArrayList();

	public CharacterMarker(SymbolEditor owner) {
		super(owner);
		initialize();
	}

	private void initialize() {
		JPanel myTab;
		{
			myTab = new JPanel();
			myTab.setName(PluginServices.getText(this, "character_marker"));
			tabs.add(myTab);
		}

	}

	public XMLEntity getXMLEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		return PluginServices.getText(this, "character_marker_symbol");
	}

	public JPanel[] getTabs() {
		return (JPanel[]) tabs .toArray(new JPanel[0]);
	}

	public void refreshControls(int layerIndex) {
		// TODO Auto-generated method stub

	}

	public Class getSymbolClass() {
		return CharacterMarker.class;
	}

}
