/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: MarkerFill.java 8687 2006-11-13 09:15:23Z jaume $
* $Log$
* Revision 1.7  2006-11-13 09:15:23  jaume
* javadoc and some clean-up
*
* Revision 1.6  2006/11/06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.5  2006/11/02 17:19:28  jaume
* *** empty log message ***
*
* Revision 1.4  2006/10/31 16:16:34  jaume
* *** empty log message ***
*
* Revision 1.3  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
* Revision 1.2  2006/10/29 23:53:49  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/27 12:41:09  jaume
* GUI
*
*
*/
package com.iver.cit.gvsig.gvsig.gui.styling;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JBlank;
import org.gvsig.gui.beans.swing.JButton;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.symbols.AbstractFillSymbol;
import com.iver.cit.gvsig.fmap.core.symbols.AbstractMarkerSymbol;
import com.iver.cit.gvsig.fmap.core.symbols.MarkerFillSymbol;
import com.iver.cit.gvsig.gui.panels.ColorChooserPanel;
import com.iver.utiles.StringUtilities;
import com.iver.utiles.XMLEntity;

import de.ios.framework.swing.JDecimalField;

public class MarkerFill extends AbstractTypeSymbolEditorPanel implements ActionListener {
    private static final String NAME = PluginServices.
    getText(MarkerFill.class, "marker_fill");
    private static final double DEFAULT_SEPARATION = 20;
    private static final double DEFAULT_OFFSET = 10;
    private ArrayList tabs = new ArrayList();
    private JDecimalField txtOffsetX;
    private JDecimalField txtOffsetY;
    private JDecimalField txtSeparationX;
    private JDecimalField txtSeparationY;
    private ColorChooserPanel markerCC;
    private JButton btnChooseMarker;
    private JButton btnOutline;
    private JRadioButton rdGrid;
    private JRadioButton rdRandom;
    private MarkerFillSymbol mfs = new MarkerFillSymbol();
    public MarkerFill(SymbolEditor owner) {
        super(owner);
        initialize();
    }

    private void initialize() {
        GridLayout layout;
        JPanel myTab;
        // Marker fill tab

        {
        	myTab = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        	myTab.setName(PluginServices.getText(this, "marker_fill"));

        	GridBagLayoutPanel p = new GridBagLayoutPanel();
        	JPanel aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
            markerCC = new ColorChooserPanel();
            markerCC.setAlpha(255);
            markerCC.addActionListener(this);
            aux.add(markerCC);

            p.addComponent(PluginServices.getText(this, "color"), aux);
            btnChooseMarker = new JButton(PluginServices.getText(this, "choose_marker"));
            btnChooseMarker.addActionListener(this);
            aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
            aux.add(btnChooseMarker);
            p.addComponent("", aux);

            aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
            btnOutline = new JButton(PluginServices.getText(this, "outline"));
            aux.add(btnOutline);
            btnOutline.setEnabled(false);
            p.addComponent("", aux);

            ButtonGroup group = new ButtonGroup();
            rdGrid = new JRadioButton(PluginServices.getText(this, "grid"));
            rdGrid.addActionListener(this);
            rdRandom = new JRadioButton(PluginServices.getText(this, "random"));
            rdRandom.addActionListener(this);
            group.add(rdGrid);
            group.add(rdRandom);



            aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
            aux.add(rdGrid);
            aux.add(rdRandom);

            p.addComponent("", aux);

            myTab.add(p);

        }
        tabs.add(myTab);

        // Fill properties tab
        {
            layout = new GridLayout();
            layout.setColumns(1);
            layout.setVgap(5);
            myTab = new JPanel();
            myTab.setName(PluginServices.getText(this, "fill_properties"));
            JPanel offsetPnl = new JPanel();
            offsetPnl.setBorder(BorderFactory.
                        createTitledBorder(null,
                                PluginServices.getText(this, "offset")));

            // add components to the offset panel here
            {
                JPanel aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5,5));
                aux.add(new JLabel("X:"));
                aux.add(txtOffsetX = new JDecimalField(10));
                offsetPnl.add(aux);

                aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5,5));
                aux.add(new JLabel("Y:"));
                aux.add(txtOffsetY = new JDecimalField(10));
                offsetPnl.add(aux);



            }
            layout.setRows(offsetPnl.getComponentCount());
            offsetPnl.setLayout(layout);

            myTab.add(offsetPnl);

            JPanel separationPnl = new JPanel();
            layout = new GridLayout();
            layout.setColumns(1);
            layout.setVgap(5);
            separationPnl.setBorder(BorderFactory.
                        createTitledBorder(null,
                                PluginServices.getText(this, "separation")));

            // add components to the separation panel here
            {
                JPanel aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5,5));
                aux.add(new JLabel("X:"));
                aux.add(txtSeparationX = new JDecimalField(10));
                separationPnl.add(aux);

                aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5,5));
                aux.add(new JLabel("Y:"));
                aux.add(txtSeparationY = new JDecimalField(10));
                separationPnl.add(aux);
            }
            layout.setRows(separationPnl.getComponentCount());
            separationPnl.setLayout(layout);
            myTab.add(separationPnl);
            layout = new GridLayout();
            layout.setColumns(1);
            layout.setVgap(5);
            layout.setRows(myTab.getComponentCount());
            myTab.setLayout(layout);
        }
       tabs.add(myTab);
    }

    public void refreshControls(int layerIndex) {
        AbstractFillSymbol sym = (AbstractFillSymbol) owner.getSymbol();
        if (sym.getLayer(layerIndex) == null) {
        	// set defaults
           	markerCC.setColor(Color.BLACK);
           	rdGrid.setSelected(true);
    		rdRandom.setSelected(false);
    		txtOffsetX.setText(String.valueOf(DEFAULT_OFFSET));
    		txtOffsetY.setText(String.valueOf(DEFAULT_OFFSET));
    		txtSeparationX.setText(String.valueOf(DEFAULT_SEPARATION));
    		txtSeparationY.setText(String.valueOf(DEFAULT_SEPARATION));
        } else {
        	XMLEntity xml = sym.getLayer(layerIndex).getXMLEntity();

        	// TODO este marker color es del MarkerSymbol,
        	// no del MarkerFill (l'has de buscar dins del fill d'este xmlEntity)
//        	markerCC.setColor(StringUtilities.string2Color(xml.getStringProperty("color")));
        	boolean b = xml.getBooleanProperty("grid");
        	rdGrid.setSelected(b);
        	rdRandom.setSelected(!b);

        	txtOffsetX.setText(String.valueOf(xml.getDoubleProperty("xOffset")));
        	txtOffsetY.setText(String.valueOf(xml.getDoubleProperty("yOffset")));

        	txtSeparationX.setText(String.valueOf(xml.getDoubleProperty("xSeparation")));
        	txtSeparationY.setText(String.valueOf(xml.getDoubleProperty("ySeparation")));
        }
    }

    public XMLEntity getXMLEntity() {
        return mfs.getXMLEntity();
    }

    public String getName() {
        return NAME;
    }

    public JPanel[] getTabs() {
        return (JPanel[]) tabs.toArray(new JPanel[0]);
    }

    public void actionPerformed(ActionEvent e) {
        JComponent comp = (JComponent) e.getSource();
        if (comp.equals(btnChooseMarker)) {
            AbstractFillSymbol ownersSymbol = (AbstractFillSymbol) owner.getSymbol();


            SymbolSelector symSelect = new SymbolSelector( ownersSymbol.getLayer(layerIndex), FShape.POINT);
            PluginServices.getMDIManager().addWindow(symSelect);
            AbstractMarkerSymbol marker = (AbstractMarkerSymbol) symSelect.getSymbol();
            mfs.setMarker(marker);
        }
        try {
            mfs.setXOffset(Double.parseDouble(txtOffsetX.getText()));
        } catch (Exception ex) {}
        try {
            mfs.setYOffset(Double.parseDouble(txtOffsetY.getText()));
        } catch (Exception ex) {}
        try {
            mfs.setXSeparation(Double.parseDouble(txtSeparationX.getText()));
        } catch (Exception ex) {}
        try {
            mfs.setYSeparation(Double.parseDouble(txtSeparationY.getText()));
        } catch (Exception ex) {}
        try {
            mfs.setGrid(rdGrid.isSelected());
        } catch (Exception ex) {}

        fireSymbolChangedEvent();
    }

	public Class getSymbolClass() {
		return MarkerFillSymbol.class;
	}

}
