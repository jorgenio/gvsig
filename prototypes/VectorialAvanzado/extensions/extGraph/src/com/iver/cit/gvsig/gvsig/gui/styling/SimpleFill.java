/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: SimpleFill.java 8687 2006-11-13 09:15:23Z jaume $
* $Log$
* Revision 1.4  2006-11-13 09:15:23  jaume
* javadoc and some clean-up
*
* Revision 1.3  2006/11/06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.2  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/27 12:41:09  jaume
* GUI
*
*
*/
package com.iver.cit.gvsig.gvsig.gui.styling;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.gvsig.gui.beans.swing.GridBagLayoutPanel;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.gui.panels.ColorChooserPanel;
import com.iver.utiles.XMLEntity;

import de.ios.framework.swing.JDecimalField;

public class SimpleFill extends AbstractTypeSymbolEditorPanel {
	private static final String NAME = PluginServices.
		getText(SimpleFill.class, "simple_fill");
	private ColorChooserPanel jccFillColor;
	private ColorChooserPanel jccOutlineColor;
	private JDecimalField txtOutlineWidth;
	private ArrayList tabs = new ArrayList();

	public SimpleFill(SymbolEditor owner) {
		super(owner);
		initialize();
	}

	public String getName() {
		return NAME;
	}

	public JPanel[] getTabs() {
		return (JPanel[]) tabs.toArray(new JPanel[0]);
	}

	private void initialize() {
		JPanel myTab = new JPanel(new FlowLayout(FlowLayout.LEADING, 5,5));
		myTab.setName(PluginServices.getText(this, "simple_fill"));
		GridBagLayoutPanel aux = new GridBagLayoutPanel();
		jccFillColor = new ColorChooserPanel();
		jccFillColor.setAlpha(255);
		// TODO restore previous color
		aux.addComponent(PluginServices.getText(this, "fill_color")+":",
			jccFillColor	);
		jccOutlineColor = new ColorChooserPanel();
		jccOutlineColor.setAlpha(255);
		// TODO restore previous outline color
		aux.addComponent(PluginServices.getText(this, "outline_color")+":",
			jccOutlineColor	);
		txtOutlineWidth = new JDecimalField(25);
		// TODO restore previous outline width
		aux.addComponent(PluginServices.getText(this, "outline_width")+":",
			txtOutlineWidth );
		aux.setPreferredSize(new Dimension(300, 300));
		myTab.add(aux);

		tabs.add(myTab);
	}

	public XMLEntity getXMLEntity() {
		// TODO Auto-generated method stub
		throw new Error("Not yet implemented!");
	}

	public void refreshControls(int layerIndex) {
		// TODO Auto-generated method stub

	}

	public Class getSymbolClass() {
		return SimpleFill.class;
	}

}
