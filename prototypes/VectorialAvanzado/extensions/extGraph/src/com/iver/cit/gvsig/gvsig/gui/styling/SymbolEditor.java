/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: SymbolEditor.java 8687 2006-11-13 09:15:23Z jaume $
* $Log$
* Revision 1.10  2006-11-13 09:15:23  jaume
* javadoc and some clean-up
*
* Revision 1.9  2006/11/06 17:08:45  jaume
* *** empty log message ***
*
* Revision 1.8  2006/11/06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.7  2006/11/06 07:33:54  jaume
* javadoc, source style
*
* Revision 1.6  2006/11/02 17:19:28  jaume
* *** empty log message ***
*
* Revision 1.5  2006/10/31 16:16:34  jaume
* *** empty log message ***
*
* Revision 1.4  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
* Revision 1.3  2006/10/29 23:53:49  jaume
* *** empty log message ***
*
* Revision 1.2  2006/10/27 12:41:09  jaume
* GUI
*
* Revision 1.1  2006/10/26 16:31:21  jaume
* GUI
*
*
*/
package com.iver.cit.gvsig.gvsig.gui.styling;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.gvsig.gui.beans.AcceptCancelPanel;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.utiles.XMLEntity;

/**
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class SymbolEditor extends JPanel implements IWindow {

    private WindowInfo wi;
    private JPanel pnlWest = null;
    private JPanel pnlCenter = null;
    private JPanel pnlPreview = null;
    private JPanel pnlLayers = null;
    private AcceptCancelPanel okCancelPanel;
    private ISymbol symbol;
    private SymbolPreview symbolPreview = null;
    private JPanel pnlTypeAndUnits = null;
    private JComboBox cmbType;
    private JComboBox cmbUnits;
    private JTabbedPane tabbedPane = null;
    private int shapeType;
    private XMLEntity oldSymbolProperties;
    private ActionListener cmbTypeActionListener;

    private AbstractTypeSymbolEditorPanel[] tabs;
	private SymbolLayerManager layerManager;


    public SymbolEditor(ISymbol symbol, int shapeType) {
    	this.symbol = symbol;
        this.oldSymbolProperties = symbol.getXMLEntity();
        this.shapeType = shapeType;
        initialize();
    }


    /**
     * This method initializes this
     *
     */
    private void initialize() {

    	cmbTypeActionListener = new ActionListener() {
    		int prevIndex = -2;
    		public void actionPerformed(ActionEvent e) {
    			int index = getCmbType().getSelectedIndex();
    			if (prevIndex != index) {
    				// needs to refresh
    				prevIndex = index;

    				if (tabbedPane != null)
    					getPnlCenter().remove(tabbedPane);
    				AbstractTypeSymbolEditorPanel options =
    					(AbstractTypeSymbolEditorPanel) getCmbType().getSelectedItem();
    				options.refreshControls(getSelectedLayerIndex());
    				JPanel[] tabs = options.getTabs();
    				tabbedPane = new JTabbedPane();
    				tabbedPane.setPreferredSize(new Dimension(300, 300));
    				for (int i = 0; i < tabs.length; i++) {
    					tabbedPane.addTab(tabs[i].getName(), tabs[i]);
    				}
    				getPnlCenter().add(tabbedPane, BorderLayout.CENTER);
    				getPnlCenter().doLayout();
    			}
    		}
    	};

    	Comparator tabComparator = new Comparator() {
			public int compare(Object o1, Object o2) {
				if (! (o1 instanceof AbstractTypeSymbolEditorPanel
					|| o1 instanceof AbstractTypeSymbolEditorPanel))
					throw new IllegalArgumentException(PluginServices.getText(this, "trying_to_add_a_non_TypeSymbolEditor_panel"));
				AbstractTypeSymbolEditorPanel pnl1 = (AbstractTypeSymbolEditorPanel) o1,
				pnl2 = (AbstractTypeSymbolEditorPanel) o2;
				int result = pnl1.getName().compareTo(pnl2.getName());
				if (result == 0)
					throw new IllegalArgumentException(PluginServices.getText(this, "two_panels_with_the_same_name"));
				return result;
			}
		};
		TreeSet set = new TreeSet(tabComparator);

    	if (shapeType == FShape.POLYGON)
    	{
    		// Polygon options

        	// add here your options for polygon symbols
        	set.add(new SimpleFill(SymbolEditor.this));
        	set.add(new MarkerFill(SymbolEditor.this));
        }
        else if (shapeType == FShape.POINT)
        {
        	// Point options

        	// add here your options for point symbol
        	set.add(new CharacterMarker(SymbolEditor.this));
        }
    	tabs = (AbstractTypeSymbolEditorPanel[]) set.toArray(new AbstractTypeSymbolEditorPanel[0]);

        this.setLayout(new BorderLayout());
        this.add(getPnlWest(), BorderLayout.WEST);
        this.add(getPnlCenter(), BorderLayout.CENTER);
        this.add(getOkCancelPanel(), BorderLayout.SOUTH);


        cmbTypeActionListener.actionPerformed(null);
        refresh();
    }

    private AcceptCancelPanel getOkCancelPanel() {
        if (okCancelPanel == null) {
            ActionListener action = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if ("CANCEL".equals(e.getActionCommand())) {
                        symbol = null;
                    }
                    PluginServices.getMDIManager().closeWindow(SymbolEditor.this);
                }
            };
            okCancelPanel = new AcceptCancelPanel(action, action);
        }
        return okCancelPanel;
    }


    public WindowInfo getWindowInfo() {
        if (wi == null) {
            wi = new WindowInfo(WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE);
            wi.setWidth(600);
            wi.setHeight(400);
            wi.setTitle(PluginServices.getText(this, "symbol_property_editor"));
        }
        return wi;
    }

    public ISymbol getSymbol() {
        return symbol;
    }
    /**
     * This method initializes pnlWest
     *
     * @return javax.swing.JPanel
     */
    private JPanel getPnlWest() {
        if (pnlWest == null) {
            pnlWest = new JPanel();
            pnlWest.setLayout(new BorderLayout());
            pnlWest.add(getPnlPreview(), java.awt.BorderLayout.NORTH);
            pnlWest.add(getPnlLayers(), java.awt.BorderLayout.SOUTH);
        }
        return pnlWest;
    }
    /**
     * This method initializes pnlCenter
     *
     * @return javax.swing.JPanel
     */
    private JPanel getPnlCenter() {
        if (pnlCenter == null) {
            pnlCenter = new JPanel(new BorderLayout());
            pnlCenter.setBorder(BorderFactory.
                    createTitledBorder(null,
                            PluginServices.getText(this, "properties")));
            pnlCenter.add(getPnlTypeAndUnits(), java.awt.BorderLayout.NORTH);
        }
        return pnlCenter;
    }
    /**
     * This method initializes pnlPreview
     *
     * @return javax.swing.JPanel
     */
    private JPanel getPnlPreview() {
        if (pnlPreview == null) {
            pnlPreview = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
            pnlPreview.setBorder(BorderFactory.createTitledBorder(null, PluginServices.getText(this, "preview")));
            pnlPreview.add(getSymbolPreview());
        }
        return pnlPreview;
    }
    /**
     * This method initializes pnlLayers
     *
     * @return javax.swing.JPanel
     */
    private JPanel getPnlLayers() {
        if (pnlLayers == null) {
            pnlLayers = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
            pnlLayers.setBorder(
            		BorderFactory.createTitledBorder(
            				null, PluginServices.getText(this, "layers")));
            pnlLayers.add(getLayerManager());
        }
        return pnlLayers;
    }


    private SymbolLayerManager getLayerManager() {
    	if (layerManager == null) {
    		layerManager = new SymbolLayerManager(this);
    	}
    	return layerManager;
    }


	/**
     * This method initializes symbolPreview
     *
     * @return com.iver.cit.gvsig.gvsig.gui.styling.SymbolPreview
     */
    private SymbolPreview getSymbolPreview() {
        if (symbolPreview == null) {
            symbolPreview = new SymbolPreview();
            symbolPreview.setPreferredSize(new Dimension(120, 100));

        }
        return symbolPreview;
    }



    /**
     * This method initializes pnlTypeAndUnits
     *
     * @return javax.swing.JPanel
     */
    private JPanel getPnlTypeAndUnits() {
        if (pnlTypeAndUnits == null) {
            pnlTypeAndUnits = new JPanel();
            pnlTypeAndUnits.setLayout(new BorderLayout());
            JPanel aux = new JPanel(new FlowLayout(FlowLayout.LEADING));
            aux.add(new JLabel(PluginServices.getText(this, "type")));
            aux.add(getCmbType());
            pnlTypeAndUnits.add(aux, BorderLayout.WEST);

            aux =  new JPanel(new FlowLayout(FlowLayout.LEADING));
            aux.add(new JLabel(PluginServices.getText(this, "units")));
            aux.add(getCmbUnits());
            pnlTypeAndUnits.add(aux, BorderLayout.EAST);

        }
        return pnlTypeAndUnits;
    }

    private JComboBox getCmbUnits() {
        if (cmbUnits == null) {
            cmbUnits = new JComboBox(new String[] {"unidad1", "unidad2"});
        }
        return cmbUnits;
    }

    private JComboBox getCmbType() {
        if (cmbType == null) {
            cmbType = new JComboBox(tabs);
            cmbType.addActionListener(cmbTypeActionListener);
        }
        return cmbType;
    }

    protected void changeSymbolProperties(XMLEntity newXMLEntity) {
    	XMLEntity symXML = symbol.getXMLEntity();
    	for (int i = 0; i < newXMLEntity.getPropertyCount(); i++) {
			String pName = newXMLEntity.getPropertyName(i);
			symXML.putProperty(pName, newXMLEntity.getStringProperty(pName));
		}
		refresh();
	}

	protected void setLayerToSymbol(int layerIndex, XMLEntity layerXML) {
    	XMLEntity symXML = symbol.getXMLEntity();
    	ArrayList layers = new ArrayList();
    	for (int i = 0; i < symXML.getChildrenCount(); i++) {
			layers.add(symXML.getChild(i));
		}
    	layers.add(layerIndex, layerXML);
    	symXML.removeAllChildren();
    	for (int i = 0; i < layers.size(); i++) {
    		symXML.addChild((XMLEntity) layers.get(i));
		}
    	symbol.setXMLEntity(symXML);
    	refresh();
    }

    protected void removeAllLayersFromSymbol() {
    	symbol.getXMLEntity().removeAllChildren();
    	refresh();
    }

    public void refresh() {
    	getSymbolPreview().setSymbol(symbol);
    	doLayout();
	}

	public int getSelectedLayerIndex() {
		int index =getLayerManager().getSelectedLayerIndex();
		if (index == -1)
			return 0;
		return index;
	}

	/**
	 * <p>
	 * Returns the type of the symbol that this panels is created for.<br>
	 * </p>
	 * <p>
	 * Possible values returned by this method are
	 *   <ol>
	 *     <li> <b> FShape.POINT </b>, for maker symbols </li>
	 *     <li> <b> FShape.POLYGON </b>, for fill symbols </li>
	 *     <li> <b> FShape.LINE </b>, for line symbols (not yet implemented) </li>
	 *     <li> <b> FShape.TEXT </b>, for text symbols (not yet implemented) </li>
	 *     <li> maybe some other in the future </li>
	 *   </ol>
	 * </p>
	 * @return
	 */
	public int getShapeType() {
		return shapeType;
	}

	public Class getNewSymbolClass() {
		return ((AbstractTypeSymbolEditorPanel) getCmbType()
					.getSelectedItem()).getSymbolClass();
	}
}
