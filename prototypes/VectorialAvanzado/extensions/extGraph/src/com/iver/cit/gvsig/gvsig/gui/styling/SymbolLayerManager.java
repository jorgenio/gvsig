/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: SymbolLayerManager.java 8588 2006-11-08 10:56:47Z jaume $
* $Log$
* Revision 1.6  2006-11-08 10:56:47  jaume
* *** empty log message ***
*
* Revision 1.5  2006/11/07 08:52:30  jaume
* *** empty log message ***
*
* Revision 1.4  2006/11/06 17:08:45  jaume
* *** empty log message ***
*
* Revision 1.3  2006/11/06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.2  2006/11/06 07:33:54  jaume
* javadoc, source style
*
* Revision 1.1  2006/11/02 17:19:28  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.gvsig.gui.styling;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.gvsig.gui.beans.swing.JButton;

import com.iver.andami.PluginServices;
import com.iver.andami.messages.NotificationManager;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.cit.gvsig.fmap.core.symbols.CharacterMarkerSymbol;
import com.iver.cit.gvsig.fmap.core.symbols.SymbolFactory;
import com.iver.utiles.XMLEntity;

/**
 * A component linked to a SymbolEditor that shows the layers
 *
 * @author jaume
 *
 */
public class SymbolLayerManager extends JPanel implements ActionListener{
    private SymbolEditor owner;
    private JList jListLayers;
    private ISymbol activeLayer;
    private ISymbol symbol;
    private JButton btnAddLayer = null;
    private JPanel pnlButtons = null;
    private JButton btnRemoveLayer = null;
    private JButton btnMoveUpLayer = null;
    private JButton btnMoveDownLayer = null;
    private final Dimension btnDimension = new Dimension(24, 24);
    private JScrollPane scroll;
    private Color cellSelectedBGColor = Color.BLUE;


    public SymbolLayerManager(SymbolEditor owner) {
        this.owner = owner;
        this.symbol = owner.getSymbol();
        initialize();
    }

    private void initialize() {
        this.setLayout(new BorderLayout());
        this.setSize(new java.awt.Dimension(155,106));
        this.add(getJScrollPane(), java.awt.BorderLayout.CENTER);
        this.add(getPnlButtons(), java.awt.BorderLayout.SOUTH);
    }

    private JScrollPane getJScrollPane() {
        if (scroll ==  null) {
            scroll = new JScrollPane();
            scroll.setViewportView(getJListLayers());
            scroll.setPreferredSize(new Dimension(120, 160));
        }
        return scroll;
    }

    /**
     * This method initializes jList
     *
     * @return javax.swing.JList
     */
    private JList getJListLayers() {
        if (jListLayers == null) {
            jListLayers = new JList();
            jListLayers.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
            jListLayers.setLayoutOrientation(JList.HORIZONTAL_WRAP);

            jListLayers.setVisibleRowCount(-1);
            jListLayers.addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent e) {
                    setLayer((ISymbol) jListLayers.getSelectedValue());
                }
            });
            jListLayers.setModel(new SymbolLayerListModel(symbol));
            jListLayers.setCellRenderer(new DefaultListCellRenderer() {

                public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                    return getListCell(list, value, index, isSelected, cellHasFocus);
                }

            });

            jListLayers.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
						SymbolSelector symSel = new SymbolSelector(activeLayer, owner.getShapeType());
						PluginServices.getMDIManager().addWindow(symSel);
						updateSymbol(symSel.getSymbol());
						setLayer(symSel.getSymbol());
					}

				}
            });
        }
        return jListLayers;
    }

    public Component getListCell(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
    	ISymbol sym = (ISymbol) value;
        JPanel pnl = new JPanel();
        LayoutManager layout = new FlowLayout(FlowLayout.LEFT, 3, 3);
        pnl.setLayout(layout);
        Color bgColor = (isSelected) ? cellSelectedBGColor
                 : Color.WHITE;
        pnl.setBackground(bgColor);
        SymbolPreview sp = new SymbolPreview();
        sp.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        sp.setAlignmentX(Component.LEFT_ALIGNMENT);
        sp.setPreferredSize(new Dimension(80, 30));
        sp.setSize(new Dimension(80, 30));
        sp.setSymbol(sym);
        sp.setBackground(Color.WHITE);
        pnl.add(sp);

        XMLEntity xml = sym.getXMLEntity();
        String source;
        if (xml.contains("locked"))
            source = xml.getBooleanProperty("locked") ?
                    "images/locked.png" :
                    "images/unlocked.png";
         else
            source = "images/unlocked.png";

        ImageIcon icon = new ImageIcon(getClass().
                getClassLoader().getResource(source));
        JButton btnLock = new JButton(icon);
        btnLock.setSize(btnDimension);
        btnLock.setPreferredSize(btnDimension);
        btnLock.setActionCommand("LOCK");
        btnLock.setBackground(bgColor);
        btnLock.setAlignmentX(Component.CENTER_ALIGNMENT);
        pnl.add(btnLock);
        pnl.setPreferredSize(new Dimension(150,37));
        return pnl;
    }

    /**
     * replaces current selected layer with this symbol
     * @param layer
     */
    private void updateSymbol(ISymbol layer) {
    	if (layer != null) {
    		int index = getJListLayers().getSelectedIndex();
    		XMLEntity xml = symbol.getXMLEntity();
    		ArrayList layers = new ArrayList() ;
    		for (int i = 0; i < xml.getChildrenCount(); i++) {
    			if (i == index) {
    				layers.add(layer.getXMLEntity());
    			} else {
    				layers.add(xml.getChild(i));
    			}
    		}
    		xml.removeAllChildren();
    		for (int i = 0; i < layers.size(); i++) {
    			xml.addChild((XMLEntity) layers.get(i));
    		}
    		symbol.setXMLEntity(xml);
    		owner.refresh();
    		getJListLayers().repaint();
    	}
    }

    private void setLayer(ISymbol symbol) {
    	if (symbol!=null) {
    		activeLayer = symbol;

    	}
    }

    /**
     * This method initializes jButton
     *
     * @return javax.swing.JButton
     */
    private JButton getBtnAddLayer() {
        if (btnAddLayer == null) {
            btnAddLayer = new JButton(new ImageIcon(getClass().
                    getClassLoader().getResource("images/add-layer.png")));
            btnAddLayer.setActionCommand("ADD");
            btnAddLayer.setSize(btnDimension);
            btnAddLayer.setPreferredSize(btnDimension);
            btnAddLayer.addActionListener(this);
        }
        return btnAddLayer;
    }

    /**
     * This method initializes pnlButtons
     *
     * @return javax.swing.JPanel
     */
    private JPanel getPnlButtons() {
        if (pnlButtons == null) {
            pnlButtons = new JPanel();
            pnlButtons.add(getBtnAddLayer(), null);
            pnlButtons.add(getBtnRemoveLayer(), null);
            pnlButtons.add(getBtnMoveUpLayer(), null);
            pnlButtons.add(getBtnMoveDown(), null);
        }
        return pnlButtons;
    }

    /**
     * This method initializes jButton1
     *
     * @return javax.swing.JButton
     */
    private JButton getBtnRemoveLayer() {
        if (btnRemoveLayer == null) {
            btnRemoveLayer = new JButton(new ImageIcon(getClass().
                    getClassLoader().getResource("images/delete.png")));
            btnRemoveLayer.setActionCommand("REMOVE");
            btnRemoveLayer.setSize(btnDimension);
            btnRemoveLayer.setPreferredSize(btnDimension);
            btnRemoveLayer.addActionListener(this);
        }
        return btnRemoveLayer;
    }

    /**
     * This method initializes jButton2
     *
     * @return javax.swing.JButton
     */
    private JButton getBtnMoveUpLayer() {
        if (btnMoveUpLayer == null) {
            btnMoveUpLayer = new JButton(new ImageIcon(getClass().
                    getClassLoader().getResource("images/up-arrow.png")));
            btnMoveUpLayer.setActionCommand("MOVE_UP");
            btnMoveUpLayer.setSize(btnDimension);
            btnMoveUpLayer.setPreferredSize(btnDimension);
            btnMoveUpLayer.addActionListener(this);
        }
        return btnMoveUpLayer;
    }

    /**
     * This method initializes jButton3
     *
     * @return javax.swing.JButton
     */
    private JButton getBtnMoveDown() {
        if (btnMoveDownLayer == null) {
            btnMoveDownLayer = new JButton(new ImageIcon(getClass().
                    getClassLoader().getResource("images/down-arrow.png")));
            btnMoveDownLayer.setActionCommand("MOVE_DOWN");
            btnMoveDownLayer.setSize(btnDimension);
            btnMoveDownLayer.setPreferredSize(btnDimension);
            btnMoveDownLayer.addActionListener(this);
        }
        return btnMoveDownLayer;
    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        XMLEntity xml = symbol.getXMLEntity();
        int index = getJListLayers().getSelectedIndex();
        if (command.equals("ADD")) {
    		String className = null;
    		ISymbol newLayer = null;
        	try {

        		Class symbolClass = owner.getNewSymbolClass();
				className = symbolClass.getName();
				newLayer = (ISymbol) symbolClass.newInstance();
        	} catch (InstantiationException iE) {
        		NotificationManager.
        		addError("Trying to instantiate an interface" +
        				" or abstract class '"+className+"'", iE);
        		return;
        	} catch (IllegalAccessException iaE) {
        		NotificationManager.addError(null, iaE);
        		return;
        	}
        	xml.addChild(newLayer.getXMLEntity());
            index = xml.getChildrenCount()-1;
        } else if (command.equals("REMOVE")) {
        	int selectedIndex = getJListLayers().getSelectedIndex();
        	if (selectedIndex == 0)
        		return;
        	try {
        		xml.removeChild(selectedIndex-1);
        	} catch (Exception ex) {
        		// Index out of bounds or so, we don't care
        	}
        } else if (command.equals("MOVE_UP")) {
            if (index>0) {
                ArrayList layers = new ArrayList();
                for (int i = 0; i < xml.getChildrenCount(); i++) {
                    if (i == index)
                        layers.add(i-1, xml.getChild(i));
                    else
                        layers.add(xml.getChild(i));
                }
                xml.removeAllChildren();
                for (int i = 0; i < layers.size(); i++) {
                    xml.addChild((XMLEntity) layers.get(i));
                }
                index --;
            }
        } else if (command.equals("MOVE_DOWN")) {
            if (index < xml.getChildrenCount()-1) {
                ArrayList layers = new ArrayList();
                for (int i = 0; i < xml.getChildrenCount(); i++) {
                    if (i == index) {
                        layers.add(i, xml.getChild(i+1));
                        layers.add(i+1, xml.getChild(i));
                        i++;
                    }
                    else
                        layers.add(xml.getChild(i));
                }
                xml.removeAllChildren();
                for (int i = 0; i < layers.size(); i++) {
                    xml.addChild((XMLEntity) layers.get(i));
                }
                index++;
            }
        } else if (command.equals("LOCK")) {
            try {
                XMLEntity layerXML = xml.getChild(index);
                boolean locked = !xml.contains("locked") || !xml.getBooleanProperty("locked");
                layerXML.putProperty("locked", locked);
            } catch (Exception ex) {
                // Index out of bounds or so, we don't care
            }
        }
        symbol.setXMLEntity(xml);
        getJListLayers().setSelectedIndex(index);
        owner.refresh();
        getJListLayers().repaint();
    }

    public int getSelectedLayerIndex() {
        /*int i = getJListLayers().getSelectedIndex();
        if (i==-1 && symbol.getXMLEntity().getChildrenCount()==0)
            i = 0;
        return i;*/
        return getJListLayers().getSelectedIndex();
    }
}

class SymbolLayerListModel extends AbstractListModel {

    private ISymbol symbol;
	private ArrayList listeners = new ArrayList();

    public SymbolLayerListModel(ISymbol symbol) {
        this.symbol = symbol;
    }

    public int getSize() {
        return symbol.getXMLEntity().getChildrenCount()+1;
    }

    public Object getElementAt(int index) {
    	XMLEntity xml = symbol.getXMLEntity();

    	if (index == 0) {
    		xml.removeAllChildren();
    		return SymbolFactory.createFromXML(xml,
    	        		PluginServices.getText(this, "layer")+0);
    	}
        return SymbolFactory.createFromXML(xml.getChild(index-1),
        		PluginServices.getText(this, "layer")+index);
    }

    public void addListDataListener(ListDataListener l) {
    	listeners.add(l);
    }

    public void removeListDataListener(ListDataListener l) {
    	listeners.remove(l);
    }

}
