/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: SymbolPreview.java 8432 2006-10-30 19:30:35Z jaume $
* $Log$
* Revision 1.7  2006-10-30 19:30:35  jaume
* *** empty log message ***
*
* Revision 1.6  2006/10/29 23:53:49  jaume
* *** empty log message ***
*
* Revision 1.5  2006/10/29 21:45:12  jaume
* centers in x the "none selected message"
*
* Revision 1.4  2006/10/29 21:43:34  jaume
* *** empty log message ***
*
* Revision 1.3  2006/10/29 21:40:29  jaume
* centers in x the "none selected message"
*
* Revision 1.2  2006/10/26 07:46:58  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/25 10:50:41  jaume
* movement of classes and gui stuff
*
* Revision 1.2  2006/10/24 19:54:55  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/24 16:31:12  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.gvsig.gui.styling;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

import javax.swing.JPanel;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.core.ISymbol;

public class SymbolPreview extends JPanel {
	private int hGap = 5, vGap = 5;
	private ISymbol symbol;


	public SymbolPreview() {
		super();
		setBackground(Color.WHITE);
	}

	public ISymbol getSymbol() {
		return symbol;
	}

	public void setSymbol(ISymbol symbol) {
		this.symbol = symbol;
		repaint();
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.translate(hGap, vGap);
		Rectangle r = getBounds();
		r = new Rectangle(0, 0, (int) (r.getWidth()-(hGap*2)), (int) (r.getHeight()-(vGap*2)));

		if (symbol != null) {
			symbol.drawInsideRectangle(g2, new AffineTransform(), r);
		} else {
			String noneSelected = "["+PluginServices.getText(this, "none_selected")+"]";
			FontMetrics fm = g2.getFontMetrics();
			int lineWidth = fm.stringWidth(noneSelected);
			float scale = (float) r.getWidth() / lineWidth;
			Font f = g2.getFont();
			float fontSize = f.getSize()*scale;
			g2.setFont(	f.deriveFont( fontSize ) );

			g2.drawString(noneSelected,	 (r.x*scale) - (hGap/2), r.height/2+vGap*scale);
		}
	}
}
