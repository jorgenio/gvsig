/*
 * Created on 26-abr-2005
 *
 * gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
package com.iver.cit.gvsig.gvsig.gui.styling;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.exolab.castor.xml.Marshaller;
import org.gvsig.gui.beans.AcceptCancelPanel;
import org.gvsig.gui.beans.swing.GridBagLayoutPanel;
import org.gvsig.gui.beans.swing.JButton;

import com.iver.andami.PluginServices;
import com.iver.andami.messages.NotificationManager;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.cit.gvsig.gui.panels.ColorChooserPanel;
import com.iver.utiles.StringUtilities;
import com.iver.utiles.XMLEntity;

import de.ios.framework.swing.JDecimalField;
import de.ios.framework.swing.JNumberField;

/**
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 */
public class SymbolSelector extends JPanel implements IWindow{

    private JTree treeFamilies = null;
    private JList jListSymbols = null;
    private JPanel jPanel = null;
    private JScrollPane jScrollPane = null;
    private JScrollPane jScrollPane1 = null;
    private SymbolPreview jPanelPreview = null;
    private GridBagLayoutPanel jPanelOptions = null;
	private WindowInfo wi;
	private JSplitPane jSplitPane = null;
	private File symbolDir;
	private AcceptCancelPanel okCancelPanel;
	private ISymbol selectedSymbol = null;
	private JPanel northPanel;
	private ColorChooserPanel jcc1;
	private ColorChooserPanel jcc2;
	private JNumberField txtSize;
	private JDecimalField txtAngle;
	private JPanel jPanelButtons;
	private JButton btnProperties;
	private int shapeType;
	private JButton btnSaveSymbol;
	private JButton btnResetSymbol;
	private ActionListener buttonListener = new MyButtonListener();
    /**
     * This method initializes
     *
     */
    public SymbolSelector(ISymbol symbol, int shapeType) {
    	super();
    	this.selectedSymbol = symbol;
    	this.shapeType = shapeType;
    	this.symbolDir = new File(getClass().
  				getClassLoader().
  				getResource("symbols").
  				getFile());
    	initialize();
    }

    /**
     * This method initializes
     *
     */
    public SymbolSelector(ISymbol symbol, int shapeType, File symbolDir) {
    	super();
    	this.selectedSymbol = symbol;
    	this.shapeType = shapeType;
    	this.symbolDir = symbolDir;
    	initialize();
    }
    /**
     * This method initializes this
     *
     */
    private void initialize() {

        this.setLayout(new BorderLayout());
        this.setSize(400, 221);

        this.add(getJNorthPanel(), BorderLayout.NORTH);
        this.add(getJSplitPane(), BorderLayout.CENTER);
        this.add(getJEastPanel(), BorderLayout.EAST);
        ActionListener okAction = new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    			PluginServices.getMDIManager().closeWindow(SymbolSelector.this);
    		}
    	}, cancelAction = new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    			setSymbol(null);
    			PluginServices.getMDIManager().closeWindow(SymbolSelector.this);
    		}
    	};

        okCancelPanel = new AcceptCancelPanel(okAction, cancelAction);


        this.add(okCancelPanel, BorderLayout.SOUTH);
    }

    private JPanel getJNorthPanel() throws IllegalArgumentException {
		if (northPanel == null) {
			String text = "";
			switch (shapeType) {
			case FShape.POINT:
				text = PluginServices.getText(this, "point_symbols");
				break;
			case FShape.LINE:
				text = PluginServices.getText(this, "line_symbols");
				break;
			case FShape.POLYGON:
				text = PluginServices.getText(this, "polygon_symbols");
				break;
			default:
				throw new IllegalArgumentException(PluginServices.getText(this, "shape_type_not_yet_supported"));
			}
			northPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
			JLabel lbl = new JLabel(text);
			lbl.setFont(lbl.getFont().deriveFont(Font.BOLD));
			northPanel.add(lbl);
		}
		return northPanel;
	}

	/**
     * This method initializes jList
     *
     * @return javax.swing.JList
     */
    private JTree getJListFav() {
    	if (treeFamilies == null) {
    		treeFamilies = new JTree();
    		treeFamilies.setPreferredSize(new java.awt.Dimension(70,100));
    		try {
    			treeFamilies.setModel(new TreeModel() {
    				final class MyFile extends File {
    					public MyFile(String pathname) {
    						super(pathname);
    					}

    					public String toString() {
    						if (this.equals(root))
    							return PluginServices.getText(this, "symbol_library");
    						String path = getAbsolutePath();
    						String prefixToRemove = symbolDir.getAbsolutePath();
    						path = path.substring(prefixToRemove.length()+1, path.length());
    						return path;
    					}
    				};
    				MyFile root = new MyFile(symbolDir.getAbsolutePath());

    				private FileFilter ff = new FileFilter() {
    					public boolean accept(File pathname) {
    						return pathname.isDirectory();
    					}
    				};

    				public Object getRoot() {
    					return root;
    				}

    				public int getChildCount(Object parent) {
    					return ((File) parent).listFiles(ff).length;
    				}

    				public boolean isLeaf(Object node) {
    					return getChildCount(node)==0;
    				}

    				public void addTreeModelListener(TreeModelListener l) {
    					// TODO Necessite?

    				}

    				public void removeTreeModelListener(TreeModelListener l) {
    					// TODO Auto-generated method stub

    				}

    				public Object getChild(Object parent, int index) {
    					return new MyFile(((File) parent).listFiles(ff)[index].getAbsolutePath());
    				}

    				public int getIndexOfChild(Object parent, Object child) {
    					if (parent == null)
    						return -1;
    					File[] files = ((File) parent).listFiles(ff);
    					for (int i = 0; i < files.length; i++) {
    						if (files[i].equals((File) child))
    							return i;
    					}
    					return -1;
    				}

    				public void valueForPathChanged(TreePath path, Object newValue) {
    					// TODO Auto-generated method stub
    				}

    			});
    			treeFamilies.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
					public void valueChanged(javax.swing.event.TreeSelectionEvent e) {
						 File node = (File)
	                       treeFamilies.getLastSelectedPathComponent();

						 	if (node == null) return;
						 	jListSymbols.setModel(
					          		new SymbolSelectorListModel(node));
					}
				});
    		} catch ( ExceptionInInitializerError ex ) {
    			JOptionPane.showMessageDialog(this, PluginServices.getText(this, "could_not_find_symbol_directory"));
    		}
    	}
    	return treeFamilies;
    }

    /**
     * This method initializes jList
     *
     * @return javax.swing.JList
     */
    private JList getJListSymbols() {
    	if (jListSymbols == null) {
    		jListSymbols = new JList();
    		jListSymbols.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
            jListSymbols.setLayoutOrientation(JList.HORIZONTAL_WRAP);
            jListSymbols.setVisibleRowCount(-1);
            jListSymbols.addListSelectionListener(new ListSelectionListener() {
            	public void valueChanged(ListSelectionEvent e) {
            		setSymbol((ISymbol) jListSymbols.getSelectedValue());
            		updateOptionsPanel();
            	}
            });
            ListCellRenderer renderer = new ListCellRenderer() {
        		private Color mySelectedBGColor = new Color(255,145,100,255);
    			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
    				ISymbol sym = (ISymbol) value;
    				JPanel pnl = new JPanel();
    				BoxLayout layout = new BoxLayout(pnl, BoxLayout.Y_AXIS);
    				pnl.setLayout(layout);
    				Color bgColor = (isSelected) ? mySelectedBGColor
    							 : getJListSymbols().getBackground();

    				pnl.setBackground(bgColor);
    				SymbolPreview sp = new SymbolPreview();
    				sp.setAlignmentX(Component.CENTER_ALIGNMENT);
    				sp.setPreferredSize(new Dimension(50, 50));
    				sp.setSymbol(sym);
    				sp.setBackground(bgColor);
    				pnl.add(sp);
    				JLabel lbl = new JLabel(sym.getDescription());
    				lbl.setBackground(bgColor);
    				lbl.setAlignmentX(Component.CENTER_ALIGNMENT);
    				pnl.add(lbl);

    				return pnl;
    			}

        	};
        	jListSymbols.setCellRenderer(renderer);
    	}
    	return jListSymbols;
    }

    protected void updateOptionsPanel() {
    	ISymbol sym = ((ISymbol) jListSymbols.getSelectedValue());
    	if (sym == null)
    		return;

		XMLEntity xml = sym.getXMLEntity();

		Color c = null;
    	if (xml.contains("color")) {
    		c = StringUtilities.string2Color(xml.getStringProperty("color"));
    	}
		jcc1.setColor(c);

		if (jcc2 == null)
			return;
		if (xml.contains("outline")) {
    		c = StringUtilities.string2Color(xml.getStringProperty("outline"));
    		jcc2.setEnabled(true);
    		jcc2.setColor(c);
    	} else {
    		jcc2.setEnabled(false);
    	}


	}

	/**
     * This method initializes jPanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getJEastPanel() {
    	if (jPanel == null) {
    		jPanel = new JPanel();
    		jPanel.setLayout(new BorderLayout());
    		jPanel.add(getJPanelOptions(), BorderLayout.CENTER);
    		JPanel aux = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
    		aux.setBorder(BorderFactory.createTitledBorder(null, PluginServices.getText(this, "preview")));
    		aux.add(getJPanelPreview());
    		jPanel.add(aux, BorderLayout.NORTH);

    		aux = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
    		aux.setBorder(BorderFactory.createTitledBorder(null, PluginServices.getText(this, "options")));
    		aux.add(getJPanelOptions());
    		jPanel.add(aux, BorderLayout.CENTER);
    		aux = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));
    		aux.add(getJPanelButtons());
    		jPanel.add(aux, BorderLayout.SOUTH);
    	}
    	return jPanel;
    }

    private JPanel getJPanelButtons() {
		if (jPanelButtons == null) {
			jPanelButtons = new JPanel();
			GridLayout layout = new GridLayout();
			layout.setColumns(1);
			layout.setVgap(5);
			jPanelButtons.add(getBtnSaveSymbol());
			jPanelButtons.add(getBtnResetSymbol());
			jPanelButtons.add(getBtnProperties());

			// do not add components bellow this line!
			layout.setRows(jPanelButtons.getComponentCount());
			jPanelButtons.setLayout(layout);
		}
		return jPanelButtons;
	}

	private JButton getBtnResetSymbol() {
		if (btnResetSymbol == null) {
			btnResetSymbol = new JButton();
			btnResetSymbol.setName("btnResetSymbol");
			btnResetSymbol.setText(PluginServices.getText(this, "reset"));
			btnResetSymbol.addActionListener(buttonListener);
		}
		return btnResetSymbol;
	}

	private JButton getBtnSaveSymbol() {
		if (btnSaveSymbol == null) {
			btnSaveSymbol = new JButton();
			btnSaveSymbol.setName("btnSaveSymbol");
			btnSaveSymbol.setText(PluginServices.getText(this, "save"));
			btnSaveSymbol.addActionListener(buttonListener);
		}
		return btnSaveSymbol;
	}

	private JButton getBtnProperties() {
		if (btnProperties == null) {
			btnProperties = new JButton();
			btnProperties.setName("btnProperties");
			btnProperties.setText(PluginServices.getText(this, "properties"));
			btnProperties.addActionListener(buttonListener);
		}
		return btnProperties;
	}

	/**
     * This method initializes jScrollPane
     *
     * @return javax.swing.JScrollPane
     */
    private JScrollPane getJScrollPane() {
    	if (jScrollPane == null) {
    		jScrollPane = new JScrollPane();
    		jScrollPane.setPreferredSize(new java.awt.Dimension(80,130));
    		jScrollPane.setHorizontalScrollBarPolicy(javax.swing.JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    		jScrollPane.setViewportView(getJListFav());
    	}
    	return jScrollPane;
    }

    /**
     * This method initializes jScrollPane1
     *
     * @return javax.swing.JScrollPane
     */
    private JScrollPane getJScrollPane1() {
    	if (jScrollPane1 == null) {
    		jScrollPane1 = new JScrollPane();
    		jScrollPane1.setViewportView(getJListSymbols());
    	}
    	return jScrollPane1;
    }

    /**
     * This method initializes jPanel1
     *
     * @return javax.swing.JPanel
     */
    private SymbolPreview getJPanelPreview() {
    	if (jPanelPreview == null) {
    		jPanelPreview = new SymbolPreview();
    		jPanelPreview.setPreferredSize(new java.awt.Dimension(100,100));
    		jPanelPreview.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    	}
    	return jPanelPreview;
    }

    /**
     * This method initializes jPanel1
     *
     * @return javax.swing.JPanel
     */
    private JPanel getJPanelOptions() {
    	if (jPanelOptions == null) {
    		jPanelOptions = new GridBagLayoutPanel();

    		jcc1 = new ColorChooserPanel();
    		jcc2 = new ColorChooserPanel();
    		jcc1.setAlpha(255);
    		jcc2.setAlpha(255);
    		jcc1.addActionListener(buttonListener);
    		jcc2.addActionListener(buttonListener);
    		if (shapeType == FShape.POINT) {
    			jPanelOptions.addComponent(
    					PluginServices.getText(this, "color")+":", jcc1);
    			jPanelOptions.addComponent(
    					PluginServices.getText(this, "size")+":",
    					txtSize = new JNumberField());
    			jPanelOptions.addComponent(
    					PluginServices.getText(this, "angle")+":",
    					txtAngle = new JDecimalField());
    		} else if (shapeType == FShape.LINE) {
    			jPanelOptions.addComponent(
    					PluginServices.getText(this, "color")+":", jcc1);
    			jPanelOptions.addComponent(
    					PluginServices.getText(this, "width")+":",
    					txtSize = new JNumberField());
    			jPanelOptions.addComponent(new JLabel("AQUI VA EL COMBO DE PATTERN DE LINEA"));
    		} else if (shapeType == FShape.POLYGON) {
    			jPanelOptions.addComponent(
    					PluginServices.getText(this, "fill_color")+":", jcc1);
    			jPanelOptions.addComponent(
    					PluginServices.getText(this, "outline_color")+":", jcc2);
    			jPanelOptions.addComponent(
    					new JLabel("AQUI VA EL ANCHO DEL CONTORNO"));
    		}
    	}
    	return jPanelOptions;
    }

	public WindowInfo getWindowInfo() {
		if (wi == null) {
			wi = new WindowInfo(WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE);
			wi.setWidth(706);
			wi.setHeight(500);
			wi.setTitle(PluginServices.getText(this, "symbol_selector"));
		}
		return wi;
	}

	/**
	 * This method initializes jSplitPane
	 *
	 * @return javax.swing.JSplitPane
	 */
	private JSplitPane getJSplitPane() {
		if (jSplitPane == null) {
			jSplitPane = new JSplitPane();
			jSplitPane.setDividerLocation(200);
			jSplitPane.setResizeWeight(0.4);
	        jSplitPane.setLeftComponent(getJScrollPane());
	        jSplitPane.setRightComponent(getJScrollPane1());
		}
		return jSplitPane;
	}

	/**
	 * Returns the symbol selected by the user or null if none
	 * @return
	 */
	public ISymbol getSymbol() {
		return selectedSymbol;
	}

	private void setSymbol(ISymbol symbol) {
		selectedSymbol = symbol;
		jPanelPreview.setSymbol(symbol);
	}


	private class MyButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (selectedSymbol ==null)
				return;

			JComponent comp = (JComponent) e.getSource();
			XMLEntity xml = selectedSymbol.getXMLEntity();

			if ( comp.equals(getBtnProperties()) ) {
				// properties pressed
				SymbolEditor se = new SymbolEditor(selectedSymbol, shapeType);
				PluginServices.getMDIManager().addWindow(se);
				setSymbol(se.getSymbol());
			} else if ( comp.equals(getBtnResetSymbol()) ) {
				// reset pressed
				setSymbol(null);
			} else if ( comp.equals(getBtnSaveSymbol()) ) {
				// save pressed
				JFileChooser jfc = new JFileChooser(SymbolSelectorListModel.SYMBOL_DIRECTORY);
				javax.swing.filechooser.FileFilter ff = new javax.swing.filechooser.FileFilter() {
					public boolean accept(File f) {
						return f.getAbsolutePath().
						toLowerCase().
						endsWith(SymbolSelectorListModel.
								SYMBOL_FILE_EXTENSION);
					}

					public String getDescription() {
						return PluginServices.getText(
								this, "gvSIG_symbol_definition_file")+ " (*.sym)";
					}
				};
				jfc.setFileFilter(ff);
				if (jfc.showSaveDialog(SymbolSelector.this) == JFileChooser.APPROVE_OPTION) {
					File targetFile = jfc.getSelectedFile();
					String fExtension = SymbolSelectorListModel.SYMBOL_FILE_EXTENSION;
					if (!targetFile.
							getAbsolutePath().
							toLowerCase().
							endsWith(fExtension))
						targetFile = new File(targetFile.getAbsolutePath() + fExtension);
					FileWriter writer;
					try {
						writer = new FileWriter(targetFile.getAbsolutePath());
						Marshaller m = new Marshaller(writer);
						m.setEncoding("ISO-8859-1");
						m.marshal(xml.getXmlTag());

					} catch (Exception ex) {
						NotificationManager.addError(
								PluginServices.getText(this, "save_error"), ex);
					}
				}
			} else if (comp.equals(jcc1) && xml != null) {
				xml.putProperty("color", StringUtilities.color2String(jcc1.getColor()));
				selectedSymbol.setXMLEntity(xml);
				setSymbol(selectedSymbol);
			} else if (comp.equals(jcc2) && xml != null) {
				xml.putProperty("outline", StringUtilities.color2String(jcc2.getColor()));
				selectedSymbol.setXMLEntity(xml);
				setSymbol(selectedSymbol);
			}

			SymbolSelector.this.repaint();
		}
	}
}  //  @jve:decl-index=0:visual-constraint="10,10"
