/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: SymbolSelectorListModel.java 8537 2006-11-06 16:06:52Z jaume $
* $Log$
* Revision 1.6  2006-11-06 16:06:52  jaume
* *** empty log message ***
*
* Revision 1.5  2006/11/06 07:33:54  jaume
* javadoc, source style
*
* Revision 1.4  2006/11/02 17:19:28  jaume
* *** empty log message ***
*
* Revision 1.3  2006/10/30 19:30:35  jaume
* *** empty log message ***
*
* Revision 1.2  2006/10/26 16:31:21  jaume
* GUI
*
* Revision 1.1  2006/10/25 10:50:41  jaume
* movement of classes and gui stuff
*
* Revision 1.2  2006/10/24 22:26:18  jaume
* *** empty log message ***
*
* Revision 1.1  2006/10/24 16:31:12  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.gvsig.gui.styling;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.event.ListDataListener;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import com.iver.andami.messages.NotificationManager;
import com.iver.cit.gvsig.fmap.core.ISymbol;
import com.iver.cit.gvsig.fmap.core.symbols.SymbolFactory;
import com.iver.utiles.XMLEntity;
import com.iver.utiles.listManager.ListModel;
import com.iver.utiles.xmlEntity.generate.XmlTag;
// TODO remove org.castor.exolab....jar from the classpath when moving this to appgvSIG
public class SymbolSelectorListModel implements ListModel {
	public static File SYMBOL_DIRECTORY;
	public static final String SYMBOL_FILE_EXTENSION = ".sym";

	private static final FileFilter filter = new FileFilter() {
		public boolean accept(File pathname) {
			return pathname.getAbsolutePath().toLowerCase().endsWith(SYMBOL_FILE_EXTENSION);
		}
	};

	private Vector symbols;
	private ArrayList listeners;

	public SymbolSelectorListModel(File file) {
		SYMBOL_DIRECTORY = file;
	}

	public Object remove(int i) throws ArrayIndexOutOfBoundsException {
		return symbols.remove(i);
	}

	public void insertAt(int i, Object o) {
		getObjects().insertElementAt(o, i);
	}

	public void add(Object o) {
		TreeSet map = new TreeSet(new Comparator() {

			public int compare(Object o1, Object o2) {
				ISymbol sym1 = (ISymbol) o1;
				ISymbol sym2 = (ISymbol) o2;
				int result = sym1.getDescription().compareTo(sym2.getDescription());
				return (result!=0) ? result: 1; /* this will allow adding symbols with
												   the same value for description than
												   a previous one. */
			}

		});

		map.addAll(symbols);
		map.add(o);
		symbols = new Vector(map);

	}

	public Vector getObjects() {
		if (symbols == null) {
			symbols = new Vector();

			File[] ff = SYMBOL_DIRECTORY.listFiles(filter);
			for (int i = 0; i < ff.length; i++) {

				XMLEntity xml;
				try {
					xml = new XMLEntity((XmlTag) XmlTag.unmarshal(new FileReader(ff[i])));
					ISymbol sym = SymbolFactory.createFromXML(xml, ff[i].getName());
					add(sym);
				} catch (MarshalException e) {
					NotificationManager.
						addWarning("Error in file ["+ff[i].getAbsolutePath()+"]. " +
								"File corrupted! Skiping it...", e);
				} catch (ValidationException e) {
					NotificationManager.
						addWarning("Error validating symbol file ["+ff[i].getAbsolutePath()+"].", e);
				} catch (FileNotFoundException e) {
					// unreachable code, but anyway...
					NotificationManager.
						addWarning("File not found: "+ ff[i].getAbsolutePath(), e);
				}

			}
		}
		return symbols;
	}

	public int getSize() {
		return getObjects().size();
	}

	public Object getElementAt(int index) {
		return getObjects().get(index);
	}

	public void addListDataListener(ListDataListener l) {
		if (listeners == null)
			listeners = new ArrayList();
		listeners.add(l);
	}

	public void removeListDataListener(ListDataListener l) {
		if (listeners!=null)
			listeners.remove(l);
	}

}

