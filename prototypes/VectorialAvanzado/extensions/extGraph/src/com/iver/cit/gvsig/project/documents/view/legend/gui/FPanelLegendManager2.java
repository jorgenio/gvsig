package com.iver.cit.gvsig.project.documents.view.legend.gui;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.XMLException;
import com.iver.cit.gvsig.fmap.layers.layerOperations.ClassifiableVectorial;
import com.iver.cit.gvsig.fmap.rendering.IntervalLegend;
import com.iver.cit.gvsig.fmap.rendering.Legend;
import com.iver.cit.gvsig.fmap.rendering.SingleSymbolLegend;
import com.iver.cit.gvsig.fmap.rendering.UniqueValueLegend;
import com.iver.cit.gvsig.fmap.rendering.VectorialIntervalLegend;
import com.iver.cit.gvsig.fmap.rendering.VectorialUniqueValueLegend;
/**
 *
 * @author jmorell
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FPanelLegendManager2 extends AbstractPanel {

 private ImageIcon iuniqueValues= new ImageIcon(this.getClass().getClassLoader()
		 	.getResource("images/ValoresUnicos.png"));
	private ImageIcon iintervalValues= new ImageIcon(this.getClass().getClassLoader()
			.getResource("images/Intervalos.png"));
//	private ImageIcon iuniqueValues= new ImageIcon("images/ValoresUnicos.png");
//	private ImageIcon iintervalValues= new ImageIcon("images/Intervalos.png");
	private MapContext mapContext;
	private FLayer layer;
    private Legend renderer; // Le asignaremos la leyenda del primer tema activo.
    private FPanelLegendDefault m_defaultLegendPanel;
    private FPanelLegendValues m_valuesLegendPanel;
    private PanelLegendBreaks m_breaksLegendPanel;
    private FPanelLegendLabels m_labelsLegendPanel;
    private PanelLegendDotDensity dotDensityLegendPanel;
    private JPanel cards;
    private ComandosListener m_actionListener = new ComandosListener(this);
    private int visibleCard;
    private final String oneSymHelp = PluginServices.getText(this,"Muestra_todos_los_elementos_de_una_capa_usando_el_mismo_simbolo");
    private final String oneValHelp = PluginServices.getText(this,"Dado_un_campo_de_atributos") + "," + PluginServices.getText(this,"muestra_los_elementos_de_la_capa_usando_un_simbolo_por_cada_valor_unico") + ".";
    private final String intervalsHelp = PluginServices.getText(this,"Muestra_los_elementos_de_la_capa_usando_una_gama_de_colores_en_funcion_del_valor_de_un_determinado_campo_de_atributos") + ".";
    private final String labelsHelp = PluginServices.getText(this,"Permite_etiquetar_los_elementos_del_mapa_con_el_valor_de_un_determinado_campo") + ".";
    private final String dotDensityHelp = PluginServices.getText(this,"Defines_a_dot_density_symbol_based_on_a_field_value") + ".";

	private JPanel topPanel = null;
	private JLabel jLabel = null;
	private JComboBox jComboBox = null;
	private JButton jButton = null;
	private JButton jButton1 = null;
	private JCheckBox jCheckBox = null;
	private JPanel jPanel = null;
	private JTextArea titleTextArea = null;

	private JPanel jPanel1 = null;

	private JRadioButton symbolButton;
	private JRadioButton valuesButton;
	private JRadioButton intervalsButton;
	private JRadioButton labelsButton;

	private JScrollPane jScrollPane = null;
	private JRadioButton dotDensityButton = null;
	public FPanelLegendManager2() {
		initialize();
		this.setSize(500, 360);
	}

    private void initialize() {
		visibleCard=0;
        JPanel selector = new JPanel();
        selector.setLayout(null);

        symbolButton = new JRadioButton();
        symbolButton.setText(PluginServices.getText(this,"Simbolo_unico"));
        symbolButton.setActionCommand("SYMBOL_CHOICE");
        symbolButton.addActionListener(m_actionListener);

        valuesButton = new JRadioButton();
        valuesButton.setText(PluginServices.getText(this,"Valores_unicos"));
        valuesButton.setActionCommand("VALUES_CHOICE");
        valuesButton.addActionListener(m_actionListener);

        intervalsButton = new JRadioButton();
        intervalsButton.setText(PluginServices.getText(this,"Intervalos"));
        intervalsButton.setActionCommand("BREAKS_CHOICE");
        intervalsButton.addActionListener(m_actionListener);

		labelsButton = new JRadioButton();
		labelsButton.setText(PluginServices.getText(this,"Etiquetados"));
		labelsButton.setActionCommand("LABELS_CHOICE");
		labelsButton.addActionListener(m_actionListener);

		dotDensityButton = new JRadioButton();
		dotDensityButton.setText(PluginServices.getText(this, "dot_density"));
		dotDensityButton.setActionCommand("DOT_DENSITY_CHOICE");
		dotDensityButton.addActionListener(m_actionListener);

		ButtonGroup jButtonGroup = new ButtonGroup();
		jButtonGroup.add(symbolButton);
		jButtonGroup.add(valuesButton);
		jButtonGroup.add(intervalsButton);
		jButtonGroup.add(labelsButton);
		jButtonGroup.add(dotDensityButton);


    	//Create the cards.
        m_defaultLegendPanel = new FPanelLegendDefault();
        m_valuesLegendPanel = new FPanelLegendValues();
        m_breaksLegendPanel = new PanelLegendBreaks();
        m_labelsLegendPanel = new FPanelLegendLabels(this);
        dotDensityLegendPanel = new PanelLegendDotDensity();

        //Create the panel that contains the cards.
        cards = new JPanel(new CardLayout());
        this.setLayout(null);
        selector.setBounds(5, 41, 134, 197);
        selector.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        selector.setBackground(java.awt.SystemColor.text);
        cards.setBounds(143, 88, 502, 270);
        cards.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null));
        symbolButton.setBounds(5, 5, 131, 23);
        symbolButton.setSelected(true);
        symbolButton.setBackground(java.awt.SystemColor.text);
        valuesButton.setBounds(5, 28, 131, 23);
        valuesButton.setBackground(java.awt.SystemColor.text);
        intervalsButton.setBounds(5, 51, 131, 23);
        intervalsButton.setBackground(java.awt.SystemColor.text);
        labelsButton.setBounds(5, 74, 131, 23);
        labelsButton.setBackground(java.awt.SystemColor.text);
        dotDensityButton.setBounds(5,97,131,21);
		dotDensityButton.setBackground(java.awt.SystemColor.text);
        this.setSize(649, 377);
        cards.add(m_defaultLegendPanel, "symbol");
        this.add(selector, null);
        selector.add(symbolButton, null);
        cards.add(m_valuesLegendPanel, "values");
        this.add(cards, null);
        selector.add(valuesButton, null);
        cards.add(m_breaksLegendPanel, "breaks");
        selector.add(intervalsButton, null);
        this.add(getTopPanel(), null);
        cards.add(m_labelsLegendPanel, "labels");
        selector.add(labelsButton, null);
        cards.add(dotDensityLegendPanel, "dot_density");
        selector.add(dotDensityButton, null);
        this.add(getJPanel(), null);
        this.add(getPreviewPanel(), null);
    }



    /**
     * En esta funci�n se coge el primer tema activo del TOC y se usa su
     * renderer para inicializar. Hay que llamarla SIEMPRE que se abra el
     * cuadro de di�logo, en el  openLegendManager() de la vista.
     *
     * @param mapContext TOC que abre este cuadro de di�logo.
     * @throws DriverException
     */
    public void setMapContext(MapContext mapContext) throws DriverException {
        this.mapContext = mapContext;
        layer = getFirstActiveLayerVect(mapContext.getLayers());
		try {
			ClassifiableVectorial aux = (ClassifiableVectorial) layer;
			renderer = aux.getLegend().cloneLegend();
		} catch (XMLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        actualizar();
    }

    /**
     * DOCUMENT ME!
     */
    public void actualizar() {
    	FLayer lyrSelected = getFirstActiveLayerVect(mapContext.getLayers());

        m_defaultLegendPanel.setLayer(lyrSelected, renderer);
        m_valuesLegendPanel.setLayer(lyrSelected, renderer);
        m_breaksLegendPanel.setLayer(lyrSelected, renderer);
        m_labelsLegendPanel.setLayer(lyrSelected, renderer);
        dotDensityLegendPanel.setLayer(lyrSelected, renderer);
        CardLayout cl = (CardLayout)(cards.getLayout());
        if (renderer instanceof SingleSymbolLegend) {
        	titleTextArea.setText(oneSymHelp);
        	getPreviewPanel().removeAll();
    		getPreviewPanel().add(m_defaultLegendPanel.getM_previewSymbol());
    		getPreviewPanel().repaint();
        	symbolButton.setSelected(true);
            getJComboBox().getModel().setSelectedItem(layer.getName() + " (" + PluginServices.getText(this,"Simbolo_unico") + ")");
            cl.show(cards, "symbol");
            setVisibleCard(0);
        }
        if (renderer instanceof UniqueValueLegend) {
        	titleTextArea.setText(oneValHelp);
        	getPreviewPanel().removeAll();
        	getPreviewPanel().repaint();
        	valuesButton.setSelected(true);
            getJComboBox().getModel().setSelectedItem(layer.getName() + " (" + PluginServices.getText(this,"Valores_unicos") + ")");
            cl.show(cards, "values");
            setVisibleCard(1);
        }
        if (renderer instanceof IntervalLegend) {
        	titleTextArea.setText(intervalsHelp);
        	getPreviewPanel().removeAll();
    		getPreviewPanel().repaint();
        	intervalsButton.setSelected(true);
            getJComboBox().getModel().setSelectedItem(layer.getName() + " (" + PluginServices.getText(this,"Intervalos") + ")");
            cl.show(cards, "breaks");
            setVisibleCard(2);
        }

    }

	public void setLayer(FLayer lyr, Legend r) {
		layer = lyr;
		if (r instanceof SingleSymbolLegend) {
			getJComboBox().getModel().setSelectedItem(layer.getName() + " (" + PluginServices.getText(this,"Simbolo_unico") + ")");
			renderer = (SingleSymbolLegend)r;
			getPreviewPanel().add(m_defaultLegendPanel.getM_previewSymbol());
            CardLayout cl = (CardLayout)(cards.getLayout());
            cl.show(cards, "symbol");
            setVisibleCard(0);
		} else if (r instanceof VectorialUniqueValueLegend) {
            getJComboBox().getModel().setSelectedItem(layer.getName() + " (" + PluginServices.getText(this,"Valores_unicos") + ")");
			renderer = (VectorialUniqueValueLegend)r;
            CardLayout cl = (CardLayout)(cards.getLayout());
            cl.show(cards, "values");
            setVisibleCard(1);
		} else if (r instanceof VectorialIntervalLegend) {
            getJComboBox().getModel().setSelectedItem(layer.getName() + " (" + PluginServices.getText(this,"Intervalos") + ")");
			renderer = (VectorialIntervalLegend)r;
            CardLayout cl = (CardLayout)(cards.getLayout());
            cl.show(cards, "breaks");
            setVisibleCard(2);
		} else {
            getJComboBox().getModel().setSelectedItem(layer.getName() + " (" + PluginServices.getText(this,"Etiquetas_estandar") + ")");
			renderer = (SingleSymbolLegend)r;
            CardLayout cl = (CardLayout)(cards.getLayout());
            cl.show(cards, "labels");
            setVisibleCard(3);
		}
	}

    private class ComandosListener implements ActionListener {


	//   private FPanelLegendManager fPanSimMan;

        public ComandosListener(FPanelLegendManager2 fPanSimMan) {
        //	this.fPanSimMan = fPanSimMan;
        }

        public void actionPerformed(ActionEvent e) {
        	getPreviewPanel().removeAll();

            if (e.getActionCommand() == "SYMBOL_CHOICE") {
            	titleTextArea.setText(oneSymHelp);
        		getPreviewPanel().add(m_defaultLegendPanel.getM_previewSymbol());
        	    CardLayout cl = (CardLayout)(cards.getLayout());
                cl.show(cards, "symbol");
                visibleCard=0;
            } else if (e.getActionCommand() == "VALUES_CHOICE") {
            	titleTextArea.setText(oneValHelp);
        		getPreviewPanel().add(new JLabel(iuniqueValues));
        	    CardLayout cl = (CardLayout)(cards.getLayout());
                cl.show(cards, "values");
                visibleCard=1;
            } else if (e.getActionCommand() == "BREAKS_CHOICE") {
            	titleTextArea.setText(intervalsHelp);
        		getPreviewPanel().add(new JLabel(iintervalValues));
        	    CardLayout cl = (CardLayout)(cards.getLayout());
                cl.show(cards, "breaks");
                visibleCard=2;
            } else if (e.getActionCommand() == "LABELS_CHOICE") {
            	titleTextArea.setText(labelsHelp);
        		getPreviewPanel().add(m_labelsLegendPanel.getFPreviewSymbol());
        	    CardLayout cl = (CardLayout)(cards.getLayout());
                cl.show(cards, "labels");
                visibleCard=3;
            } else if (e.getActionCommand() == "DOT_DENSITY_CHOICE") {
            	titleTextArea.setText(dotDensityHelp);
//    TODO set a preview        	getPreviewPanel().add(dotDensityLegendPanel.getFPreviewSymbol());
        	    CardLayout c1 = (CardLayout) (cards.getLayout());
            	c1.show(cards, "dot_density");
            	visibleCard=4;
            } else {}
        	getPreviewPanel().repaint();

        }
    }

	public int getVisibleCard() {
		return visibleCard;
	}

	/**
	 * This method initializes jPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getTopPanel() {
		if (topPanel == null) {
			jLabel = new JLabel();
			topPanel = new JPanel();
			topPanel.setLayout(null);
			topPanel.setBounds(5, 5, 638, 31);
			topPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null));
			jLabel.setBounds(6, 4, 68, 22);
			jLabel.setText(PluginServices.getText(this,"Leyenda") + ":");
			topPanel.add(jLabel, null);
			topPanel.add(getJComboBox(), null);
			topPanel.add(getJButton(), null);
			topPanel.add(getJButton1(), null);
			topPanel.add(getJCheckBox(), null);
		}
		return topPanel;
	}
	/**
	 * This method initializes jComboBox
	 *
	 * @return javax.swing.JComboBox
	 */
	public JComboBox getJComboBox() {
		if (jComboBox == null) {
			jComboBox = new JComboBox();
			jComboBox.setBounds(78, 5, 250, 22);
		}
		return jComboBox;
	}
	/**
	 * This method initializes jButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButton() {
		if (jButton == null) {
			jButton = new JButton();
			jButton.setBounds(345, 4, 22, 22);
			jButton.setEnabled(false);
		}
		return jButton;
	}
	/**
	 * This method initializes jButton1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setBounds(371, 4, 22, 22);
			jButton1.setEnabled(false);
		}
		return jButton1;
	}
	/**
	 * This method initializes jCheckBox
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getJCheckBox() {
		if (jCheckBox == null) {
			jCheckBox = new JCheckBox();
			jCheckBox.setBounds(417, 4, 202, 22);
			jCheckBox.setText(PluginServices.getText(this,"Leyenda_Por_Defecto"));
			jCheckBox.setEnabled(false);
		}
		return jCheckBox;
	}
	/**
	 * This method initializes jPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJPanel() {
		if (jPanel == null) {
			jPanel = new JPanel();
			jPanel.setLayout(null);
			jPanel.setBounds(143, 39, 501, 46);
			jPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
			jPanel.add(getJScrollPane(), null);
		}
		return jPanel;
	}
	/**
	 * This method initializes jTextArea
	 *
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getJTextArea() {
		if (titleTextArea == null) {
			titleTextArea = new JTextArea();
			titleTextArea.setText(oneSymHelp);
			titleTextArea.setBackground(java.awt.SystemColor.control);
			titleTextArea.setLineWrap(true);
			titleTextArea.setRows(0);
			titleTextArea.setWrapStyleWord(false);
			titleTextArea.setEditable(false);
			titleTextArea.setPreferredSize(new java.awt.Dimension(495,40));
		}
		return titleTextArea;
	}
	/**
	 * This method initializes jPanel1
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getPreviewPanel() {
		if (jPanel1 == null) {
			jPanel1 = new JPanel();
			jPanel1.setBounds(6, 242, 132, 117);
			jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
			jPanel1.setBackground(java.awt.SystemColor.text);
		}
		return jPanel1;
	}
	/**
	 * @return Returns the m_breaksLegendPanel.
	 */
	public PanelLegendBreaks getM_breaksLegendPanel() {
		return m_breaksLegendPanel;
	}
	/**
	 * @param legendPanel The m_breaksLegendPanel to set.
	 */
	public void setM_breaksLegendPanel(PanelLegendBreaks legendPanel) {
		m_breaksLegendPanel = legendPanel;
	}
	/**
	 * @return Returns the m_defaultLegendPanel.
	 */
	public FPanelLegendDefault getM_defaultLegendPanel() {
		return m_defaultLegendPanel;
	}
	/**
	 * @param legendPanel The m_defaultLegendPanel to set.
	 */
	public void setM_defaultLegendPanel(FPanelLegendDefault legendPanel) {
		m_defaultLegendPanel = legendPanel;
	}
	/**
	 * @return Returns the m_labelsLegendPanel.
	 */
	public FPanelLegendLabels getM_labelsLegendPanel() {
		return m_labelsLegendPanel;
	}
	/**
	 * @param legendPanel The m_labelsLegendPanel to set.
	 */
	public void setM_labelsLegendPanel(FPanelLegendLabels legendPanel) {
		m_labelsLegendPanel = legendPanel;
	}
	/**
	 * @return Returns the m_valuesLegendPanel.
	 */
	public FPanelLegendValues getM_valuesLegendPanel() {
		return m_valuesLegendPanel;
	}

	public PanelLegendDotDensity getPanelDotDensityLegend() {
		return dotDensityLegendPanel;
	}
	/**
	 * @param legendPanel The m_valuesLegendPanel to set.
	 */
	public void setM_valuesLegendPanel(FPanelLegendValues legendPanel) {
		m_valuesLegendPanel = legendPanel;
	}
	/**
	 * @return Returns the renderer.
	 */
	public Legend getRenderer() {
		return renderer;
	}
	/**
	 * @param renderer The renderer to set.
	 */
	public void setRenderer(Legend renderer) {
		this.renderer = renderer;
	}
	/**
	 * @param visibleCard The visibleCard to set.
	 */
	public void setVisibleCard(int visibleCard) {
		this.visibleCard = visibleCard;
	}
	/**
	 * This method initializes jScrollPane
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setBounds(2, 2, 498, 42);
			jScrollPane.setViewportView(getJTextArea());
		}
		return jScrollPane;
	}

}  //  @jve:decl-index=0:visual-constraint="26,17"