package com.iver.cit.gvsig.project.documents.view.legend.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.ValidationException;

import com.hardcode.gdbms.engine.data.DataSource;
import com.hardcode.gdbms.engine.instruction.FieldNotFoundException;
import com.iver.andami.PluginServices;
import com.iver.andami.messages.NotificationManager;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.MapContext;
import com.iver.cit.gvsig.fmap.drivers.DriverIOException;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLayers;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.XMLException;
import com.iver.cit.gvsig.fmap.layers.layerOperations.AlphanumericData;
import com.iver.cit.gvsig.fmap.layers.layerOperations.ClassifiableVectorial;
import com.iver.cit.gvsig.fmap.rendering.ClassifiedLegend;
import com.iver.cit.gvsig.fmap.rendering.Legend;
import com.iver.cit.gvsig.fmap.rendering.LegendFactory;
import com.iver.cit.gvsig.fmap.rendering.SingleSymbolLegend;
import com.iver.cit.gvsig.fmap.rendering.VectorialLegend;
import com.iver.cit.gvsig.gui.panels.LinkPanel;
import com.iver.cit.gvsig.project.documents.view.IProjectView;
import com.iver.cit.gvsig.project.documents.view.legend.CreateSpatialIndexMonitorableTask;
import com.iver.cit.gvsig.project.documents.view.legend.scale.gui.FPanelScaleManager;
import com.iver.utiles.GenericFileFilter;
import com.iver.utiles.XMLEntity;
import com.iver.utiles.swing.threads.IMonitorableTask;
import com.iver.utiles.xmlEntity.generate.XmlTag;

/**
 * @author jmorell (jose.morell@gmail.com)
 * @version 24-ene-2005
 */
public final class FThemeManagerWindow2 extends AbstractPanel implements IWindow {
	private MapContext mapContext;
	private FLayer layer;
    private Legend renderer; // Le asignaremos la leyenda del primer tema activo.
	private FPanelScaleManager fPanelScaleManager;
    private FPanelLegendManager2 fPanelLegendManager;
    private LinkPanel linkPanel;
    private JTabbedPane organizador = new JTabbedPane();  //  @jve:decl-index=0:
    private ComandosListener m_actionListener = new ComandosListener(this);
	//private ProjectView view;
	private NumberFormat nf = NumberFormat.getInstance();

    /**
     * Creates new form JPanel
     */
    public FThemeManagerWindow2() {
        initialize();
		this.setSize(650, 420);
    }

    /**
     * Sobrecarga del constructor. Este se utiliza cuando se llama a este
     * di�logo desde la barra de comandos.
     *
     */
    public FThemeManagerWindow2(FLayer l, MapContext fMap) {
    	initialize();
    	layer = l;
    	mapContext = fMap;
		try {
			ClassifiableVectorial aux = (ClassifiableVectorial) layer;
			renderer = (Legend) aux.getLegend().cloneLegend();
			fPanelScaleManager.setMapContext(mapContext);
			fPanelLegendManager.setMapContext(mapContext);
		} catch (XMLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DriverException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        fPanelScaleManager.setLayer(layer, renderer);
        fPanelLegendManager.setLayer(layer, renderer);

        organizador.setSelectedIndex(0);

		this.setSize(650, 420);
    }

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private  void initialize() {

		fPanelScaleManager = new FPanelScaleManager();
		fPanelLegendManager = new FPanelLegendManager2();

		organizador.addTab(PluginServices.getText(this,"General"), fPanelScaleManager);
		organizador.addTab(PluginServices.getText(this,"Simbologia"), fPanelLegendManager);

        JPanel panelButtons = new JPanel();

        JButton btnAceptar = new JButton(PluginServices.getText(this,"Aceptar"));
        btnAceptar.setActionCommand("OK");
        btnAceptar.addActionListener(m_actionListener);

        JButton btnApply = new JButton(PluginServices.getText(this,"Aplicar"));
        btnApply.setActionCommand("APPLY");
        btnApply.addActionListener(m_actionListener);

        JButton btnSaveLegend = new JButton(PluginServices.getText(this,"Guardar_leyenda")+"...");
        btnSaveLegend.setActionCommand("SAVE_LEGEND");
        btnSaveLegend.addActionListener(m_actionListener);

        JButton btnLoadLegend = new JButton(PluginServices.getText(this,"Recuperar_leyenda")+"...");
        btnLoadLegend.setActionCommand("LOAD_LEGEND");
        btnLoadLegend.addActionListener(m_actionListener);

        JButton btnCancelar = new JButton(PluginServices.getText(this,"Cerrar"));
        btnCancelar.setActionCommand("CANCEL");
        btnCancelar.addActionListener(m_actionListener);

        panelButtons.add(btnApply, null);

		setLayout(new BorderLayout());
		panelButtons.setLayout(null);
		organizador.setPreferredSize(new java.awt.Dimension(640,390));
		btnAceptar.setBounds(9, 5, 89, 23);
		btnApply.setBounds(107, 5, 89, 23);
		btnSaveLegend.setBounds(205, 5, 159, 23);
		btnLoadLegend.setBounds(373, 5, 159, 23);
		btnCancelar.setBounds(541, 5, 89, 23);
		panelButtons.setPreferredSize(new java.awt.Dimension(493,33));
		add(organizador, java.awt.BorderLayout.NORTH);
		panelButtons.add(btnAceptar, null);
		add(panelButtons, java.awt.BorderLayout.SOUTH);
		panelButtons.add(btnSaveLegend, null);
		panelButtons.add(btnLoadLegend, null);
		panelButtons.add(btnCancelar, null);
	}



    /**
     * En esta funci�n se coge el primer tema activo del TOC y se usa su
     * renderer para inicializar. Hay que llamarla SIEMPRE que se abra el
     * cuadro de di�logo, en el  openLegendManager() de la vista.
     *
     * @param mapContext TOC que abre este cuadro de di�logo.
     * @throws DriverException
     */
    public void setMapContext(MapContext mapContext) throws DriverException {
        this.mapContext = mapContext;
        layer = getFirstActiveLayerVect(mapContext.getLayers());
		try {
			ClassifiableVectorial aux = (ClassifiableVectorial) layer;
			renderer = (Legend) aux.getLegend().cloneLegend();
		} catch (XMLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fPanelScaleManager.setMapContext(mapContext);
		fPanelLegendManager.setMapContext(mapContext);
        actualizar();
    }

    /**
     * DOCUMENT ME!
     */
    private void actualizar() {
    	FLayer lyrSelected = getFirstActiveLayerVect(mapContext.getLayers());

        fPanelScaleManager.setLayer(lyrSelected, renderer);
        fPanelLegendManager.setLayer(lyrSelected, renderer);

        organizador.setSelectedIndex(0);
    }

    private void btnApply_actionPerformed(ActionEvent evt) {
       // ClassifiableVectorial aux = (ClassifiableVectorial) layer;

    	switch (fPanelLegendManager.getVisibleCard()) {
        case 0: // default symbol
            renderer = fPanelLegendManager.getM_defaultLegendPanel().getLegend();
            fPanelLegendManager.getJComboBox().getModel().setSelectedItem(layer.getName() + PluginServices.getText(this,"_(Simbolo_unico)"));
            break;
        case 1: // Unique Values
            renderer = fPanelLegendManager.getM_valuesLegendPanel().getLegend();
            fPanelLegendManager.getJComboBox().getModel().setSelectedItem(layer.getName() + "_(Valores_unicos)");
            break;
        case 2:// break values
           	renderer = fPanelLegendManager.getM_breaksLegendPanel().getLegend();
            fPanelLegendManager.getJComboBox().getModel().setSelectedItem(layer.getName() + "_(Intervalos)");
           	break;
        case 3://text
        	fPanelLegendManager.getJComboBox().getModel().setSelectedItem(layer.getName() + "_(Etiquetas_estandar)");
        	break;
        case 4://
        	renderer = fPanelLegendManager.getPanelDotDensityLegend().getLegend();
        	break;
        }
        // System.out.println("Color punto 1 " + renderer.getDefaultSymbol().getColor().toString());
    	// fPanelLegendManager.getM_labelsLegendPanel().setFSymbol(renderer.getDefaultSymbol());
    	// boolean bShapesVisibles = renderer.getDefaultSymbol().isShapeVisible();
    	fPanelLegendManager.getM_labelsLegendPanel().updateValuesFromControls((VectorialLegend)renderer);
    	// renderer.getDefaultSymbol().setShapeVisible(bShapesVisibles);
        // System.out.println("Color punto 2 " + renderer.getDefaultSymbol().getColor().toString());
        try {
			// Intentamos aplicar la leyenda a todas
            // las capas activas a las que se les pueda aplicar
            FLayer[] activeLyrs =  mapContext.getLayers().getActives();
            for (int i=0; i < activeLyrs.length; i++)
            {
            	FLayer laux=activeLyrs[i];
            	if (activeLyrs[i] instanceof FLayers){
            		laux=getFirstActiveLayerVect((FLayers)activeLyrs[i]);
            	}
                if (laux instanceof ClassifiableVectorial)
                {
                    ClassifiableVectorial aux2 = (ClassifiableVectorial) laux;
                    if (renderer instanceof ClassifiedLegend)
                    {
                        // Es una leyenda que necesita un recordset con un
                        // nombre de campo. Comprobamos que ese recordset
                        // tiene ese nombre de campo y es del tipo esperado
                        ClassifiedLegend cl = (ClassifiedLegend) renderer;
                        if (aux2 instanceof AlphanumericData)
                        {
                            DataSource rs = ((AlphanumericData) aux2).getRecordset();
                            if (cl.getValues().length==0) {
                            	JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),PluginServices.getText(this,"no_es_posible_aplicar_leyenda_vacia"));
                            	return;
                            }
                            int idField = rs.getFieldIndexByName(cl.getFieldName());
                            if (idField != -1)
                            {
                                //TODO: Aqu� deber�amos comprobar si el tipo de campo
                                // es el tipo de campo esperado.
                            	/*if (renderer instanceof VectorialUniqueValueLegend){
                            		((VectorialUniqueValueLegend)renderer).setLabelHeightField(null);
                            		((VectorialUniqueValueLegend)renderer).setLabelRotationField(null);
                            	}
                            	*/
                                aux2.setLegend((VectorialLegend) renderer);
                            }

                        }
                    }
                    else if (renderer instanceof SingleSymbolLegend)
                        aux2.setLegend((VectorialLegend) renderer);
                }

            }
			// aux.setLegend((VectorialLegend) m_Renderer);

		} catch (FieldNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (com.hardcode.gdbms.engine.data.driver.DriverException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

		if (fPanelScaleManager.getJRadioButton1().isSelected()) {
			try
			{
				layer.setMinScale((nf.parse(fPanelScaleManager.getJTextField1().getText())).doubleValue());
			}
			catch (ParseException ex)
			{
			    if (fPanelScaleManager.getJTextField1().getText().compareTo("") == 0)
			        layer.setMinScale(-1);
			    else
			        System.err.print(ex.getLocalizedMessage());
			}
			try
			{
			    layer.setMaxScale((nf.parse(fPanelScaleManager.getJTextField2().getText())).doubleValue());
			}
			catch (ParseException ex)
			{
			    if (fPanelScaleManager.getJTextField2().getText().compareTo("") == 0)
			        layer.setMaxScale(-1);
			    else
			        System.err.print(ex.getLocalizedMessage());
			}

		} else {
	        layer.setMinScale(-1);
	        layer.setMaxScale(-1);
		}
		if (!fPanelScaleManager.getLayerName().equals(layer.getName())){
			layer.setName(fPanelScaleManager.getLayerName());
		}
        if (layer instanceof FLyrVect)
        {
            FLyrVect lyrVect = (FLyrVect) layer;
            if (fPanelScaleManager.isSpatialIndexSelected())
            {
//                if (lyrVect.getSpatialIndex() == null)
            	if(lyrVect.getISpatialIndex() == null)
                {
                	//AZABALA
                	try {
						PluginServices.
							cancelableBackgroundExecution(getCreateSpatialIndexTask());
					} catch (DriverIOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DriverException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
//                    lyrVect.createSpatialIndex();
                }
            }
            //AZABALA
            /*
             * If we unselect the spatial index checkbox...Must we delete
             * spatial index file, or we only have to put Spatial Index
             * reference to null?. I have followed the second choice
             */
            else{
                lyrVect.deleteSpatialIndex();
            }


        }
		//layer.getFMap().invalidate();
		layer.getMapContext().callLegendChanged();
    }


    /**
     * Instanciates a ITask (@see com.iver.utiles.swing.threads.ITask)
     * to create a spatial index for the selected layer in background.
     * This task also allow to monitor process evolution, and to cancel
     * this process.
     * @throws DriverException
     * @throws DriverIOException
     */
    public IMonitorableTask getCreateSpatialIndexTask() throws DriverIOException,
    															DriverException{

//    	  FIXME REVISAR ESTO (Quizas lanzar TaskException)
			return new CreateSpatialIndexMonitorableTask((FLyrVect)layer);
    }

    /**
     * DOCUMENT ME!
     *
     * @author Fernando Gonz�lez Cort�s
     */
    private class ComandosListener implements ActionListener {
        private FThemeManagerWindow2 fLayerPropertiesWindow;

        /**
         * Creates a new ComandosListener object.
         *
         * @param lg DOCUMENT ME!
         */
        public ComandosListener(FThemeManagerWindow2 fLayerPropertiesWindow) {
        	this.fLayerPropertiesWindow = fLayerPropertiesWindow;
        }

        /* (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand() == "OK") {
                // Implementaci�n para hacer funcionar el hyperlink ...
                //com.iver.cit.gvsig.gui.View theView = (com.iver.cit.gvsig.gui.View)PluginServices.getMDIManager().getActiveView();
                //ProjectView view = theView.getModel();
            	if (PluginServices.getMainFrame() != null)
            	{
            		IProjectView view = fPanelScaleManager.getView();
            		view.setTypeLink(fPanelScaleManager.getLinkType());
            		if (fPanelScaleManager.getSelectedLinkField()!=null)
            		{
            			view.setSelectedField(fPanelScaleManager.getSelectedLinkField().toString().trim());
            			view.setExtLink(fPanelScaleManager.getExtensionLink());
            		}
            	}
                //
                fLayerPropertiesWindow.btnApply_actionPerformed(e);
            	if (PluginServices.getMainFrame() == null)
            		((JDialog) (getParent().getParent().getParent().getParent())).dispose();
            	else
            		PluginServices.getMDIManager().closeWindow(FThemeManagerWindow2.this);
                System.out.println("Ok");
            } else if (e.getActionCommand() == "CANCEL") {
            	if (PluginServices.getMainFrame() == null)
            		((JDialog) (getParent().getParent().getParent().getParent())).dispose();
            	else
            		PluginServices.getMDIManager().closeWindow(FThemeManagerWindow2.this);
                System.out.println("CANCEL");
            } else if (e.getActionCommand() == "APPLY") {
                fLayerPropertiesWindow.btnApply_actionPerformed(e);
                System.out.println("APPLY");
            } else if (e.getActionCommand() == "LOAD_LEGEND") {
	        	JFileChooser jfc = new JFileChooser();
	    		jfc.addChoosableFileFilter(new GenericFileFilter("gvl",
	    				PluginServices.getText(this, "tipo_de_leyenda")));
	    		//jfc.addChoosableFileFilter(new GenericFileFilter("sld","sld"));

	    		if (jfc.showOpenDialog((Component) PluginServices.getMainFrame()) == JFileChooser.APPROVE_OPTION) {
	    			File file=jfc.getSelectedFile();
	    			if (!(file.getPath().endsWith(".gvl") || file.getPath().endsWith(".GVL")))
	    			{
	    				file=new File(file.getPath()+".gvl");
	    			}
	    			readLegend(file);
	    			fPanelLegendManager.setRenderer(renderer);
	    			fPanelLegendManager.actualizar();
	    		}
            } else if (e.getActionCommand() == "SAVE_LEGEND") {
				fLayerPropertiesWindow.btnApply_actionPerformed(e);
				JFileChooser jfc = new JFileChooser();
				jfc.addChoosableFileFilter(new GenericFileFilter("sld","*.sld"));
				jfc.addChoosableFileFilter(new GenericFileFilter("gvl",
						PluginServices.getText(this, "tipo_de_leyenda")));


				if (jfc.showSaveDialog((Component) PluginServices.getMainFrame()) == JFileChooser.APPROVE_OPTION)
				{
					File file=jfc.getSelectedFile();
					if (jfc.getFileFilter().accept(new File("dummy.sld")))
					{
						if (!(file.getPath().toLowerCase().endsWith(".sld"))){
							file=new File(file.getPath()+".sld");
						}
						export2SLD(file);
					}
					else{
						if (!(file.getPath().toLowerCase().endsWith(".gvl"))){
							file=new File(file.getPath()+".gvl");
						}
						writeLegend(file);
					}
				}
            } else {}
        }
    }

    private void export2SLD(File file) {
		try {
			FileWriter writer = new FileWriter(file.getAbsolutePath());
			writer.write( renderer.getSLDString(layer.getName()));
			writer.close();

//			Marshaller m = new Marshaller(writer);
//			m.setEncoding("ISO-8859-1");
//			m.marshal(renderer.getSLDString());
		} catch (Exception e) {
			NotificationManager.addError(PluginServices.getText(this, "Error_exportando_SLD"), e);
		}
	}

    private void writeLegend(File file) {
		try {
			FileWriter writer = new FileWriter(file.getAbsolutePath());
			Marshaller m = new Marshaller(writer);
			m.setEncoding("ISO-8859-1");
			m.marshal(renderer.getXMLEntity().getXmlTag());
		} catch (Exception e) {
			NotificationManager.addError(PluginServices.getText(this, "Error_guardando_la_leyenda"), e);
		}
	}

	public void readLegend(File file) {
		try {
			File xmlFile = new File(file.getAbsolutePath());
			FileReader reader;
			reader = new FileReader(xmlFile);

			XmlTag tag = (XmlTag) XmlTag.unmarshal(reader);
			renderer = LegendFactory.createFromXML(new XMLEntity(tag));
			//fPanelLegendManager.setRenderer(LegendFactory.createFromXML(new XMLEntity(tag)));
		} catch (FileNotFoundException e) {
			NotificationManager.addError(PluginServices.getText(this, "Al_leer_la_leyenda"), e);
		} catch (MarshalException e) {
			NotificationManager.addError(PluginServices.getText(this, "Al_leer_la_leyenda"), e);
		} catch (ValidationException e) {
			NotificationManager.addError(PluginServices.getText(this, "Al_leer_la_leyenda"), e);
		} catch (XMLException e) {
			NotificationManager.addError(PluginServices.getText(this, "Al_leer_la_leyenda"), e);
		}
	}

	/* (non-Javadoc)
	 * @see com.iver.andami.ui.mdiManager.View#getViewInfo()
	 */
	public WindowInfo getWindowInfo() {
		// Solo se llama una vez
		WindowInfo viewInfo = new WindowInfo(WindowInfo.MODELESSDIALOG);
		viewInfo.setWidth(650);
		viewInfo.setHeight(420);
		viewInfo.setTitle(PluginServices.getText(this,"propiedades_de_la_capa"));
		return viewInfo;
	}

	/**
	 * @see com.iver.mdiApp.ui.MDIManager.View#viewActivated()
	 */
	/*public void viewActivated() {
	}

	public FMap getMapContext() {
		return mapContext;
	}
	public FLayer getLayer() {
		return layer;
	}
	public Legend getRenderer() {
		return renderer;
	}*/

	/**
	 * @return Returns the organizador.
	 */
	public JTabbedPane getOrganizador() {
		return organizador;
	}
	/**
	 * @param organizador The organizador to set.
	 */
	public void setOrganizador(JTabbedPane organizador) {
		this.organizador = organizador;
	}
}
