/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2005 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */

/* CVS MESSAGES:
*
* $Id: PanelLegendDotDensity.java 11295 2007-04-20 07:49:39Z caballero $
* $Log$
* Revision 1.4.2.1  2007-04-20 07:49:39  caballero
* cambio de key para el s�mbolo
*
* Revision 1.4  2006/11/17 13:53:45  cesar
* *** empty log message ***
*
* Revision 1.3  2006/11/17 12:50:36  jaume
* tama�o de punto defecto 2
*
* Revision 1.2  2006/11/15 12:57:31  jaume
* *** empty log message ***
*
* Revision 1.1  2006/11/14 11:10:27  jaume
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.project.documents.view.legend.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Types;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.gvsig.gui.beans.swing.GridBagLayoutPanel;

import com.hardcode.gdbms.engine.data.DataSource;
import com.hardcode.gdbms.engine.data.DataSourceFactory;
import com.hardcode.gdbms.engine.values.NumericValue;
import com.hardcode.gdbms.engine.values.Value;
import com.hardcode.gdbms.engine.values.ValueFactory;
import com.iver.andami.PluginServices;
import com.iver.andami.messages.NotificationManager;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.symbols.DotDensityFillSymbol;
import com.iver.cit.gvsig.fmap.core.symbols.SimpleFillSymbol;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.SelectableDataSource;
import com.iver.cit.gvsig.fmap.rendering.DotDensityLegend;
import com.iver.cit.gvsig.fmap.rendering.Legend;
import com.iver.cit.gvsig.fmap.rendering.VectorialUniqueValueLegend;
import com.iver.cit.gvsig.gui.panels.ColorChooserPanel;
import com.iver.cit.gvsig.project.documents.view.legend.gui.ILegendPanel;
import com.iver.utiles.StringUtilities;
import com.iver.utiles.XMLEntity;

/**
 *
 * @author jaume dominguez faus - jaume.dominguez@iver.es
 *
 */
public class PanelLegendDotDensity extends JPanel implements ILegendPanel {
	private FLyrVect layer;
	private JPanel northPanel = null;
	private GridBagLayoutPanel densityButtonsPanel = null;
	private JPanel pnlDensities = null;
	private JComboBox cmbLegendField = null;
	private JRadioButton rdBtnHigh = null;
	private JRadioButton rdBtnMedium = null;
	private JRadioButton rdBtnLow = null;
	private JTextField numDotSize = null;
	private double max, min, mean;
	private JTextField nmbrDotValue = null;
	private JLabel lblLabellingField = null;
	private DotDensityLegend legend;
	private String fieldName;
	private MyListener cmbAction = new MyListener();
	NumberFormat nf = NumberFormat.getInstance();

	{
		nf.setMaximumFractionDigits(3);
	}

	private class MyListener implements ItemListener, ActionListener {

		public void itemStateChanged(ItemEvent e) {
			doIt();
		}

		public void actionPerformed(ActionEvent e) {
			doIt();
		}

		private void doIt() {
			int index = cmbLegendField.getSelectedIndex();
            try {
                SelectableDataSource sds = layer.getRecordset();

                if (index != -1) {
                    fieldName = (String) cmbLegendField.getSelectedItem();
                } else {
                    fieldName = (String) cmbLegendField.getItemAt(0);
                }

                String sql = "select "+fieldName+" from '"+sds.getName()+"';";

                DataSource recordset = sds.getDataSourceFactory().executeSQL(sql, DataSourceFactory.AUTOMATIC_OPENING );
                // TODO implement MAX() and MIN() functions in GDBMS!!!!!
                max = Double.MIN_VALUE;
                min = Double.MAX_VALUE;
                for (int i = 0; i < sds.getRowCount(); i++) {
                    double value = ((NumericValue) recordset.getFieldValue(i,0)).doubleValue();
                    max = Math.max(max, value);
                    min = Math.min(min, value);
                }
                mean = (max+min) /2;
                buttonsListener.actionPerformed(null);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
		}
	};
	private ActionListener buttonsListener = new ActionListener() {
    	public void actionPerformed(ActionEvent e) {
    		if (getRdBtnHigh().isSelected()) {
    			getSldDensity().setValue(66);
    		} else if (getRdBtnLow().isSelected()) {
    			getSldDensity().setValue(33);
    		} else if (getRdBtnMedium().isSelected()) {
    			getSldDensity().setValue(50);
    		}
    	}
    };
	private JPanel centerPanel = null;
	private JSlider sldDensity = null;
	private JPanel pnlSldDensity;
	private ChangeListener sldListener = new ChangeListener() {

		public void stateChanged(ChangeEvent e) {

			// This is my messy statistic aggregation function
			// TODO if I can, I'll have a look to my statistic books to figure out one better
			int quantileIndex = getSldDensity().getValue();
			final int topLimit = 98;
			if (quantileIndex > topLimit)
				quantileIndex = topLimit;
			double quantileAmount = (max-min)/100;
			nmbrDotValue.setText(nf.format(3*quantileAmount/Math.log(quantileIndex)));
		}

	};
	private ColorChooserPanel jcc;
	private ColorChooserPanel outlineColorChooserPanel;
	public PanelLegendDotDensity() {
        super();
        initialize();
    }

    /**
     * This method initializes this
     *
     */
    private void initialize() {
        this.setLayout(new BorderLayout());
        this.setSize(new java.awt.Dimension(492,278));
        this.add(getNorthPanel(), java.awt.BorderLayout.NORTH);

        this.add(getCenterPanel(), java.awt.BorderLayout.CENTER);
    }

    public void setLayer(FLayer lyr, Legend legend) {
        this.layer = (FLyrVect) lyr;
        try {
        	SelectableDataSource sds = layer.getRecordset();
        	cmbLegendField.removeItemListener(cmbAction);
        	cmbLegendField.removeAllItems();
        	String[] fNames = sds.getFieldNames();
        	for (int i = 0; i < fNames.length; i++) {
        		if (isNumericField(sds.getFieldType(i))) {
        			cmbLegendField.addItem(fNames[i]);
        		}
        	}
        	if (!(legend instanceof DotDensityLegend)) {
        		legend = new DotDensityLegend();
        		((DotDensityLegend) legend).setFieldName((String) cmbLegendField.getItemAt(0));
        	}

        	DotDensityLegend theLegend = (DotDensityLegend) legend;

        	cmbLegendField.addItemListener(cmbAction);
        	cmbLegendField.addActionListener(cmbAction);
        	cmbLegendField.setSelectedItem(theLegend.getFieldName());
        	try {
        		getDotColorChooserPanel().setColor(theLegend.getDotColor());
        	} catch (NullPointerException npEx) {
        		getDotColorChooserPanel().setColor(Color.RED);
        	}
        	try {
        		getOutlineColorChooserPanel().setColor(theLegend.getOutlineColor());
        	} catch (NullPointerException npEx) {
        		getOutlineColorChooserPanel().setColor(Color.BLACK);
        	}
        	try {
        		double dotValue = theLegend.getDotValue();
        		if (dotValue <= 0)
        			dotValue = 100;
        		getNmbrDotValue().setText(String.valueOf(dotValue));
        	} catch (NullPointerException npEx) {
        		getNmbrDotValue().setText(String.valueOf(max));
        	}
        	try {
        		double dotSize = theLegend.getDotSize();
        		if (dotSize <= 0)
        			dotSize = 2;
        		getNumDotSize().setText(String.valueOf(dotSize));
        	} catch (NullPointerException npEx) {
        		getNumDotSize().setText(String.valueOf(3));
        	}

        } catch (Exception e) {
        	e.printStackTrace();
        }
    }

    private boolean isNumericField(int fieldType) {
        switch (fieldType) {
        case Types.BIGINT:
        case Types.DECIMAL:
        case Types.DOUBLE:
        case Types.FLOAT:
        case Types.INTEGER:
        case Types.NUMERIC:
        case Types.REAL:
        case Types.SMALLINT:
        case Types.TINYINT:
            return true;
        default:
            return false;
        }

    }

    public Legend getLegend() {
    	try {
			SimpleFillSymbol symbol = null;


    		int shapeType = layer.getShapeType(); // should be always polygon
    		if (shapeType != FShape.POLYGON) {
    			NotificationManager.addError(PluginServices.getText(this, "cannot_apply_to_a_non_polygon_layer"), new Exception());
    		}
    		SelectableDataSource sds;
    		sds = layer.getRecordset();
    		if (-1 == sds.getFieldIndexByName(fieldName))
    			return null;

    		double dotValue;
    		double dotSize;
    		try {
    			dotValue = Double.parseDouble(nmbrDotValue.getText());
    		} catch (Exception e) {
    			dotValue = nf.parse(nmbrDotValue.getText()).doubleValue();
    		}
    		if (dotValue == 0)
    			dotValue = 1;
    		try {
    			dotSize = Double.parseDouble(numDotSize.getText());
    		} catch (Exception e) {
    			dotSize = nf.parse(numDotSize.getText()).doubleValue();
    		}
    		DotDensityFillSymbol densitySym = new DotDensityFillSymbol();
    		densitySym.setDotSize(dotSize);
    		densitySym.setDotColor(getDotColorChooserPanel().getColor());
    		symbol = new SimpleFillSymbol();
    		XMLEntity xml = symbol.getXMLEntity();
    		xml.putProperty("color", StringUtilities.color2String(Color.WHITE));
    		xml.putProperty("outline", StringUtilities.color2String(getOutlineColorChooserPanel().getColor()));

    		xml.addChild(densitySym.getXMLEntity());
    		symbol.setXMLEntity(xml);

    		legend = new DotDensityLegend();
    		legend.addSymbol(ValueFactory.createValue("theSymbol"), symbol);
    		legend.setDefaultSymbol(symbol);
    		legend.setDotValue(dotValue);
    		legend.setFieldName(fieldName);


    	} catch (Exception e) {
    		NotificationManager.addError(PluginServices.getText(this, "could_not_setup_legend"), e);
    	}
    	return legend;

    }

    /**
     * This method initializes centerPanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getNorthPanel() {
        if (northPanel == null) {
            lblLabellingField = new JLabel();
            lblLabellingField.setText(PluginServices.getText(this, "labelling_field"));
            northPanel = new JPanel(new FlowLayout(FlowLayout.LEADING,15,0));
            northPanel.add(lblLabellingField, null);
            northPanel.add(getCmbLegendField(), null);

        }
        return northPanel;
    }

    private ColorChooserPanel getDotColorChooserPanel() {
    	if (jcc == null) {
    		jcc = new ColorChooserPanel() ;
    		jcc.setAlpha(255);
    	}
    	return jcc;
	}

	/**
     * This method initializes southPanel
     *
     * @return javax.swing.JPanel
     */
    private JPanel getDensityButtonsPanel() {
        if (densityButtonsPanel == null) {
        	densityButtonsPanel = new GridBagLayoutPanel();
        	LayoutManager layout = new FlowLayout(FlowLayout.LEADING, 0,0);
        	JPanel aux = new JPanel(layout);
        	aux.add(getNumDotSize());
        	densityButtonsPanel.addComponent(
        			PluginServices.getText(this, "dot_size"), aux);
        	aux = new JPanel(layout);
        	aux.add(getNmbrDotValue());
            densityButtonsPanel.addComponent(
            		PluginServices.getText(this,"dot_value"), aux);
            aux = new JPanel(layout);
            aux.add(getDotColorChooserPanel());
            densityButtonsPanel.addComponent(
            		PluginServices.getText(this, "color"), aux);
            aux = new JPanel(layout);
            aux.add(getOutlineColorChooserPanel());
            densityButtonsPanel.addComponent(
            		PluginServices.getText(this, "outline_color"), aux);
        }
        return densityButtonsPanel;
    }

    private ColorChooserPanel getOutlineColorChooserPanel() {
    	if (outlineColorChooserPanel == null) {
    		outlineColorChooserPanel = new ColorChooserPanel();
    		outlineColorChooserPanel.setAlpha(255);
    	}
    	return outlineColorChooserPanel;
	}

	/**
     * This method initializes pnlDensities
     *
     * @return javax.swing.JPanel
     */
    private JPanel getPnlDensities() {
        if (pnlDensities == null) {
            pnlDensities = new JPanel(new BorderLayout(5,0));
            pnlDensities.setBorder(BorderFactory.createTitledBorder(null,
					PluginServices.getText(this, "densities")));
            JPanel aux2 = new JPanel();
            JPanel aux;
			aux = new JPanel(new GridLayout(1,3));
            aux.add(new JLabel(PluginServices.getText(this, "high")));
            aux.add(new JLabel(PluginServices.getText(this, "medium")));
            aux.add(new JLabel(PluginServices.getText(this, "low")));

            aux2.add(aux);

			aux = new JPanel(new GridLayout(1,3));
            aux.add(getRdBtnHigh());
            aux.add(getRdBtnMedium());
            aux.add(getRdBtnLow());

            aux2.add(aux);
            aux2.setLayout(new BoxLayout(aux2, BoxLayout.Y_AXIS));

            pnlDensities.add(aux2, BorderLayout.NORTH);
            pnlDensities.add(getSldDensity(), BorderLayout.CENTER);
            pnlDensities.add(getDensityButtonsPanel(), BorderLayout.SOUTH);

            ButtonGroup group = new ButtonGroup();
            group.add(getRdBtnHigh());
            group.add(getRdBtnLow());
            group.add(getRdBtnMedium());
            getRdBtnMedium().setSelected(true);
        }
        return pnlDensities;
    }


	/**
     * This method initializes cmbLegendField
     *
     * @return javax.swing.JComboBox
     */
    private JComboBox getCmbLegendField() {
        if (cmbLegendField == null) {
            cmbLegendField = new JComboBox();
        }
        return cmbLegendField;
    }

    /**
     * This method initializes rdBtnHigh
     *
     * @return javax.swing.JRadioButton
     */
    private JRadioButton getRdBtnHigh() {
        if (rdBtnHigh == null) {
            rdBtnHigh = new JRadioButton(new ImageIcon(
            		getClass().getClassLoader().
            			getResource("images/high-density-sample.png")));
            rdBtnHigh.addActionListener(buttonsListener);
        }
        return rdBtnHigh;
    }

    /**
     * This method initializes rdBtnMedium
     *
     * @return javax.swing.JRadioButton
     */
    private JRadioButton getRdBtnMedium() {
        if (rdBtnMedium == null) {
            rdBtnMedium = new JRadioButton(new ImageIcon(
            		getClass().getClassLoader().
        			getResource("images/medium-density-sample.png")));
            rdBtnMedium.addActionListener(buttonsListener);
        }
        return rdBtnMedium;
    }

    /**
     * This method initializes rdBtnMax
     *
     * @return javax.swing.JRadioButton
     */
    private JRadioButton getRdBtnLow() {
        if (rdBtnLow == null) {
            rdBtnLow = new JRadioButton(new ImageIcon(
            		getClass().getClassLoader().
        			getResource("images/low-density-sample.png")));
            rdBtnLow.addActionListener(buttonsListener);
        }
        return rdBtnLow;
    }

    /**
     * This method initializes numDotSize
     *
     * @return de.ios.framework.swing.JNumberField
     */
    private JTextField getNumDotSize() {
        if (numDotSize == null) {
            numDotSize = new JTextField(4);
        }
        return numDotSize;
    }

	/**
	 * This method initializes nmbrDotValue
	 *
	 * @return de.ios.framework.swing.JNumberField
	 */
	private JTextField getNmbrDotValue() {
		if (nmbrDotValue == null) {
			nmbrDotValue = new JTextField(15);

		}
		return nmbrDotValue;
	}

	/**
	 * This method initializes centerPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getCenterPanel() {
		if (centerPanel == null) {
			centerPanel = new JPanel();
			centerPanel.setLayout(new BorderLayout(5, 5));
			centerPanel.add(getPnlDensities(), java.awt.BorderLayout.WEST);
		}
		return centerPanel;
	}

	/**
	 * This method initializes sldDensity
	 *
	 * @return javax.swing.JSlider
	 */
	private JSlider getSldDensity() {
		if (sldDensity == null) {
			sldDensity = new JSlider();
			sldDensity.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
			sldDensity.addChangeListener(sldListener);
		}
		return sldDensity;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
