package com.iver.cit.gvsig.project.documents.view.toc;

import javax.swing.JDialog;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrAnnotation;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.layerOperations.ClassifiableVectorial;
import com.iver.cit.gvsig.project.documents.view.legend.gui.FThemeManagerWindow2;
import com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction;
import com.iver.cit.gvsig.project.documents.view.toc.ITocItem;
/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
/* CVS MESSAGES:
 *
 * $Id: FLyrVectEditPropertiesTocMenuEntry2.java 11094 2007-04-10 14:38:10Z jmvivo $
 * $Log$
 * Revision 1.1.2.2  2007-04-10 14:38:10  jmvivo
 * Sustituida la entrada del toc de 'propiedades'
 *
 * Revision 1.1.2.1  2007/04/10 11:05:45  jaume
 * REMOVED UPDATE WINDOW PROPERTIES FROM build.xml
 *
 * Revision 1.1.2.1  2007/04/10 10:51:49  jaume
 * *** empty log message ***
 *
 * Revision 1.2.2.2  2007/01/23 12:25:44  caballero
 * No mostrar propiedades de una capa de anotaciones
 *
 * Revision 1.2.2.1  2006/11/15 04:10:44  jjdelcerro
 * *** empty log message ***
 *
 * Revision 1.2  2006/10/02 13:52:34  jaume
 * organize impots
 *
 * Revision 1.1  2006/09/15 10:41:30  caballero
 * extensibilidad de documentos
 *
 * Revision 1.1  2006/09/12 15:58:14  jorpiell
 * "Sacadas" las opcines del men� de FPopupMenu
 *
 *
 */
/**
 * Muestra el men� de propiedades del tema.
 *
 * @author jmorell
 */


public class FLyrVectEditPropertiesTocMenuEntry2 extends AbstractTocContextMenuAction {
	public String getGroup() {
		return "group2"; //FIXME
	}

	public int getGroupOrder() {
		return 20;
	}

	public int getOrder() {
		return 0;
	}

	public String getText() {
		return PluginServices.getText(this, "propiedades");
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return selectedItems.length == 1;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			FLayer lyr = getNodeLayer(item);
            if ((lyr instanceof ClassifiableVectorial)) {
            	if (!((lyr instanceof FLyrVect) &&
            			!((FLyrVect)lyr).isPropertiesMenuVisible())){
            		if (!(lyr instanceof FLyrAnnotation))
            			return true;
            	}

            }
		}
		return false;

	}


	public void execute(ITocItem item, FLayer[] selectedItems) {
		FLayer[] actives = selectedItems;
		FThemeManagerWindow2 fThemeManagerWindow;
    	if (actives.length==1) {
    		if (!actives[0].isAvailable()) return;
    		System.out.println("openPliegoThemeProperties(): Una sola capa. Abrimos el panel para la capa " + actives[0]);
    		fThemeManagerWindow = new FThemeManagerWindow2();
    		try {
    			fThemeManagerWindow.setMapContext(getMapContext());
    		} catch (com.iver.cit.gvsig.fmap.DriverException e1) {
    			e1.printStackTrace();
    		}
            //PluginServices.getMDIManager().addView(fThemeManagerWindow);
    		if (PluginServices.getMainFrame() == null) {
    			JDialog dlg = new JDialog();
    			fThemeManagerWindow.setPreferredSize(fThemeManagerWindow.getSize());
    			dlg.getContentPane().add(fThemeManagerWindow);
    			dlg.setModal(false);
    			dlg.pack();
    			dlg.show();
    		} else {
    			PluginServices.getMDIManager().addWindow(fThemeManagerWindow);
    		}
    	} else {
        	for (int i = 0; i < actives.length; i++){
        		if (actives[0].isAvailable()) {
        			System.out.println("openPliegoThemeProperties(): Muchas capas. Abrimos el panel para la capa " + actives[i]);
        			fThemeManagerWindow = new FThemeManagerWindow2(actives[i], getMapContext());
        			//PluginServices.getMDIManager().addView(fThemeManagerWindow);
        			if (PluginServices.getMainFrame() == null) {
        				JDialog dlg = new JDialog();
        				fThemeManagerWindow.setPreferredSize(fThemeManagerWindow.getSize());
        				dlg.getContentPane().add(fThemeManagerWindow);
        				dlg.setModal(false);
        				dlg.pack();
        				dlg.show();
        			} else {
        				PluginServices.getMDIManager().addWindow(fThemeManagerWindow);
        			}
        		}
        	}
    	}
	}

}
