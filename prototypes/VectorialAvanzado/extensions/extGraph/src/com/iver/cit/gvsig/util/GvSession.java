/*
 * Created on 06-nov-2006
 *
 * gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
/* CVS MESSAGES:
*
* $Id: GvSession.java 8651 2006-11-09 13:30:40Z azabala $
* $Log$
* Revision 1.2  2006-11-09 13:30:40  azabala
* *** empty log message ***
*
* Revision 1.1  2006/11/07 19:50:54  azabala
* *** empty log message ***
*
*
*/
package com.iver.cit.gvsig.util;

import java.util.Map;

import com.iver.cit.gvsig.fmap.MapControl;
import com.vividsolutions.jump.util.Blackboard;


/**
 * 
 * Prototype of user's session.
 * 
 * This class will save information of the
 * user session, to recover later.
 * 
 * The key of the session will be the Andami's view
 * (to allows a user to save different user sessions
 * for different views)
 * */

public class GvSession {
	private final static GvSession session = new GvSession();
	
	Blackboard blackboard = new Blackboard();
	
	private GvSession(){}
	
	public static GvSession getInstance(){
		return session;
	}
	
	public void put(MapControl map, String key, Object sessionObject){
		String gKey = map.toString() + ":" + key;
		blackboard.put(gKey, sessionObject );
	}
	
	public Object get(MapControl map, String key){
		return blackboard.get(map.toString() + ":" + key);
	}
	
	public void delete(MapControl map, String key){
		Map props = blackboard.getProperties();
		String newKey = map.toString()+":"+key;
		if(props.containsKey(newKey))
		{
			props.remove(newKey);
		}
		
	}
	
	
}

