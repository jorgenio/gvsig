/*
 * Created on 03-oct-2006
 *
 * gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
/* CVS MESSAGES:
*
* $Id: JtsTests.java 8193 2006-10-19 16:06:48Z azabala $
* $Log$
* Revision 1.2  2006-10-19 16:06:48  azabala
* *** empty log message ***
*
* Revision 1.1  2006/10/17 18:25:53  azabala
* *** empty log message ***
*
* Revision 1.1  2006/10/05 19:20:57  azabala
* first version in cvs
*
*
*/
package com.vividsolutions.jts.operation;

import com.vividsolutions.jts.operation.overlay.SnappingOverlayOperationTest;

import junit.framework.Test;
import junit.framework.TestSuite;

public class JtsTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(
				"Test for com.iver.cit.gvsig.fmap.topology");
		//$JUnit-BEGIN$
		suite.addTestSuite(SnappingOverlayOperationTest.class);
		//$JUnit-END$
		return suite;
	}

}

