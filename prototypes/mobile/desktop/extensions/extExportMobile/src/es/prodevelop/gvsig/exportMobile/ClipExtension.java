package es.prodevelop.gvsig.exportMobile;

import java.util.ArrayList;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.cit.gvsig.About;
import com.iver.cit.gvsig.fmap.layers.FLayers;
import com.iver.cit.gvsig.project.ProjectExtent;
import com.iver.cit.gvsig.project.documents.view.gui.View;

import es.prodevelop.gvsig.exportMobile.ui.order.FLayerWithNewName;
import es.prodevelop.gvsig.exportMobile.ui.panels.ExportPanel;


/**
 * 
 * This extension allows a user to export all the layers in a view
 * 
 * @author amoreno
 *
 */
public class ClipExtension extends Extension {

	/**
	 * Starts the extension
	 */
	public void initialize() {
		
        // about
        java.net.URL newurl =
        	createResourceUrl("about/export-mobile-about.html");
        About claseAbout = (About) PluginServices.getExtension(com.iver.cit.gvsig.About.class);
        claseAbout.getAboutPanel().addAboutUrl("Export to gvSIG Mobile", newurl);
	}

	
	public boolean isEnabled() {

		return true;
	}

	/**
	 * is visible when a view is the active window
	 */
	public boolean isVisible() {
		com.iver.andami.ui.mdiManager.IWindow f = (com.iver.andami.ui.mdiManager.IWindow) PluginServices
				.getMDIManager().getActiveWindow();

		if (f == null) {
			return false;
		}

		return (f instanceof View);
	}

	/**
	 * @param actionCommand not used
	 * It's called when the export to gvsig mobile button is pressed
	 */
	public void execute(String actionCommand) {

		View view = (View) PluginServices.getMDIManager().getActiveWindow();
		view.getModel().getProject().getExtents();
		// root
		FLayers rt = view.getMapControl().getMapContext().getLayers();
		ArrayList lyrsNewName = FLayerWithNewName.getAll(rt);
		ProjectExtent[] listExtent = view.getModel().getProject().getExtents();
		ExportPanel infPanel = new ExportPanel(lyrsNewName, listExtent);
		PluginServices.getMDIManager().addWindow(infPanel);
	}
	
    private java.net.URL createResourceUrl(String path) {
        return getClass().getClassLoader().getResource(path);
    }
	
}
