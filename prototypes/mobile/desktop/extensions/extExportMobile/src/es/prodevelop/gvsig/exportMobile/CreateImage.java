package es.prodevelop.gvsig.exportMobile;

import java.net.URL;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;

/**
 * Class to access the icons used by the extension
 * @author amoreno
 *
 */
public class CreateImage {

	private Logger logger = Logger.getLogger(CreateImage.class.getClass());
	
	public CreateImage() {

	}

	/**
	 * Get path image
	 * 
	 * @param path
	 * @return URL
	 */
	public java.net.URL createResourceUrl(String path) {
		return getClass().getClassLoader().getResource(path);
	}

	/**
	 * Create image icon
	 * 
	 * @param path
	 *            where the image is into.
	 * @param description
	 *            of the image.
	 * @return ImageIcon
	 */
	public ImageIcon createImageIcon(URL path, String description) {

		java.net.URL imgURL = path;
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			logger.error("Couldn't find file: " + path);
			return null;
		}
	}

}
