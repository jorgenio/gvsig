package es.prodevelop.gvsig.exportMobile;


import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.iver.cit.gvsig.fmap.drivers.FieldDescription;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrRaster;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.FLyrWMS;

/**
 * Create a layer object with its checkbox, button, combo and description change
 * the name to the layer if the name layer is duplicate
 * 
 * @author Anabel Moreno
 * 
 */
public class LayerObject {

	private JCheckBox boxLyr;

	private String nameLyr;

	private JComboBox comboLyr;

	private FieldDescription[] description;

	private FLayer lyr;

	private JButton buttonLyr;
	
	private JPanel parent;
	
	private JLabel isInto;
	
	// private String nameNew;

	/**
	 * @param box
	 *            layer
	 * @param name
	 *            layer order
	 * @param combo
	 *            layer
	 * @param layer
	 * @param button
	 *            layer
	 */
	public LayerObject(JCheckBox box, String name, JComboBox combo,
			FLayer layerwn, JButton button,JPanel pa) {

		parent=pa;
		boxLyr = box;
		nameLyr = name;
		comboLyr = combo;
		buttonLyr = button;
					

		try {
			// lyr = layer.cloneLayer();
			// lyr.setName(nameLyr);
			// lyr.setProjection(layer.getProjection());
			// jldominguez december 2007
			lyr = layerwn;
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(boxLyr.isSelected()==true){
				
			if(lyr instanceof FLyrVect){
				comboLyr.setEnabled(true);
				buttonLyr.setEnabled(true);
			}
			if(lyr instanceof FLyrRaster){
				comboLyr.setEnabled(true);
				buttonLyr.setEnabled(false);
			}
		}
		else{
				comboLyr.setEnabled(false);
				buttonLyr.setEnabled(false);
		}
			
		
		
		if (lyr instanceof FLyrRaster) {
			buttonLyr.setEnabled(false);
		}
		if (lyr instanceof FLyrWMS) {

			comboLyr.setEnabled(false);
			buttonLyr.setEnabled(false);
		}
	}
	
	/**
	 * @return parent
	 */
	public JPanel getParent(){
		return parent;
	}
	
	/**
	 * Set box layer selected
	 * 
	 * @param st
	 */
	public void setBoxStatus(boolean st) {

		boxLyr.setSelected(st);
		
	}

	/**
	 * @return layer button
	 */
	public JButton getButton() {

		return buttonLyr;
	}


	/**
	 * @return layer name
	 */
	public String getName() {

		return nameLyr;
	}
	
	/**
	 * Change layer name
	 * @param name
	 */
	public void setName(String n){
		
		nameLyr = n;
	}

	/**
	 * @return layer checkbox
	 */
	public JCheckBox getBox() {

		return boxLyr;
	}

	/**
	 * @return layer combo
	 */
	public JComboBox getCombo() {

		return comboLyr;
	}

	/**
	 * set combo properties (enabled and selected index)
	 * @param combo
	 * @param indx
	 */
	public void setCombo(boolean combo,int indx){
		
		comboLyr.setEnabled(combo);
		comboLyr.setSelectedIndex(indx);
	}
	
	/**
	 * Set layer field description
	 * 
	 * @param atribSelected
	 */
	public void setDescription(FieldDescription[] atribSelected) {

		description = atribSelected;
	}

	/**
	 * @return layer description
	 */
	public FieldDescription[] getDescription() {

		return description;
	}
	
	/**
	 * set image layer, if this is into the view, and create a label with this image
	 * @param imgInto
	 */
	public void setImage(JLabel imgInto){
		
		isInto=imgInto;
		
	}
	
	/**
	 * @return image label
	 */
	public JLabel getImgLbl(){
		
		return isInto;
	}

	public FLayer getLyr() {
		return lyr;
	}

	public void setLyr(FLayer lyr) {
		this.lyr = lyr;
	}


}
