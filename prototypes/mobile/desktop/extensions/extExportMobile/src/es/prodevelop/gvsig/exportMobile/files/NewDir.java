package es.prodevelop.gvsig.exportMobile.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;


/**
 * Manages the directory when a gvsig mobile project 
 * with his data will be stored
 * 
 * @author Anabel Moreno
 *
 */
public class NewDir {

	private static Logger logger = Logger.getLogger(NewDir.class.getName());

	private String namePath;

	private File newFileProy;

	private File[] filesDir;

	private int numFiles;
	
	public static final String tempDirectoryPath = System.getProperty("java.io.tmpdir")+"/tmp-andami";

	/**
	 * Create new output file
	 * 
	 * @param dirOutPut
	 */
	public NewDir(String dirOutPut) {

		namePath = (dirOutPut + File.separator);
		newFileProy = new File(namePath);

	}

	/** 
	 * @return true if the directory is empty
	 */
	public boolean isEmpty() {

		filesDir = newFileProy.listFiles();
		numFiles = filesDir.length;

		if (contFiles(filesDir) == 0)
			return true;
		else
			return false;

	}

	/**
	 * Count the name of files in a directory
	 * 
	 * @param arrFiles
	 * @return the number of files in a directory
	 */
	private int contFiles(File[] arrFiles) {

		int cont = 0;
		for (int i = 0; i < arrFiles.length; i++) {

			if (arrFiles[i].exists()) {
				cont++;
			}
		}
		return cont;
	}

	/**
	 * Delete files
	 */
	public void deleteFiles() {

		for (int i = 0; i < numFiles; i++) {
			filesDir[i].delete();

		}

	}

	/**
	 * if file exists and is directory, while this file is full, the files that
	 * are into the file will be deleted
	 * 
	 * @return output directory
	 */
	public File ifExistDir() {

		File outputPath = null;

		if (newFileProy.exists()) {

			if (newFileProy.isDirectory()) {

				// to delete, the directory has to be empty

				while (isEmpty() == false) {

					deleteFiles();
				}

				File deletedFile = newFileProy;
				boolean delete = deletedFile.delete();
				// if (delete) System.out.println("delete file");
				// else System.out.println("not delete");

			}
		}

		/** new directory */

		boolean mk = newFileProy.mkdir();

		if (mk == true) {

			/* create output File for result layer */
			outputPath = new File(newFileProy.getAbsolutePath());

		}

		return outputPath;
	}
	
	public static void copyFile(File in, File out) {

		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
		    fis  = new FileInputStream(in);
		    fos = new FileOutputStream(out);
	        byte[] buf = new byte[1024];
	        int i = 0;
	        while ((i = fis.read(buf)) != -1) {
	            fos.write(buf, 0, i);
	        }
	        if (fis != null) fis.close();
	        if (fos != null) fos.close();
	    } catch (Exception e) {
	        logger.error("Unable to copy file: " + e.getMessage());
	    }
	}
	
	public static void copyShpDbfShxTempToFinal(File source, File dest) {
		
		if (dest == null) {
			logger.error("Unable to copy temp to file: finalFile is NULL !");
		}
		
		if (source == null) {
			logger.error("Unable to copy temp to file: tempFile is NULL !");
		}
		
		int temp_last_shp = source.getAbsolutePath().toLowerCase().lastIndexOf(".shp");
		String temp_base = source.getAbsolutePath().substring(0, temp_last_shp);
		int final_last_shp = dest.getAbsolutePath().toLowerCase().lastIndexOf(".shp");
		String final_base = dest.getAbsolutePath().substring(0, final_last_shp);
		
		NewDir.copyFile(source, dest);
		
		File f_in = new File(temp_base + ".dbf"); 
		File f_out = new File(final_base + ".dbf");
		NewDir.copyFile(f_in, f_out);
		
		f_in = new File(temp_base + ".shx"); 
		f_out = new File(final_base + ".shx");
		NewDir.copyFile(f_in, f_out);
	}

	public static File getTempShpFile() {

		File f = null;
		String t = "" + System.currentTimeMillis();
		try {
			f = File.createTempFile(t, ".shp");
			f.deleteOnExit();
		} catch (IOException e) {
			logger.error("Unable to create temp file: " + e.getMessage());
		}
		return f;
	}
	
}
