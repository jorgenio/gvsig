package es.prodevelop.gvsig.exportMobile.layerexporters;

import java.awt.geom.Rectangle2D;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.cresques.cts.IProjection;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.drivers.FieldDescription;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.rendering.XmlBuilder;
import com.iver.utiles.swing.threads.AbstractMonitorableTask;

import es.prodevelop.gvsig.exportMobile.xml.Bounds;
import es.prodevelop.gvsig.exportMobile.xml.XmlProjectTags;
/**
 * 
 * This  process executes a series of tasks
 * 
 * @author jcarras
 *
 */
public class ExportProcess  extends AbstractMonitorableTask{
	private ArrayList tasks = new ArrayList();
	private int numSteps=0;
	private XmlBuilder xml = new XmlBuilder();
	private Rectangle2D rect;
	private String name;
	private IProjection projection;
	private String dirPath;
	public static String PROJECTEXTENSION = ".gvm";
	

	/**
	 * Constructor with all needed parameters 
	 * @param rectangle
	 * @param name
	 * @param dirPath
	 * @param projection
	 */
	public ExportProcess(Rectangle2D rectangle, String name, String dirPath, IProjection projection){
		setRect(rectangle);
		setName(name);
		setProjection(projection);
		setDirPath(dirPath);
	}
	
	/**
	 * Adds a ExporterSubTask task to the list of task to execute
	 * The order the tasks are added is the order they will be executed
	 * @param task
	 */
	public void addTask(ExporterSubTask task){
		tasks.add(task);
		numSteps+=task.getFinalStep();
	}
	
	/**
	 * Creates a VectorialExporterTask and adds it to the list of tasks to execute
	 * @param lyr
	 * @param outputFields
	 * @param processType
	 */
	public void addVectorialTask(FLayer lyr, FieldDescription[] outputFields, int processType){
		VectorialExporterTask task = new VectorialExporterTask(this,
				lyr,
				getRect(),
				dirPath,
				processType,
				outputFields,
				xml);
		addTask(task);
	}
	
	/**
	 * Creates a RasterExporter task and adds it to the list of tasks to execute
	 * @param lyr
	 */
	public void addRasterTask(FLayer lyr){
		RasterExporter task = new RasterExporter(this,lyr,rect, new File(dirPath),xml);
		addTask(task);
	}
	
	/**
	 * Creates a WMSExporter task and adds it to the list of tasks to execute
	 * @param lyr
	 */
	public void addWMSTask(FLayer lyr){
		WMSExporter task = new WMSExporter(this,lyr,getRect(),xml);
		addTask(task);
	}
	
	/**
	 * Closes the oppened xml tags and stores the project file
	 */
	private void writeProjectFile(){
		// done
		
		xml.closeTag();
		//</Layers>
				
		xml.closeTag();
		//</Project>
		
		//new file
		File fileout = new File(dirPath+ File.separator + name + PROJECTEXTENSION);
		createFile(fileout,xml.getXML());
	}
	
	/**
	 * Starts the xml to be ready for pushing the layers into
	 */
	private void initProjectFile(){
		xml.writeHeader();
		//<Project>
		xml.openTag(XmlProjectTags.PROJECT);
		Bounds bounds = Bounds.createBounds(rect);
			
			//<BoundingBox>
			bounds.setXML(xml);
			bounds.createXML();
			//</BoundingBox>
			
			//<Name>
			xml.writeTag(XmlProjectTags.NAME, name);
			//<Name>
			
			//<CRS>
			String crsProyect = getProjection().getAbrev();
			xml.writeTag(XmlProjectTags.CRS, crsProyect);
			//</CRS>
			
			//<Layers>
			xml.openTag(XmlProjectTags.LAYERS);
	}

	/**
	 * To be executed before running this
	 */
	public void preProcess(){
		setInitialStep(0);
		this.setFinalStep(numSteps);
		setDeterminatedProcess(true);
	}
	
	/**
	 * Executes all the tasks in the list, creates the project file and 
	 * reports the steps not reported yet
	 */
	public void run() throws Exception {
		
		initProjectFile();
		for (int i=0; i<tasks.size(); i++){
			ExporterSubTask task = (ExporterSubTask)tasks.get(i);
			if (!isCanceled()){
				task.run();
			}else{
				task.setCancelled(true);
			}
		}
		setNote("Generating project file");
		writeProjectFile();
		JOptionPane.showMessageDialog(null,
				PluginServices.getText(this, "export_done") + "\n" + dirPath,
				PluginServices.getText(this, "exporMob"),
				JOptionPane.INFORMATION_MESSAGE);
		
		reportToEnd();
	}

	/**
	 * Reports steps to indicate the task list has been completed  
	 */
	private void reportToEnd(){
		while (getCurrentStep()<getFinalStep())
			reportStep();
	}
	
	public Rectangle2D getRect() {
		return rect;
	}

	public void setRect(Rectangle2D rect) {
		this.rect = rect;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public IProjection getProjection() {
		return projection;
	}

	public void setProjection(IProjection projection) {
		this.projection = projection;
	}

	public String getDirPath() {
		return dirPath;
	}

	public void setDirPath(String dirPath) {
		this.dirPath = dirPath;
	}
	
	/**
	 * Writes the xml to the file
	 * 
	 * @param f
	 * @param xml
	 */
	public static void createFile(File f, String xml) {
		if (xml != null) {
			try {
				if (!f.exists()) {
					f.createNewFile();
				}
				
				OutputStream fout = new FileOutputStream(f);
				OutputStreamWriter out = new OutputStreamWriter(fout,"UTF-8");
				BufferedWriter bw = new BufferedWriter(out);
				bw.write(xml);
				
				fout.flush();
				out.flush();
				bw.flush();
				
				bw.close();
				fout.close();
				out.close();
			} catch (IOException e) {
				
			}
		}
	}

}
