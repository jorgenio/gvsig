/**
 * 
 */
package es.prodevelop.gvsig.exportMobile.layerexporters;

import java.awt.geom.Rectangle2D;
import java.io.File;

import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.rendering.XmlBuilder;
import com.iver.utiles.swing.threads.AbstractMonitorableTask;

import es.prodevelop.gvsig.exportMobile.xml.XmlProjectTags;

/**
 * 
 * This class mother has to be the ancestor of every class
 * executable in an ExportProcess 
 * 
 * @author jcarras
 *
 */
public abstract class ExporterSubTask{

	protected FLayer inLayer; 
	protected Rectangle2D rect;
	protected AbstractMonitorableTask parent;
	protected int finalStep;
	protected XmlBuilder xml;
	private boolean xmlInited = false;
	private int currentStep = 0;
	private boolean isCancelled = false;
	
	public abstract void run();
	
	/**
	 * Constructor
	 * 
	 * @param parentProcess
	 * @param layer
	 * @param rect
	 * @param xml
	 */
	public ExporterSubTask(AbstractMonitorableTask parentProcess, FLayer layer, Rectangle2D rect, XmlBuilder xml){
		this.inLayer=layer;
		this.rect=rect;
		this.parent=parentProcess;
		this.xml=xml;
	}

	/**
	 * Indicates to the ExportProcess that the task is going on
	 * reportStep has to be called exactly getFinalStep() times
	 */
	public void reportStep(){
		currentStep++;
		parent.reportStep();
	}

	/**
	 * It reports numSteps Steps
	 * @param numSteps
	 */
	public void reportSteps(int numSteps){
		for (int i=0; i<numSteps; i++)
			reportStep();
	}
	
	/**
	 * Changes the status text of the task 
	 * @param statusMessage
	 */
	public void setStatusMessage(String statusMessage){
		parent.setStatusMessage(statusMessage);
	}
	
	/**
	 * Changes the note of the task
	 * (It is showed on progress dialog)
	 * 
	 * @param note
	 */
	public void setNote(String note){
		parent.setNote(note);
	}

	/**
	 * Any class extending ExporterSubClass has to implement this method
	 * to comunicate the number of steps it will report
	 * @return
	 */
	public abstract int getFinalStep();

	/**
	 * If the proposed file exists, this method returns one not existing
	 * file similar to the proposed one
	 * 
	 * @param proposedFile
	 * @return
	 */
	public File getFreeFile(File proposedFile){
		if (!proposedFile.exists())
			return proposedFile;
		else {
			String base = proposedFile.getAbsolutePath();
			int dotPos = base.lastIndexOf(".");
			String ext = base.substring(dotPos);
			base = base.substring(0, dotPos);
			for (int i=0;i<99999;i++){
				String newPath = base + "_" + i + ext;
				File newFile = new File(newPath);
				if (!newFile.exists())
					return newFile;
			}
		}
		return null;
	}
	
	/**
	 * Starts the XML with the layer heading
	 */
	protected void initXML(){
		//<Layer>		
		xml.openTag(XmlProjectTags.LAYER);
		xmlInited=true;
	}
	
	/**
	 * Add the layer footer to the XML
	 */
	protected void closeXML(){
		if (isXmlInited())
			xml.closeTag();
		//</Layer>
		xmlInited=false;
	}

	public boolean isXmlInited() {
		return xmlInited;
	}

	public int getCurrentStep() {
		return currentStep;
	}
	
	protected void reportToEnd(){
		while (getCurrentStep()<getFinalStep()){
			reportStep();
		}
	}

	public boolean isCancelled() {
		return isCancelled;
	}

	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
	
	
}
