package es.prodevelop.gvsig.exportMobile.layerexporters;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.log4j.Logger;

import com.hardcode.driverManager.Driver;
import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.drivers.FieldDescription;
import com.iver.cit.gvsig.fmap.drivers.raster.CmsRasterDriver;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrRaster;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.rendering.XmlBuilder;
import com.iver.utiles.swing.threads.AbstractMonitorableTask;

import es.prodevelop.gvsig.exportMobile.xml.XmlProjectTags;

/**
 * Exportation subtask
 * Allows a raster layer to be exported
 * 
 * @author jcarras
 *
 */
public class RasterExporter extends ExporterSubTask{

	private Logger logger = Logger.getLogger(RasterExporter.class.getClass());
	private File outputdir;
	private static int INDX_PATH = 10;
	private FLyrRaster inLyrRast;

	/**
	 * COnstructor with all the needed attributes
	 * 
	 * @param parentProcess
	 * @param layer
	 * @param rect this parameter is not used currently
	 * @param outputdir
	 * @param xml
	 */
	public RasterExporter(AbstractMonitorableTask parentProcess, FLayer layer,
			Rectangle2D rect, File outputdir, XmlBuilder xml) {
		super(parentProcess, layer, rect, xml);
		this.outputdir=outputdir;
		inLyrRast = (FLyrRaster)layer;
	}

	
	/**
	 * Get old path file, and copy this file to a new file
	 */
	public void export() {
		setNote(PluginServices.getText(this, "exporting_")  + " " +  inLayer.getName());
		try {
			/*String pathLayer = inLayer.getXMLEntity().getXmlTag()
			.getProperty(INDX_PATH).getValue();
			File oldFile = new File(pathLayer);*/
			Driver driver =  inLyrRast.getSource().getDriver();
			if (driver instanceof CmsRasterDriver){
				initXML();
				CmsRasterDriver drR =(CmsRasterDriver)driver;
				File oldFile = drR.getFile();
				if (oldFile.exists()) {
					File destFile = new File(outputdir + File.separator + oldFile.getName());
					destFile = getFreeFile(destFile);
					File wldFile = findWldFile(oldFile);
					if (wldFile == null) {
						logger.warn("Did not find world file for: " + oldFile.getName());
					} else {
						String newWldPath = destFile.getAbsolutePath();
						newWldPath = newWldPath.substring(0, newWldPath.lastIndexOf("."));
						File newWldFile = new File(newWldPath + ".wld");
						copyFileToFile(wldFile, newWldFile, 5);
					}
					copyFileToFile(oldFile, destFile, 95);
					writeXML(inLayer.getName(), destFile.getName());
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			closeXML();
			reportToEnd();
		}
	}


	/**
	 * Searches the WLD file associated with the layer
	 * 
	 * @param img_file
	 * @return
	 */
	private File findWldFile(File img_file) {
		
		String img_path = img_file.getAbsolutePath();
		String img_name = img_file.getName();
		
		int lastp = img_name.lastIndexOf(".");
		String img_name_no_ext = img_name.substring(0, lastp); 
		
		String ext = img_path.substring(img_path.length() - 4, img_path.length());
		File resp = null;
		
		if ((ext.compareToIgnoreCase(".jpg") == 0) || (ext.compareToIgnoreCase("jpeg") == 0)) {
			resp = new File(img_file.getParent() + File.separator + img_name_no_ext + ".jgw");
			if (resp.exists()) return resp;
			resp = new File(img_file.getParent() + File.separator + img_name_no_ext + ".jpw");
			if (resp.exists()) return resp;
			resp = new File(img_file.getParent() + File.separator + img_name_no_ext + ".jpgw");
			if (resp.exists()) return resp;
		}
		
		if ((ext.compareToIgnoreCase(".tif") == 0) || (ext.compareToIgnoreCase("tiff") == 0)) {
			resp = new File(img_file.getParent() + File.separator + img_name_no_ext + ".tfw");
			if (resp.exists()) return resp;
		}
		
		resp = new File(img_file.getParent() + File.separator + img_name_no_ext + ".wld");
		if (resp.exists()) return resp;
		return null;
	}

	/**
	 * Copies the in file to the outdir directory
	 * 
	 * @param in
	 * @param outdir
	 * @param numStepsToReport the copy will report this number of steps
	 * @throws Exception
	 */
	public void copyFileToDir(File in, File outdir, int numStepsToReport) throws Exception {

		File f = new File(outdir.getAbsolutePath() + File.separator + in.getName());
		
		copyFileToFile(in, f, numStepsToReport);
	}
	/**
	 * Copies the in file data to the outFile file
	 * 
	 * @param in
	 * @param outFile
	 * @param numStepsToReport the copy will report this number of steps
	 * @throws Exception
	 */
	public void copyFileToFile(File in, File outFile, int numStepsToReport) throws Exception {

		File f = outFile;
		
		long flenght = in.length();
		int currStep = 0;
		long bytesPerStep = flenght / numStepsToReport;
		long bytesCopied = 0;
		
		FileInputStream fis = new FileInputStream(in);
		FileOutputStream fos = new FileOutputStream(f);
		byte[] buf = new byte[1024];
		int i = 0;
		while ((i = fis.read(buf)) != -1) {
			fos.write(buf, 0, i);
			bytesCopied+=i;
			int newStep = (int)(bytesCopied / bytesPerStep);
			if (newStep>currStep){
				reportSteps(newStep-currStep);
				currStep=newStep;
			}
		}
		fis.close();
		fos.close();
	}


	/**
	 * Number of steps this subtask report
	 */
	public int getFinalStep() {
		return 100;
	}


	/**
	 * runs the exportation
	 */
	public void run() {
		export();
	}
	
	/**
	 * Writes the layer attributes on the gvsig mobile xml project file 
	 * 
	 * @param name
	 * @param path
	 */
	private void writeXML(String name, String path){
		//<Type>
		xml.writeTag(XmlProjectTags.TYPE, "RASTER");
		//</Type>
		
		//<Path>
		xml.writeTag(XmlProjectTags.PATH, path);
		//</Path>
		
		//<Name>
		xml.writeTag(XmlProjectTags.NAME, name);
		//</Name>
	}

}
