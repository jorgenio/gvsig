package es.prodevelop.gvsig.exportMobile.layerexporters;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.cresques.cts.ICoordTrans;

import com.hardcode.driverManager.Driver;
import com.hardcode.gdbms.engine.values.Value;
import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.core.DefaultFeature;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.IFeature;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.core.ShapeFactory;
import com.iver.cit.gvsig.fmap.core.v02.FLabel;
import com.iver.cit.gvsig.fmap.drivers.DriverAttributes;
import com.iver.cit.gvsig.fmap.drivers.DriverIOException;
import com.iver.cit.gvsig.fmap.drivers.FieldDescription;
import com.iver.cit.gvsig.fmap.drivers.ILayerDefinition;
import com.iver.cit.gvsig.fmap.drivers.SHPLayerDefinition;
import com.iver.cit.gvsig.fmap.drivers.shp.IndexedShpDriver;
import com.iver.cit.gvsig.fmap.edition.DefaultRowEdited;
import com.iver.cit.gvsig.fmap.edition.EditionException;
import com.iver.cit.gvsig.fmap.edition.IWriter;
import com.iver.cit.gvsig.fmap.edition.ShpSchemaManager;
import com.iver.cit.gvsig.fmap.edition.writers.shp.ShpWriter;
import com.iver.cit.gvsig.fmap.layers.FBitSet;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrAnnotation;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.fmap.layers.ReadableVectorial;
import com.iver.cit.gvsig.fmap.layers.SelectableDataSource;
import com.iver.cit.gvsig.fmap.rendering.XmlBuilder;
import com.iver.cit.gvsig.geoprocess.core.fmap.DefinitionUtils;
import com.iver.utiles.swing.threads.AbstractMonitorableTask;
import com.iver.utiles.swing.threads.IMonitorableTask;

import es.prodevelop.gvsig.exportMobile.files.NewDir;
import es.prodevelop.gvsig.exportMobile.layerexporters.clip.ClipProcess;
import es.prodevelop.gvsig.exportMobile.rectangle.DrawRectangle;
import es.prodevelop.gvsig.exportMobile.xml.Style;
import es.prodevelop.gvsig.exportMobile.xml.XmlProjectTags;

/**
 * Exportation subtask to export a vectorial layer
 * It supports selecting features by rect, clipping
 * features by rect and exporting all features
 * It splits the input data in polygons, lines and
 * points to save it as shapefiles.
 * 
 * @author jcarras
 *
 */
public class VectorialExporterTask extends ExporterSubTask {

	public static final int CLIPRECT = 1;
	public static final int SELECTRECT = 2;
	public static final int ALL = 3;
	
	protected String destPath;
	protected String XMLDefinition = "";
	protected int processType;
	protected FieldDescription[] fieldsDescription;
	protected FLyrVect inLayerVec;
	
	public static final  String CLIPSUFFIX = "_clip";
	public static final  String SELECTSUFFIX = "_select";
	public static final  String ALLSUFFIX = "_copy";
	public static final  String POINTSSUFFIX = "_points";
	public static final  String POLYGONSSUFFIX = "_polygons";
	public static final  String LINESSUFFIX = "_lines";
	public static final  String NOSUFFIX = "";
	public static final  String SHAPESUFFIX = ".shp";
	
	/**
	 * Returns the suffix associated wit the input process type
	 * @param processType
	 * @return the corresponding suffix
	 */
	public String getSuffix(int processType){
		if (processType == CLIPRECT)
			return CLIPSUFFIX;
		if (processType == SELECTRECT)
			return SELECTSUFFIX;
		if (processType == ALL)
			return ALLSUFFIX;
		return NOSUFFIX;
	}

	/**
	 * Remove the defined suffixes of the input string with the objective
	 * of not adding twice the suffixes on several exportations
	 * 
	 * @param name
	 * @return the input string without the suffixes
	 */
	public String removeSuffixes(String name){
		String res = name;
		
		String[] suf = new String[7];
		suf[0] = CLIPSUFFIX.toLowerCase();
		suf[1] = SELECTSUFFIX.toLowerCase();
		suf[2] = ALLSUFFIX.toLowerCase();
		suf[3] = POINTSSUFFIX.toLowerCase();
		suf[4] = POLYGONSSUFFIX.toLowerCase();
		suf[5] = LINESSUFFIX.toLowerCase();
		suf[6] = SHAPESUFFIX.toLowerCase();
		
		boolean modified=true;
		while(modified){
			modified=false;
			for (int i = 0; i<7; i++){
				if (name.toLowerCase().endsWith(suf[i])){
					name=name.substring(0,name.toLowerCase().lastIndexOf(suf[i]));
					modified=true;
				}
			}
		}
		
		return res;
	}

	private Logger logger = Logger.getLogger(this.getClass());

/**
 * Constructor with all the needed attributes
 * @param parentProcess
 * @param layer
 * @param rect
 * @param destPath
 * @param processType One of those VectorialExporterTask.CLIPRECT, VectorialExporterTask.ALL or VectorialExporterTask.SELECTRECT 
 * @param fieldsDescription Description of the fields of the resulting layers
 * @param xml 
 */
	public VectorialExporterTask(AbstractMonitorableTask parentProcess,
			FLayer layer, Rectangle2D rect, String destPath, int processType,
			FieldDescription[] fieldsDescription, XmlBuilder xml) {

		super(parentProcess, layer, rect, xml);
		this.destPath = destPath;
		this.processType = processType;
		this.fieldsDescription = fieldsDescription;
		this.inLayerVec = (FLyrVect) layer;

	}

	private void writeFeatures(IWriter writer, Driver reader, FBitSet bitSet, int numStepsToReport)
			throws DriverException, DriverIOException, EditionException {
		writer.preProcess();
		ReadableVectorial va = inLayerVec.getSource();
		va.start();
		ICoordTrans ct = inLayerVec.getCoordTrans();
		SelectableDataSource sds = this.inLayerVec.getRecordset();
		DriverAttributes attr = va.getDriverAttributes();
		boolean bMustClone = false;
		if (attr != null) {
			if (attr.isLoadedInMemory()) {
				bMustClone = attr.isLoadedInMemory();
			}
		}
		
		long shapesPerStep = bitSet.cardinality() / numStepsToReport;
		int stepsPerShape = 0;
		if (shapesPerStep==0)
			stepsPerShape = numStepsToReport / bitSet.cardinality();
			
		int counter = 0;
		for (int i = bitSet.nextSetBit(0); i >= 0; i = bitSet.nextSetBit(i + 1)) {
			try {
				IGeometry geom = va.getShape(i);

				if (inLayerVec instanceof FLyrAnnotation
						&& geom.getGeometryType() != FShape.POINT) {
					Point2D p = FLabel.createLabelPoint((FShape) geom
							.getInternalShape());
					geom = ShapeFactory.createPoint2D(p.getX(), p.getY());
				}
				if (ct != null) {
					if (bMustClone)
						geom = geom.cloneGeometry();
					geom.reProject(ct);
				}

				
				if (parent.isCanceled())
					break;

				if (geom != null) {

					Value[] values = extractValues(sds, fieldsDescription, i);
					IFeature feat = new DefaultFeature(geom, values, "" + i);
					DefaultRowEdited edRow = new DefaultRowEdited(feat,
							DefaultRowEdited.STATUS_ADDED, i);
					edRow.setAttributes(values);
					writer.process(edRow);
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			} finally{
				counter++;
				if (shapesPerStep>0){
					if (counter % shapesPerStep == 0){
						reportStep();
					}
				}else {
					reportSteps(stepsPerShape);
				}
			}
		}

		writer.postProcess();
		va.stop();


	}
	
	private void clipFeatures(IWriter writer, Driver reader, FBitSet bitSet, ShpSchemaManager schemaManager, ILayerDefinition outputDef, int numStepsToReport){
		ClipProcess clip = new ClipProcess(
				inLayerVec, 
				fieldsDescription, bitSet);

		/* build the second operand */
		FLyrVect lyrRect = DrawRectangle.createRectangleLayer(rect, inLayer.getProjection());
		clip.setSecondOperand(lyrRect);
		clip.setResultLayerDefinition(outputDef);
		clip.setResultLayerProperties(writer, schemaManager);
		try {
			clip.checkPreconditions();
			IMonitorableTask task = clip.createTask();
			task.run();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			reportSteps(numStepsToReport);
		}
		
	}

	/**
	 * Makes the exportation
	 */
	public void  export() {
		setNote(PluginServices.getText(this, "exporting_") + " " + inLayer.getName());
		
		SHPLayerDefinition definition = new SHPLayerDefinition();
		 try {
			definition = DefinitionUtils.
				createLayerDefinition(inLayerVec);
		} catch (com.hardcode.gdbms.engine.data.driver.DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		definition.setFieldsDesc(fieldsDescription);
		String basename = inLayer.getName();
		if (basename.toLowerCase().endsWith(".shp"))
			basename=basename.substring(0,basename.lastIndexOf("."));

		// POINTS
		try{
			FBitSet bitSet;
			if (processType==ALL)
				bitSet = queryByRectAndType(null, FShape.POINT);
			else
				bitSet = queryByRectAndType(rect, FShape.POINT);
			logger.debug("Found " + bitSet.cardinality() + " points");
			if (bitSet.cardinality() > 0) {
				initXML();
				ShpWriter writer = (ShpWriter) LayerFactory.getWM().getWriter(
						"Shape Writer");
				
				File tempFile = NewDir.getTempShpFile();
				
				definition.setFile(tempFile);
				definition.setName(tempFile.getName());
				definition.setShapeType(FShape.POINT);
				writer.setFile(tempFile);
				writer.initialize(definition);
				Driver driver = getOpenShpDriver(tempFile);
				ShpSchemaManager schemaManager = new ShpSchemaManager(tempFile.getAbsolutePath());

				if (processType==CLIPRECT)
					clipFeatures(writer, driver, bitSet, schemaManager,definition,33);
				else
					writeFeatures(writer, driver, bitSet, 33);
									
				String lyrName = removeSuffixes(basename);
				lyrName += getSuffix(processType);
				lyrName += POINTSSUFFIX;
				File filePoints = new File(destPath + File.separator + lyrName + SHAPESUFFIX);
				filePoints = getFreeFile(filePoints);
				NewDir.copyShpDbfShxTempToFinal(tempFile, filePoints);
				writeXML(lyrName, FShape.POINT, filePoints.getName());
			}
		} catch (EditionException e1){
			e1.printStackTrace();
		} catch (IOException e2){
			e2.printStackTrace();
		} catch (DriverIOException e3){
			e3.printStackTrace();
		} catch (DriverException e4){
			e4.printStackTrace();
		} finally{
			closeXML();
			//reportStep();
		}

		// LINES
		try{
			FBitSet bitSet;
			if (processType==ALL)
				bitSet = queryByRectAndType(null, FShape.LINE);
			else
				bitSet = queryByRectAndType(rect, FShape.LINE);
			logger.debug("Found " + bitSet.cardinality() + " lines");
			if (bitSet.cardinality() > 0) {
				initXML();
				ShpWriter writer = (ShpWriter) LayerFactory.getWM().getWriter(
						"Shape Writer");
				
				File tempFile = NewDir.getTempShpFile();
				
				definition.setFile(tempFile);
				definition.setName(tempFile.getName());
				definition.setShapeType(FShape.LINE);
				writer.setFile(tempFile);
				writer.initialize(definition);
				Driver driver = getOpenShpDriver(tempFile);
				ShpSchemaManager schemaManager = new ShpSchemaManager(tempFile.getAbsolutePath());

				if (processType==CLIPRECT)
					clipFeatures(writer, driver, bitSet, schemaManager,definition,33);
				else
					writeFeatures(writer, driver, bitSet, 33);
								
				String lyrName = removeSuffixes(basename);
				lyrName += getSuffix(processType);
				lyrName += LINESSUFFIX;
				File filePoints = new File(destPath + File.separator + lyrName + SHAPESUFFIX);
				filePoints = getFreeFile(filePoints);
				NewDir.copyShpDbfShxTempToFinal(tempFile, filePoints);

				writeXML(lyrName, FShape.LINE, filePoints.getName());
			}
		} catch (EditionException e1){
			e1.printStackTrace();
		} catch (IOException e2){
			e2.printStackTrace();
		} catch (DriverIOException e3){
			e3.printStackTrace();
		} catch (DriverException e4){
			e4.printStackTrace();
		} finally{
			closeXML();
			//reportStep();
		}

		// POLYGONS
		try{
			FBitSet bitSet;
			if (processType==ALL)
				bitSet = queryByRectAndType(null, FShape.POLYGON);
			else
				bitSet = queryByRectAndType(rect, FShape.POLYGON);
			logger.debug("Found " + bitSet.cardinality() + " polygons");
			if (bitSet.cardinality() > 0) {
				initXML();
				ShpWriter writer = (ShpWriter) LayerFactory.getWM().getWriter(
						"Shape Writer");
				
				File tempFile = NewDir.getTempShpFile();
				
				definition.setFile(tempFile);
				definition.setName(tempFile.getName());
				definition.setShapeType(FShape.POLYGON);
				
				
				writer.setFile(tempFile);
				writer.initialize(definition);
				Driver driver = getOpenShpDriver(tempFile);
				ShpSchemaManager schemaManager = new ShpSchemaManager(tempFile.getAbsolutePath());

				if (processType==CLIPRECT){
					clipFeatures(writer, driver, bitSet, schemaManager, definition,33);
					
				}
				else
					writeFeatures(writer, driver, bitSet, 33);
				
				String lyrName = removeSuffixes(basename);
				lyrName += getSuffix(processType);
				lyrName += POLYGONSSUFFIX;
				File filePoints = new File(destPath + File.separator + lyrName + SHAPESUFFIX);
				filePoints = getFreeFile(filePoints);
				NewDir.copyShpDbfShxTempToFinal(tempFile, filePoints);
				writeXML(lyrName, FShape.POLYGON, filePoints.getName());
			}
		} catch (EditionException e1){
			e1.printStackTrace();
		} catch (IOException e2){
			e2.printStackTrace();
		} catch (DriverIOException e3){
			e3.printStackTrace();
		} catch (DriverException e4){
			e4.printStackTrace();
		} finally{
			closeXML();
			reportToEnd();
		}

		
	}

	/**
	 * It iterates over every feature to return a bitSet with the features of
	 * the input type and inside the input rectangle
	 * @param rect
	 * @param type
	 * @return
	 * @throws DriverException
	 */
	public FBitSet queryByRectAndType(Rectangle2D rect, int type)
			throws DriverException {

		ReadableVectorial va = inLayerVec.getSource();
		ICoordTrans ct = inLayerVec.getCoordTrans();

		FBitSet bitset = new FBitSet();
		try {
			va.start();
			int numShp = va.getShapeCount();
			DriverAttributes attr = va.getDriverAttributes();
			boolean bMustClone = false;
			if (attr != null) {
				if (attr.isLoadedInMemory()) {
					bMustClone = attr.isLoadedInMemory();
				}
			}

			for (int i = 0; i < numShp; i++) {
				// if it's not the type expected we discard it
				IGeometry geom = va.getShape(i);
				if (geom == null)
					continue;
				if (type != (geom.getGeometryType() % 512))
					continue;
				// idRec = (Integer) lyr.getSource().getShape(i).get;
				// index = i;//dRec.intValue();
				// if we want the whole features we select it
				if (rect == null) {
					bitset.set(i, true);
					continue;
				}
				// IGeometry geom = shp;

				if (ct != null) {
					if (bMustClone)
						geom = geom.cloneGeometry();
					geom.reProject(ct);
				}
				if (geom.intersects(rect))
					bitset.set(i, true);
			}
			va.stop();
		} catch (DriverIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bitset;

	}

	
	private IndexedShpDriver getOpenShpDriver(File file) throws IOException {

		IndexedShpDriver drv = new IndexedShpDriver();
		if (!file.exists()) {

			file.createNewFile();
			File newFileSHX = new File(file.getAbsolutePath().replaceAll(
					"[.]shp", ".shx"));
			newFileSHX.createNewFile();
			File newFileDBF = new File(file.getAbsolutePath().replaceAll(
					"[.]shp", ".dbf"));
			newFileDBF.createNewFile();
		}
		drv.open(file);
		return drv;
	}

	private Value[] extractValues(SelectableDataSource sds2,
			FieldDescription[] descripExLyr2, int i)
			throws com.hardcode.gdbms.engine.data.driver.DriverException {

		FieldDescription[] desLyrTot = sds2.getFieldsDescription();
		int[] indices = getArrayIndex(descripExLyr2, desLyrTot);
		Value[] newvalue = new Value[indices.length];

		try {
			// geom description
			Value[] val = sds2.getRow(i);

			for (int k = 0; k < indices.length; k++) {

				int indx = indices[k];
				Value valu = val[indx];
				newvalue[k] = valu;
			}

		} catch (com.hardcode.gdbms.engine.data.driver.DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return newvalue;
	}

	/**
	 * Compare two FieldsDescription[], and show which atributes agreed. Then we
	 * create a new array index of this atributes
	 * 
	 * @param descripSel,
	 *            selected layer`s description
	 * @param descripTot,
	 *            original layer`s description
	 * @return
	 */
	private int[] getArrayIndex(FieldDescription[] descripSel,
			FieldDescription[] descripTot) {

		int[] index = new int[descripSel.length];
		int cont = 0;

		for (int i = 0; i < descripTot.length; i++) {

			String nameFieldTot = descripTot[i].getFieldName();
			for (int j = 0; j < descripSel.length; j++) {

				String nameFielSele = descripSel[j].getFieldName();
				if (nameFieldTot.equals(nameFielSele)) {

					cont = i;
					index[j] = cont;

				}
			}
		}
		return index;
	}

	/**
	 * Number of steps the Exportation will report
	 */
	public int getFinalStep() {
		return 100;
	}

	/**
	 * Runs the exportation task
	 */
	public void  run() {
		export();
	}
	
	/**
	 * Writes the layer attributes to the input xml
	 * @param name
	 * @param shpType
	 * @param path
	 */
	private void writeXML(String name, int shpType, String path){
		//xml.openTag(XmlProjectTags.LAYER);
		//<Type>
		xml.writeTag(XmlProjectTags.TYPE, "SHP");
		//</Type>
		
		//<Path>						
		xml.writeTag(XmlProjectTags.PATH, path);
		//</Path>
		
		//<Name>
		xml.writeTag(XmlProjectTags.NAME, name);
		//</Name>
		
		//<Visible>
		String isVisible = (inLayer.isVisible()) ? "1" : "0"; 
		xml.writeTag(XmlProjectTags.VISIBLE, isVisible);
		//</Visible>
		
		// scale max min
		double scaleMax = inLayer.getMaxScale();
		double scaleMin = inLayer.getMinScale();
		xml.writeTag(XmlProjectTags.SCALE_MAX, "" + Double.toString(scaleMax));
		xml.writeTag(XmlProjectTags.SCALE_MIN, "" + Double.toString(scaleMin));

		Style style = new Style();
		style.setLayer(inLayerVec,shpType);
		style.setXML(xml);
		style.createXML();
		
		//xml.closeTag();
	}
}
