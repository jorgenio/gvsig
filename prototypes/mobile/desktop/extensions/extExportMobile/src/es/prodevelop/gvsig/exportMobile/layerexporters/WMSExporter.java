package es.prodevelop.gvsig.exportMobile.layerexporters;

import java.awt.geom.Rectangle2D;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.rendering.XmlBuilder;
import com.iver.utiles.swing.threads.AbstractMonitorableTask;

import es.prodevelop.gvsig.exportMobile.xml.XMLwms;

/**
 * Exports a gvsig wms layer to a gvsig mobile wms layer
 * @author Anabel Moreno
 *
 */
public class WMSExporter extends ExporterSubTask{
		
	/**
	 * Constructor with all the required attributes
	 * @param parentProcess
	 * @param layer
	 * @param rect
	 * @param xml
	 */
	public WMSExporter(AbstractMonitorableTask parentProcess, FLayer layer,
			Rectangle2D rect, XmlBuilder xml) {
		super(parentProcess, layer, rect, xml);
		
	}

	
	/**
	 * Adds to the xml the layer attributes
	 */
	public void export(){
		initXML();
		setNote(PluginServices.getText(this, "exporting_")  + " " +  inLayer.getName());
		XMLwms xmlwms = new XMLwms();
		xmlwms.setxml(inLayer,xml);
		xmlwms.createXML();
		reportToEnd();
		closeXML();
	}

	/**
	 * Number of steps the layer will report
	 */
	public int getFinalStep() {
		return 100;
	}

	/**
	 * Starts the exportation task
	 */
	public void run() {
		export();
	}
}
