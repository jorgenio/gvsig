package es.prodevelop.gvsig.exportMobile.layerexporters.clip;

import java.awt.geom.Rectangle2D;
import java.util.Map;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.core.v02.FConverter;
import com.iver.cit.gvsig.fmap.drivers.DriverIOException;
import com.iver.cit.gvsig.fmap.drivers.FieldDescription;
import com.iver.cit.gvsig.fmap.drivers.ILayerDefinition;
import com.iver.cit.gvsig.fmap.edition.ISchemaManager;
import com.iver.cit.gvsig.fmap.edition.IWriter;
import com.iver.cit.gvsig.fmap.layers.FBitSet;
import com.iver.cit.gvsig.fmap.layers.FLayerVectorialDB;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.operations.strategies.Strategy;
import com.iver.cit.gvsig.fmap.operations.strategies.StrategyManager;
import com.iver.cit.gvsig.fmap.operations.strategies.VisitException;
import com.iver.cit.gvsig.geoprocess.core.fmap.AbstractGeoprocess;
import com.iver.cit.gvsig.geoprocess.core.fmap.GeoprocessException;
import com.iver.cit.gvsig.geoprocess.core.fmap.IOverlayGeoprocess;
import com.iver.cit.gvsig.geoprocess.core.fmap.XTypes;
import com.iver.cit.gvsig.geoprocess.impl.clip.fmap.ClipVisitor;
import com.iver.cit.gvsig.geoprocess.impl.convexhull.fmap.ScalableUnionVisitor;
import com.iver.utiles.swing.threads.CancellableMonitorable;
import com.iver.utiles.swing.threads.DefaultCancellableMonitorable;
import com.iver.utiles.swing.threads.IMonitorableTask;
import com.vividsolutions.jts.geom.Geometry;

//ClipProcess is as ClipGeoprocess but createLayerDefinition method
//is diferent because we have to add some original layer`s atributes (new description)
//to export not all layer`s atributes

public class ClipProcess extends AbstractGeoprocess implements
		IOverlayGeoprocess {

	private FLyrVect auxLayer;

	private boolean onlyFirstLayerSelection = false;

	private boolean onlyClipLayerSelection = false;

	private ClipVisitor visitor;

	private ILayerDefinition resultLayerDefinition;

	private FieldDescription[] description;
	
	//private FLayerWithNewName firstWN;
	private FLyrVect firstWN;
	private FBitSet bitset;

	public ClipProcess(FLyrVect lyr_wn, FieldDescription[] desc) {

		firstWN = lyr_wn;
		setFirstOperand(lyr_wn);
		description = desc;
	}

	public ClipProcess(FLyrVect lyr_wn, FieldDescription[] desc, FBitSet bitset) {

		firstWN = lyr_wn;
		setFirstOperand(lyr_wn);
		description = desc;
		this.bitset=bitset;
	}
	
	/** set parameters */
	public void setParameters(Map params) throws GeoprocessException {

		Boolean firstLayerSelection = (Boolean) params
				.get("firstlayerselection");
		if (firstLayerSelection != null)
			this.onlyFirstLayerSelection = firstLayerSelection.booleanValue();

		Boolean secondLayerSelection = (Boolean) params
				.get("secondlayerselection");
		if (secondLayerSelection != null)
			this.onlyClipLayerSelection = secondLayerSelection.booleanValue();

	}

	public void checkPreconditions() throws GeoprocessException {
		// TODO Auto-generated method stub
		if (firstLayer == null)
			throw new GeoprocessException("Clip: capa de entrada a null");
		if (auxLayer == null)
			throw new GeoprocessException("Clip: capa de clip a null");
		if (this.writer == null || this.schemaManager == null) {
			throw new GeoprocessException(
					"Operacion de clip sin especificar capa de resultados");
		}

		try {
			if (auxLayer.getShapeType() != XTypes.POLYGON
					&& (auxLayer.getShapeType() != XTypes.MULTI)) {
				throw new GeoprocessException(
						"La capa de recorte no es de polígonos");
			}
		} catch (com.iver.cit.gvsig.fmap.DriverException e) {
			throw new GeoprocessException(
					"Error al tratar de chequear si la capa de recorte es de polígonos");
		}
	}

	public void process() throws GeoprocessException {
		// TODO Auto-generated method stub
		Geometry clippingGeometry = null;
		try {
			clippingGeometry = computeJtsClippingPoly();
		} catch (Exception e) {
			throw new GeoprocessException(
					"Error calculando el convex hull de la clipping layer");
		}
		visitor = new ClipVisitor(clippingGeometry, resultLayerDefinition,
				schemaManager, writer);
		Strategy strategy = StrategyManager.getStrategy(firstLayer);
		try {
			if (onlyFirstLayerSelection) {
				strategy.process(visitor, firstLayer.getRecordset()
						.getSelection());
			} else {
				strategy.process(visitor);
			}
		} catch (Exception e) {
			throw new GeoprocessException(
					"Problemas al leer los datos de la capa a clipear");
		}
	}

	public ILayerDefinition createLayerDefinition() {
		
		/*LayerDescription descripLayer = new LayerDescription(
				resultLayerDefinition, firstWN, description);
		resultLayerDefinition = descripLayer.lyrDescrip();*/
		return resultLayerDefinition;

	}

	public void setSecondOperand(FLyrVect secondLayer) {
		// TODO Auto-generated method stub
		this.auxLayer = secondLayer;
	}

	public void setFirstOperand(FLyrVect lyr) {
		// TODO Auto-generated method stub
		this.firstLayer = lyr;
	}

	public IMonitorableTask createTask() {
		final CancellableMonitorable cancelMonitor = createCancelMonitor();

		return new IMonitorableTask() {
			String CLIP_GEOP_MSG = PluginServices.getText(this, "Mensaje_clip");

			String CLIP_MESSAGE = PluginServices.getText(this,
					"Mensaje_procesando_clip_primero");

			String INTERS_MESSAGE = PluginServices.getText(this,
					"Mensaje_procesando_clip_segundo");

			String of = PluginServices.getText(this, "De");

			String currentMsg = CLIP_MESSAGE;

			private boolean finished = false;

			public int getInitialStep() {
				return cancelMonitor.getInitialStep();
			}

			public int getFinishStep() {
				return cancelMonitor.getFinalStep();
			}

			public int getCurrentStep() {
				return cancelMonitor.getCurrentStep();
			}

			public String getStatusMessage() {
				return CLIP_GEOP_MSG;
			}

			public String getNote() {
				return currentMsg + " " + getCurrentStep() + " " + of + " "
						+ getFinishStep();
			}

			public void cancel() {
				((DefaultCancellableMonitorable) cancelMonitor)
						.setCanceled(true);
				ClipProcess.this.cancel();
			}

			public void run() {

				Geometry clippingGeometry = null;
				try {
					clippingGeometry = computeJtsClippingPoly();
				} catch (Exception e) {
					e.printStackTrace();
				}
				currentMsg = INTERS_MESSAGE;
				visitor = new ClipVisitor(clippingGeometry,
						resultLayerDefinition, schemaManager, writer);
				Strategy strategy = StrategyManager.getStrategy(firstLayer);
				Rectangle2D clippingRect = FConverter
						.convertEnvelopeToRectangle2D(clippingGeometry
								.getEnvelopeInternal());
				try {
					if (bitset!=null){
						visitor.setSelection(bitset);
					}
					strategy.process(visitor, clippingRect, cancelMonitor);

				} catch (Exception e) {
					e.printStackTrace();
				}
				finished = true;
			}

			public boolean isDefined() {
				return cancelMonitor.isDeterminatedProcess();
			}

			public boolean isCanceled() {
				return cancelMonitor.isCanceled();
			}

			public boolean isFinished() {
				return finished;
			}
		};
	}

	private Geometry computeJtsClippingPoly()
			throws com.iver.cit.gvsig.fmap.DriverException, VisitException {

		ScalableUnionVisitor visitor = new ScalableUnionVisitor();

		Strategy strategy = StrategyManager.getStrategy(auxLayer);
		if (onlyClipLayerSelection) {
			strategy.process(visitor, auxLayer.getRecordset().getSelection());
		} else {
			strategy.process(visitor);
		}
		return visitor.getJtsConvexHull();
	}

	public void setResultLayerProperties(IWriter writer,
			ISchemaManager schemaManager) {
		this.writer = writer;
		this.schemaManager = schemaManager;

	}

	private DefaultCancellableMonitorable createCancelMonitor() {
		DefaultCancellableMonitorable monitor = new DefaultCancellableMonitorable();
		monitor.setInitialStep(0);
		monitor.setDeterminatedProcess(true);
		int clipSteps = 0;
		try {
			if (onlyClipLayerSelection) {
				FBitSet selection = auxLayer.getRecordset().getSelection();
				clipSteps = selection.cardinality();
			} else {
				clipSteps = auxLayer.getSource().getShapeCount();

			}
			int firstSteps = 0;
			if (onlyFirstLayerSelection) {
				FBitSet selection = firstLayer.getRecordset().getSelection();
				firstSteps = selection.cardinality();
			} else {
				firstSteps = firstLayer.getSource().getShapeCount();

			}
			int totalSteps = clipSteps + firstSteps;
			monitor.setFinalStep(totalSteps);
		} catch (DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DriverIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return monitor;
	}

	public ILayerDefinition getResultLayerDefinition() {
		return resultLayerDefinition;
	}

	public void setResultLayerDefinition(ILayerDefinition resultLayerDefinition) {
		this.resultLayerDefinition = resultLayerDefinition;
	}
}
