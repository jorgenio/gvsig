package es.prodevelop.gvsig.exportMobile.layerexporters.clip;

import java.io.File;

import javax.swing.JOptionPane;

import com.iver.utiles.swing.threads.IMonitorableTask;
import com.iver.utiles.swing.threads.MonitorableDecoratorMainFirst;

import es.prodevelop.gvsig.exportMobile.files.NewDir;

public class PostProcessMonitorableDecoratorMainFirst extends
		MonitorableDecoratorMainFirst {
	
	
	private File tempFile, finalFile;
	
	public PostProcessMonitorableDecoratorMainFirst(
			IMonitorableTask mainTask,
			IMonitorableTask secondaryTask,
			File tmp, File finalf){
		
		super(mainTask, secondaryTask);
		
		tempFile = tmp;
		finalFile = finalf;
	}
	
	private void postProcess() {
		
		NewDir.copyShpDbfShxTempToFinal(tempFile, finalFile);
	}
	
	public void run() throws Exception {
		super.run();
		postProcess();
	}


}
