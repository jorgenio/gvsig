package es.prodevelop.gvsig.exportMobile.rectangle;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import org.cresques.cts.IProjection;

import com.hardcode.gdbms.engine.values.Value;
import com.hardcode.gdbms.engine.values.ValueFactory;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.core.FPolygon2D;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.GeneralPathX;
import com.iver.cit.gvsig.fmap.drivers.ConcreteMemoryDriver;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLayerVectorialDB;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.project.documents.view.gui.View;

/**
 * Utility to create layers with rectangles
 * 
 * @author Anabel Moreno
 * 
 */
public class DrawRectangle {

	private Rectangle2D rect;

	private FShape shp;

	private View v;

	private static String RECTANGLE = "Rectangle";

	public DrawRectangle(View view) {

		v = view;
	}

	public Rectangle2D getRectangle() {

		return rect;
	}

	/**
	 * Set the rectangle of the view
	 */
	public void setRectView() {

		rect = v.getMapControl().getViewPort().getAdjustedExtent();
	}

	/**
	 * Set rectangle of the extent
	 * @param recExtent
	 */
	public void setRectExtent(Rectangle2D recExtent) {

		rect = recExtent;
	}

	/**
	 * Create and draw a new rectangle poligon
	 * 
	 * @param mapCtrl
	 * @return layer
	 */
	public FLayer draw(MapControl mapCtrl) {

		/* DRIVER DEFINITION (SHAPE TYPE AND FIELDS) */
		ConcreteMemoryDriver driver = new ConcreteMemoryDriver();
		driver.setShapeType(FShape.POLYGON);

		ArrayList arrayFields = new ArrayList();
		arrayFields.add("ID");
		Value[] auxRow = new Value[1];

		driver.getTableModel().setColumnIdentifiers(arrayFields.toArray());

		/* GEOMETRY DEFINITION */
		GeneralPathX rectangulo = new GeneralPathX();

		// rect is the rectangle extent of the view
		/* NEW RECTANGLE */
		rectangulo.moveTo(rect.getMinX(), rect.getMinY());
		rectangulo.lineTo(rect.getMinX(), rect.getMaxY());
		rectangulo.lineTo(rect.getMaxX(), rect.getMaxY());
		rectangulo.lineTo(rect.getMaxX(), rect.getMinY());
		rectangulo.closePath();

		shp = new FPolygon2D(rectangulo);

		/* ATRIBUTES */
		auxRow[0] = ValueFactory.createValue(0);

		/* ADD RECORD */
		driver.addShape(shp, auxRow);

		/* CREATE AND ADD LAYER */
		FLayer lyr;
		String layerName = RECTANGLE;
		lyr = LayerFactory.createLayer(layerName, driver, mapCtrl
				.getProjection());

		return lyr;

	}
	/**
	 * Create and draw a new rectangle poligon
	 * 
	 * @param rect
	 * @return layer
	 */
	public static FLyrVect createRectangleLayer(Rectangle2D rect, IProjection projection) {

		/* DRIVER DEFINITION (SHAPE TYPE AND FIELDS) */
		ConcreteMemoryDriver driver = new ConcreteMemoryDriver();
		driver.setShapeType(FShape.POLYGON);

		ArrayList arrayFields = new ArrayList();
		arrayFields.add("ID");
		Value[] auxRow = new Value[1];

		driver.getTableModel().setColumnIdentifiers(arrayFields.toArray());

		/* GEOMETRY DEFINITION */
		GeneralPathX rectangulo = new GeneralPathX();

		// rect is the rectangle extent of the view
		/* NEW RECTANGLE */
		rectangulo.moveTo(rect.getMinX(), rect.getMinY());
		rectangulo.lineTo(rect.getMinX(), rect.getMaxY());
		rectangulo.lineTo(rect.getMaxX(), rect.getMaxY());
		rectangulo.lineTo(rect.getMaxX(), rect.getMinY());
		rectangulo.closePath();

		FPolygon2D shp = new FPolygon2D(rectangulo);

		/* ATRIBUTES */
		auxRow[0] = ValueFactory.createValue(0);

		/* ADD RECORD */
		driver.addShape(shp, auxRow);

		/* CREATE AND ADD LAYER */
		FLyrVect lyr;
		String layerName = RECTANGLE;
		lyr = (FLyrVect)LayerFactory.createLayer(layerName, driver, projection);

		return lyr;

	}
	
}
