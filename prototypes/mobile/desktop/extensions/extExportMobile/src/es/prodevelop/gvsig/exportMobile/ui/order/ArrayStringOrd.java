package es.prodevelop.gvsig.exportMobile.ui.order;

import java.util.ArrayList;

import com.iver.cit.gvsig.fmap.layers.FLayer;

/**
 * Build a order list layer`s name, if any name is duplicated, rename it
 * @author Anabel Moreno
 *
 */
public class ArrayStringOrd {

	ArrayList arrayString;

	ArrayList arrayLayers;

	public ArrayStringOrd(ArrayList arLyrs) {

		arrayLayers = arLyrs;
	}

	/**
	 * Get array laye`s name, (if any name is duplicate rename it)
	 * 
	 * @return array order strings
	 */
	public ArrayList getArrayStrings() {

		/* new ArrayList layer�s names */
		arrayString = new ArrayList();
		for (int i = 0; i < arrayLayers.size(); i++) {

			FLayer layer_nn = (FLayer) ((FLayerWithNewName)arrayLayers.get(i)).getLayer();
			String nameLayer = layer_nn.getName();
			arrayString.add(i, nameLayer);
		}

		/* rename list if any name is duplicate */
		// OrderLayers arrayListOrder = new OrderLayers();
		// int dimList = arrayLayers.size();
		// arrayListOrder.ordNotDuplicate(dimList, arrayString);

		return arrayString;

	}
}
