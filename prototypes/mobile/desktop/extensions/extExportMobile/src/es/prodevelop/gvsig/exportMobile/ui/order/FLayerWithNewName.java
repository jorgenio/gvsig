package es.prodevelop.gvsig.exportMobile.ui.order;

import java.util.ArrayList;
import java.util.Arrays;

import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLayers;

public class FLayerWithNewName {
	
	private FLayer lyr = null;
	private String suffix = "";
	private String differencer = "";

	public FLayerWithNewName(FLayer l) {
		lyr = l;
		suffix = getSuffix(lyr);
	}
	
	public static String getSuffix(FLayer l) {

		String suff = "";
		FLayer item = l.getParentLayer();
		while ((item != null) && (item.getParentLayer() != null)) {
			suff = suff + "." + item.getName();
			item = item.getParentLayer();
		}
		return removePointShp(suff);
	}
	
	public static String removePointShp(String str) {
		String aux = str.toLowerCase();
		aux = aux.replaceAll("\\.shp", "");
		return aux;
	}

	public String getNewName() {

		return removePointShp(lyr.getName()) + differencer + suffix;
	}
	
	public String getNewName(String op) {
		return removePointShp(lyr.getName()) + differencer + "." + op + suffix;
	}
	
	public String getNewNameNoDiff() {
		return removePointShp(lyr.getName()) + suffix;
	}
	
	
	public void setDiff(String d) {
		differencer = d;
	}

	public FLayer getLayer() {
		return lyr;
	}
	
	public static ArrayList getAll(FLayers root) {
		
		ArrayList resp = new ArrayList();
		addThisOrChildren(resp, root);
		setDiffs(resp);
		return resp;
		
	}
	

	private static void setDiffs(ArrayList l) {
		
		int sz = l.size();
		FLayerWithNewName[] non_sorted = new FLayerWithNewName[sz];
		for (int i=0; i<sz; i++)  non_sorted[i] = (FLayerWithNewName) l.get(i);
		Arrays.sort(non_sorted, new LayerNamesComparator());
		
		int ind = 1;
		for (int i=1; i<sz; i++) {
			
			FLayerWithNewName item_0 = non_sorted[i-1];
			FLayerWithNewName item_1 = non_sorted[i];
			if (item_0.getNewNameNoDiff().compareToIgnoreCase(item_1.getNewNameNoDiff()) == 0) {
				item_1.setDiff("_" + ind);
				ind++;
			} else {
				ind = 1;
			}
		}
	}

	public static void addThisOrChildren(ArrayList list, FLayer l) {
		
		if ((! l.isOk()) || (! l.isAvailable())) {
			// excluded layers
			return;
		}
		
		if (l instanceof FLayers) {
			FLayers col = (FLayers) l;
			for (int i=0; i<col.getLayersCount(); i++) {
				addThisOrChildren(list, col.getLayer(i));
			}
		} else {
			list.add(new FLayerWithNewName(l));
		}
		
	}

	public String getOriginalName() {
		return lyr.getName();
	}

}
