package es.prodevelop.gvsig.exportMobile.ui.order;

import java.util.Comparator;

public class LayerNamesComparator implements Comparator {

	public  LayerNamesComparator() {}
	
	public int compare(Object arg0, Object arg1) {
		
		if ((arg0 instanceof FLayerWithNewName) && (arg0 instanceof FLayerWithNewName)) {
			
			FLayerWithNewName a0 = (FLayerWithNewName) arg0;
			FLayerWithNewName a1 = (FLayerWithNewName) arg1;
			return a0.getNewNameNoDiff().compareToIgnoreCase(a1.getNewNameNoDiff());
			
		} else {
			
			return arg0.toString().compareTo(arg1.toString());
			
		}
	}

}
