package es.prodevelop.gvsig.exportMobile.ui.order;

import java.util.ArrayList;

/**
 * Order a list layer
 * @author Anabel Moreno
 *
 */
public class OrderLayers {

	public OrderLayers() {

	}

	/**
	 * Get layer name and compare it with the others names, if this name is
	 * duplicate, rename it.
	 * 
	 * @param dimList
	 * @param array
	 */
	public void ordNotDuplicate(int dimList, ArrayList array) {

		if (dimList == 1) {
		} else {

			for (int i = 0; i < dimList; i++) {

				int cont = 1;

				/* get name object i */
				String namelayer = (String) array.get(i);

				if (cont == 1) {

					int sig = i + 1;
					for (int j = sig; j < dimList; j++) {

						/* get name object j */
						String nameComp = (String) array.get(j);

						/* compare names */
						if (namelayer.equals(nameComp)) {

							cont = cont + 1;
							nameComp = cont + nameComp;
							/* rename object j */
							array.set(j, nameComp);
						}
					}
				}
			}
		}
	}
}
