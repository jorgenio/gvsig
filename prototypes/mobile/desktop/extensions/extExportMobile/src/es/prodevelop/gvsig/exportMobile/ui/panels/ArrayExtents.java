package es.prodevelop.gvsig.exportMobile.ui.panels;

import java.awt.geom.Rectangle2D;
import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.project.ProjectExtent;
import com.iver.cit.gvsig.project.documents.view.gui.View;

/**
 * Add to a list the view`s extents,and return the selected extents
 * @author Anabel Moreno
 *
 */
public class ArrayExtents {

	private ProjectExtent[] listExtent;

	private ProjectExtent extentSelected;

	/**
	 * Get extents of the view
	 */
	public ArrayExtents() {

		View view = (View) PluginServices.getMDIManager().getActiveWindow();
		listExtent = view.getModel().getProject().getExtents();
	}

	/**
	 * @return list extents
	 */
	public ProjectExtent[] getlistExt() {
		return listExtent;
	}

	/**
	 * Set extent selected in list extents
	 * 
	 * @param selecOpt
	 */
	public void setExtSelected(int selecOpt) {

		extentSelected = listExtent[selecOpt];
	}

	/**
	 * Get Extents rectangle
	 * 
	 * @return
	 */
	public Rectangle2D getRectExtents() {

		Rectangle2D rectExtent = extentSelected.getExtent().getBounds2D();
		return rectExtent;
	}
}
