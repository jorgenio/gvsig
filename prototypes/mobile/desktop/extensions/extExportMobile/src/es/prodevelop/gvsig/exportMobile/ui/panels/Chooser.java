package es.prodevelop.gvsig.exportMobile.ui.panels;

import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;
import com.iver.andami.PluginServices;


/**
 * Choose the output directory to export the new files
 * @author Anabel Moreno
 *
 */
public class Chooser {

	public File dirOutPut;

	public String nameDirOut;

	public Chooser() {

		JFileChooser jfc = new JFileChooser();

		/* to choose only directories files */
		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		if (jfc.showOpenDialog((Component) PluginServices.getMainFrame()) == JFileChooser.APPROVE_OPTION) {

			dirOutPut = jfc.getSelectedFile();
			nameDirOut = dirOutPut.getAbsolutePath();

		}
	}

	public String getNameDir() {

		return nameDirOut;
	}
}
