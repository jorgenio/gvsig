package es.prodevelop.gvsig.exportMobile.ui.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.apache.log4j.Logger;

import com.iver.andami.PluginServices;
import com.iver.andami.messages.Messages;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.fmap.drivers.FieldDescription;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrRaster;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.FLyrWMS;
import com.iver.cit.gvsig.project.ProjectExtent;
import com.iver.cit.gvsig.project.documents.view.gui.View;

import es.prodevelop.gvsig.exportMobile.LayerObject;
import es.prodevelop.gvsig.exportMobile.files.NewDir;
import es.prodevelop.gvsig.exportMobile.layerexporters.ExportProcess;
import es.prodevelop.gvsig.exportMobile.layerexporters.VectorialExporterTask;
import es.prodevelop.gvsig.exportMobile.rectangle.DrawRectangle;

/**
 * Principal panel extension 
 * @author Anabel Moreno
 *
 */

public class ExportPanel extends JPanel implements IWindow, ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(ExportPanel.class);
	
	private ArrayList listLyrsObj = new ArrayList();

	private ArrayList arLayers = new ArrayList();

	public ArrayList arrDescLayers;
	
	private File newdirout;

	private int cont = 0 ;
	private int indxsele;

	private JPanel topPanel = new JPanel();

	private JPanel bottomPanel = new JPanel();

	private JPanel topPathPanel = new JPanel();

	private JPanel topOptionsPanel = new JPanel();

	private JPanel centerPanel = new JPanel();

	private JPanel titlePanel = new JPanel();

	private JTextField pathImport;

	private JTextField nameProy;

	private JLabel path;
	private JLabel typLayer;
	private JLabel nomLayer;
	private JLabel atLayer;
	private JLabel cboLayer;
	private JLabel lyrInView;

	private JLabel proyect;

	private JButton execute;

	//private JButton synchro;

	private JButton pathBoton;

	private JButton cancel;

	private JButton save;

	private JButton open;

	private JCheckBox allSelect;

	private JRadioButton viewAc;

	private JRadioButton extent;

	private String namePath;

	//private String namProy = "";

	private String exportZone;

	private ButtonGroup actionsTopPanel;

	private boolean proyEmpty;

	private boolean dirEmpty;

	private JComboBox comboExtents;

	private JScrollPane scroll = new JScrollPane();

	private ProjectExtent[] listExtents;

	private PanelIntoScroll panelScroll;

	private View view;

	private DrawRectangle rect;

	private Rectangle2D rectangle;
	
	private static String EMPTY = "";

	private static String VIEW_ACTUAL = "VA";

	private static String EXTENT = "EN";

	private static int WIDTH_PANEL = 560;

	private static int HEIGHT_PANEL = 500;

	private static int HEIGHT_TOP_PANEL = 140;

	private static int HEIGTH_TOP_PATH_PANEL = 80;

	private static int HEIGTH_TOP_OPTIONS_PANEL = 60;

	private static int HEIGTH_BOTTOM_PANEL = 45;

	private static int HEIGTH_CENTER_PANEL = 30;
	
	private static int HEIGTH_TITLE_PANEL = 24;

	private static int PROY_X = 20;

	private static int PROY_Y = 10;

	private static int PROY_W = 100;

	private static int PROY_H = 20;

	private static int NPROY_X = 140;

	private static int NPROY_Y = 10;

	private static int NPROY_W = 400;

	private static int NPROY_H = 20;

	private static int PATH_X = 20;

	private static int PATH_Y = 45;

	private static int PATH_W = 100;

	private static int PATH_H = 20;

	private static int PATHIMPORT_X = 140;

	private static int PATHIMPORT_Y = 45;

	private static int PATHIMPORT_W = 350;

	private static int PATHIMPORT_H = 20;

	private static int PATHBUTTON_X = 510;

	private static int PATHBUTTON_Y = 40;

	private static int PATHBUTTON_W = 30;

	private static int PATHBUTTON_H = 30;

	private static int VIEWACT_X = 120;

	private static int VIEWACT_Y = 20;

	private static int VIEWACT_W = 100;

	private static int VIEWACT_H = 20;

	private static int EXTENT_X = 290;

	private static int EXTENT_Y = 20;

	private static int EXTENT_W = 80;

	private static int EXTENT_H = 20;

	private static int EXTENTCOMBO_X = 390;

	private static int EXTENTCOMBO_Y = 20;

	private static int EXTENTCOMBO_W = 120;

	private static int EXTENTCOMBO_H = 20;

	private static int EX_X = 15;

	private static int BUTT_Y = 10;

	private static int BUTT_W = 90;

	private static int BUTT_H = 25;

	private static int SY_X = 125;

	private static int CAN_X = 235;

	private static int OP_X = 345;

	private static int SAVE_X = 455;

	private static int ALLSEL_X = 30;

	private static int LABEL_Y = 5;

	private static int LABEL_H = 14;

	private static int ALLSEL_W = 17;

	private static int ALLSEL_H = 13;

	private static int TYPLYR_X = 77;

	private static int TYPLYR_W = 40;

	private static int LYRINVIEW_X = 135;

	private static int LYRINVIEW_W = 55;

	private static int NAMELAYER_X = 200;

	private static int NAMELAYER_W = 100;

	private static int ATLYR_X = 325;

	private static int ATLYR_W = 50;

	private static int CBOLYR_X = 450;

	private static int CBOLYR_W = 70;
	

	public WindowInfo getWindowInfo() {

		WindowInfo m_viewInfo = new WindowInfo(WindowInfo.MODALDIALOG);
		m_viewInfo.setWidth(566);
		m_viewInfo.setHeight(506);
		m_viewInfo.setTitle(PluginServices.getText(this, "exporMob"));
		return m_viewInfo;
	}

	/**
	 * Create export panel
	 * 
	 * @param arrLayers,
	 *            list with all layers
	 * @param listExt,
	 *            list extents
	 */
	public ExportPanel(ArrayList arrLayers, ProjectExtent[] listExt) {

		super();

		view = (View) PluginServices.getMDIManager().getActiveWindow();
		
		
		listExtents = listExt;
		arLayers = arrLayers;

		/* PRINCIPAL PANEL */
		setSize(WIDTH_PANEL, HEIGHT_PANEL);
		this.setLayout(new BorderLayout());

		/* TOP PANEL */
		topPanel.setLayout(new BorderLayout());
		topPanel.setSize(new Dimension(WIDTH_PANEL, HEIGHT_TOP_PANEL));

		path = new JLabel(PluginServices.getText(this, "direxport"));
		pathImport = new JTextField(EMPTY);
		pathImport.setEditable(false);
		pathImport.setBackground(Color.WHITE);

		nameProy = new JTextField(EMPTY);
		proyect = new JLabel(PluginServices.getText(this, "nomproy"));
		pathBoton = new JButton("...");
		viewAc = new JRadioButton(PluginServices.getText(this, "viewA"));
		viewAc.setSelected(true);
		extent = new JRadioButton(PluginServices.getText(this, "exent"));
		extent.setSelected(false);

		actionsTopPanel = new ButtonGroup();
		actionsTopPanel.add(viewAc);
		actionsTopPanel.add(extent);

		topPathPanel.setLayout(null);
		topPathPanel.setPreferredSize(new Dimension(WIDTH_PANEL,
				HEIGTH_TOP_PATH_PANEL));

		proyect.setBounds(PROY_X, PROY_Y, PROY_W, PROY_H);
		nameProy.setBounds(NPROY_X, NPROY_Y, NPROY_W, NPROY_H);
		path.setBounds(PATH_X, PATH_Y, PATH_W, PATH_H);
		pathImport.setBounds(PATHIMPORT_X, PATHIMPORT_Y, PATHIMPORT_W,
				PATHIMPORT_H);
		pathBoton.setBounds(PATHBUTTON_X, PATHBUTTON_Y, PATHBUTTON_W,
				PATHBUTTON_H);
		topPathPanel.add(proyect);
		topPathPanel.add(path);
		topPathPanel.add(nameProy);
		topPathPanel.add(pathImport);
		topPathPanel.add(pathBoton);

		topOptionsPanel.setLayout(null);
		topOptionsPanel.setPreferredSize(new Dimension(WIDTH_PANEL,
				HEIGTH_TOP_OPTIONS_PANEL));

		comboExtents = new JComboBox();
		comboExtents.setEnabled(false);
		comboExtents.addActionListener(this);

		int dimListExt = listExtents.length;
		if (dimListExt == 0) {

			extent.setEnabled(false);
			comboExtents.setEnabled(false);
		}
		if (dimListExt > 0) {

			extent.setEnabled(true);
			if (cont == 0) {
				for (int i = 0; i < dimListExt; i++) {

					String name = listExtents[i].getDescription();
					comboExtents.addItem(name);
					comboExtents.repaint();
				}
				cont++;
			}
		}

		viewAc.setBounds(VIEWACT_X, VIEWACT_Y, VIEWACT_W, VIEWACT_H);
		extent.setBounds(EXTENT_X, EXTENT_Y, EXTENT_W, EXTENT_H);
		comboExtents.setBounds(EXTENTCOMBO_X, EXTENTCOMBO_Y, EXTENTCOMBO_W,
				EXTENTCOMBO_H);
		topOptionsPanel.add(viewAc);
		topOptionsPanel.add(extent);
		topOptionsPanel.add(comboExtents);

		TitledBorder title1 = BorderFactory.createTitledBorder(PluginServices
				.getText(this, "actExtent"));
		title1.setTitleColor(Color.BLUE);
		topOptionsPanel.setBorder(title1);

		topPanel.add(topPathPanel, BorderLayout.NORTH);
		topPanel.add(topOptionsPanel, BorderLayout.SOUTH);

		/* BOTTOM PANEL */
		bottomPanel.setPreferredSize(new Dimension(WIDTH_PANEL,
				HEIGTH_BOTTOM_PANEL));
		bottomPanel.setLayout(null);

		execute = new JButton(PluginServices.getText(this, "acept"));
		execute.setBounds(EX_X, BUTT_Y, BUTT_W, BUTT_H);
		//synchro = new JButton(PluginServices.getText(this, "synchro"));
		//synchro.setBounds(SY_X, BUTT_Y, BUTT_W, BUTT_H);
		
		// ------------------------ jldominguez nov 2007
		// synchro.setVisible(false);
		// ------------------------
		
		cancel = new JButton(PluginServices.getText(this, "close"));
		cancel.setBounds(CAN_X, BUTT_Y, BUTT_W, BUTT_H);
		open = new JButton(PluginServices.getText(this, "open"));
		open.setBounds(OP_X, BUTT_Y, BUTT_W, BUTT_H);
		open.setEnabled(false);
		save = new JButton(PluginServices.getText(this, "save"));
		save.setBounds(SAVE_X, BUTT_Y, BUTT_W, BUTT_H);
		save.setEnabled(false);

		
		bottomPanel.add(execute);
		//bottomPanel.add(synchro);
		bottomPanel.add(cancel);
		bottomPanel.add(open);
		bottomPanel.add(save);

		/* DISABLED BUTTON */
		execute.setEnabled(false);

		/* CENTER PANEL */
		centerPanel.setSize(new Dimension(WIDTH_PANEL, HEIGTH_CENTER_PANEL));
		centerPanel.setLayout(new BorderLayout());

		TitledBorder title = BorderFactory.createTitledBorder(PluginServices
				.getText(this, "layers"));
		title.setTitleColor(Color.BLUE);
		centerPanel.setBorder(title);

		allSelect = new JCheckBox();
		allSelect.addActionListener(this);
		allSelect.setBackground(Color.LIGHT_GRAY);

		typLayer = new JLabel(PluginServices.getText(this, "tLayer"));
		lyrInView = new JLabel(PluginServices.getText(this, "lyrview"));
		nomLayer = new JLabel(PluginServices.getText(this, "nLayer"));
		atLayer = new JLabel(PluginServices.getText(this, "batLayer"));
		cboLayer = new JLabel(PluginServices.getText(this, "comLayer"));

		titlePanel.setPreferredSize(new Dimension(WIDTH_PANEL, HEIGTH_TITLE_PANEL));
		titlePanel.setLayout(null);
		titlePanel.setBackground(Color.LIGHT_GRAY);

		allSelect.setBounds(1 + PanelIntoScroll.EXPORT_X, LABEL_Y, ALLSEL_W, ALLSEL_H);
		typLayer.setBounds(1 + PanelIntoScroll.ICON_X, LABEL_Y, TYPLYR_W, LABEL_H);
		lyrInView.setBounds(1 + PanelIntoScroll.IN_VIEW_X, LABEL_Y, LYRINVIEW_W, LABEL_H);
		nomLayer.setBounds(1 + PanelIntoScroll.NAME_X, LABEL_Y, NAMELAYER_W, LABEL_H);
		atLayer.setBounds(1 + PanelIntoScroll.ATRIB_X, LABEL_Y, ATLYR_W, LABEL_H);
		cboLayer.setBounds(1 + PanelIntoScroll.CMBOATR_X, LABEL_Y, CBOLYR_W, LABEL_H);

		titlePanel.add(allSelect);
		titlePanel.add(typLayer);
		titlePanel.add(lyrInView);
		titlePanel.add(nomLayer);
		titlePanel.add(atLayer);
		titlePanel.add(cboLayer);

		rect = new DrawRectangle(view);
		rect.setRectView();
		rectangle = rect.getRectangle();
		
		panelScroll = new PanelIntoScroll(arLayers, this, rectangle);
		scroll.setViewportView(panelScroll.getPanel());
		
		// to activate selAll
		dimArrBx();

		centerPanel.add(scroll, BorderLayout.CENTER);
		centerPanel.add(titlePanel, BorderLayout.NORTH);

		pathBoton.addActionListener(this);
		execute.addActionListener(this);
		open.addActionListener(this);
		cancel.addActionListener(this);
		save.addActionListener(this);
		extent.addActionListener(this);
		viewAc.addActionListener(this);
		comboExtents.addActionListener(this);
		//synchro.addActionListener(this);

		/* ADD PANELS TO PRINCIPAL PANEL */
		this.add(topPanel, BorderLayout.NORTH);
		this.add(centerPanel, BorderLayout.CENTER);
		this.add(bottomPanel, BorderLayout.SOUTH);

	}

	/**
	 * To set enabled the executed button. If directory is ok and any layer is
	 * selected, the button is enabled
	 */
	public void activateOk() {

		dirEmpty = pathImport.getText().equals(EMPTY);

		if ((dirEmpty == false) && (allSelect.isSelected() == true)) {

			execute.setEnabled(true);
		} else {

			execute.setEnabled(false);
		}
	}

	/**
	 * Get the number of boxes selected and active the box All Selected
	 * 
	 * @return number of boxes selected
	 */
	public int dimArrBx() {

		int c = 0;
		int dim = panelScroll.getarrBox().size();
		for (int i = 0; i < dim; i++) {

			JCheckBox box = (JCheckBox) panelScroll.getarrBox().get(i);
			if (box.isSelected() == true)
				c++;
		}
		if (c == 0) {
			allSelect.setSelected(false);
		} else {
			allSelect.setSelected(true);

		}
		return c;
	}

	public void actionPerformed(ActionEvent arg0) {

		Object src = arg0.getSource();

		if (src == pathBoton) {

			Chooser fc = new Chooser();
			namePath = fc.getNameDir();

			/* COMPLETE TEXTBOX WITH THE PATH */
			pathImport.setText(namePath);
			dirEmpty = pathImport.getText().equals(EMPTY);
			proyEmpty = nameProy.getText().equals(EMPTY);
				
//			if (proyEmpty == false) {
//
//				namProy = pathImport.getText() + "/" + nameProy.getText();
//
//			}
			activateOk();
			return;
		}

	
		if (src == viewAc) {

			viewAc.setSelected(true);
			extent.setSelected(false);
			comboExtents.setEnabled(false);
			rect = new DrawRectangle(view);
			rect.setRectView();
			rectangle = rect.getRectangle();

			ArrayList arrL = panelScroll.getNewArrayLayers();
			
			for(int k=0;k<arrL.size();k++){
				
				LayerObject lo = (LayerObject) arrL.get(k);
				FLayer l = lo.getLyr();
				JPanel parentPanel = lo.getParent();
				parentPanel.remove(lo.getImgLbl());
							
				JLabel lbl = new JLabel();
				lo.setImage(lbl);
				JLabel newLabel = panelScroll.paintImage(l, rectangle, parentPanel,lo.getCombo());
				lo.setImage(newLabel);
				
				parentPanel.repaint();
				
			}
			return;
		}

		if (src == extent) {

			extent.setSelected(true);
			viewAc.setSelected(false);
			int dimListExtents = listExtents.length;
			if (dimListExtents == 0) {

				comboExtents.setEnabled(false);

			}
			if (dimListExtents > 0) {

				if (cont == 0) {
					for (int i = 0; i < dimListExtents; i++) {

						String name = listExtents[i].getDescription();
						comboExtents.addItem(name);
						comboExtents.repaint();
					}
					cont++;
				}
				comboExtents.setEnabled(true);
				indxsele = comboExtents.getSelectedIndex();
				rectangle = listExtents[indxsele].getExtent();
				
				
				ArrayList arrL = panelScroll.getNewArrayLayers();
				
				for(int k=0;k<arrL.size();k++){
					
					LayerObject lo = (LayerObject) arrL.get(k);
					FLayer l = lo.getLyr();
					JPanel parentPanel = lo.getParent();
					parentPanel.remove(lo.getImgLbl());
					
					JLabel newLabel = panelScroll.paintImage(l, rectangle, parentPanel,lo.getCombo());
					lo.setImage(newLabel);
					
					parentPanel.repaint();
					
				}
			}
			return;
		}

		if (src == comboExtents) {

			if (comboExtents.isEnabled()) {
				indxsele = comboExtents.getSelectedIndex();
				rectangle = listExtents[indxsele].getExtent();
				
				ArrayList arrL = panelScroll.getNewArrayLayers();
				
				for(int k=0;k<arrL.size();k++){
					
					LayerObject lo = (LayerObject) arrL.get(k);
					FLayer l = lo.getLyr();
					JPanel parentPanel = lo.getParent();
					parentPanel.remove(lo.getImgLbl());
					
					JLabel lbl = new JLabel();
					lo.setImage(lbl);
					
					JLabel newLabel = panelScroll.paintImage(l, rectangle, parentPanel,lo.getCombo());
					lo.setImage(newLabel);
					
					parentPanel.repaint();
					
				}
			}
			return;
		}

		if (src == allSelect) {
			FLayer lyr;

			listLyrsObj = panelScroll.getNewArrayLayers();
			
			if (allSelect.isSelected() == true) {

				for (int i = 0; i < listLyrsObj.size(); i++) {

					LayerObject lyrObj = (LayerObject) listLyrsObj.get(i);
					lyr = (FLayer) lyrObj.getLyr();
					JCheckBox aux = (JCheckBox) lyrObj.getBox();
					aux.setSelected(true);
					JButton buttonAux = (JButton) lyrObj.getButton();
					JComboBox comboAux = (JComboBox)lyrObj.getCombo();
					if(lyr instanceof FLyrVect){
						
						buttonAux.setEnabled(true);
						comboAux.setEnabled(true);
					}	
					if(lyr instanceof FLyrRaster)
						comboAux.setEnabled(true);
				}
				activateOk();
			}

			if (allSelect.isSelected() == false) {

				for (int i = 0; i < listLyrsObj.size(); i++) {

					LayerObject lyrObj = (LayerObject) listLyrsObj.get(i);
					lyr = (FLayer) lyrObj.getLyr();
					JCheckBox aux = (JCheckBox) lyrObj.getBox();
					aux.setSelected(false);
					JButton buttonAux = (JButton) lyrObj.getButton();
					JComboBox comboAux = (JComboBox)lyrObj.getCombo();
					buttonAux.setEnabled(false);
					comboAux.setEnabled(false);
				}
				activateOk();
			}
			return;
		}

		if (src == execute) {
			ExportProcess process = null;
			File exportFolder = null;
			
			if (!validPath(pathImport.getText()) || !validProjectName(nameProy.getText())) {
				
				return;
				
			} else {
				String namProy = pathImport.getText() + File.separator + nameProy.getText();
				process = new ExportProcess(
						rectangle,
						nameProy.getText(),
						namProy,
						view.getProjection());
				 
				exportFolder = new File(namProy);
				
				if (exportFolder.exists()) {
					
					int option = JOptionPane.showConfirmDialog(this,
							exportFolder.getAbsolutePath() + " "
							+ Messages.getString("folder_exists_delete"),
							Messages.getString("acept"),
							JOptionPane.YES_NO_OPTION);
					
					if (option == JOptionPane.NO_OPTION) {
						return;
					} else {
						deleteFilesIn(exportFolder);
					}
					
				} else {
					exportFolder.mkdirs();
				}
				
				// NewDir dirOut = new NewDir(namProy);
				// dirOut.ifExistDir();
				newdirout = exportFolder; 
				// get layer list
				
				listLyrsObj = panelScroll.getNewArrayLayers();				

				for (int i = 0; i < listLyrsObj.size(); i++) {

					LayerObject objLyr = (LayerObject)listLyrsObj.get(i);
					JCheckBox box = (JCheckBox) objLyr.getBox();
					FLayer layer2 = objLyr.getLyr();
					
					if (box.isSelected()==true) {
						logger.debug("Layer " + layer2.getName() + " to be exported");
						if(layer2 instanceof FLyrVect){
							JComboBox combo = objLyr.getCombo();
							// get active combo option
							int indxSel = combo.getSelectedIndex();
							String nameOption = (String) combo.getItemAt(indxSel);
							//Export clip,select, all layer
							FLyrVect lyr = (FLyrVect) layer2;
							FieldDescription[] desLayer = objLyr.getDescription();
							String name = lyr.getName();
							if (name.toLowerCase().endsWith(".shp"))
								name=name.substring(0,name.lastIndexOf("."));
							
							int processType = VectorialExporterTask.ALL;
							if (nameOption == PluginServices.getText(this, "cut")) {
								processType = VectorialExporterTask.CLIPRECT;
							}
							if (nameOption == PluginServices.getText(this, "select")) {
								processType = VectorialExporterTask.SELECTRECT;
							}
							if (nameOption == PluginServices.getText(this, "allyr")) {
								processType = VectorialExporterTask.ALL;
							}
							
							process.addVectorialTask(lyr, desLayer, processType);

						}
						if(layer2 instanceof FLyrRaster){
							process.addRasterTask(layer2);
							
						}
						if(layer2 instanceof FLyrWMS){
							process.addWMSTask(layer2);
						}
					}
				}			
			
			process.preProcess();
			PluginServices.cancelableBackgroundExecution(process);

			
			}
			return;
		}
		
		/* CLOSE WINDOW */
		if (src == cancel) {

				PluginServices.getMDIManager().closeWindow(this);
				return;

		}

		if (src instanceof JCheckBox) {

			dimArrBx();
			activateOk();

		}

		// -------------------------------------------------------------------
		/*
		 * START SYNCHRONISATION AFTER EXPORTATION
		 */
		/*
		if (src == synchro) {
			
			int opt = JOptionPane.showConfirmDialog(this,
					// 
					PluginServices.getText(this, "initial_synchro") + "?",
					PluginServices.getText(this, "synchro"),
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

			String errorMessage=null;
			
			//list All files in destination exportation folder
			File directory = new File(pathImport.getText()+File.separator+nameProy.getText());
			if(directory.exists() && directory.isDirectory()){
				
				
				if (opt == JOptionPane.YES_OPTION) {

					// ------------------------------------- yes ----------------
					//send ListFileTo OpenMIS synchro
					
					try {
						errorMessage = synchro.sendNewListFile(directory.listFiles(), nameProy.getText());
					} catch (Exception e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null,"Synchronisation Impossible!\n","error", JOptionPane.ERROR_MESSAGE);
					}
					if(errorMessage != null) {
						JOptionPane.showMessageDialog(null,"Synchronisation Impossible!\n"+errorMessage,"error", JOptionPane.ERROR_MESSAGE);
						
					}else{
						//start OpenMIS synchro 
						try {
							errorMessage = synchro.launchProjectSynchronisation(nameProy.getText());
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null,"Synchronisation Impossible!\n","error", JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						}
						if(errorMessage != null)
							JOptionPane.showMessageDialog(null,"Synchronisation Impossible!\n"+errorMessage,"error", JOptionPane.ERROR_MESSAGE);
					}	
					// ------------------------------------- yes end ----------------
					
				} else if (opt == JOptionPane.NO_OPTION) {
					
					// ------------------------------------- no ----------------
					try {
						errorMessage = synchro.launchProjectSynchronisation(nameProy.getText());
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null,"Synchronisation Impossible!\n","error", JOptionPane.ERROR_MESSAGE);
						e.printStackTrace();
					}
					if(errorMessage != null)
						JOptionPane.showMessageDialog(null,"Synchronisation Impossible!\n"+errorMessage,"error", JOptionPane.ERROR_MESSAGE);
					// ------------------------------------- no end ----------------
				}
				
			}else{
				JOptionPane.showMessageDialog(null,"Synchronisation Impossible!\nProject directory Not Found","error", JOptionPane.ERROR_MESSAGE);
			}
		
		}		
*/
		
		
	}

	private boolean validProjectName(String text) {
		// TODO Auto-generated method stub
		String aux = text.replaceAll("\\\\", "");
		aux = aux.replaceAll("/", "");
		aux = aux.replaceAll("\\.", "");
		aux = aux.replaceAll("\\*", "");
		aux = aux.trim();
		return (aux.length() > 0);
	}

	private boolean validPath(String text) {
		return (text.length() > 0);
	}
	
	private void deleteFilesIn(File f) {
		
		if (!f.isDirectory()) {
			logger.error("It is not a folder (?) : " + f.getAbsolutePath());
			return;
		}
		
		File[] subfiles = f.listFiles();
		for (int i=0; i<subfiles.length; i++) {
			if (subfiles[i].isFile()) {
				try {
					subfiles[i].delete();
				} catch (Exception ex) {
					logger.debug("Unable to delete file: " + subfiles[i].getAbsolutePath());
				}
			}
		}
	}	

}

