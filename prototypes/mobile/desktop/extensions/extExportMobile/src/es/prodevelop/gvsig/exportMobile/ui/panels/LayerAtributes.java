package es.prodevelop.gvsig.exportMobile.ui.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.fmap.drivers.FieldDescription;
import com.iver.cit.gvsig.fmap.layers.FLayer;

/**
 * Panel to select the new layer atributes
 * @author Anabel Moreno
 *
 */
public class LayerAtributes extends JPanel implements IWindow, ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel topPanel = new JPanel();

	private JPanel centerPanel = new JPanel();

	private JPanel bottomPanel = new JPanel();

	private JScrollPane scroll = new JScrollPane();

	private JPanel titlePanel = new JPanel();

	private JCheckBox selAllBox;

	private FLayer layer;

	private JLabel nomLayer, typLayer;

	private JButton exit, inverSel, cancel;

	private PanelAtribIntoScroll atrbScroll;

	private ArrayList arrLayers = new ArrayList();

	private int atrSelec, atribTot;

	private ArrayList selectAtrib;

	private FieldDescription[] atribSel;

	private FieldDescription[] atribTotal;

	private int cont;

	private boolean okPress = false;

	private static int PANEL_WIDTH = 300;

	private static int PANEL_HEIGHT = 250;

	private static int TOP_PANEL_HEIGHT = 20;

	private static int BOTTOM_PANEL_HEIGHT = 35;

	private static int CENTER_PANEL_HEIGHT = 195;

	private static int TITTLE_CENTER_PANEL_HEIGHT = 24;

	private static int INVERTSEL_X = 10;

	private static int EXIT_X = 110;

	private static int CANCEL_X = 210;

	private static int BUTTON_Y = 5;

	private static int BUTTON_W = 80;

	private static int BUTTON_H = 25;

	private static int LABEL_Y = 5;

	private static int SELALLBX_X = 20;

	private static int SELALLBX_W = 17;

	private static int SELALLBX_H = 13;

	private static int NOMLYR_X = 60;

	private static int NOMLYR_W = 80;

	private static int NOMLYR_H = 12;

	private static int TYPLYR_X = 170;

	private static int TYPLYR_W = 40;

	private static int TYPLYR_H = 12;

	
	
	public WindowInfo getWindowInfo() {

		WindowInfo viewAtrib = new WindowInfo(WindowInfo.MODALDIALOG);
		viewAtrib.setWidth(306);
		viewAtrib.setHeight(256);
		viewAtrib.setTitle(PluginServices.getText(this, "atriblayer"));
		return viewAtrib;
	}

	public boolean isOkPress() {

		return okPress;
	}

	/**
	 * Window to select the layers atributes
	 * 
	 * @param layer1,
	 *            layer selected
	 * @param arrSelect,list
	 *            with the description of the layer
	 * @param cont1,
	 *            number of atributes selected
	 */
	public LayerAtributes(FLayer layer_wn, ArrayList arrSelect, int cont1) {

		super();
		arrLayers.add(layer);
		layer = layer_wn;
		selectAtrib = arrSelect;
		cont = cont1;

		/* PRINCIPAL PANEL */
		setSize(PANEL_WIDTH, PANEL_HEIGHT);
		this.setLayout(new BorderLayout());

		/* TOP PANEL */
		topPanel.setSize(PANEL_WIDTH, TOP_PANEL_HEIGHT);

		/* BOTTOM PANEL */
		bottomPanel.setPreferredSize(new Dimension(PANEL_WIDTH,
				BOTTOM_PANEL_HEIGHT));
		bottomPanel.setLayout(null);

		inverSel = new JButton(PluginServices.getText(this, "invertSet"));
		inverSel.setBounds(INVERTSEL_X, BUTTON_Y, BUTTON_W, BUTTON_H);
		bottomPanel.add(inverSel);
		inverSel.addActionListener(this);

		exit = new JButton(PluginServices.getText(this, "ok"));
		exit.setBounds(EXIT_X, BUTTON_Y, BUTTON_W, BUTTON_H);
		bottomPanel.add(exit);
		exit.addActionListener(this);

		cancel = new JButton(PluginServices.getText(this, "cancel"));
		cancel.setBounds(CANCEL_X, BUTTON_Y, BUTTON_W, BUTTON_H);
		bottomPanel.add(cancel);
		cancel.addActionListener(this);

		
		/* CENTER PANEL */
		centerPanel.setSize(new Dimension(PANEL_WIDTH, CENTER_PANEL_HEIGHT));
		centerPanel.setLayout(new BorderLayout());
		
		TitledBorder title = BorderFactory.createTitledBorder(layer.getName());
		title.setTitleColor(Color.BLUE);
		centerPanel.setBorder(title);

		selAllBox = new JCheckBox();
		selAllBox.addActionListener(this);
		selAllBox.setSelected(true);
		selAllBox.setBackground(Color.LIGHT_GRAY);
		nomLayer = new JLabel(PluginServices.getText(this, "nAtrib"));
		typLayer = new JLabel(PluginServices.getText(this, "tAtrib"));
		titlePanel.setPreferredSize(new Dimension(PANEL_WIDTH,
				TITTLE_CENTER_PANEL_HEIGHT));
		titlePanel.setLayout(null);
		selAllBox.setBounds(SELALLBX_X, LABEL_Y, SELALLBX_W, SELALLBX_H);
		nomLayer.setBounds(NOMLYR_X, LABEL_Y, NOMLYR_W, NOMLYR_H);
		typLayer.setBounds(TYPLYR_X, LABEL_Y, TYPLYR_W, TYPLYR_H);

		titlePanel.add(nomLayer);
		titlePanel.add(typLayer);
		titlePanel.add(selAllBox);
		titlePanel.setBackground(Color.LIGHT_GRAY);

		atrbScroll = new PanelAtribIntoScroll(layer, arrSelect, cont, this);
		scroll.setViewportView(atrbScroll.getPanel());

		centerPanel.add(scroll, BorderLayout.CENTER);
		centerPanel.add(titlePanel, BorderLayout.NORTH);

		/* adds */
		this.add(topPanel, BorderLayout.NORTH);
		this.add(centerPanel, BorderLayout.CENTER);
		this.add(bottomPanel, BorderLayout.SOUTH);

	}

	/**
	 * @return number of layer`s atributes selected
	 */
	public int getAtrSelected() {

		return atrSelec;
	}

	/**
	 * @return number of all layer`s atributes
	 */
	public int getAtrib() {

		return atribTot;
	}

	/**
	 * @return a list with the layer`s atributes selected
	 */
	public ArrayList getSelectAtrib() {

		return selectAtrib;
	}

	/**
	 * @return a new layer description with the atributes selected
	 */
	public FieldDescription[] getAtribSelected() {

		return atribSel;
	}

	/**
	 * to activate checkbox all selected, and enabled ok button
	 * 
	 * @return true if any atribute is selected and false if any atribute is
	 *         selected
	 */
	public boolean dimArrBx() {

		FieldDescription[] aux = atrbScroll.getAtribSelected();
		if (aux.length == 0) {
			selAllBox.setSelected(false);
			return false;
		} else {

			selAllBox.setSelected(true);
			return true;
		}
	}

	/**
	 * @return a layer description with all original atributes
	 */
	public FieldDescription[] getAtribTotal() {

		atribTotal = atrbScroll.getAtribTotal();
		return atribTotal;
	}

	public void actionPerformed(ActionEvent arg0) {

		Object src = arg0.getSource();

		if (src == exit) {

			okPress = true;
			selectAtrib = atrbScroll.getSelectionAtributs();

			// array fields layer
			atribSel = atrbScroll.getAtribSelected();

			atrSelec = atrbScroll.getNumAtribSelected();
			atribTot = atrbScroll.getNumAtrib();
			PluginServices.getMDIManager().closeWindow(this);
			return;
		}

		if (src == selAllBox) {

			ArrayList arrAux = atrbScroll.getArrBox();
			if (selAllBox.isSelected() == true) {

				for (int i = 0; i < arrAux.size(); i++) {

					JCheckBox aux = (JCheckBox) arrAux.get(i);
					aux.setSelected(true);
				}
				exit.setEnabled(true);
			}

			if (selAllBox.isSelected() == false) {

				for (int i = 0; i < arrAux.size(); i++) {

					JCheckBox aux = (JCheckBox) arrAux.get(i);
					aux.setSelected(false);
				}
				exit.setEnabled(false);
			}

		}

		if (src == cancel) {

			PluginServices.getMDIManager().closeWindow(this);
			return;
		}

		if (src == inverSel) {

			atrbScroll.setInvSelected();
			ArrayList arrAux = atrbScroll.getArrBox();
			int contaux = 0;
			int dim = arrAux.size();
			for (int i = 0; i < dim; i++) {

				JCheckBox boxAux = (JCheckBox) arrAux.get(i);
				if (boxAux.isSelected() == true) {
					contaux++;
				}
			}
			exit.setEnabled(dimArrBx());
			return;
		}

		if (src instanceof JCheckBox) {

			exit.setEnabled(dimArrBx());
		}
	}
}
