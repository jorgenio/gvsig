package es.prodevelop.gvsig.exportMobile.ui.panels;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.core.IGeometry;
import com.iver.cit.gvsig.fmap.drivers.DriverIOException;
import com.iver.cit.gvsig.fmap.layers.FBitSet;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.ReadableVectorial;

/**
 * To know if the layer instersects with the view
 * @author Anabel Moreno
 *
 */
public class LayerInView {

	private FLayer lyr;
	private FLyrVect layer;
	private FBitSet bitSet;
	private boolean intersects=false;
	private int cont = 0;
	private Rectangle2D rect;                                                                                                                                                                                                                               ;

	/**
	 * Constructor
	 * @param layer
	 *            to work
	 * @param rectangle
	 *            of the view
	 */
	public LayerInView(FLayer layer, Rectangle2D rectangle) {

		lyr = layer;
		rect = rectangle;
	}

	/**
	 * if layer is vectorial look for layer`s geometries that intersects with the view,
	 * if any geometries intersects, the method return true.
	 * @return true if layer intersects with the view
	 */
	public boolean isInView() {

		Rectangle2D lyrR = null;
		Rectangle r = null;
		
		try {
			lyrR = lyr.getFullExtent();
			r = rect.getBounds();
			
			if (r.intersects(lyrR)){
				intersects = true;
				
				if(lyr instanceof FLyrVect){
					
					layer = (FLyrVect) lyr;
					
					bitSet = layer.queryByRect(rect);
										
					ReadableVectorial va = layer.getSource();
					
					va.start();
					
					for (int i = bitSet.nextSetBit(0); i >= 0; i = bitSet.nextSetBit(i + 1)) {
		
						IGeometry geo = va.getShape(i);
						if (geo != null) {
							cont++;
						}
					}
					va.stop();
					
					if (cont >0)
						intersects = true;
					else
						intersects = false;
				}
			}				
			else
				intersects =  false;

		} catch (DriverException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (DriverIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return intersects;
	}

}
