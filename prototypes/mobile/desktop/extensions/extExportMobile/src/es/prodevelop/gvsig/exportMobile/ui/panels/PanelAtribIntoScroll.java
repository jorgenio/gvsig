package es.prodevelop.gvsig.exportMobile.ui.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.hardcode.gdbms.engine.data.driver.DriverException;
import com.iver.cit.gvsig.fmap.drivers.FieldDescription;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;

/**
 * Subpanel to select the new layer atributes
 * @author Anabel Moreno
 *
 */
public class PanelAtribIntoScroll {

	private JPanel panelAtrib = new JPanel();

	private JCheckBox boxAtrib;

	private JLabel etAtrib, typeAtrib;

	private int dim;

	private ArrayList arrBox = new ArrayList();

	private FieldDescription[] atribute;

	private JCheckBox boxAux;

	private LayerAtributes parent;

	private FLayer lyrwn;

	private static int AUX = 3;

	private static int BOXATIB_X = 19;

	private static int BOXATIB_W = 17;

	private static int BOXATIB_H = 13;

	private static int ETATRIB_X = 60;

	private static int ETATRIB_W = 80;

	private static int ETATRIB_H = 20;

	private static int TYPATRB_X = 170;

	private static int TYPATRB_W = 40;

	private static int TYPATRB_H = 20;
	
	private static int PLYR_W = 26;
	
	private static int PLYR_H = 290;
		

	/**
	 * Panel into parent panel LayerAtributes. get layer`s atributes to build a
	 * list box with this description
	 * 
	 * @param layer,
	 *            layer to work
	 * @param arSelec,
	 *            list of layer`s selected atributes
	 * @param cont,
	 *            number of selected atributes
	 * @param pa,
	 *            parent panel
	 */
	public PanelAtribIntoScroll(FLayer layerwn, ArrayList arSelec, int cont,
			LayerAtributes pa) {

		lyrwn = layerwn;
		parent = pa;

		try {

			if (lyrwn instanceof FLyrVect) {

				FLyrVect layr = (FLyrVect) lyrwn;
				/* get field description array */
				atribute = layr.getRecordset().getFieldsDescription();

				dim = atribute.length;
				panelAtrib.setLayout(null);
				panelAtrib.setPreferredSize(new Dimension(0,(dim * PLYR_W)));
				panelAtrib.setBackground(Color.WHITE);

				for (int i = 0; i < dim; i++) {

					JPanel layerPanel = new JPanel();
					layerPanel.setLayout(null);
					layerPanel.setBounds(0, PLYR_W*i,PLYR_H, PLYR_W);
					
					boxAtrib = new JCheckBox();
					boxAtrib.setBounds(BOXATIB_X, AUX+3, BOXATIB_W,
							BOXATIB_H);
					boxAtrib.addActionListener(parent);

					if ((cont == 0) || (cont == 1)) {

						boxAtrib.setSelected(true);
					}

					
					// add box to aux array
					arrBox.add(boxAtrib);

					if (cont > 1) {

						boxAux = (JCheckBox) arrBox.get(i);

						// if box was selected 
						if (boxAux.isSelected() == true)
							boxAux.setSelected(true);
						else
							boxAux.setSelected(false);
					}

					// get atribute`s name and buil a label with the name
					FieldDescription aux = (FieldDescription) atribute[i];
					String name = aux.getFieldName();
					etAtrib = new JLabel(name);
					etAtrib.setBounds(ETATRIB_X, AUX, ETATRIB_W,
							ETATRIB_H);

					int type = aux.getFieldType();
					String typename = aux.typeToString(type);

					typeAtrib = new JLabel(typename);
					typeAtrib.setBounds(TYPATRB_X, AUX, TYPATRB_W,
							TYPATRB_H);

					if((i%2)==1){
						layerPanel.setBackground(Color.decode("#B0E0E6"));
						boxAtrib.setBackground(Color.decode("#B0E0E6"));						
					}
					else{
						layerPanel.setBackground(Color.WHITE);
						boxAtrib.setBackground(Color.WHITE);						
					}
					
					// add box and label
					layerPanel.add(boxAtrib);
					layerPanel.add(etAtrib);
					layerPanel.add(typeAtrib);
					
					panelAtrib.add(layerPanel);

				}
				
			}

		} catch (DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (com.iver.cit.gvsig.fmap.DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * when I close the window the selected boxes must be selected the next
		 * time if the window is open again
		 */

		int l = arSelec.size();
		for (int i = 0; i < l; i++) {

			int sel = ((Integer) arSelec.get(i)).intValue();
			((JCheckBox) arrBox.get(sel)).setSelected(true);

		}

	}

	public ArrayList getArrBox() {

		return arrBox;
	}

	// invert selection
	public void setInvSelected() {

		for (int i = 0; i < arrBox.size(); i++) {

			JCheckBox box = (JCheckBox) arrBox.get(i);
			if (box.isSelected())
				box.setSelected(false);
			else
				box.setSelected(true);
		}
	}

	/**
	 * For any atribute of the list get the atribute`s name and get the number
	 * of fields, then add the description to a new list
	 * 
	 * @return total atributes description
	 */
	public FieldDescription[] getAtribTotal() {

		int dimTotal = getNumAtrib();
		FieldDescription[] atrbTotal = new FieldDescription[dimTotal];

		int cont = 0;

		for (int i = 0; i < atribute.length; i++) {

			// get box selected
			JCheckBox boxSel = (JCheckBox) arrBox.get(i);
			if (boxSel.isSelected()) {

				// get atribute`s name wich index is the same that the selected
				// box
				FieldDescription atrib = (FieldDescription) atribute[i];

				// get the number of fields that it has and build a new
				// description

				FieldDescription nuevo = new FieldDescription();
				nuevo = atrib.cloneField();
				
				nuevo.setFieldLength(atrib.getFieldLength()); // jldominguez

				// add description to new FieldDescrption[]
				atrbTotal[cont] = nuevo;
				cont = cont + 1;
			}

		}
		return atrbTotal;
	}

	/**
	 * Get selected atributes
	 * 
	 * @return selected description
	 */
	public FieldDescription[] getAtribSelected() {

		int dimSelect = getNumAtribSelected();
		FieldDescription[] atrb = new FieldDescription[dimSelect];

		int cont = 0;

		for (int i = 0; i < atribute.length; i++) {

			// get selected box
			JCheckBox boxSel = (JCheckBox) arrBox.get(i);
			if (boxSel.isSelected()) {

				// get name box
				FieldDescription atrib = (FieldDescription) atribute[i];

				// get atribute`s name wich index is the same that the selected
				// box
				FieldDescription nuevo = new FieldDescription();
				nuevo = atrib.cloneField();
				
				nuevo.setFieldLength(atrib.getFieldLength()); // jldominguez

				// add description to new FieldDescrption[]
				atrb[cont] = nuevo;
				cont = cont + 1;
			}

		}
		return atrb;
	}

	/**
	 * @return a list with the selected atributes
	 */
	public ArrayList getSelectionAtributs() {

		ArrayList result = new ArrayList();
		int l = arrBox.size();
		for (int i = 0; i < l; i++) {

			if (((JCheckBox) arrBox.get(i)).isSelected())
				result.add(new Integer(i));
		}

		return result;
	}

	/**
	 * @return panel to draw it
	 */
	public JPanel getPanel() {

		return panelAtrib;

	}

	/**
	 * @return number of all layer`s atributes
	 */
	public int getNumAtrib() {

		return dim;
	}

	/**
	 * @return number of checks selected atributes
	 */
	public int getNumAtribSelected() {
		int res = 0;
		for (int i = 0; i < dim; i++) {
			JCheckBox box = (JCheckBox) arrBox.get(i);
			if (box.isSelected()) {

				res++;
			}
		}
		return res;
	}

}
