package es.prodevelop.gvsig.exportMobile.ui.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.drivers.FieldDescription;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrRaster;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.FLyrWMS;

import es.prodevelop.gvsig.exportMobile.CreateImage;
import es.prodevelop.gvsig.exportMobile.LayerObject;
import es.prodevelop.gvsig.exportMobile.ui.order.ArrayStringOrd;
import es.prodevelop.gvsig.exportMobile.ui.order.FLayerWithNewName;

/**
 * SubPanel to the principal panel extension
 * @author Anabel Moreno
 *
 */

public class PanelIntoScroll implements ActionListener {
	
	private static Logger logger = Logger.getLogger(PanelIntoScroll.class.getName()); 
	private static Color color1 = new Color(234,234,234,234);
	private static Color color2 = new Color(254,237,214);
	SystemColor a;

	private int height,width;
	
	private boolean into = true;
	
	private JPanel newPanel = new JPanel();
		
	private JComboBox comboActions;

	private JButton chooseAtrib;

	private JCheckBox export;

	private JLabel name, icon, atribute;
	
	private ArrayList arrLayers = new ArrayList();
	
	private FLayer layer;

	private String nameLayer;

	private ArrayList arrExports = new ArrayList();

	private int numAtribTot, numAtribSelect;

	private LayerAtributes atribPanel;

	private PanelAtribIntoScroll atriLayer;

	private ArrayList arrLabel = new ArrayList();

	private FLayer lyr;

	private boolean open = false;

	private ArrayList allAtrib = new ArrayList();

	private FieldDescription[] arrDescrLyr;

	private int auxabrt, atr;

	private int[] arrOpen;

	private ExportPanel parent;

	private LayerInView lyrVw;

	private Rectangle2D rectangle;

	private boolean lyrIntoView;
	
	private ArrayList newArrayLayers = new ArrayList();

	private static int PANEL_WIDTH = 500;
	
	private static int COMPONENT_Y = 3;

	private static int AUX = 20;
	
	private static int AUX2 = 26;

	public static int EXPORT_X = 8;
	private static int EXPORT_W = 17;
	private static int EXPORT_H = 13;

	private static int HEIGHT = 20;
	public static int NAME_X = 145;
	private static int NAME_W = 200;
	
	public static int IN_VIEW_X = 88;

	public static int ATRIB_X = 335;
	private static int ATRIB_W = 65;

	public static int CHATRB_X = 385;
	private static int CHATRB_W = 20;

	public static int CMBOATR_X = 430;
	private static int CMBOATR_W = 100;

	public static int ICON_X = 45;

	private static String VECTORIAL = "Capa Vectorial";

	private static String RASTER = "Capa no Vectorial";
	
	private static String WMS = "WMS";

	
	/**
	 * Panel into ExportPanel (scroll)
	 * 
	 * @param arLayers,
	 *            layer list
	 * @param pa,
	 *            parent
	 * @param rect,
	 *            rectangle
	 */
	public PanelIntoScroll(ArrayList arLayers, ExportPanel pa, Rectangle2D rect) {

		rectangle = rect;
		parent = pa;
		arrLayers = arLayers;
		int dim = arrLayers.size();
		arrOpen = new int[dim];
		
		newPanel.setLayout(null);
		newPanel.setPreferredSize(new Dimension(PANEL_WIDTH, ((dim + 1) * AUX)));
		newPanel.setBackground(Color.WHITE);

		ArrayStringOrd arrays = new ArrayStringOrd(arLayers);
		ArrayList arrNames = arrays.getArrayStrings();
		
		
		for (int i=0; i < arrLayers.size(); i++) {

			JPanel layerPanel = new JPanel();
			layerPanel.setLayout(null);
			layerPanel.setBounds(0, AUX2*i, 600, AUX2);
			
			layer = (FLayer) ((FLayerWithNewName)arrLayers.get(i)).getLayer();
			nameLayer = layer.getName();

			export = new JCheckBox();
			export.setBounds(EXPORT_X, COMPONENT_Y+3, EXPORT_W, EXPORT_H);
			export.addActionListener(parent);
			export.addActionListener(this);

			name = new JLabel(nameLayer);
			name.setBounds(NAME_X, COMPONENT_Y, NAME_W, HEIGHT);

			if (open == true) {

				atribute = new JLabel(numAtribSelect + "/" + numAtribTot);
				atribute.setBounds(ATRIB_X, COMPONENT_Y, ATRIB_W, HEIGHT);
				arrLabel.add(atribute);
				
			} else {

				ArrayList auxArr = new ArrayList();
				allAtrib.add(auxArr);
				auxabrt = 0;
				arrOpen[i] = auxabrt;
				atriLayer = new PanelAtribIntoScroll(layer, auxArr, auxabrt,null);
				numAtribTot = atriLayer.getNumAtrib();
				numAtribSelect = atriLayer.getNumAtribSelected();
				atribute = new JLabel(numAtribSelect + "/" + numAtribTot);
				atribute.setBounds(ATRIB_X, COMPONENT_Y, ATRIB_W, HEIGHT);
				arrLabel.add(atribute);

			}

			chooseAtrib = new JButton("...");
			chooseAtrib.setBounds(CHATRB_X, COMPONENT_Y, CHATRB_W, HEIGHT);
			chooseAtrib.addActionListener(this);

			comboActions = new JComboBox();
			comboActions.setBounds(CMBOATR_X, COMPONENT_Y, CMBOATR_W, HEIGHT);	
//			comboActions.addItem(PluginServices.getText(this,"select" ));
//			comboActions.addItem(PluginServices.getText(this,"cut" ));
//			comboActions.addItem(PluginServices.getText(this, "allyr"));
//			comboActions.repaint();

//			if (layer.isVisible() == true) 
//				export.setSelected(true);
//			else 
//				export.setSelected(false);
//			
			
			// ICON TO SHOW
			if (layer instanceof FLyrVect) {

				CreateImage image = new CreateImage();
				URL path = image.createResourceUrl("images/icolayerV.PNG");
				ImageIcon imIcon = image.createImageIcon(path, VECTORIAL);
				height = imIcon.getIconHeight();
				width = imIcon.getIconWidth();
				icon = new JLabel(imIcon, JLabel.CENTER);
				comboActions.addItem(PluginServices.getText(this,"select" ));
				comboActions.addItem(PluginServices.getText(this,"cut" ));
				comboActions.addItem(PluginServices.getText(this, "allyr"));
				comboActions.repaint();
				
			} 
			if (layer instanceof FLyrRaster){
				
				CreateImage image = new CreateImage();
				URL path = image.createResourceUrl("images/lyrR.PNG");
				ImageIcon imIcon = image.createImageIcon(path, RASTER);
				height = imIcon.getIconHeight();
				width = imIcon.getIconWidth();
				icon = new JLabel(imIcon, JLabel.CENTER);
				chooseAtrib.setEnabled(false);
				comboActions.addItem(PluginServices.getText(this,"cut" ));
				comboActions.addItem(PluginServices.getText(this, "allyr"));
				comboActions.repaint();
//				comboActions.removeItemAt(0);
//				comboActions.setEnabled(true);
//				comboActions.repaint();
			
				
			}
			if (layer instanceof FLyrWMS){
				
				CreateImage image = new CreateImage();
				URL path = image.createResourceUrl("images/icoWMS.png");
				ImageIcon imIcon = image.createImageIcon(path, WMS);
				height = imIcon.getIconHeight();
				width = imIcon.getIconWidth();
				icon = new JLabel(imIcon, JLabel.CENTER);
				chooseAtrib.setEnabled(false);
				comboActions.addItem(PluginServices.getText(this,"config"));
				comboActions.setEnabled(false);
//				comboActions.removeAllItems();
//				comboActions.addItem(PluginServices.getText(this,"config"));
//				comboActions.setSelectedIndex(0);
//				comboActions.setEnabled(false);
				comboActions.repaint();
				
			}
			icon.setBounds(ICON_X, COMPONENT_Y+1, height, width);

			
			if ((layer.isVisible() == true) && (validGeometryType(layer))) {
				export.setSelected(true);
				if((layer instanceof FLyrVect)||(layer instanceof FLyrRaster)){
					comboActions.setEnabled(true);
					comboActions.repaint();
					chooseAtrib.setEnabled(true);
				}
				
			}
			else {
				export.setSelected(false);
				chooseAtrib.setEnabled(false);
				comboActions.setEnabled(false);
				comboActions.repaint();
			}
			
			
			
//			if(export.isSelected()==false){
//				
//				chooseAtrib.setEnabled(false);
//				comboActions.setEnabled(false);
//				
//			}
//			else{
//				
//				chooseAtrib.setEnabled(true);
//				comboActions.setEnabled(true);
//				
//			}
						
			if((i%2)==1){
				
				layerPanel.setBackground(color1);
				export.setBackground(color1);
				chooseAtrib.setBackground(color1);
			}
			else{
				layerPanel.setBackground(color2);
				export.setBackground(color2);
				chooseAtrib.setBackground(color2);
			}
			
			JLabel isInto = paintImage(layer, rectangle,layerPanel,comboActions);

			// 	adds panel components
			layerPanel.add(export);
			layerPanel.add(name);
			layerPanel.add(icon);
			layerPanel.add(atribute);
			layerPanel.add(chooseAtrib);
			layerPanel.add(comboActions);
			
			newPanel.add(layerPanel);
			arrExports.add(export);
			
			//New layer object
			String newname = (String) arrNames.get(i);
			LayerObject layerObj = new LayerObject(
					export,
					newname,
					comboActions,
					layer,
					chooseAtrib,
					layerPanel);
			layerObj.setImage(isInto);
			if(layerObj.getLyr() instanceof FLyrVect){
				layerObj.setDescription(atriLayer.getAtribSelected());
			}
			else
				layerObj.setDescription(null);
			//add object to array
			newArrayLayers.add(layerObj);
								
		}
	}

	private boolean validGeometryType(FLayer lyr) {
		
		if (! (lyr instanceof FLyrVect)) {
			return true;
		} else {
			/*
			int shp_type = FShape.NULL;
			
			try {
				shp_type = ((FLyrVect) lyr).getShapeType();
			} catch (DriverException e) {
				logger.error("While getting shp type: " + e.getMessage());
				return false;
			}
			
			return (
					(shp_type == FShape.POLYGON)
					|| (shp_type == FShape.LINE)
					|| (shp_type== FShape.POINT));*/
			return true;
		}
	}

	/**
	 * If layer intersects with the view`s bounds ( rectangle ) paint a tik
	 * image, but if not intersects paint a X image.
	 * 
	 * @param layer,
	 *            to work
	 * @param rectang,
	 *            look if layer intersects with rectangle
	 */
	public JLabel paintImage(FLayer layer, Rectangle2D rectang,JPanel parentPanel,JComboBox comboA) {

		lyrVw = new LayerInView(layer, rectang);
		lyrIntoView = lyrVw.isInView();
		JLabel isInto;
		
		if (lyrIntoView == true) {

			CreateImage image = new CreateImage();
			URL path = image.createResourceUrl("images/tik.png");
			ImageIcon img = image.createImageIcon(path, "image into view");
			int heightInto = img.getIconHeight();
			int widthInto = img.getIconWidth();
			isInto = new JLabel(img, JLabel.CENTER);
			isInto.setBounds(IN_VIEW_X, COMPONENT_Y, heightInto, widthInto);
			into=true;
			
		} else {
			
			CreateImage image = new CreateImage();
			URL path = image.createResourceUrl("images/remove.png");
			ImageIcon img = image.createImageIcon(path, "image out of view");
			int heightInto = img.getIconHeight();
			int widthInto = img.getIconWidth();
			isInto = new JLabel(img, JLabel.CENTER);
			isInto.setBounds(IN_VIEW_X, COMPONENT_Y, heightInto, widthInto);
			into=false;
			
		}
		
		if((into == false)){
			
			comboA.removeAllItems();
			comboA.addItem(PluginServices.getText(this, "allyr"));
			
		}
		else{
			comboA.setEnabled(true);
			comboA.setSelectedIndex(0);
		}
		
		// ---------------- jldominguez - we are not going to cut rasters:
		if (layer instanceof FLyrRaster) {
			comboA.removeAllItems();
			comboA.addItem(PluginServices.getText(this, "allyr"));
		}
		// ---------------- 

		parentPanel.add(isInto);
		return isInto;
	}
	
	
	public int getSelectedIndex(){
		
		return comboActions.getSelectedIndex();
	}
	
		
	/**
	 * @return boxs list of all layers
	 */
	public ArrayList getarrBox() {

		return arrExports;
	}
	
	
	/**
	 * @return panel scroll
	 */
	public JPanel getPanel() {

		return newPanel;
	}
	
	/**
	 * @return array layers
	 */
	public ArrayList getNewArrayLayers(){
		
		return newArrayLayers;
	}

	
	public void actionPerformed(ActionEvent arg0) {

		Object src = arg0.getSource();

		//all type of layers
		for (int i = 0; i < newArrayLayers.size(); i++) {

			LayerObject lyrObj = (LayerObject) newArrayLayers.get(i);
			lyr = lyrObj.getLyr();
			JCheckBox boxExp = lyrObj.getBox();
			JComboBox combo = lyrObj.getCombo();
			JButton button = lyrObj.getButton();

			if (src == boxExp) {

				if(boxExp.isSelected()==true){
					boxExp.setSelected(false);
					if ((lyr instanceof FLyrVect)){
						
						combo.setEnabled(true);
						button.setEnabled(true);
						lyrObj.setBoxStatus(true);
					}	
					if(lyr instanceof FLyrRaster){
						
						combo.setEnabled(true);
						button.setEnabled(false);
						lyrObj.setBoxStatus(true);
					}
				}
				else{
					boxExp.setSelected(true);
					combo.setEnabled(false);
					button.setEnabled(false);
					lyrObj.setBoxStatus(false);
				}
					
				return;
			}

			// if push atributes button
			if (src == button) {

				JLabel label = (JLabel) arrLabel.get(i);
				open = true;
				atr = arrOpen[i];

				atr++;

				// show panel
				atribPanel = new LayerAtributes(lyrObj.getLyr(), (ArrayList) allAtrib
						.get(i), atr);
				ArrayList aux = atribPanel.getSelectAtrib();
				PluginServices.getMDIManager().addWindow(atribPanel);

				// when close the panel the label atribute has to change
				if (atribPanel.isOkPress()) {

					aux = atribPanel.getSelectAtrib();
					allAtrib.set(i, aux);
					numAtribTot = atribPanel.getAtrib();
					numAtribSelect = atribPanel.getAtrSelected();
					label.setText(numAtribSelect + "/" + numAtribTot);

					newPanel.repaint();
					arrOpen[i] = atr;

					// get selected atributes FieldDescription[]
					arrDescrLyr = atribPanel.getAtribSelected();

					// add the new layer`s fieldDescription into a new list
					lyrObj.setDescription(arrDescrLyr);
					
				}

				else {

					numAtribSelect = atriLayer.getNumAtribSelected();
					atribute = new JLabel(numAtribSelect + "/" + numAtribTot);
					newPanel.repaint();
					atr++;
				}
				return;
			}
		}
	}
}
