package es.prodevelop.gvsig.exportMobile.xml;

import java.awt.geom.Rectangle2D;
import com.iver.cit.gvsig.fmap.rendering.XmlBuilder;

/**
 * Get bounds of view and create new tags to the xml project
 * @author Anabel Moreno
 *
 */
public class Bounds extends Rectangle2D.Double implements XMLgetter {
	public static final String tag = "bounds";
	
	public XmlBuilder xml;
	
	public static Bounds createBounds(Rectangle2D rect){
		Bounds bounds = new Bounds();
		bounds.setRect(rect);
		return bounds;
	}
	
	public void setXML(XmlBuilder xml_builder){
		
		this.xml = xml_builder;
	}
	
	public void createXML(){
		
		//<Bounds>
		xml.openTag(XmlProjectTags.B_BOX);
		
			//<xMin>
			xml.writeTag(XmlProjectTags.X_MIN,java.lang.Double.toString(this.getMinX()));
			//</xMin>
			
			//<xMax>
			xml.writeTag(XmlProjectTags.X_MAX,java.lang.Double.toString(this.getMaxX()));
			//</xMax>
			
			//<yMin>
			xml.writeTag(XmlProjectTags.Y_MIN,java.lang.Double.toString(this.getMinY()));
			//</yMin>
			
			//<yMax>
			xml.writeTag(XmlProjectTags.Y_MAX,java.lang.Double.toString(this.getMaxY()));
			//</yMax>
			
		xml.closeTag();
		//</Bounds>
	}
	
}
