
package es.prodevelop.gvsig.exportMobile.xml;

import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.rendering.XmlBuilder;

/**
 * Get style of a layer, and create a new tag
 * @author Anabel Moreno
 *
 */

public class Style implements XMLgetter{

	FLayer lyr;
	String color;
	String colorFill;
	int size;
	int fill;
	XmlBuilder xml;
	int shpType;
	
	public void setXML(XmlBuilder xml_builder){
		
		this.xml = xml_builder;
	}
	
	public void setLayer(FLayer layer, int shpType){
		this.lyr = layer;
		this.shpType=shpType;
	}
	
	
	public void createXML(){
		
		XMLlyr xmlyr = new XMLlyr();
		xmlyr.createTextToXML(lyr, shpType);
		FLyrVect lyrV = (FLyrVect) lyr;
		
		//<Style>
		xml.openTag(XmlProjectTags.STYLE);
		
		//try {
			
			if(shpType == FShape.POINT){
				
				//<PointSymbol>
				xml.openTag(XmlProjectTags.POINT_SYMBOL);
				
					//<Color>
					xml.writeTag(XmlProjectTags.COLOR, xmlyr.getColor());
					//</Color>
					
					//<Size>
					xml.writeTag(XmlProjectTags.SIZE, java.lang.Integer.toString(xmlyr.getSize()));
					//</Size>
					
				xml.closeTag();
				//</PointSymbol>
				
			}
			if(shpType == FShape.LINE){
				
				//<LineSymbol>
				xml.openTag(XmlProjectTags.LINE_SYMBOL);
				
					//<Color>
					xml.writeTag(XmlProjectTags.COLOR, xmlyr.getColor());
					//</Color>
					
					//<Size>
					xml.writeTag(XmlProjectTags.SIZE, java.lang.Integer.toString(xmlyr.getSize()));
					//</Size>
					
				xml.closeTag();
				//</LineSymbol>
				
			}
			
			
			if(shpType == FShape.POLYGON){
				
				//<PoligonSymbol>
				xml.openTag(XmlProjectTags.POLYGON_SYMBOL);
				
					//<LineSymbol>
					xml.openTag(XmlProjectTags.LINE_SYMBOL);
					
						//<Color>
						xml.writeTag(XmlProjectTags.COLOR, xmlyr.getColor());
						//</Color>
						
						//<Size>
						xml.writeTag(XmlProjectTags.SIZE, java.lang.Integer.toString(xmlyr.getSize()));
						//</Size>
						
					xml.closeTag();
					//</LineSymbol>
				
					//<FillColor>
					xml.writeTag(XmlProjectTags.FILL_COLOR, xmlyr.getColorFill());
					//</FillColor>
					
					//<Fill>
					xml.writeTag(XmlProjectTags.FILL, java.lang.Integer.toString(xmlyr.getFill()));
					//</FillColor>
				xml.closeTag();
				//</PoligonSymbol>
							
			}
		/*} catch (DriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
			
		
		xml.closeTag();
		//</Style>

		
	}
}
