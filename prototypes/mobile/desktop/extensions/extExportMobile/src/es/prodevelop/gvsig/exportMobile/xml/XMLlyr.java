package es.prodevelop.gvsig.exportMobile.xml;

import java.awt.BasicStroke;
import java.awt.Color;

import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.core.FShape;
import com.iver.cit.gvsig.fmap.core.v02.FSymbol;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.rendering.SingleSymbolLegend;
import com.iver.cit.gvsig.fmap.rendering.VectorialIntervalLegend;
import com.iver.cit.gvsig.fmap.rendering.VectorialLegend;
import com.iver.cit.gvsig.fmap.rendering.VectorialUniqueValueLegend;
import com.iver.utiles.StringUtilities;

/**
 * 
 * @author amoreno
 *
 */
public class XMLlyr {

	private FLayer layer;
	private String color;
	private String colorFill;
	private int size;
	private int fill;

	public  XMLlyr(){
		
	}
	
	public String getColor(){
		return this.color;
	}
	public int getSize(){
		return this.size;
	}
	public int getFill(){
		return this.fill;
	}
	public String getColorFill(){
		return this.colorFill;
	}
	
		
	public void createTextToXML(FLayer lyr, int shpType){
		
		layer = lyr;
		if(layer instanceof FLyrVect){
			
			FLyrVect layerV = (FLyrVect) layer;
			try {
				
				FSymbol symbol = null;
				if (shpType == FShape.LINE) {
					
					VectorialLegend vleg = (VectorialLegend)layerV.getLegend();
					if (vleg instanceof SingleSymbolLegend){
						symbol = (FSymbol)((SingleSymbolLegend)vleg).getDefaultSymbol();
					}
					//tomar de vectorialintervallegend y vectorialuniquevaluelegend el primer valor
					//como symbol, y luego de symbol obtener lo que necesitemos
					if(vleg instanceof VectorialIntervalLegend){
						
						symbol = (FSymbol)((VectorialIntervalLegend)vleg).getSymbol(0);
					}
					if(vleg instanceof VectorialUniqueValueLegend){
						
						symbol = (FSymbol)((VectorialUniqueValueLegend)vleg).getSymbol(0);
					}
					Color col=symbol.getColor();
					this.color = StringUtilities.color2String(col);
					
					// instead of symbol.getSize(), we have to use this:
					// jldominguez
					int stroke_width = Math.round(((BasicStroke) symbol.getStroke()).getLineWidth());
					this.size = stroke_width;
				}
				
				if(shpType == FShape.POINT){
					
					VectorialLegend vleg = (VectorialLegend)layerV.getLegend();
					if (vleg instanceof SingleSymbolLegend){
						symbol = (FSymbol)((SingleSymbolLegend)vleg).getDefaultSymbol();
					}
					//tomar de vectorialintervallegend y vectorialuniquevaluelegend el primer valor
					//como symbol, y luego de symbol obtener lo que necesitemos
					if(vleg instanceof VectorialIntervalLegend){
						
						symbol = (FSymbol)((VectorialIntervalLegend)vleg).getSymbol(0);
					}
					if(vleg instanceof VectorialUniqueValueLegend){
						
						symbol = (FSymbol)((VectorialUniqueValueLegend)vleg).getSymbol(0);
					}
					Color col=symbol.getColor();
					this.color = StringUtilities.color2String(col);
					
					// instead of symbol.getSize(), we have to use this:
					// jldominguez
					if (symbol.isSizeInPixels()) {
						size = symbol.getSize();
					} else {
						// TODO calcular el tamanyo en pixeles
						size = 3;
					}
				}
				
				
				if(shpType == FShape.POLYGON){
					
					VectorialLegend vleg = (VectorialLegend)layerV.getLegend();
					if (vleg instanceof SingleSymbolLegend){
						symbol = (FSymbol)((SingleSymbolLegend)vleg).getDefaultSymbol();
					}
					//tomar de vectorialintervallegend y vectorialuniquevaluelegend el primer valor
					//como symbol, y luego de symbol obtener lo que necesitemos
					if(vleg instanceof VectorialIntervalLegend){
						
						symbol = (FSymbol)((VectorialIntervalLegend)vleg).getSymbol(0);
					}
					if(vleg instanceof VectorialUniqueValueLegend){
						
						symbol = (FSymbol)((VectorialUniqueValueLegend)vleg).getSymbol(0);
					}
					Color colFill=symbol.getColor();
					
					
					// there was a np exception when polygons were not filled
					// jldominguez
					if (colFill == null) {
						this.colorFill = StringUtilities.color2String(Color.WHITE);
						fill = 0;
					} else {
						this.colorFill = StringUtilities.color2String(colFill);
						fill = 1;
					}

					// instead of symbol.getSize(), we have to use this:
					// jldominguez
					int stroke_width = Math.round(((BasicStroke) symbol.getStroke()).getLineWidth());
					this.size = stroke_width;

					Color col = symbol.getOutlineColor();
					
					// there was a np exception when polygons were not filled
					// jldominguez
					if (col == null) {
						this.color = StringUtilities.color2String(Color.BLACK);
					} else {
						this.color = StringUtilities.color2String(col);
					}

					/*
					int symboltype = symbol.getSymbolType();
					FConstant f = null;
					if(symboltype == f.SYMBOL_TYPE_FILL)
						this.fill=1;
					else
						this.fill=0;
						*/	
				}
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (DriverException e) {
				e.printStackTrace();
			}
		}		
	}	
}
