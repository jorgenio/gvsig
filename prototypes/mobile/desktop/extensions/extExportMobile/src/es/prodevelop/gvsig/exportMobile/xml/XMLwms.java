package es.prodevelop.gvsig.exportMobile.xml;


import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.XMLException;
import com.iver.cit.gvsig.fmap.rendering.XmlBuilder;
import com.iver.utiles.xmlEntity.generate.Property;

/**
 * get wms layer properties and create its xml project
 * @author Anabel Moreno
 *
 */
public class XMLwms implements XMLgetter{

	private static int NAME = 2;
	
	private static int URL = 10;
	
	private static int LAYERQUERY = 12;
	
	private static int CAPLAYERSTYLES = 21;
	
	private static int CRS = 14;
	
	private static int IMAGEFORMAT = 13;
	
	private static int TRANSPARENCY = 6;
	
	XmlBuilder xml;
	
	private FLayer layerWMS;
	
	public void setxml(FLayer layerwn,XmlBuilder xml_builder){
		
		this.layerWMS = layerwn;
		this.xml = xml_builder;
	}
				
	public void createXML(){
		
		Property[] pr;
		try {
			pr = layerWMS.getXMLEntity().getXmlTag().getProperty();
			//<WMSCONFIG>
			//xml.openTag(XmlProjectTags.WMSCONFIG);
				xml.writeTag(XmlProjectTags.TYPE, "WMS");
				//<LAYERNAME>
				xml.writeTag(XmlProjectTags.NAME, layerWMS.getName());
				//</LAYERNAME>
				
				//<URL>
				xml.writeTag(XmlProjectTags.URL,pr[URL].getValue());
				//</URL>
				
				//<LAYERQUERY>
				//xml.openTag(XmlProjectTags.LAYERQUERY);
					
					//<NAME>
					xml.writeTag(XmlProjectTags.WMSLAYERS, pr[LAYERQUERY].getValue());
					//</NAME>
						
					//<STYLE>
					xml.writeTag(XmlProjectTags.WSMSTYLES,pr[CAPLAYERSTYLES].getValue());
					//</STYLE>
						
				//xml.closeTag();
				//</LAYERQUERY>
				
				
				//<SRS>
				xml.writeTag(XmlProjectTags.CRS, pr[CRS].getValue());
				//</SRS>
				
				//<IMAGEFORMAT>
				xml.writeTag(XmlProjectTags.IMAGE_FORMAT, pr[IMAGEFORMAT].getValue());
				//</IMAGEFORMAT>
				
				//<TRANSPARENCY>
				xml.writeTag(XmlProjectTags.TRANSPARENCY,pr[TRANSPARENCY].getValue());
				//</TRANSPARENCY>
				
			//xml.closeTag();
			//</WMSCONFIG>
			
		} catch (XMLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
