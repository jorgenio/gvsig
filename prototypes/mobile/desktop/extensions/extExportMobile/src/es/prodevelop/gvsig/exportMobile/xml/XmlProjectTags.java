package es.prodevelop.gvsig.exportMobile.xml;


public class XmlProjectTags {

	private XmlProjectTags(){};
	
	public static final String PROJECT = "Project";
	public static final String B_BOX = "BoundingBox";
	public static final String X_MIN = "xMin";
	public static final String X_MAX = "xMax";
	public static final String Y_MIN = "yMin";
	public static final String Y_MAX = "yMax";
	public static final String NAME = "Name";
	public static final String VISIBLE = "Visible";
	public static final String SCALE_MAX = "ScaleMax";
	public static final String SCALE_MIN = "ScaleMin";
	public static final String LAYERS = "Layers";
	public static final String LAYER = "Layer";
	public static final String TYPE = "Type";
	public static final String PATH = "Path";
	public static final String STYLE = "Style";
	public static final String POINT_SYMBOL = "PointSymbol";
	public static final String LINE_SYMBOL = "LineSymbol";
	public static final String POLYGON_SYMBOL = "PolygonSymbol";
	public static final String COLOR = "Color";
	public static final String SIZE = "Size";
	public static final String FILL_COLOR = "FillColor";
	public static final String FILL = "Fill";
	public static final String WMSCONFIG = "WMSConfig";
	public static final String LAYER_NAME = "LayerName";
	public static final String URL = "URL";
	public static final String LAYERQUERY = "LayerQuery";
	// public static final String SRS = "SRS";
	public static final String IMAGE_FORMAT = "ImageFormat";
	public static final String TRANSPARENCY = "Transparency";
	public static final String WMSLAYERS = "WmsLayers";
	public static final String WSMSTYLES = "WmsStyles";
	public static final String CRS = "CRS";
	
	
}
