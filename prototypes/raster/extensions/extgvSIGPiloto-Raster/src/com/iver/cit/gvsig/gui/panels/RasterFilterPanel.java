/*
 * Created on 25-jul-2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 */
package com.iver.cit.gvsig.gui.panels;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.gvsig.gui.beans.treelist.TreeListContainer;
import org.gvsig.gui.beans.treelist.event.TreeListEvent;

import com.iver.andami.PluginServices;

/**
 * @author Nacho Brodin <brodin_ign@gva.es>
 */
public class RasterFilterPanel extends JPanel{

	private int					HEIGHT_BUTTONS = 35;//35
	private int					MARGIN = 12;//12
	private int					WIDTH_SELECTOR = 165;//165
	private JPanel 				pMain = null;
	private TreeListContainer 	pSelector = null;
	private JPanel 				pRight = null;
	private int 				width = 0;
	private int 				height = 0;
	private JPanel 				pSelectedFilter = null;
	private JPanel 				pButtons = null;
	private JButton 			bAccept = null;
	private JButton 			bApply = null;
	private JButton 			bClose = null;
	/**
	 * Titulo del panel. Este aparece en el Borde del panel.
	 */
	private String 				title = null;
	/**
	 * Lista de paneles
	 */
	private static ArrayList	listPanels = new ArrayList();
	/**
	 * Tabla de hash cuya clave es el nombre de la entrada de menu que devuelve su posici�n
	 */
	private static TreeMap		panelEntry = new TreeMap();
	
	/**
	 * This method initializes 
	 * 
	 */
	public RasterFilterPanel(int width, int height) {
		super();
		this.width = width - MARGIN;
		this.height = height + 30;
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.gridx = 1;
		gridBagConstraints1.gridy = 0;
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.insets = new java.awt.Insets(0,0,0,0);
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridx = 0;
		this.setLayout(new GridBagLayout());
		this.setPreferredSize(new java.awt.Dimension(width, height));
		this.add(getPSelector(), gridBagConstraints);
		this.add(getPRight(), gridBagConstraints1);
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	public TreeListContainer getPSelector() {
		if (pSelector == null) {
			pSelector = new TreeListContainer();
			pSelector.setComponentSize(WIDTH_SELECTOR, height);
			pSelector.getTree().expandRow(0);
		}
		return pSelector;
	}

	/**
	 * This method initializes jPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPRight() {
		if (pRight == null) {
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.gridx = 0;
			gridBagConstraints3.gridy = 1;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.gridx = 0;
			gridBagConstraints2.gridy = 0;
			pRight = new JPanel();
			pRight.setLayout(new GridBagLayout());
			pRight.setPreferredSize(new java.awt.Dimension(width - WIDTH_SELECTOR, height));
			pRight.add(getPSelectedFilter(), gridBagConstraints2);
			pRight.add(getPButtons(), gridBagConstraints3);
		}
		return pRight;
	}

	/**
	 * This method initializes jPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPSelectedFilter() {
		if (pSelectedFilter == null) {
			pSelectedFilter = new JPanel();
			pSelectedFilter.setPreferredSize(new java.awt.Dimension(width - WIDTH_SELECTOR - MARGIN, height - HEIGHT_BUTTONS));
			pSelectedFilter.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Prueba", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null));
			pSelectedFilter.setLayout(new GridBagLayout());
			
			for(int i = 0; i < listPanels.size(); i++){
				GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
				gridBagConstraints2.gridx = i;
				gridBagConstraints2.gridy = 0;
				((JPanel)listPanels.get(i)).setPreferredSize(new java.awt.Dimension(width - WIDTH_SELECTOR - (MARGIN * 3), height - HEIGHT_BUTTONS - (MARGIN * 3)));
				((JPanel)listPanels.get(i)).setVisible(false);
				pSelectedFilter.add((JPanel)listPanels.get(i), gridBagConstraints2);
			}
			
			if(((JPanel)listPanels.get(0)) != null)
				((JPanel)listPanels.get(0)).setVisible(true);
		}
		return pSelectedFilter;
	}

	/**
	 * This method initializes jPanel1	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPButtons() {
		if (pButtons == null) {
			FlowLayout flowLayout1 = new FlowLayout();
			flowLayout1.setAlignment(java.awt.FlowLayout.RIGHT);
			flowLayout1.setVgap(8);
			flowLayout1.setHgap(3);
			pButtons = new JPanel();
			pButtons.setLayout(flowLayout1);
			pButtons.setPreferredSize(new java.awt.Dimension(width - WIDTH_SELECTOR - MARGIN, HEIGHT_BUTTONS));
			pButtons.add(getBAccept(), null);
			pButtons.add(getBApply(), null);
			pButtons.add(getBClose(), null);
		}
		return pButtons;
	}

	/**
	 * This method initializes jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	public JButton getBAccept() {
		if(bAccept == null){
			bAccept = new JButton();
			bAccept.setText(PluginServices.getText(this, "Aceptar"));
			bAccept.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		}
		return bAccept;
	}

	/**
	 * This method initializes jButton1	
	 * 	
	 * @return javax.swing.JButton	
	 */
	public JButton getBApply() {
		if(bApply == null){
			bApply = new JButton();
			bApply.setText(PluginServices.getText(this, "Aplicar"));
			bApply.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		}
		return bApply;
	}

	/**
	 * This method initializes jButton2	
	 * 	
	 * @return javax.swing.JButton	
	 */
	public JButton getBClose() {
		if(bClose == null){
			bClose = new JButton();
			bClose.setText(PluginServices.getText(this, "Cerrar"));
			bClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		}
		return bClose;
	}
	
	/**
	 * Redimensiona el componente al tama�o pasado por par�metro
	 * respetando los margenes establecidos.
	 * @param w Nuevo ancho del componente
	 * @param h Nuevo alto del componente
	 */
	public void setSizeComponent(int w, int h){
		/*this.width = w - MARGIN;
		this.height = h;
		this.setPreferredSize(new java.awt.Dimension(width, height));
		getPSelector().setPreferredSize(new java.awt.Dimension(WIDTH_SELECTOR, height));
		getPSelector().setComponentSize(WIDTH_SELECTOR, height);
		getPRight().setPreferredSize(new java.awt.Dimension(width - WIDTH_SELECTOR, height));
		getPSelectedFilter().setPreferredSize(new java.awt.Dimension(width - WIDTH_SELECTOR - MARGIN, height - HEIGHT_BUTTONS));
		getPButtons().setPreferredSize(new java.awt.Dimension(width - WIDTH_SELECTOR - MARGIN, HEIGHT_BUTTONS));*/
	}

	/**
	 * Asigna el panel que se mostrar� por defecto al abrir el dialogo
	 * @param name Nombre del panel
	 */
	public void setVisiblePanel(int pos){
		//Los ponemos todos invisibles
		for(int i = 0; i < listPanels.size(); i++)
			((JPanel)listPanels.get(i)).setVisible(false);
		
		//Ponemos visible solo el panel seleccionado
		if(((JPanel)listPanels.get(pos)) != null)
			((JPanel)listPanels.get(pos)).setVisible(true);
	}
	
	/**
	 * Obtiene el panel visible
	 * @return panel visible
	 */
	public JPanel getVisiblePanel(){
		for(int i = 0; i < listPanels.size(); i++)
			if(((JPanel)listPanels.get(i)).isVisible())
				return ((JPanel)listPanels.get(i));
		return null;
	}
	
	/**
	 * Asigna el t�tulo al panel
	 * @param title T�tulo del panel
	 */
	public void setTitle(String title){
		this.title = title;
		this.getPSelectedFilter().setBorder(javax.swing.BorderFactory.createTitledBorder(null, title, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null));
	}
	
	/**
	 * Obtiene el t�tulo del panel
	 * @param title T�tulo del panel
	 */
	public String getTitle(){
		return title;
	}
	
	//----------------------------------------------------------------
	//Gesti�n de paneles
	
	/**
	 * Asigna un panel con un nombre de entrada de men� y una posici�n en la lista
	 * @param panel Panel a registrar
	 * @param panelPos Posici�n del panel para esa entrada
	 * @param menuEntry Entrada de men�
	 */
	public static void addPanel(JPanel panel, int panelPos, String menuEntry){
		listPanels.add(panelPos, panel);
		panelEntry.put(menuEntry, new Integer(panelPos));
	}
	
	/**
	 * Obtiene un panel asociado a un nombre que corresponde con la entrada de men�
	 * @param name Nombre de la entrada de men�
	 * @return Panel que corresponde con esa entrada
	 */
	public static JPanel getPanel(String name){
		int index = ((Integer)panelEntry.get(name)).intValue();
		return (JPanel)listPanels.get(index);
	}
	
	/**
	 * Obtiene la posici�n del panel que corresponde a la entrada de men�
	 * @param menuEntry Entrada de men�
	 * @return Posici�n del panel para esa entrada
	 */
	public static int getPos(String menuEntry){
		try{
			return ((Integer)panelEntry.get(menuEntry)).intValue();
		}catch(NullPointerException exp){
			return -1;
		}
	}
	
	/**
	 * Obtiene el panel de la posici�n i
	 * @param pos Posici�n del panel a recuperar
	 * @return Panel de la posici�n i
	 */
	public static JPanel getPanel(int pos){
		try{
			return (JPanel)listPanels.get(pos);
		}catch(NullPointerException exp){
			return null;
		}
	}
	
	/**
	 * Obtiene el n�mero de paneles registrados
	 * @return N�mero de paneles
	 */
	public static int getPanelCount(){
		try{
			return listPanels.size();
		}catch(NullPointerException exp){
			return 0;
		}
	}
	
	/**
	 * Resetea las listas de paneles y asociaci�n de entradas de men�
	 */
	public static void resetLists(){
		listPanels.clear();
		panelEntry.clear();
	}
	

}  

