/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
*
* Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
*
* For more information, contact:
*
*  Generalitat Valenciana
*   Conselleria d'Infraestructures i Transport
*   Av. Blasco Ib��ez, 50
*   46010 VALENCIA
*   SPAIN
*
*      +34 963862235
*   gvsig@gva.es
*      www.gvsig.gva.es
*
*    or
*
*   IVER T.I. S.A
*   Salamanca 50
*   46005 Valencia
*   Spain
*
*   +34 963163400
*   dac@iver.es
*/
package com.iver.cit.gvsig.gui.toc;

import javax.swing.JMenuItem;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrWCS;
import com.iver.cit.gvsig.project.documents.view.gui.dialogs.HistogramDialog;
import com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction;
import com.iver.cit.gvsig.project.documents.view.toc.ITocItem;

/**
 * Entrada del men� contextual para el histograma
 * @author Nacho Brodin (brodin_ign@gva.es)
 */
public class WCSHistogramTocMenuEntry  extends AbstractTocContextMenuAction {
	private JMenuItem 				propsMenuItem;
	private FLayer 					lyr = null;
	private HistogramDialog			histogramDialog = null;
	private int 					width = 650;
	private int 					height = 500;

	public String getGroup() {
		return "RasterTools"; 
	}

	public int getGroupOrder() {
		return 80;
	}

	public int getOrder() {
		return 0;
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return selectedItems.length == 1;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			lyr = getNodeLayer(item);
            if ((lyr instanceof FLyrWCS)) {
            	return true;
            }
		}
		return false;
	}
		
	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.gui.toc.TocMenuEntry#execute(com.iver.cit.gvsig.gui.toc.ITocItem)
	 */
	public void execute(ITocItem item, FLayer[] selectedItems) {
       	lyr = getNodeLayer(item);
        
		if(lyr instanceof FLyrWCS && ((FLyrWCS)lyr).getPxRaster()!=null){
    		histogramDialog = new HistogramDialog(width, height);
    		histogramDialog.setOnlyViewValue();
    		histogramDialog.setHistogramObj(((FLyrWCS)lyr).getGrid().getHistogram());
    		
    		//Par�metros de inicializaci�n del histograma
    		histogramDialog.getHistogramPanel().setHistogramDataSource(0); //Fuente de datos; la vista
    		histogramDialog.getHistogramPanel().setType(0); //Tipo de histograma; no acumulado
    		histogramDialog.setRGBInBandList(); //Asignaci�n R,G,B en el combo
    		histogramDialog.getHistogramPanel().showHistogram(); //Dibujamos histograma
       		       					
       		PluginServices.getMDIManager().addWindow(histogramDialog);    	
		}
	}

	public String getText() {
		return PluginServices.getText(this, "histograma");
	}
}