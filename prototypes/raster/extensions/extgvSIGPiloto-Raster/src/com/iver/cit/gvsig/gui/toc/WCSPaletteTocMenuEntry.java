/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
*
* Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
*
* For more information, contact:
*
*  Generalitat Valenciana
*   Conselleria d'Infraestructures i Transport
*   Av. Blasco Ib��ez, 50
*   46010 VALENCIA
*   SPAIN
*
*      +34 963862235
*   gvsig@gva.es
*      www.gvsig.gva.es
*
*    or
*
*   IVER T.I. S.A
*   Salamanca 50
*   46005 Valencia
*   Spain
*
*   +34 963163400
*   dac@iver.es
*/
package com.iver.cit.gvsig.gui.toc;

import javax.swing.JMenuItem;

import org.cresques.filter.RasterFilterStackManager;
import org.cresques.filter.bands.PaletteFilter;
import org.cresques.filter.bands.PaletteStackManager;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrWCS;
import com.iver.cit.gvsig.project.documents.view.gui.dialogs.PaletteDialog;
import com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction;
import com.iver.cit.gvsig.project.documents.view.toc.ITocItem;

/**
 * Entrada del men� contextual para el histograma
 * @author Nacho Brodin (brodin_ign@gva.es)
 */
public class WCSPaletteTocMenuEntry  extends AbstractTocContextMenuAction {
	private JMenuItem 					propsMenuItem;
	private FLayer 					lyr = null;
	private PaletteDialog				paletteDialog = null;
	private RasterFilterStackManager	stackManager = null;
	private int 						width = 408;
	private int 						height = 420;

	public String getGroup() {
		return "RasterTools"; 
	}

	public int getGroupOrder() {
		return 80;
	}

	public int getOrder() {
		return 1;
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return selectedItems.length == 1;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			lyr = getNodeLayer(item);
            if ((lyr instanceof FLyrWCS)) {
            	return true;
            }
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.gui.toc.TocMenuEntry#execute(com.iver.cit.gvsig.gui.toc.ITocItem)
	 */
	public void execute(ITocItem item, FLayer[] selectedItems) {
       	lyr = getNodeLayer(item);
        
		if(lyr instanceof FLyrWCS && ((FLyrWCS)lyr).getPxRaster()!=null){
			FLyrWCS layer = (FLyrWCS) lyr;
			stackManager = new RasterFilterStackManager(layer.getFilterStack());

    		paletteDialog = new PaletteDialog((FLyrWCS)lyr, stackManager, width, height);
			PaletteFilter pf = (PaletteFilter)layer.getFilterStack().getByType(PaletteStackManager.palette);
			
			//Si hay en la pila alguna paleta la carga en el dialogo
			if(pf != null && pf.getPalette() != null){
				paletteDialog.getPPalette().loadPanelFromPalette(pf.getPalette());
				if(pf.isExec())
					paletteDialog.getPPalette().getCActive().setSelected(true);
				else
					paletteDialog.getPPalette().getCActive().setSelected(false);
				
				if(pf.isInterpolate())
					paletteDialog.getPPalette().getCbInterpolar().setSelected(true);
				else
					paletteDialog.getPPalette().getCbInterpolar().setSelected(false);
			}
	
			//No se puede modificar la paleta de un WMS ya que esta se recarga cada 
			//vez que se hace un zoom. Habr�a que llevar un control de cambios para cada
			//entrada de la tabla y volver a aplicarlo al siguiente zoom
			paletteDialog.getPPalette().getBAccept().setEnabled(false);
			paletteDialog.getPPalette().getBApply().setEnabled(false);
			
       		PluginServices.getMDIManager().addWindow(paletteDialog);    	
		}
	}
	
	public String getText() {
		return PluginServices.getText(this, "paleta");
	}
}