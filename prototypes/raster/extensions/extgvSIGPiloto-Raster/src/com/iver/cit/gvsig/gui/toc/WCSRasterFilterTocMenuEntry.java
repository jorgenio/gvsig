/*
 * Created on 19-jul-2006
 * 
 * gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 */
package com.iver.cit.gvsig.gui.toc;

import javax.swing.JMenuItem;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;

import org.cresques.filter.RasterFilter;
import org.cresques.filter.RasterFilterStack;
import org.cresques.filter.RasterFilterStackManager;
import org.cresques.filter.convolution.ConvolutionFilter;
import org.cresques.filter.segmentation.FirstDerivativeFilter;
import org.cresques.ui.filter.FirstDerivativePanel;
import org.cresques.ui.filter.KernelFiltersPanel;
import org.gvsig.gui.beans.treelist.TreeListContainer;
import org.gvsig.gui.beans.treelist.event.TreeListEvent;
import org.gvsig.gui.beans.treelist.listeners.TreeListComponentListener;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrWCS;
import com.iver.cit.gvsig.gui.panels.RasterFilterPanel;
import com.iver.cit.gvsig.project.documents.view.gui.dialogs.RasterFilterDialog;
import com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction;
import com.iver.cit.gvsig.project.documents.view.toc.ITocItem;


/**
 * Entrada en del men� contextual del TOC correspondiente a los filtros para WCS.
 * 
 * @author Nacho Brodin (brodin_ign@gva.es)
 *
 */
public class WCSRasterFilterTocMenuEntry extends AbstractTocContextMenuAction implements TreeSelectionListener, TreeListComponentListener{
	private JMenuItem 						filters;
	private FLayer							lyr = null;
	private RasterFilterDialog				rasterFilterDialog = null;
	private RasterFilterStackManager		stackManager = null;
	private int 							width = 620;
	private int 							height = 310;
	
	//Paneles insertados en el interfaz general de filtros
	private FirstDerivativePanel 			sobel = new FirstDerivativePanel();
	private FirstDerivativePanel 			roberts = new FirstDerivativePanel();
	private FirstDerivativePanel 			prewitt = new FirstDerivativePanel();
	private FirstDerivativePanel 			frei_chen = new FirstDerivativePanel();
	private KernelFiltersPanel 				paso_bajo = new KernelFiltersPanel(1);
	private KernelFiltersPanel 				mediana = new KernelFiltersPanel(4);
	private KernelFiltersPanel				media = new KernelFiltersPanel(4);
	private KernelFiltersPanel 				gauss = new KernelFiltersPanel(2);
	private KernelFiltersPanel 				laplace = new KernelFiltersPanel(2);
	
	public String getGroup() {
		return "RasterTools"; 
	}

	public int getGroupOrder() {
		return 80;
	}

	public int getOrder() {
		return 2;
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return selectedItems.length == 1;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			lyr = getNodeLayer(item);
            if ((lyr instanceof FLyrWCS)) {
            	return true;
            }
		}
		return false;
	}
	
	/**
	 * Gestiona la apertura del dialogo de propiedades de raster cuando se pulsa
	 * la opci�n asignando a este las propiedades iniciales. 
	 */
	public void execute(ITocItem item, FLayer[] selectedItems)  {
       	lyr = getNodeLayer(item);
                   
       	if(lyr instanceof FLyrWCS){
       		stackManager = new RasterFilterStackManager(((FLyrWCS)lyr).getFilterStack());
       		
       		//A�adimos paneles al cuadro
       		addPanels();
       		
       		rasterFilterDialog = new RasterFilterDialog((FLyrWCS)lyr, width, height, stackManager);
       		       		
       		//Construimos el men�
       		menuBuild();       	
       		
       		//Cargamos la lista con lo que hay en la pila de filtros
       		loadList();
       		
       		//Cargamos el panel que es visible para el usuario
       		loadVisiblePanel();
       		
       		PluginServices.getMDIManager().addWindow(rasterFilterDialog);
        }
	}
	
	/**
	 * A�ade paneles al dialogo de filtros
	 */
	private void addPanels(){
		RasterFilterPanel.resetLists();
  
		RasterFilterPanel.addPanel(sobel, 0, PluginServices.getText(this, "sobel"));
   		RasterFilterPanel.addPanel(roberts, 1, PluginServices.getText(this, "roberts"));
   		RasterFilterPanel.addPanel(prewitt, 2, PluginServices.getText(this, "prewitt"));
   		RasterFilterPanel.addPanel(frei_chen, 3, PluginServices.getText(this, "frei_chen"));
   		RasterFilterPanel.addPanel(paso_bajo, 4, PluginServices.getText(this, "paso_bajo"));
   		RasterFilterPanel.addPanel(mediana, 5, PluginServices.getText(this, "mediana"));
   		RasterFilterPanel.addPanel(media, 6, PluginServices.getText(this, "media"));
   		RasterFilterPanel.addPanel(gauss, 7, PluginServices.getText(this, "gauss"));
   		RasterFilterPanel.addPanel(laplace, 8, PluginServices.getText(this, "laplace"));
	}
	
	/**
	 * Construye el arbol para el men� de filtros disponibles
	 */
	private void menuBuild(){
		TreeListContainer lc = rasterFilterDialog.getSelector();
		lc.getTree().addTreeSelectionListener(this);
		lc.addTreeListListener(this);
		lc.addClass(PluginServices.getText(this, "detecci�n_bordes"), 0);
		lc.addEntry(PluginServices.getText(this, "sobel"), PluginServices.getText(this, "detecci�n_bordes"),"sobel");
		lc.addEntry(PluginServices.getText(this, "roberts"), PluginServices.getText(this, "detecci�n_bordes"),"roberts");
		lc.addEntry(PluginServices.getText(this, "laplace"), PluginServices.getText(this, "detecci�n_bordes"),"laplace");
		lc.addEntry(PluginServices.getText(this, "prewitt"), PluginServices.getText(this, "detecci�n_bordes"),"prewitt");
		lc.addEntry(PluginServices.getText(this, "frei_chen"), PluginServices.getText(this, "detecci�n_bordes"),"frei_chen");
		lc.addClass(PluginServices.getText(this, "suavizado"), 1);
		lc.addEntry(PluginServices.getText(this, "paso_bajo"), PluginServices.getText(this, "suavizado"),"paso_bajo");
		lc.addEntry(PluginServices.getText(this, "mediana"), PluginServices.getText(this, "suavizado"),"mediana");
		lc.addEntry(PluginServices.getText(this, "media"), PluginServices.getText(this, "suavizado"),"media");
		lc.addEntry(PluginServices.getText(this, "gauss"), PluginServices.getText(this, "suavizado"),"gauss");
		lc.getTree().expandRow(0);
	}

	/**
	 * Carga la lista de filtros con los que hay actualmente en la pila.
	 * Solo a�adir� los filtros que tienen nombre, es decir que la variable
	 * filterName tiene un valor distinto de vacio que es el valor con el que
	 * se inicializa.
	 */
	private void loadList(){
		TreeListContainer lc = rasterFilterDialog.getSelector();
		RasterFilterStack filterStack = this.stackManager.getFilterStack();
		for(int iFilter = 0; iFilter < filterStack.lenght(); iFilter ++){
			RasterFilter rasterFilter = filterStack.get(iFilter);
			if(!rasterFilter.getFilterName().equals("")&& lc.getMap().containsValue(rasterFilter.getFilterName()))
				lc.addElementInList(PluginServices.getText(this,rasterFilter.getFilterName()));
		}
	}
	
	/**
	 * Carga el panel que es visible al usuario. Si hay filtros en la lista
	 * se mostrar� el primero de la lista sino se mostrar� el primero del arbol
	 * de filtros.
	 */
	private void loadVisiblePanel(){
		String firstFilter = "";
		TreeListContainer lc = rasterFilterDialog.getSelector();
		DefaultTreeModel modelTree = (DefaultTreeModel)lc.getTree().getModel();
		
		firstFilter = modelTree.getChild(modelTree.getChild(modelTree.getRoot(), 0), 0).toString();
		lc.getTree().expandRow(1);
		lc.getTree().setSelectionInterval(2, 2);
				
		int pos = RasterFilterPanel.getPos(firstFilter);
		//Si pos es -1 es pq para el primero de la lista no existe panel para �l.  
		if(pos == -1)
			return;
		else{
			rasterFilterDialog.setVisiblePanel(pos);
			rasterFilterDialog.getPFilter().setTitle(PluginServices.getText(this, "filtro_de")+" "+firstFilter);
		}
	}
	
	/**
	 * Este m�todo activa el panel visible si su filtro correspondiente est� en la lista de filtros 
	 * activos sino lo desactiva.
	 * @param lc Lista a comprobar si est� el filtro insertado
	 * @param filter Nombre del filtro del cual se comprueba si est� en la lista 
	 */
	private void setEnableVisiblePanel(TreeListContainer lc, String filter){
		if(lc.isInList(PluginServices.getText(this, filter)))
   			rasterFilterDialog.getVisiblePanel().setEnabled(true);
   		else
   			rasterFilterDialog.getVisiblePanel().setEnabled(false);
	}
		
	/**
	 * Acciones realizadas para el paso de par�metros al panel del filtro de primera derivada
	 * @panel N�mero de orden del panel a seleccionar como visible
	 * @param selection Entrada de men� seleccionada
	 * @param filter Filtro del cual queremos realizar el paso de par�metros
	 * @param lc Lista a comprobar si est� el filtro insertado
	 * @param panel Panel al que se le asignaran los par�metros
	 */
	private void actionsForFirstDerivativeFilter(int nPanel, String selection, String filter, TreeListContainer lc, FirstDerivativePanel panel){
		if(selection.equals(PluginServices.getText(this, filter))){
			rasterFilterDialog.getPFilter().setTitle(PluginServices.getText(this, "filtro_de")+" "+PluginServices.getText(this, filter));
       		rasterFilterDialog.setVisiblePanel(nPanel);
       		setEnableVisiblePanel(lc, filter);
       		FirstDerivativeFilter rf = (FirstDerivativeFilter)stackManager.getFilter(filter);
       		if(rf != null)
       			panel.getTUmbral().setText(rf.getUmbral() + "");
		}
	}
	
	/**
	 * Acciones realizadas para el paso de par�metros al panel del filtro de primera derivada
	 * @panel N�mero de orden del panel a seleccionar como visible
	 * @param selection Entrada de men� seleccionada
	 * @param filter Filtro del cual queremos realizar el paso de par�metros
	 * @param lc Lista a comprobar si est� el filtro insertado
	 * @param panel Panel al que se le asignaran los par�metros
	 */
	private void actionsForConvolutionFilter(int nPanel, String selection, String filter, TreeListContainer lc, KernelFiltersPanel panel){
		if(selection.equals(PluginServices.getText(this, filter))){
			rasterFilterDialog.getPFilter().setTitle(PluginServices.getText(this, "filtro_de")+" "+PluginServices.getText(this, filter));
       		rasterFilterDialog.setVisiblePanel(nPanel);
       		setEnableVisiblePanel(lc, filter);
       		ConvolutionFilter rf = (ConvolutionFilter)stackManager.getFilter(filter);
       		if(rf != null){
       			int index = 0;
       			switch(rf.getKernel().kernel.length){
       			case 5:index = 1; break;
       			case 7:index = 2; break;
       			case 9:index = 3; break;
       			}
       			panel.getCbKernel().setSelectedIndex(index);
       		}
		}
	}
	
	/**
	 * Este m�todo se ejecuta cada vez que cambia el valor seleccionado en el men�, es decir cuando se pincha
	 * sobre una entrada de men� define que acciones se realizan. En �l, hay que definir para cada entrada (addEntry)
	 * que hallamos puesto en la funci�n menuBuild cual ser� el comportamiento de esta, es decir que panel se muestra
	 * cuando se selecciona dicha entrada.  
	 */
	public void valueChanged(TreeSelectionEvent e) {
		String path = null;
		try{
			path = e.getNewLeadSelectionPath().toString();
		}catch(NullPointerException exc){
			return;
		}
		if(path == null)
			return;
		
		String selection = path.substring(path.lastIndexOf(", ") + 2, path.lastIndexOf("]"));
		
		TreeListContainer lc = rasterFilterDialog.getSelector();
		
		actionsForFirstDerivativeFilter(0, selection, "sobel", lc, sobel);
		actionsForFirstDerivativeFilter(1, selection, "roberts", lc, roberts);
		actionsForFirstDerivativeFilter(2, selection, "prewitt", lc, prewitt);
		actionsForFirstDerivativeFilter(3, selection, "frei_chen", lc, frei_chen);
		actionsForConvolutionFilter(4, selection, "paso_bajo", lc, paso_bajo);
		actionsForConvolutionFilter(5, selection, "mediana", lc, mediana);
		actionsForConvolutionFilter(6, selection, "media", lc, media);
		actionsForConvolutionFilter(7, selection, "gauss", lc, gauss);
		actionsForConvolutionFilter(8, selection, "laplace", lc, laplace);
		
	}

	/**
	 * Este m�todo es ejecutado cuando se inserta un nuevo elemento en la lista
	 * @param e TreeListEvent
	 */
	public void elementAdded(TreeListEvent e){
		int pos = RasterFilterPanel.getPos(e.getElementAdded());
		try{
			if(pos > -1)
				rasterFilterDialog.getPanel(pos).setEnabled(true);
		}catch(ArrayIndexOutOfBoundsException exc){
			return;
		}
	}
	
	/**
	 * Este m�todo es ejecutado cuando se elimina un elemento de la lista
	 * @param e TreeListEvent
	 */
	public void elementRemoved(TreeListEvent e){
		int pos = RasterFilterPanel.getPos(e.getElementRemoved());
		try{
			if(pos > -1)
				rasterFilterDialog.getPanel(pos).setEnabled(false);
		}catch(ArrayIndexOutOfBoundsException exc){
			return;
		}
	}

	/**
	 * Este m�todo es ejecutado cuando se mueve un elemento de la lista
	 * @param e TreeListEvent
	 */
	public void elementMoved(TreeListEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public String getText() {
		return PluginServices.getText(this, "filters");
	}
}
