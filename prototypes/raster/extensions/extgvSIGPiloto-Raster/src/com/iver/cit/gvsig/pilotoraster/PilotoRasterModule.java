/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 */
package com.iver.cit.gvsig.pilotoraster;

import com.iver.andami.PluginServices;
import com.iver.andami.plugins.Extension;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.cit.gvsig.gui.toc.WCSHistogramTocMenuEntry;
import com.iver.cit.gvsig.gui.toc.WCSPaletteTocMenuEntry;
import com.iver.cit.gvsig.gui.toc.WCSRasterFilterTocMenuEntry;
import com.iver.cit.gvsig.gui.toc.WMSHistogramTocMenuEntry;
import com.iver.cit.gvsig.gui.toc.WMSPaletteTocMenuEntry;
import com.iver.cit.gvsig.gui.toc.WMSRasterFilterTocMenuEntry;
import com.iver.cit.gvsig.project.documents.view.toc.actions.CutRasterTocMenuEntry;
import com.iver.cit.gvsig.project.documents.view.toc.actions.HistogramTocMenuEntry;
import com.iver.cit.gvsig.project.documents.view.toc.actions.PaletteTocMenuEntry;
import com.iver.cit.gvsig.project.documents.view.toc.actions.RasterFilterTocMenuEntry;
import com.iver.utiles.extensionPoints.ExtensionPoints;
import com.iver.utiles.extensionPoints.ExtensionPointsSingleton;


/**
 * Extensi�n para el piloto de raster. A�ade las entrada de men� necesarias.
 * @author Nacho Brodin (brodin_ign@gva.es)
 */
public class PilotoRasterModule extends Extension {
	
    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#inicializar()
     */
    public void initialize() {
    	ExtensionPoints extensionPoints = ExtensionPointsSingleton.getInstance();
    	extensionPoints.add("View_TocActions","Histogram",new HistogramTocMenuEntry());
       	extensionPoints.add("View_TocActions","RasterFilter",new RasterFilterTocMenuEntry());
       	extensionPoints.add("View_TocActions","Palette",new PaletteTocMenuEntry());
       	extensionPoints.add("View_TocActions","CutRaster",new CutRasterTocMenuEntry());
       	
       	//Puntos de extensi�n para WCS
       	extensionPoints.add("View_TocActions","HistogramWCS",new WCSHistogramTocMenuEntry());
       	extensionPoints.add("View_TocActions","RasterFilterWCS",new WCSRasterFilterTocMenuEntry());
       	extensionPoints.add("View_TocActions","PaletteWCS",new WCSPaletteTocMenuEntry());
       	
       	//Puntos de extensi�n para WMS
       	extensionPoints.add("View_TocActions","HistogramWMS",new WMSHistogramTocMenuEntry());
       	extensionPoints.add("View_TocActions","RasterFilterWMS",new WMSRasterFilterTocMenuEntry());
       	extensionPoints.add("View_TocActions","PaletteWMS",new WMSPaletteTocMenuEntry());
    }
   
    /**
     * Cargamos el listener y ejecutamos la herramienta de salvar a Raster.
     */
    public void execute(String actionCommand) {
    	
    }

    /* (non-Javadoc)
     * @see com.iver.andami.plugins.Extension#isEnabled()
     */
    public boolean isEnabled() {
        return true;
    }

    /**
     * Mostramos el control si hay alguna capa cargada.
     */
    public boolean isVisible() {
        com.iver.andami.ui.mdiManager.IWindow f = PluginServices.getMDIManager()
                                                             .getActiveWindow();
        if (f == null) 
            return false;
      
        return (f  instanceof IWindow);
    }
}
