/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 */
package com.iver.cit.gvsig.project.documents.view.gui.dialogs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.log4j.Logger;
import org.cresques.io.data.CutRasterProcess;
import org.cresques.io.data.Grid;
import org.cresques.io.data.ICutRaster;
import org.cresques.io.data.IQueryableRaster;
import org.cresques.px.Extent;
import org.cresques.ui.raster.CutRasterPanel;
import org.gvsig.gui.beans.table.models.CheckBoxModel;

import com.hardcode.driverManager.Driver;
import com.iver.andami.PluginServices;
import com.iver.andami.Utilities;
import com.iver.andami.messages.NotificationManager;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.IWindowListener;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.drivers.RasterDriver;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrRaster;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.fmap.tools.Behavior.Behavior;
import com.iver.cit.gvsig.fmap.tools.Behavior.MouseMovementBehavior;
import com.iver.cit.gvsig.fmap.tools.Behavior.RectangleBehavior;
import com.iver.cit.gvsig.project.documents.view.gui.View;
import com.iver.cit.gvsig.project.documents.view.toolListeners.CutRasterListener;
import com.iver.cit.gvsig.project.documents.view.toolListeners.StatusBarListener;



public class CutRasterDialog extends JPanel implements IWindow, ActionListener,
		ComponentListener, ICutRaster, IWindowListener {
		
	private static Logger logger = Logger.getLogger(CutRasterDialog.class.getName());
	
	private int 						width = 0, height = 0;
	/**
	 * Panel de recortado de imagen que est� en la libreria raster
	 */
	private CutRasterPanel				cutPanel = null;
	/**
	 * Capa raster cargada en la vista que es la fuente de datos
	 */
	private FLyrRaster					fLayer = null;
	/**
	 * Ancho y alto en pixeles de la imagen a recortar
	 */
	private int 						widthPx = 0, heightPx = 0;
	/**
	 * clase que realiza el proceso de recortado y extracci�n de bandas
	 */
	private CutRasterProcess			process = null;
	/**
	 * Grid del raster fuente de datos
	 */
	private Grid 						grid = null;
	/**
	 * Valores iniciales de ancho y alto en pixeles de la ventana.
	 */
	private double 						initWidth, initHeight;
	/**
	 * Recuerda la �ltima ruta seleccionada por el usuario
	 */
	private static String				lastPath = "./";
	/**
	 * Lista de ficheros creados. Si se ha creado una imagen por banda solo 
	 * tendr� un elemento, sino tendr� uno por banda.
	 */
	private String[]					fileNames = null;
	/**
	 * Herramienta seleccionada en el momento de la apertura del dialogo
	 */
	private String						lastTool = null;
	private MapControl					mapCtrl = null;
	private String						viewName = null;

	/**
	 * Constructor
	 * @param width Ancho
	 * @param height Alto
	 */
	public CutRasterDialog(FLyrRaster layer, int width, int height, MapControl mapCtrl, String viewName){
		this.width = width;
		this.height = height;
		this.fLayer = layer;
		this.mapCtrl = mapCtrl;
		this.lastTool = mapCtrl.getCurrentTool();
		this.viewName = viewName;
		
		ArrayList attr = ((FLyrRaster)layer).getSource().getAttributes();
   		for (int i=0; i<attr.size(); i++) {
			Object[] a = (Object []) attr.get(i);
			if(a[0].toString().equals("Width"))
				widthPx = ((Integer)a[1]).intValue();
			if(a[0].toString().equals("Height"))
				heightPx = ((Integer)a[1]).intValue();
		}
		
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		this.setPreferredSize(new Dimension(width, height));
		
		this.add(getPCut(), gridBagConstraints);
		this.addComponentListener(this);
		
		getPCut().getBSave().addActionListener(this);
		getPCut().getBAccept().addActionListener(this);
		getPCut().getBCancel().addActionListener(this);
	
	}
	
	/**
	 * Obtiene el panel de recorte
	 * @return TailImagePanel
	 */
	public CutRasterPanel getPCut(){
		if(cutPanel == null){
			try {
				cutPanel = new CutRasterPanel(fLayer.getFullExtent(), widthPx, heightPx, this);
			} catch (DriverException e) {
				NotificationManager.addError("No se ha podido obtener el extent de la capa al crear el panel de recorte", e);
				return null;
			}
			cutPanel.setComponentSize(this.width - 10, this.height - 40);
			cutPanel.addComponentListener(this);
		}
		return cutPanel;
	}
	
	/**
	 * Asigna el valor inicial en ancho y alto de la ventana en pixeles .
	 * @param w Ancho en pixeles
	 * @param h Alto en pixeles
	 */
	public void setInitValuesToSize(double w, double h){
		initWidth = w;
		initHeight = h;
	}
		
	/**
	 * @see com.iver.mdiApp.ui.MDIManager.View#getViewInfo()
	 */
	public WindowInfo getWindowInfo() {
		WindowInfo m_viewinfo=new WindowInfo(WindowInfo.MODELESSDIALOG /*| ViewInfo.RESIZABLE*/);
    	m_viewinfo.setTitle(PluginServices.getText(this, "recorte"));
    	m_viewinfo.setWidth(width);
    	m_viewinfo.setHeight(height - 36);
		return m_viewinfo;
	}

	
	/**
	 * Al pulsar Cancelar o Aceptar la ventana se cierra.
	 */
	public void actionPerformed(ActionEvent e) {		
		
		//Bot�n de Aceptar o Aplicar
		if(e.getSource() == cutPanel.getBAccept())
			accept();
		
		//Bot�n de Cerrar
		if(e.getSource() == cutPanel.getBCancel()){
			try{
	        	if(lastTool != null)
	        		mapCtrl.setTool(lastTool);
				PluginServices.getMDIManager().closeWindow(CutRasterDialog.this);
			}catch(ArrayIndexOutOfBoundsException ex){
				//Si la ventana no se puede eliminar no hacemos nada
			}
		}
		
		//Bot�n de Salvar
		if(e.getSource() == cutPanel.getBSave())
			saveRaster();
				
		fLayer.getMapContext().invalidate();
	}

	/**
	 * Acciones realizadas cuando se acepta en el dialogo. Se obtendr�n los datos de recorte desde el dialogo,
	 * crearemos el objeto TailRasterProcess que gestiona el recortado, ajustamos el tama�o del grid
	 * de salida y procesamos.
	 */
	private void accept(){
		grid = fLayer.getGrid();
		
		//Controlamos las coordenadas del recorte que no se salgan de la imagen.
		//De ser as� mostramos un error
		String[] values = cutPanel.getCoordReal();
		try{
			double minX = Double.parseDouble(values[0]);
			double maxY = Double.parseDouble(values[1]);
			double maxX = Double.parseDouble(values[2]);
			double minY = Double.parseDouble(values[3]);
			Extent ext = grid.getExtent();
			if(minX > ext.maxX() || maxX < ext.minX() || minY > ext.maxY() || maxY < ext.minY()){
				JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),
						PluginServices.getText(this, "coordenadas_erroneas"));
				return;
			}
		}catch(NumberFormatException e){
			JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),
					PluginServices.getText(this, "coordenadas_erroneas"));
			return;
		}
		
		//Obtenemos las coordenadas del recorte
		String[] sValues = cutPanel.getCoordPixel();
		int[] dValues = new int[sValues.length];
		try{
			for(int i = 0; i < sValues.length; i ++)
				dValues[i] = (int)Math.round(Double.valueOf(sValues[i]).doubleValue());
		}catch(NumberFormatException exc){
			//Los valores de las cajas son incorrectos. Terminamos la funci�n
			return ;
		}
					
		//Seleccionamos las bandas que se usaran en el recorte a partir de la tabla
		int countBands = 0;
		int rowCount = ((CheckBoxModel)getPCut().getTSelection().getModel()).getRowCount();
		for(int iRow = 0; iRow < rowCount; iRow++)
			if((((Boolean)((CheckBoxModel)getPCut().getTSelection().getModel()).getValueAt(iRow, 0))).booleanValue())
				countBands++;
		
		int[] drawableBands = new int[countBands];
		int i = 0;
		for(int iRow = 0; iRow < rowCount; iRow++){
			if((((Boolean)((CheckBoxModel)getPCut().getTSelection().getModel()).getValueAt(iRow, 0))).booleanValue()){
				drawableBands[i ++] = iRow;
			}
		}
				
		//Usamos el directorio de temporales de andami
		String tmpImgs = Utilities.createTempDirectory();
		
		process = new CutRasterProcess(	dValues,
											drawableBands, 
											grid, 
											tmpImgs + File.separator + "cutLayer", cutPanel.getCbOneLyrPerBand().isSelected(),
											(int)initWidth,
											(int)initHeight,
											this);
		
		//Remuestreamos por si se ha variado el tama�o del raster de salida
		grid.setAdjustedWindow((int)cutPanel.getWidthText(), (int)cutPanel.getHeightText(), IQueryableRaster.INTERPOLATION_NearestNeighbour, null);
		
		fileNames = process.createWriters(grid.getRasterBuf(), (int)cutPanel.getWidthText(), (int)cutPanel.getHeightText());
		process.start();
		getPCut().getBSave().setEnabled(true);
	
	}
	
	/**
	 * Acciones realizadas cuando se salva los recortes de raster. Aparecer� un dialogo para introducir
	 * la ruta y salvaremos el raster completo con el nombre proporcionado o si se ha salvado por bandas
	 * se guardar� un fichero por cada una con el nombre proporcionado m�s _Bn.
	 */
	private void saveRaster(){
		JFileChooser chooser = new JFileChooser(lastPath);
		chooser.setDialogTitle(PluginServices.getText(this, "seleccionar_directorio"));
		
		int returnVal = chooser.showOpenDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION){
		 	String fName = chooser.getSelectedFile().toString();

		 	if(fileNames == null || fileNames.length == 0)
		 		return;
		 	
		 	try {
			 	if(fileNames.length == 1){
				 	if(!fName.endsWith(".tif"))
				 		fName += ".tif";
				 	File oldFile = new File(fileNames[0]);
					org.cresques.util.Utilities.copyFile(fileNames[0], fName);
			 	}else{
			 		if(fName.endsWith(".tif"))
			 			fName = fName.substring(0, fName.indexOf("."));
			 		for(int iFile = 0; iFile < fileNames.length; iFile ++){
			 			File oldFile = new File(fileNames[iFile]);
			 			org.cresques.util.Utilities.copyFile(fileNames[iFile], fName + "_B" + iFile + ".tif");
			 		}
			 	}
		 	} catch (FileNotFoundException e1) {
				logger.error(PluginServices.getText(this,"file_not_found "), e1);
			} catch (IOException e1) {
				logger.error(PluginServices.getText(this,"io_exception"), e1);
			}				 	
		 		
		 	lastPath = chooser.getCurrentDirectory().getAbsolutePath();
		}
		 	
	}
	
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentResized(ComponentEvent e) {
		PluginServices.getMDIManager().getWindowInfo(PluginServices.getMDIManager().getActiveWindow()).setWidth(cutPanel.getWidth()+10);
		PluginServices.getMDIManager().getWindowInfo(PluginServices.getMDIManager().getActiveWindow()).setHeight(cutPanel.getHeight()+40);
	}

	public void componentShown(ComponentEvent e) {
		
	}

	/**
	 * Obtiene la capa raster
	 * @return capa raster
	 */
	public FLyrRaster getFLayer() {
		return fLayer;
	}

	/**
	 * Acciones que se realizan al finalizar de crear los recortes de imagen.
	 * Este m�todo es llamado por el thread TailRasterProcess al finalizar.
	 */
	public void cutFinalize(String[] fileNames) {
		//seleccionamos la vista de gvSIG
		com.iver.cit.gvsig.project.documents.view.gui.View theView = null;
		try{
			IWindow[] allViews = PluginServices.getMDIManager().getAllWindows();
			for(int i = 0; i < allViews.length; i++){
				if(allViews[i] instanceof com.iver.cit.gvsig.project.documents.view.gui.View &&
						PluginServices.getMDIManager().getWindowInfo((View)allViews[i]).getTitle().equals(viewName))
					theView = (com.iver.cit.gvsig.project.documents.view.gui.View) allViews[i];					
			}
			if(theView == null)
				return;
		}catch(ClassCastException ex){
			logger.error(PluginServices.getText(this,"cant_get_view "), ex);
			return;
		}
		
		//Cargamos las capas
		for(int iFName = 0; iFName < fileNames.length; iFName ++){
			theView.getMapControl().getMapContext().beginAtomicEvent();
			try {
				Driver driver = LayerFactory.getDM().getDriver("gvSIG Image Driver");
				int endIndex = fileNames[iFName].lastIndexOf(".");
				if(endIndex < 0)
					endIndex = fileNames[iFName].length();
				FLayer lyr = LayerFactory.createLayer(fileNames[iFName].substring(fileNames[iFName].lastIndexOf(File.separator) + 1, endIndex), 
						(RasterDriver)driver, 
						new File(fileNames[iFName]),
						theView.getMapControl().getProjection());
				theView.getMapControl().getMapContext().getLayers().addLayer(lyr);
			}catch (DriverException e) {
				//Intentamos cargar el siguiente 
				continue;
			}
			theView.getMapControl().getMapContext().endAtomicEvent();
		}
		theView.getMapControl().getMapContext().invalidate();
		grid.free();
		
		/*try{
			PluginServices.getMDIManager().closeView(this);
		}catch(ArrayIndexOutOfBoundsException ex){
			//Si la ventana no se puede eliminar no hacemos nada
		}*/
	}

	/**
	 * Acciones que se realizan para seleccionar la tool CutRaster
	 */
	public void selectToolButton() {
		//seleccionamos la vista de gvSIG
		com.iver.cit.gvsig.project.documents.view.gui.View theView = null;
		try{
			IWindow[] allViews = PluginServices.getMDIManager().getAllWindows();
			for(int i = 0; i < allViews.length; i++){
				if(allViews[i] instanceof com.iver.cit.gvsig.project.documents.view.gui.View &&
						PluginServices.getMDIManager().getWindowInfo((View)allViews[i]).getTitle().equals(viewName))
					theView = (com.iver.cit.gvsig.project.documents.view.gui.View) allViews[i];					
			}
			if(theView == null)
				return;
		}catch(ClassCastException ex){
			logger.error(PluginServices.getText(this,"cant_get_view "), ex);
			return;
		}
    	MapControl m_MapControl = theView.getMapControl();
    	
		//Listener de eventos de movimiento que pone las coordenadas del rat�n en la barra de estado
        StatusBarListener sbl = new StatusBarListener(m_MapControl);
        
        //Cortar Raster
        CutRasterListener crl = new CutRasterListener(m_MapControl, this);
        m_MapControl.addMapTool("cutRaster", new Behavior[]{
        				new RectangleBehavior(crl), new MouseMovementBehavior(sbl)});
        
        m_MapControl.setTool("cutRaster");
	}

	public void windowActivated() {
		// TODO Auto-generated method stub
		
	}

	public void windowClosed() {
		if(lastTool != null)
    		mapCtrl.setTool(lastTool);
	}

}