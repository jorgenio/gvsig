/*
 * Created on 19-jul-2006
 *
 */
/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 */
package com.iver.cit.gvsig.project.documents.view.gui.dialogs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.cresques.io.datastruct.Histogram;
import org.cresques.ui.raster.HistogramPanel;

import com.hardcode.driverManager.DriverLoadException;
import com.hardcode.gdbms.engine.data.DataSource;
import com.hardcode.gdbms.engine.data.DataSourceFactory;
import com.hardcode.gdbms.engine.data.NoSuchTableException;
import com.hardcode.gdbms.engine.data.driver.DriverException;
import com.hardcode.gdbms.engine.values.Value;
import com.hardcode.gdbms.engine.values.ValueFactory;
import com.iver.andami.PluginServices;
import com.iver.andami.messages.NotificationManager;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.ProjectExtension;
import com.iver.cit.gvsig.fmap.drivers.shp.DbaseFileHeaderNIO;
import com.iver.cit.gvsig.fmap.drivers.shp.DbaseFileWriterNIO;
import com.iver.cit.gvsig.fmap.edition.EditableAdapter;
import com.iver.cit.gvsig.fmap.layers.LayerFactory;
import com.iver.cit.gvsig.fmap.layers.SelectableDataSource;
import com.iver.cit.gvsig.project.ProjectFactory;
import com.iver.cit.gvsig.project.documents.table.ProjectTable;
import com.iver.cit.gvsig.project.documents.table.ProjectTableFactory;
import com.iver.cit.gvsig.project.documents.table.gui.Table;

/**
 * @author Nacho Brodin <brodin_ign@gva.es>
 */
public class HistogramDialog extends JPanel implements IWindow, ActionListener, ComponentListener{
	private final int		HBUTTONS = 28;
	private final int		MARGIN = 10;
	
	private int 			width = 0;
	private int 			height = 0;
	private HistogramPanel 	histPanel = null;
	private JPanel			pButtons = null;
	private JButton			bClose = null;

	
	public HistogramDialog(int width, int height){
		this.width = width;
		this.height = height;
		
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
		gridBagConstraints1.gridx = 0;
		gridBagConstraints1.gridy = 1;
		
		this.add(getPHistogram(), gridBagConstraints);
		this.add(getPButtons(), gridBagConstraints1);
		this.addComponentListener(this);
	}
	
	/**
	 * Obtiene el panel con el histograma
	 * @return HistogramPanel
	 */
	public HistogramPanel getPHistogram(){
		if(histPanel == null){
			histPanel = new HistogramPanel(width - MARGIN, height - MARGIN);
			histPanel.getBCreateTable().addActionListener(this);
		}
		return histPanel;
	}
	
	/**
	 * Obtiene el panel con los botones inferiores
	 * @return JPanel
	 */
	public JPanel getPButtons(){
		if(pButtons == null){
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setAlignment(java.awt.FlowLayout.RIGHT);
			flowLayout.setHgap(2);
			flowLayout.setVgap(0);
			pButtons = new JPanel();	
			//pButtons.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.gray,1));
			pButtons.setLayout(flowLayout);
			pButtons.add(getBClose(), null);
			pButtons.setPreferredSize(new Dimension(width - MARGIN, HBUTTONS));
		}
		return pButtons;
	}
	
	/**
	 * Obtiene el bot�n inferior
	 * @return JButton
	 */
	public JButton getBClose(){
		if(bClose == null){
			bClose = new JButton();
			bClose.setText(PluginServices.getText(this, "Cerrar"));
			bClose.setPreferredSize(new java.awt.Dimension(100,25));
			bClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
			bClose.addActionListener(this);
		}
		return bClose;
	}
	
	/**
	 * @see com.iver.mdiApp.ui.MDIManager.View#getViewInfo()
	 */
	public WindowInfo getWindowInfo() {
		WindowInfo m_viewinfo=new WindowInfo(WindowInfo.MODALDIALOG | WindowInfo.RESIZABLE);
    	m_viewinfo.setTitle(PluginServices.getText(this, "histograma"));
    	m_viewinfo.setWidth(width);
    	m_viewinfo.setHeight(height);
		return m_viewinfo;
	}

	/**
	 * Asigna el tama�o del componente
	 * @param w Nuevo ancho del componente padre
	 * @param h Nuevo alto del componente padre
	 */
	/*public void setComponentSize(int w, int h){
		getPButtons().setPreferredSize(new Dimension(w - MARGIN, HBUTTONS));
	}*/
	
	public void actionPerformed(ActionEvent e) {
		
		//Bot�n de cerrar
		if(e.getSource() == bClose){
			try{
				PluginServices.getMDIManager().closeWindow(HistogramDialog.this);
			}catch(ArrayIndexOutOfBoundsException ex){
				//Si la ventana no se puede eliminar no hacemos nada
			}
		}
//		--------------------------------------		
		//Bot�n Crear tabla.
		JButton table = histPanel.getBCreateTable();
		if(e.getSource() == table){

	        try {
//	        	-------Mostrar un fileChooser------------------
	        	String fName;
	        	JFileChooser chooser = new JFileChooser();
	    		chooser.setDialogTitle(PluginServices.getText(this, "guardar_tabla"));
	    		
	    		int returnVal = chooser.showOpenDialog(this);
	    		if(returnVal == JFileChooser.APPROVE_OPTION){
	    		 	fName = chooser.getSelectedFile().toString();
	    			if(!fName.endsWith(".dbf"))
	    				fName += ".dbf";
	        	     
	    			//-------------Crear el dbf----------------------
	    		
		    		DbaseFileWriterNIO dbfWrite = null;
			        DbaseFileHeaderNIO myHeader;
			        Value[] record;
		        	
		        	int histogram[][]=histPanel.getLastHistogram();
		        	int numBands = histogram.length;
		        	int numRecors = histogram[0].length;
		        	
		        	File file = new File(fName);
		        	
		        	String names[] = new String[numBands+1];
		        	int types[] = new int [numBands+1];
		        	int lengths[] = new int [numBands+1];
		        	
		        	names[0]="Value";
	        		types[0]=4;
	        		lengths[0]=15;
		        	for (int band = 0; band < numBands; band++){
		        		names[band+1]="Band"+band;
		        		types[band+1]=4;
		        		lengths[band+1]=15;
		        	}
		        	
		            myHeader = DbaseFileHeaderNIO.createDbaseHeader(names,types,lengths);
	
		            myHeader.setNumRecords(numRecors);
		            dbfWrite = new DbaseFileWriterNIO(myHeader,
		                    (FileChannel) getWriteChannel(file.getPath()));
		            record = new Value[numBands+1];
	
		            for (int j = 0; j < numRecors; j++) {
		            	record[0] = ValueFactory.createValue(j);
		                for (int r = 0; r < numBands; r++) {
		                    record[r+1] = ValueFactory.createValue(histogram[r][j]);
		                }
	
		                dbfWrite.write(record);
		            }
	
		            dbfWrite.close();
		            
		            //------------A�adir el dbf al proyecto--------------
					ProjectExtension ext = (ProjectExtension) PluginServices.getExtension(ProjectExtension.class);
					String name = file.getName();
					LayerFactory.getDataSourceFactory()
							.addFileDataSource("gdbms dbf driver",
									name, fName);
					DataSource dataSource;
						dataSource = LayerFactory.getDataSourceFactory()
													.createRandomDataSource(name, DataSourceFactory.AUTOMATIC_OPENING);
					
					SelectableDataSource sds = new SelectableDataSource(dataSource);
					EditableAdapter auxea=new EditableAdapter();
					auxea.setOriginalDataSource(sds);
					ProjectTable projectTable = ProjectFactory.createTable(name, auxea);
					//ext.getProject().addTable(projectTable);
					ext.getProject().addDocument(projectTable);
					
					Table t = new Table();
					t.setModel(projectTable);
					//projectTable.setAndamiWindow(t);
					PluginServices.getMDIManager().addWindow(t);
	    		}
			} catch (DriverLoadException e1) {
				JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),this.getName() + " " +
						PluginServices.getText(this,"table_not_create"));
				NotificationManager.addError(e1);
			} catch (NoSuchTableException e1) {
				JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),this.getName() + " " +
						PluginServices.getText(this,"table_not_create"));
				NotificationManager.addError(e1);
			} catch (DriverException e1) {
				JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),this.getName() + " " +
						PluginServices.getText(this,"table_not_create"));
				NotificationManager.addError(e1);
			}catch (IOException ex) {
				JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),this.getName() + " " +
						PluginServices.getText(this,"table_not_create"));
				NotificationManager.addError("Error al crear un DbaseFileHeaderNIO", ex);
	        }
		}
		
		//--------------------------------------
	}
	/**
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	private WritableByteChannel getWriteChannel(String path)throws IOException {
		WritableByteChannel channel;
		
		File f = new File(path);
		
		if (!f.exists()) {
			System.out.println("Creando fichero " + f.getAbsolutePath());
			
			if (!f.createNewFile()) {
				throw new IOException("Cannot create file " + f);
			}
		}
		
		RandomAccessFile raf = new RandomAccessFile(f, "rw");
		channel = raf.getChannel();
		
		return channel;
	}
	 
	/**
	 * Asigna el objeto histograma para la gesti�n de procesos de histograma
	 * @param histogramObj
	 */
	public void setHistogramObj(Histogram histogramObj) {
		histPanel.setHistogram(histogramObj);
	}

	/**
	 * Obtiene el panel con el histograma gr�fico y los controles
	 * @return HistogramPanel
	 */
	public HistogramPanel getHistogramPanel() {
		return histPanel;
	}
	
	/**
	 * Asigna el n�mero de bandas al combobox
	 * @param bands N�mero de bandas de la imagen
	 */
	public void setBands(int bands){
		histPanel.setBands(bands);
	}

	/**
	 * Resetea el control de bandas del panel con los valores RGB para 
	 * cuando se est� haciendo el histograma de la visualizaci�n en
	 * vez del histograma con los datos
	 *
	 */
	public void setRGBInBandList(){
		histPanel.setRGBInBandList();
	}
	
	/**
	 *Asigna el combo "Origen" el valor de solo vista para el calculo del histograma. Esa opci�n es 
	 *utilizada para extensiones que necesitan histograma pero no pueden acceder a la fuente de datos.
	 */
	public void setOnlyViewValue(){
		histPanel.setOnlyViewValue();
	}

	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentResized(ComponentEvent e) {
		getPButtons().setPreferredSize(new Dimension(getWidth() + 8 - MARGIN, HBUTTONS));
		histPanel.setComponentSize(getWidth() + 8 - MARGIN, getHeight() - 42);
		PluginServices.getMDIManager().refresh(this);
	}

	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}
}  

