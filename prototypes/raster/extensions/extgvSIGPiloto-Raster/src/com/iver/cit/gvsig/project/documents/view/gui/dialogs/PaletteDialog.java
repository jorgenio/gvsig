/*
 * Created on 19-jul-2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 */
package com.iver.cit.gvsig.project.documents.view.gui.dialogs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.cresques.filter.RasterFilter;
import org.cresques.filter.RasterFilterStack;
import org.cresques.filter.RasterFilterStackManager;
import org.cresques.filter.bands.PaletteFilter;
import org.cresques.filter.bands.PaletteStackManager;
import org.cresques.io.datastruct.Palette;
import org.cresques.ui.raster.PalettePanel;
import org.gvsig.gui.beans.table.exceptions.NotInitializeException;
import org.gvsig.gui.beans.table.models.TableButtonModel;

import com.iver.andami.PluginServices;
import com.iver.andami.messages.NotificationManager;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.RasterOperations;

/**
 * @author Nacho Brodin <brodin_ign@gva.es>
 */
public class PaletteDialog extends JPanel implements IWindow, ActionListener, ComponentListener{
	private final int					MARGIN = 8;
	
	private int 						width = 0;
	private int 						height = 0;
	private PalettePanel 				palPanel = null;
	private RasterFilterStackManager	stackManager = null;
	private RasterOperations			fLayer = null;
	private ArrayList					filters = null;
	
	/**
	 * Constructor
	 * @param width Ancho
	 * @param height Alto
	 */
	public PaletteDialog(RasterOperations layer, RasterFilterStackManager stackManager, int width, int height){
		this.width = width;
		this.height = height;
		this.stackManager = stackManager;
		this.fLayer = layer;
		
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		this.setPreferredSize(new Dimension(width, height));
		
		this.add(getPPalette(), gridBagConstraints);
		this.addComponentListener(this);
		
		getPPalette().getCbInterpolar().addActionListener(this);
		getPPalette().getBAccept().addActionListener(this);
		getPPalette().getBApply().addActionListener(this);
		getPPalette().getBCancel().addActionListener(this);
		getPPalette().getBSave().addActionListener(this);
		getPPalette().getBSaveAs().addActionListener(this);
		
		//Salvamos el estado del filtro de paleta, si hay alguno en la pila
		RasterFilterStack filterStack = stackManager.getFilterStack();
		PaletteFilter pf = (PaletteFilter)filterStack.getByType(PaletteStackManager.palette);
		if(pf != null){
			filters = new ArrayList();
			PaletteStackManager paletteStackManager =  (PaletteStackManager)stackManager.getManagerByClass(PaletteStackManager.class);
			if(paletteStackManager != null)
				filters = paletteStackManager.getStringsFromStack(filters, pf);
		}
	}
	
	/**
	 * Obtiene el panel con el histograma
	 * @return HistogramPanel
	 */
	public PalettePanel getPPalette(){
		if(palPanel == null){
			palPanel = new PalettePanel(width - MARGIN, 
										height - MARGIN,
										System.getProperty("user.home") + File.separator +
											PluginServices.getArguments()[0] + File.separator + "palettes.xml");
			palPanel.setComponentSize(width - MARGIN, height - MARGIN);
		}
		return palPanel;
	}
		
	/**
	 * @see com.iver.mdiApp.ui.MDIManager.View#getViewInfo()
	 */
	public WindowInfo getWindowInfo() {
		WindowInfo m_viewinfo=new WindowInfo(WindowInfo.MODELESSDIALOG /*| ViewInfo.RESIZABLE*/);
    	m_viewinfo.setTitle(PluginServices.getText(this, "paleta"));
    	m_viewinfo.setWidth(width);
    	m_viewinfo.setHeight(height + 10);
    	Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    	m_viewinfo.setX( ((((int)d.getWidth()) >> 1) - (width >> 1)) );
    	m_viewinfo.setY( ((((int)d.getHeight()) >> 1) - (int)(height / 1.5)) );
		return m_viewinfo;
	}

	
	/**
	 * Valida que el campo de valores RGB de la tabla tenga 3 valores enteros separados
	 * por comas y que el rango de valores sea numerico
	 * @return
	 */
	private boolean validateFields(){
		try{
			TableButtonModel model = (TableButtonModel)palPanel.getPTable().getModel();
			for(int iRow = 0; iRow < palPanel.getPTable().getRowCount(); iRow ++){
				for(int iCol = 1; iCol < 4; iCol ++){
					String value = (String)model.getValueAt(iRow, iCol);
					switch(iCol){
						case 2:	try{
									String[] rgb = value.split(",");
									Integer.valueOf(rgb[0]);
									Integer.valueOf(rgb[1]);
									Integer.valueOf(rgb[2]);
								}catch(NumberFormatException exc){
									return false;
								}catch(ArrayIndexOutOfBoundsException exc){
									return false;
								}
								break;
						case 3:	try{
									Double.valueOf(value);
								}catch(NumberFormatException exc){
									return false;
								}
								break;
					}
				}
				String value = (String)model.getValueAt(iRow, 4);
				try{
					Integer.valueOf(value);
				}catch(NumberFormatException exc){
					return false;
				}
			}
			return true;
		}catch(NotInitializeException exc){
			return false;
		}
	}
	
	/**
	 * Obtiene el objeto paleta desde el filtro de paleta cargado en la pila
	 * @return Objeto palette
	 */
	public Palette getPalette(){
		return ((Palette)getPaletteFilter().getParam("palette"));
	}
	
	/**
	 * Obtiene el filtro de paleta cargado en la pila
	 * @return Filtro de paleta
	 */
	public PaletteFilter getPaletteFilter(){
		RasterFilterStack filterStack = stackManager.getFilterStack();
		return (PaletteFilter)filterStack.getByType(PaletteStackManager.palette);
	}
	
	/**
	 * Al pulsar Aceptar o Aplicar se a�ade un filtro de paleta en la pila en caso de que
	 * no exista ninguno. Si hay alguno entonces se modifica el que hay.
	 * 
	 * Al pulsar Cancelar o Aceptar la ventana se cierra.
	 */
	public void actionPerformed(ActionEvent e) {		
				
		//Bot�n de Aceptar o Aplicar
		if(	e.getSource() == palPanel.getBAccept() || 
			e.getSource() == palPanel.getBApply() || 
			e.getSource() == palPanel.getBSave() || 
			e.getSource() == palPanel.getBSaveAs()){
			
			if(palPanel.isActivePanel()){
				if(!validateFields())
					return;
							
				palPanel.orderRows();
				
				if(getPaletteFilter() != null){
					if(e.getSource() == palPanel.getBAccept() || e.getSource() == palPanel.getBApply())
						getPaletteFilter().setExec(true);
					
					//Modificar paleta de la pila
					try {
						if(palPanel.getTablePalette() != null){
							Palette pal = getPalette();
							pal.createPaletteFromTable(	palPanel.getTablePalette(), 
														fLayer.getDataType());
							pal.addName(palPanel.getCbList().getSelectedItem().toString());
							getPaletteFilter().addParam("interpolate",new Boolean(palPanel.getCbInterpolar().isSelected()));
						}
						else
							stackManager.getFilterStack().removeFilter(PaletteStackManager.palette);
					} catch (NotInitializeException e1) {
						JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),this.getName() + " " +
								PluginServices.getText(this,"palette_not_create"));
						NotificationManager.addError("No se ha podido crear la paleta desde la tabla", e1);
					}
				}else{
					//A�adir un filtro de paleta
					Palette palette = new Palette(palPanel.getCbList().getSelectedItem().toString());
					
					try {
						if(palPanel.getTablePalette() != null){
							palette.createPaletteFromTable(palPanel.getTablePalette(), fLayer.getDataType());
							PaletteStackManager psm = (PaletteStackManager)stackManager.getManagerByClass(PaletteStackManager.class);
			                stackManager.getFilterStack().removeFilter(PaletteStackManager.palette);
			                psm.addPaletteFilter(palette,palPanel.getCbInterpolar().isSelected());
			                
			                if(e.getSource() == palPanel.getBSave() || e.getSource() == palPanel.getBSaveAs())
								getPaletteFilter().setExec(false);
			                else
			                	getPaletteFilter().setExec(true);
						
						}
					} catch (NotInitializeException e1) {
						JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),this.getName() + " " +
								PluginServices.getText(this,"palette_not_create"));
						NotificationManager.addError("No se ha podido crear la paleta desde la tabla", e1);
					}
				}
			}else { //Si no est� activo el panel
				//Decimos al filtro que no se ejecute
				RasterFilter rf  = stackManager.getFilter("palette");
				if(rf != null)
					rf.setExec(false);
			}
			
		}
		
		//Bot�n de Aceptar o Cerrar
		if(e.getSource() == palPanel.getBAccept() || e.getSource() == palPanel.getBCancel()){
			//Solo si ha cancelado restauramos el valor inicial del filtro de paleta en la pila
			if(e.getSource() == palPanel.getBCancel()){
				stackManager.getFilterStack().removeFilter(PaletteStackManager.palette);
				if(filters != null){
					PaletteStackManager paletteStackManager =  (PaletteStackManager)stackManager.getManagerByClass(PaletteStackManager.class);
					if(paletteStackManager != null)
						paletteStackManager.createStackFromStrings(filters, (String)filters.get(0), null, 0);
				}
			}
			try{
				PluginServices.getMDIManager().closeWindow(PaletteDialog.this);
			}catch(ArrayIndexOutOfBoundsException ex){
				//Si la ventana no se puede eliminar no hacemos nada
			}
		}
		
		//Check de Interpolaci�n
		if(e.getSource() == palPanel.getCbInterpolar()){
				if(getPaletteFilter() != null)
					getPaletteFilter().addParam("interpolate", new Boolean(palPanel.getCbInterpolar().isSelected()));
			return;
		}
		
		if(fLayer instanceof FLayer)
			((FLayer)fLayer).getMapContext().invalidate();
	}

	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentResized(ComponentEvent e) {
		/*this.width = this.getWidth() + 10;
		this.height = this.getHeight() - 4;
		getPPalette().setComponentSize(width - MARGIN, height - MARGIN);*/
	}

	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

}  


