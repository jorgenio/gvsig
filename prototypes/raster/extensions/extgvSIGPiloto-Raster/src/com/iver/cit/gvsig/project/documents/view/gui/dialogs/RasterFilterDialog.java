/*
 * Created on 25-jul-2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 */
package com.iver.cit.gvsig.project.documents.view.gui.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.ListModel;

import org.cresques.filter.RasterFilter;
import org.cresques.filter.RasterFilterStack;
import org.cresques.filter.RasterFilterStackManager;
import org.cresques.filter.RasterFilter.Kernel;
import org.cresques.filter.convolution.ConvolutionStackManager;
import org.cresques.filter.correction.MedianStackManager;
import org.cresques.filter.segmentation.FirstDerivativeFilter;
import org.cresques.filter.segmentation.FirstDerivativeStackManager;
import org.cresques.ui.filter.FirstDerivativePanel;
import org.cresques.ui.filter.KernelFiltersPanel;
import org.gvsig.gui.beans.treelist.TreeListContainer;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.gui.panels.RasterFilterPanel;

/**
 * Dialogo para los filtros de raster.
 * 
 * @author Nacho Brodin <brodin_ign@gva.es>
 */
public class RasterFilterDialog extends JPanel implements IWindow, ActionListener, ComponentListener{
	private final int					MARGIN = 4;
	
	private int 						width = 0;
	private int 						height = 0;
	private RasterFilterPanel 			filterPanel = null;
	private JPanel						pButtons = null;
	private JButton						bClose = null;
	private RasterFilterStackManager	stackManager = null;
	private FLayer	 				fLayer = null;
	
	/**
	 * Constructor 
	 * @param width	Ancho del panel
	 * @param height Alto del panel
	 * @param stackManager Gestor de la pila de filtros
	 */
	public RasterFilterDialog(FLayer layer, int width, int height, RasterFilterStackManager stackManager){
		this.width = width;
		this.height = height;
		this.fLayer = layer;
		this.stackManager = stackManager;
		
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		this.setLayout(new GridBagLayout());
		this.add(getPFilter(), gridBagConstraints);
		this.addComponentListener(this);
	}
		
	/**
	 * Obtiene el panel de filtros
	 * @return HistogramPanel
	 */
	public RasterFilterPanel getPFilter(){
		if(filterPanel == null){
			filterPanel = new RasterFilterPanel(width, height);
			filterPanel.getBAccept().addActionListener(this);
			filterPanel.getBApply().addActionListener(this);
			filterPanel.getBClose().addActionListener(this);
		}
		return filterPanel;
	}
	
	/**
	 * @see com.iver.mdiApp.ui.MDIManager.View#getViewInfo()
	 */
	public WindowInfo getWindowInfo() {
		WindowInfo m_viewinfo=new WindowInfo(WindowInfo.MODALDIALOG /*| WindowInfo.RESIZABLE*/);
    	m_viewinfo.setTitle(PluginServices.getText(this, "Filtros"));
    	m_viewinfo.setWidth(width + MARGIN);
    	m_viewinfo.setHeight(height + MARGIN);
		return m_viewinfo;
	}

	/**
	 * Acciones realizadas al pulsar los botones de Aceptar, Aplicar o Cancelar del dialogo general
	 * de filtros.
	 */
	public void actionPerformed(ActionEvent e) {

		//Al pulsar Aceptar o aplicar se a�aden los filtros
		if(e.getSource() == getPFilter().getBApply() || e.getSource() == getPFilter().getBAccept()){
			RasterFilterStack stack = stackManager.getFilterStack();

			// Quitamos lo filtros de la pila que se han eliminado de la lista
			removeFiltersFromStack(stack);
			
			ListModel model = getSelector().getList().getModel();
			for(int iListFilter = 0; iListFilter < model.getSize(); iListFilter ++){
				String filterName = getSelector().getMap().get(model.getElementAt(iListFilter).toString()).toString();
				addFilterByName(filterName);
			}
			
			fLayer.getMapContext().invalidate();
		}
		
		//Al pulsar Cerrar o Aceptar la ventana se cierra
		if(e.getSource() == getPFilter().getBClose() || e.getSource() == getPFilter().getBAccept()){
			try{
				PluginServices.getMDIManager().closeWindow(RasterFilterDialog.this);
			}catch(ArrayIndexOutOfBoundsException ex){
				//Si la ventana no se puede eliminar no hacemos nada
			}
		}
				
	}

	/**
	 * A�ade un filtro desde el dialogo en la pila de filtros a partir de los par�metros
	 * del panel.
	 * @param filterName Nombre del filtro.
	 */
	private void addFilterByName(String filterName){
		if(filterName.equals("sobel")||filterName.equals("roberts")||filterName.equals("prewitt")||filterName.equals("frei_chen")){
			//stackManager.removeFilter(FirstDerivativeStackManager.firstDerivative);
			FirstDerivativePanel panel =  (FirstDerivativePanel)RasterFilterPanel.getPanel(PluginServices.getText(this,filterName));
			int umbral = 0;
			try{
				if (panel.getCbUmbralizar().isSelected())
					umbral = Integer.valueOf(panel.getTUmbral().getText()).intValue();
			}catch(NumberFormatException ex){
				//No se ha podido leer el umbral y no hacemos nada. Tendr� el valor por defecto
			}
			boolean compare = panel.getCbUmbralizar().isSelected();
			FirstDerivativeStackManager manager = (FirstDerivativeStackManager)stackManager.getManagerByClass(FirstDerivativeStackManager.class);
			
			int operator = -1;
			if (filterName.equals("sobel")) operator = FirstDerivativeFilter.TYPE_SOBEL;
			else if (filterName.equals("roberts"))operator = FirstDerivativeFilter.TYPE_ROBERTS;
			else if (filterName.equals("prewitt"))operator = FirstDerivativeFilter.TYPE_PREWITT;
			else if (filterName.equals("frei_chen"))operator = FirstDerivativeFilter.TYPE_FREICHEN;
			
			manager.addFirstDerivativeFilter(umbral, operator ,compare,filterName);
			
		}else if(filterName.equals("laplace")){
			KernelFiltersPanel panel =  (KernelFiltersPanel)RasterFilterPanel.getPanel(PluginServices.getText(this,filterName));
			ConvolutionStackManager manager = (ConvolutionStackManager)stackManager.getManagerByClass(ConvolutionStackManager.class);
			double k[][] = null;
			double k3[][]= {{-1,-1,-1},{-1,8,-1},{-1,-1,-1}};
			double k5[][]= {{0,0,-1,0,0},{0,-1,-2,-1,0},{-1,-2,16,-2,-1},{0,-1,-2,-1,0},{0,0,-1,0,0}};
			switch(Integer.valueOf(panel.getCbKernel().getSelectedItem().toString()).intValue()){
				case 3:
					k = k3;
					break;
				case 5:
					k = k5;
					break;
			}
				
			manager.addConvolutionFilter(new Kernel(k),0,filterName);
			
		}else if(filterName.equals("gauss")){
			KernelFiltersPanel panel =  (KernelFiltersPanel)RasterFilterPanel.getPanel(PluginServices.getText(this,filterName));
			ConvolutionStackManager manager = (ConvolutionStackManager)stackManager.getManagerByClass(ConvolutionStackManager.class);
			double k[][] = null;
			double k3[][]= {{1,4,1},{4,12,4},{1,4,1}};
			double k5[][]= {{1,2,3,2,1},{2,7,11,7,2},{3,11,17,11,3},{2,7,11,7,2},{1,2,3,2,1}};
			switch(Integer.valueOf(panel.getCbKernel().getSelectedItem().toString()).intValue()){
				case 3:
					k = k3;
					break;
				case 5:
					k = k5;
					break;
			}
				
			manager.addConvolutionFilter(new Kernel(k),0,filterName);
			
		}else if(filterName.equals("media")){
			KernelFiltersPanel panel =  (KernelFiltersPanel)RasterFilterPanel.getPanel(PluginServices.getText(this,filterName));
			int ladoVentana = Integer.valueOf(panel.getCbKernel().getSelectedItem().toString()).intValue();
			ConvolutionStackManager manager = (ConvolutionStackManager)stackManager.getManagerByClass(ConvolutionStackManager.class);
			
			double k[][]= new double[ladoVentana][ladoVentana];
			for (int i=0; i<ladoVentana; i++)
				for (int j=0; j<ladoVentana; j++)
					k[i][j]=1;
			manager.addConvolutionFilter(new Kernel(k),0,filterName);
			
		}else if(filterName.equals("mediana")){
			KernelFiltersPanel panel =  (KernelFiltersPanel)RasterFilterPanel.getPanel(PluginServices.getText(this,filterName));
			MedianStackManager manager = (MedianStackManager)stackManager.getManagerByClass(MedianStackManager.class);
			manager.addMedianFilter(Integer.valueOf(panel.getCbKernel().getSelectedItem().toString()).intValue());
			
		}else if(filterName.equals("paso_bajo")){
			ConvolutionStackManager manager = (ConvolutionStackManager)stackManager.getManagerByClass(ConvolutionStackManager.class);
			double k[][]= {{0,1,0},{1,6,1},{0,1,0}};
			manager.addConvolutionFilter(new Kernel(k),0,filterName);
		}
		
	}
	
	/**
	 *Quita los filtros de la pila que no est�n en la lista. Esto solo tiene efecto para aquellos filtros con nombre
	 *ya que son los filtros de usuario. Los filtros que no tienen nombre (filterName) no se visualizan en la lista por
	 *lo que no son eliminados de esta.
	 *@param stack Pila de filtros.
	 */
	private void removeFiltersFromStack(RasterFilterStack stack){
		//Recorremos la pila de filtros y eliminamos los que est�n "registrados" en el interfaz de filtros.
		for(int iFilter = 0; iFilter < stack.lenght(); iFilter ++){
			RasterFilter rf = stack.get(iFilter);
			if(!rf.getFilterName().equals("") && getSelector().getMap().containsValue(rf.getFilterName())){
					stack.removeFilter(rf);
					iFilter--;
			}
		}	
	}
	
	/**
	 * Obtiene el panel con el selector de filtros
	 * @return TreeListContainer
	 */
	public TreeListContainer getSelector(){
		return filterPanel.getPSelector();
	}
	
	//-------------------------------------------
	//M�TODOS DEL LISTENER
	
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void componentResized(ComponentEvent e) {
		this.width = this.getWidth();
		this.height = this.getHeight();
		getPFilter().setSizeComponent(width - MARGIN, height - MARGIN);
	}

	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
	}
	
	//-------------------------------------------
	//M�TODOS DEL COMPONENTE
	
	/**
	 * Asigna el panel visible en el dialogo
	 * @param pos Posici�n del panel
	 */
	public void setVisiblePanel(int pos){
		getPFilter().setVisiblePanel(pos);
	}
	
	/**
	 * Obtiene el panel visible
	 * @return panel visible
	 */
	public JPanel getVisiblePanel(){
		return getPFilter().getVisiblePanel();
	}
	
	/**
	 * Obtiene el panel de la posici�n solicitada
	 * @param pos Posici�n del panel solicitado
	 * @return panel solicitado
	 */
	public JPanel getPanel(int pos){
		return RasterFilterPanel.getPanel(pos);
	}
	
	
}  

