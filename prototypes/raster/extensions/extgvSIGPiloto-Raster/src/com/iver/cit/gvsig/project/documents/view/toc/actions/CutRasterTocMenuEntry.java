/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2006 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 */
package com.iver.cit.gvsig.project.documents.view.toc.actions;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JMenuItem;

import org.cresques.io.GeoRasterFile;
import org.gvsig.gui.beans.table.exceptions.NotInitializeException;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrRaster;
import com.iver.cit.gvsig.fmap.tools.Behavior.Behavior;
import com.iver.cit.gvsig.fmap.tools.Behavior.MouseMovementBehavior;
import com.iver.cit.gvsig.fmap.tools.Behavior.RectangleBehavior;
import com.iver.cit.gvsig.project.documents.view.gui.View;
import com.iver.cit.gvsig.project.documents.view.gui.dialogs.CutRasterDialog;
import com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction;
import com.iver.cit.gvsig.project.documents.view.toc.ITocItem;
import com.iver.cit.gvsig.project.documents.view.toc.gui.FPopupMenu;
import com.iver.cit.gvsig.project.documents.view.toolListeners.CutRasterListener;
import com.iver.cit.gvsig.project.documents.view.toolListeners.StatusBarListener;

/**
 * Entrada en del men� contextual del TOC correspondiente al cuadro
 * de de recorte de raster, cambio de resoluci�n y extracci�n de bandas.
 * 
 * @author Diego Guerrero (diego.guerrero@uclm.es)
 *
 */
public class CutRasterTocMenuEntry extends AbstractTocContextMenuAction {
	private JMenuItem 						recorte;
	private FLayer							lyr = null;
	private CutRasterDialog					cutRasterDialog = null;
	private int 							width = 438;
	private int 							height = 617;

	public String getGroup() {
		return "RasterTools"; 
	}

	public int getGroupOrder() {
		return 80;
	}

	public int getOrder() {
		return 3;
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return selectedItems.length == 1;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			lyr = getNodeLayer(item);
            if ((lyr instanceof FLyrRaster)) {
            	return true;
            }
		}
		return false;
	}
		
	/**
	 * Gestiona la apertura del dialogo de recorte de raster  
	 * cambio de resoluci�n y extracci�n de bandas cuando se pulsa
	 * la opci�n asignando a este las propiedades iniciales. 
	 */
	public void execute(ITocItem item, FLayer[] selectedItems) {
       	lyr = getNodeLayer(item);
                   
       	if(lyr instanceof FLyrRaster){
       		
       		View theView = (View) PluginServices.getMDIManager().getActiveWindow();
       		String viewName = PluginServices.getMDIManager().getWindowInfo(theView).getTitle();
        	MapControl m_MapControl = theView.getMapControl();
        	
        	cutRasterDialog = new CutRasterDialog((FLyrRaster)lyr, width, height, m_MapControl, viewName);			
       		PluginServices.getMDIManager().addWindow(cutRasterDialog);
        	
        	//Listener de eventos de movimiento que pone las coordenadas del rat�n en la barra de estado
	        StatusBarListener sbl = new StatusBarListener(m_MapControl);
	        
	        //Cortar Raster
	        CutRasterListener crl = new CutRasterListener(m_MapControl,cutRasterDialog);
	        m_MapControl.addMapTool("cutRaster", new Behavior[]{
	        				new RectangleBehavior(crl), new MouseMovementBehavior(sbl)});
	        
	        //m_MapControl.setTool("cutRaster");
	        
	        GeoRasterFile[] files = ((FLyrRaster)lyr).getSource().getFiles();
	        
	        for (int i=0;i<files.length;i++){
	        	String fileName = files[i].getName();
	        	for(int j=0;j<files[i].getBandCount();j++){
	        		Object row[] = {new Boolean(true),new String("B"+(j+1)+" - "+fileName.substring(fileName.lastIndexOf(File.separator)+1,fileName.length()))};
	        		try {
						cutRasterDialog.getPCut().getTSelection().addRow(row);
					} catch (NotInitializeException e1) {
						e1.printStackTrace();
					}
	        	}
	        }
        }
	}

	public String getText() {
		return PluginServices.getText(this, "recorte");
	}
}
