/*
 * Created on 19-jul-2006
 * 
 * gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 */
package com.iver.cit.gvsig.project.documents.view.toc.actions;

import java.awt.event.ActionEvent;

import javax.swing.JMenuItem;

import org.cresques.filter.RasterFilterStackManager;
import org.cresques.io.data.RasterBuf;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrRaster;
import com.iver.cit.gvsig.fmap.layers.FLyrVect;
import com.iver.cit.gvsig.fmap.layers.layerOperations.ClassifiableVectorial;
import com.iver.cit.gvsig.project.documents.view.gui.dialogs.HistogramDialog;
import com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction;
import com.iver.cit.gvsig.project.documents.view.toc.ITocItem;
import com.iver.cit.gvsig.project.documents.view.toc.gui.FPopupMenu;


/**
 * Entrada en del men� contextual del TOC correspondiente al cuadro
 * del histograma raster
 * @author Nacho Brodin (brodin_ign@gva.es)
 *
 */
public class HistogramTocMenuEntry extends AbstractTocContextMenuAction {
	private JMenuItem 						properties;
	private FLayer							lyr = null;
	private HistogramDialog					histogramDialog = null;
	private RasterFilterStackManager		stackManager = null;
	private int 							width = 600;//650
	private int 							height = 430;//500

	public String getGroup() {
		return "RasterTools"; 
	}

	public int getGroupOrder() {
		return 80;
	}

	public int getOrder() {
		return 0;
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return selectedItems.length == 1;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			lyr = getNodeLayer(item);
            if ((lyr instanceof FLyrRaster)) {
            	return true;
            }
		}
		return false;
	}
		
	/**
	 * Gestiona la apertura del dialogo de histogramas de raster cuando se pulsa
	 * la opci�n asignando a este las propiedades iniciales. 
	 */
	public void execute(ITocItem item, FLayer[] selectedItems) {
		stackManager = new RasterFilterStackManager(((FLyrRaster)lyr).getSource().getFilterStack());
		lyr = getNodeLayer(item);
        
       	if(lyr instanceof FLyrRaster){
       		stackManager = new RasterFilterStackManager(((FLyrRaster)lyr).getSource().getFilterStack());

    		histogramDialog = new HistogramDialog(width, height);
   
    		if( ((FLyrRaster)lyr).getGrid().getDataType() == RasterBuf.TYPE_DOUBLE ||
    			((FLyrRaster)lyr).getGrid().getDataType() == RasterBuf.TYPE_FLOAT)
    			histogramDialog.setOnlyViewValue();
    		
    		histogramDialog.setHistogramObj(((FLyrRaster)lyr).getGrid().getHistogram());
    		
    		//Par�metros de inicializaci�n del histograma
    		histogramDialog.getHistogramPanel().setHistogramDataSource(0); //Fuente de datos; la vista
    		histogramDialog.getHistogramPanel().setType(0); //Tipo de histograma; no acumulado
    		histogramDialog.setRGBInBandList(); //Asignaci�n R,G,B en el combo
    		histogramDialog.getHistogramPanel().showHistogram(); //Dibujamos histograma
       		       					
       		PluginServices.getMDIManager().addWindow(histogramDialog);
        }
	}

	public String getText() {
		return PluginServices.getText(this, "histograma");
	}

}
