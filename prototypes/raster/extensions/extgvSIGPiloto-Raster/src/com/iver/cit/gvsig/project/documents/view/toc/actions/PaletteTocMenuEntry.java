/*
 * Created on 19-jul-2006
 * 
 * gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 */
package com.iver.cit.gvsig.project.documents.view.toc.actions;

import javax.swing.JMenuItem;

import org.cresques.filter.RasterFilterStack;
import org.cresques.filter.RasterFilterStackManager;
import org.cresques.filter.bands.PaletteFilter;
import org.cresques.filter.bands.PaletteStackManager;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.layers.FLayer;
import com.iver.cit.gvsig.fmap.layers.FLyrRaster;
import com.iver.cit.gvsig.project.documents.view.gui.dialogs.PaletteDialog;
import com.iver.cit.gvsig.project.documents.view.toc.AbstractTocContextMenuAction;
import com.iver.cit.gvsig.project.documents.view.toc.ITocItem;


/**
 * Entrada en del men� contextual del TOC correspondiente al cuadro
 * del paletas raster
 * @author Nacho Brodin (brodin_ign@gva.es)
 *
 */
public class PaletteTocMenuEntry extends AbstractTocContextMenuAction {
	private JMenuItem 						paleta;
	private FLayer							lyr = null;
	private PaletteDialog					paletteDialog = null;
	private RasterFilterStackManager		stackManager = null;
	private int 							width = 408;
	private int 							height = 420;

	public String getGroup() {
		return "RasterTools"; 
	}

	public int getGroupOrder() {
		return 80;
	}

	public int getOrder() {
		return 1;
	}

	public boolean isEnabled(ITocItem item, FLayer[] selectedItems) {
		return selectedItems.length == 1;
	}

	public boolean isVisible(ITocItem item, FLayer[] selectedItems) {
		if (isTocItemBranch(item)) {
			lyr = getNodeLayer(item);
            if ((lyr instanceof FLyrRaster)) {
            	if(((FLyrRaster)lyr).getSource().getNumBands() != 1)
            		return false;
            	return true;
            }
		}
		return false;
	}
		
	/**
	 * Gestiona la apertura del dialogo de paletas de raster cuando se pulsa
	 * la opci�n asignando a este las propiedades iniciales. 
	 */
	public void execute(ITocItem item, FLayer[] selectedItems) {
		stackManager = new RasterFilterStackManager(((FLyrRaster)lyr).getSource().getFilterStack());
       	lyr = getNodeLayer(item);
                   
       	if(lyr instanceof FLyrRaster){
       		RasterFilterStack filterStack = ((FLyrRaster)lyr).getSource().getFilterStack();
       		stackManager = new RasterFilterStackManager(filterStack);

    		paletteDialog = new PaletteDialog((FLyrRaster)lyr, stackManager, width, height);
			PaletteFilter pf = (PaletteFilter)filterStack.getByType(PaletteStackManager.palette);
			
			//Si hay en la pila alguna paleta la carga en el dialogo
			if(pf != null && pf.getPalette() != null){
				paletteDialog.getPPalette().loadPanelFromPalette(pf.getPalette());
				if(pf.isExec())
					paletteDialog.getPPalette().getCActive().setSelected(true);
				else
					paletteDialog.getPPalette().getCActive().setSelected(false);
				
				if(pf.isInterpolate())
					paletteDialog.getPPalette().getCbInterpolar().setSelected(true);
				else
					paletteDialog.getPPalette().getCbInterpolar().setSelected(false);
				
				//Asignamos el nombre en el dialogo
				for(int i = 0; i < paletteDialog.getPPalette().getCbList().getItemCount(); i++){
					String name = paletteDialog.getPPalette().getCbList().getItemAt(i).toString();
					if(name.equals(paletteDialog.getPalette().getName())){
						paletteDialog.getPPalette().getCbList().setSelectedIndex(i);
						break;
					}
				}
			}
	
       		PluginServices.getMDIManager().addWindow(paletteDialog);
        }
	}

	public String getText() {
		return PluginServices.getText(this, "paleta");
	}

}
