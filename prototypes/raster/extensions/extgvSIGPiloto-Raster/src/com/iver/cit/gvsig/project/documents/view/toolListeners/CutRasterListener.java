package com.iver.cit.gvsig.project.documents.view.toolListeners;

import java.awt.Component;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.iver.andami.PluginServices;
import com.iver.cit.gvsig.fmap.DriverException;
import com.iver.cit.gvsig.fmap.MapControl;
import com.iver.cit.gvsig.fmap.layers.FLyrRaster;
import com.iver.cit.gvsig.fmap.tools.SaveRasterListenerImpl;
import com.iver.cit.gvsig.fmap.tools.Events.RectangleEvent;
import com.iver.cit.gvsig.project.documents.view.gui.dialogs.CutRasterDialog;


/**
 * Listener del recorte de raster. Al seleccionar un �rea sobre la vista debe cargar
 * el cuadro con los datos de coordenadas pixel, coordenadas reales, ancho y alto
 * del raster resultante, tama�o de celda.
 */
public class CutRasterListener extends SaveRasterListenerImpl {
	private FLyrRaster 			layer = null;
	private CutRasterDialog 	cutRasterDialog;
	private MapControl			mapCtrl = null;
	private String				currentTool = null;
	
	/**
	 * Constructor.
	 * @param mapCtrl
	 * @param cutRasterDialog
	 */
	public CutRasterListener(MapControl mapCtrl, CutRasterDialog cutRasterDialog) {
		super(mapCtrl);
		this.cutRasterDialog = cutRasterDialog;
		this.layer = cutRasterDialog.getFLayer();
		this.mapCtrl = mapCtrl;
		currentTool = mapCtrl.getCurrentTool();
	}

	/**
	 * Asigna la capa raster.
	 * @param flyr Capa raster
	 */
	public void  setRasterLayer(FLyrRaster flyr){
		layer = flyr;
	}
	
	/**
	 * Realiza las acciones de selecci�n del �rea de recorte por medio de un rectangulo
	 * sobre la vista.
	 */
	public void rectangle(RectangleEvent event) {
		super.rectangle(event);
		
		//Obtenemos el extent completo de la capa
		Rectangle2D extent = null;
		try {
			extent = ((FLyrRaster)layer).getFullExtent();	
		} catch (DriverException e1) {
			return;
		}
		
		if(extent == null)
			return;
		
		//Si nos salimos de los margenes de la capa lo ajustamos a esta
		double minx = rect.getMinX(), miny = rect.getMinY(), maxx = rect.getMaxX(), maxy = rect.getMaxY();
		
		//Controlamos las coordenadas del recorte que no se salgan de la imagen.
		//De ser as� mostramos un error
		if(minx > extent.getMaxX() || maxx < extent.getMinX() || miny > extent.getMaxY() || maxy < extent.getMinY()){
			JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),
					PluginServices.getText(this, "coordenadas_erroneas"));
			return;
		}
		
		if(extent.getMinX() > rect.getMinX())
			minx = extent.getMinX();
		if(extent.getMinY() > rect.getMinY())
			miny = extent.getMinY();
		if(extent.getMaxX() < rect.getMaxX())
			maxx = extent.getMaxX();
		if(extent.getMaxY() < rect.getMaxY())
			maxy = extent.getMaxY();
		rect = new Rectangle2D.Double(minx, miny, Math.abs(maxx - minx), Math.abs(maxy - miny));
		
		//Asignamos las coordenadas reales
		cutRasterDialog.getPCut().setCoorRealFromDouble(rect.getMinX(), rect.getMaxY(), rect.getMaxX(), rect.getMinY(), 6);
		try {
			int widthPx = 0;
			int heightPx = 0;
			ArrayList attr = ((FLyrRaster)layer).getSource().getAttributes();
	   		for (int i=0; i<attr.size(); i++) {
				Object[] a = (Object []) attr.get(i);
				if(a[0].toString().equals("Width"))
					widthPx = ((Integer)a[1]).intValue();
				if(a[0].toString().equals("Height"))
					heightPx = ((Integer)a[1]).intValue();
			}
	
	   		Rectangle2D window = new Rectangle2D.Double(rect.getMinX(), rect.getMinY(), rect.getWidth(), rect.getHeight());
	   		Rectangle2D r = org.cresques.util.Utilities.getPxRectFromMapRect(layer.getFullExtent(), widthPx, heightPx, window);
	   		cutRasterDialog.getPCut().setCoorPixelFromDouble(r.getMinX(), r.getMinY(), r.getMaxX(), r.getMaxY(), 3);
	   		cutRasterDialog.getPCut().setWidthText(r.getWidth(), 0);
	   		cutRasterDialog.getPCut().setHeightText(r.getHeight(), 0);
	   		cutRasterDialog.getPCut().setCellSizeText((rect.getWidth() / r.getWidth()), 4);
	   		cutRasterDialog.getPCut().setRelWidthHeight((double)(r.getWidth() / r.getHeight()));
	   		cutRasterDialog.setInitValuesToSize(r.getWidth(), r.getHeight());
	   		if(currentTool != null)
	   			mapCtrl.setTool(currentTool);
		} catch (DriverException e) {
		
		} catch (NumberFormatException e) {
		
		}
	}

}