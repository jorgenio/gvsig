/*
 * Created on 20-dic-2006
 */
/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2006 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 */
package org.gvsig.rasterTools.grass;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.cresques.cts.ICoordTrans;
import org.cresques.cts.IProjection;
import org.cresques.filter.RasterFilterStack;
import org.cresques.geo.ViewPortData;
import org.cresques.io.GdalFile;
import org.cresques.io.GeoRasterFile;
import org.cresques.io.data.Grid;
import org.cresques.io.data.RasterBuf;
import org.cresques.px.Extent;
import org.cresques.px.PxRaster;

import com.iver.cit.gvsig.fmap.ViewPort;
import com.iver.cit.gvsig.fmap.drivers.DriverIOException;
import com.iver.cit.gvsig.fmap.drivers.GeorreferencedRasterDriver;


public class GrassDriver implements GeorreferencedRasterDriver{
	
	/* (non-Javadoc)
	 * @see com.hardcode.driverManager.Driver#getName()
	 */
	public String getName() {
		return "gvSIG Grass Driver";
	}

	
	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#fileAccepted(java.io.File)
	 */
	public boolean fileAccepted(File file) {
		int index = file.getAbsolutePath().lastIndexOf(File.separator);
		String path = file.getAbsolutePath().substring(0, index);
		return path.endsWith("PERMANENT/cellhd");
	}
	
	private File file = null;
	private IProjection proj = null;
	private GeoRasterFile rasterFile = null;
	private PxRaster raster = null;
	private Grid	grid = null;

	int trans = 255;

	static {
		 Class c[] = {GdalFile.class};
	}

	/**
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#open(java.io.File)
	 */
	public void open(File f) throws IOException {
		file = f;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#close()
	 */
	public void close() throws IOException {
		rasterFile.close();
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#initialize()
	 */
	public void initialize() throws IOException {
		if (proj != null){
			String fName = null;
			if(file != null)
				fName = file.getAbsolutePath();
				
			if(fName != null)
				rasterFile = GeoRasterFile.openFile(proj, fName);
			
			if(rasterFile == null)
				throw new IOException("Formato no valido");
			grid = new Grid(rasterFile);
			createPxRaster();
		} else
			throw new IOException("Proyecci�n no asignada");
	}

	/**
	 * @see com.iver.cit.gvsig.fmap.drivers.GeorreferencedRasterDriver#initialize(org.cresques.cts.IProjection)
	 */
	public void initialize(IProjection proj) throws IOException {
		this.proj = proj;
		rasterFile = GeoRasterFile.openFile(proj, file.getAbsolutePath());
		if(rasterFile == null)
			return;
		grid = new Grid(rasterFile);
		createPxRaster();
	}

	private void createPxRaster() {
		raster = new PxRaster(rasterFile, null, rasterFile.getExtent());
		raster.setTransparency(false);
		grid.addRenderizer(raster);
	}

	/**
	 * A�ade un fichero al PxRaste
	 * @param fileName Nombre del fichero a a�adir
	 */
	public void addFile(String fileName){
		if(raster!=null){
			GeoRasterFile grf = raster.addFile(fileName);
			grid.addFile(grf);
		}
	}

	/**
	 * Elimina un fichero al PxRaste
	 * @param fileName Nombre del fichero a a�adir
	 */
	public void delFile(String fileName){
		if(raster!=null){
			GeoRasterFile grf = raster.delFile(fileName);
			grid.removeFile(grf);
		}
	}

	/*
	 * @see com.iver.cit.gvsig.fmap.drivers.GeorreferencedRasterDriver#getFullExtent()
	 */
	public Rectangle2D getFullExtent() {
		return rasterFile.getExtent().toRectangle2D();
	}

	public int getTransparency() {
		return 255-(raster.getAlpha());
	}

	public void setTransparency(int trans) {
		this.trans = trans;
		if (raster != null) {
			raster.setTransparency(trans);
		}
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#draw(java.awt.image.BufferedImage, java.awt.Graphics2D, com.iver.cit.gvsig.fmap.ViewPort)
	 */
	public void draw(BufferedImage image, Graphics2D g, ViewPort vp) throws DriverIOException {
		Extent e = new Extent(vp.getAdjustedExtent());
		Dimension imgSz = vp.getImageSize();
		ViewPortData vp2 = new ViewPortData(vp.getProjection(), e, imgSz );
		vp2.setMat(vp.getAffineTransform());
		raster.draw(g, vp2);
	}

	/*
	 * @see com.iver.cit.gvsig.fmap.drivers.GeorreferencedRasterDriver#getProjection()
	 */
	public IProjection getProjection() {
		return proj;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.GeorreferencedRasterDriver#setProjection(org.cresques.cts.IProjection)
	 */
	public void setProjection(IProjection proj) {
		this.proj = proj;
	}

	/*
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getNumBands()
	 */
	public int getNumBands() {
		return rasterFile.getBandCount();
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getPixel(int, int, byte[])
	 */
	public byte[] getPixel(int x, int y, byte[] dArray) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getPixel(int, int, int[])
	 */
	public int[] getPixel(int x, int y, int[] dArray) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getRasterDataType()
	 */
	public int getRasterDataType() {
		return raster.getDataType();
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getData(int, int, int)
	 */
	public Object getData(int x, int y, int band) {
		return raster.getFiles()[0].getData(x, y, band);
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getDataAsByte(int, int, int)
	 */
	public byte getDataAsByte(int x, int y, int band) {
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getDataAsFloat(int, int, int)
	 */
	public float getDataAsFloat(int x, int y, int band) {
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getDataAsDouble(int, int, int)
	 */
	public double getDataAsDouble(int x, int y, int band) {
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getDataAsInt(int, int, int)
	 */
	public int getDataAsInt(int x, int y, int band) {
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getImage(java.awt.Dimension, java.awt.geom.Rectangle2D, org.cresques.cts.ICoordTrans)
	 */
	public Image getImage(Dimension size, Rectangle2D userSize, ICoordTrans rp) {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#getAttributes()
	 * �AVISO! Variar los tipos de datos devueltos puede hacer que alguna extensi�n
	 * no funcione bien. Se pueden a�adir atributos pero no es recomendable quitar ninguno. 
	 */
	public ArrayList getAttributes() {
		ArrayList attr = new ArrayList();
		String dataType = "Byte";
		if (rasterFile.getDataType() == DataBuffer.TYPE_BYTE) dataType = "Byte";
		else if (rasterFile.getDataType() == DataBuffer.TYPE_SHORT)
			dataType = "Short";
		else if (rasterFile.getDataType() == DataBuffer.TYPE_USHORT)
			dataType = "Unsigned Short";
		else if (rasterFile.getDataType() == DataBuffer.TYPE_INT)
			dataType = "Integer";
		else if (rasterFile.getDataType() == DataBuffer.TYPE_FLOAT)
			dataType = "Float";
		else if (rasterFile.getDataType() == DataBuffer.TYPE_DOUBLE)
			dataType = "Double";
		else
			dataType = "Unknown";
		Object [][] a = {
			{"Filename",rasterFile.getName()},
			{"Filesize",new Long(rasterFile.getFileSize())},
			{"Width",new Integer(rasterFile.getWidth())},
			{"Height", new Integer(rasterFile.getHeight())},
			{"Bands", new Integer(rasterFile.getBandCount())}
//			{"BandDataType", dataType}
		};
		for (int i=0; i<a.length; i++)
			attr.add(a[i]);
		return attr;
	}

	/**
	 * Devuelve el colorBand activo en la banda especificada.
	 * @param flag banda.
	 * @return color de banda activo
	 */
	public int getBand(int flag){
		return raster.getBand(flag);
	}

	/**
	 * Devuelve la posici�n del fichero para la banda especificada.
	 * @param flag banda.
	 * @return posici�n del fichero
	 */
	public int getPosFile(int flag){
		return raster.getPosFile(flag);
	}

	/**
	 * Activa o desactiva la transparencia
	 * @param t	true activa la transparencia y false la desactiva
	 */
	public void setTransparency(boolean t){
		raster.setTransparency(t);
	}

	/**
	 * Asocia un colorBand al rojo, verde o azul.
	 * @param flag cual (o cuales) de las bandas.
	 * @param nBand	que colorBand
	 */
	public void setBand(int flag, int nBand){
		raster.setBand(flag, nBand);
	}

	/**
	 * Obtiene la pila de filtros
	 * @return pila de filtros
	 */
	public RasterFilterStack getFilterStack(){
		return raster.filterStack;
	}

	/**
	 * Asigna la pila de filtros
	 * @return pila de filtros
	 */
	public void setFilterStack(RasterFilterStack stack){
		raster.filterStack = stack;
	}

	/**
	 * Obtiene el valor del pixel del Image en la posici�n x,y
	 * @param x Posici�n x
	 * @param y Posici�n y
	 * @return valor de pixel
	 */
	public int[] getPixel(double wcx, double wcy){
		return raster.getPixel(wcx, wcy);
	}

	/**
	 *
	 */
	public GeoRasterFile [] getFiles(){
		return raster.getFiles();
	}
	
	/**
	 * Obtiene el flag que dice si la imagen est� o no georreferenciada
	 * @return true si est� georreferenciada y false si no lo est�.
	 */
	public boolean isGeoreferenced() {
		return rasterFile.isGeoreferenced();
	}
	
	/**
	 * Asigna una transformaci�n al georrasterfile para la transformar la 
	 * asignaci�n de setView. Del mismo modo asigna los extents a PxRaster para
	 * transformar la visualizaci�n.
	 * @param t Matriz de transformaci�n
	 */
	public void setAffineTransform(AffineTransform t){ 
		for (int i = 0; i < raster.getFiles().length; i++) {
			raster.getFiles()[i].setAffineTransform(t);
			raster.setExtent(raster.getFiles()[i].getExtent());
			raster.setExtentForRequest(raster.getFiles()[i].getExtentForRequest());
		}
	}
	
	public AffineTransform getAffineTransform(){
		return raster.getFiles()[0].getAffineTransform();
	}
	
	/**
	 * Obtiene el grid asociado 
	 * @return grid
	 */
	public Grid getGrid(){
		return grid;
	}
	
	/**
	�* It returns a new file created from the selected
	 * raster file. e.g: When the user name chooses a
	 * RAW file the file to open is a VRT file. This 
	 * method creates the new file and returns its name.
	 * @param file
	 * Selected File
	 * @return
	 * New File to load
	 */
	public File getTransformedFile(File file){
		return null;
	}

	/**
	 * Set data buffer to create a memory raster driver
	 * @param buf data buffer
	 */
	public void setBuffer(RasterBuf buf){
	}
	
	/**
	 * Set extent to create a memory raster driver
	 * @param ext Extent
	 */
	public void setExtent(Extent ext){
	}

}
