package org.gvsig.rasterTools.raw;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import org.cresques.cts.IProjection;
import org.cresques.io.GeoRasterFile;
import org.cresques.px.PxRaster;
import org.gvsig.rasterTools.raw.ui.main.OpenRawFileDefaultView;

import com.iver.cit.gvsig.fmap.drivers.raster.CmsRasterDriver;

/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
/* CVS MESSAGES:
 *
 * $Id: RawDriver.java 9045 2006-11-28 11:49:25Z nacho $
 * $Log$
 * Revision 1.1  2006-11-28 11:49:25  nacho
 * *** empty log message ***
 *
 * Revision 1.1  2006/10/10 16:11:23  nacho
 * *** empty log message ***
 *
 * Revision 1.2  2006/08/01 12:56:00  jorpiell
 * Se abre el cuadro de di�logo en el momento de abrir el fichero
 *
 * Revision 1.1  2006/08/01 11:19:45  jorpiell
 * Eliminada la extensi�n y a�adido el driver
 *
 *
 */
/**
 * This driver loads a Raw file from a file. It opens a 
 * properties window to set some params to create a VRT
 * file and then uses it to load the raw file in the
 * gvSIG current view
 * 
 * @author Jorge Piera Llodr� (piera_jor@gva.es)
 */
public class RawDriver extends CmsRasterDriver  {
	
	/* (non-Javadoc)
	 * @see com.hardcode.driverManager.Driver#getName()
	 */
	public String getName() {
		return "gvSIG Raw Image Driver";
	}
	
	/* (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.RasterDriver#fileAccepted(java.io.File)
	 */
	public boolean fileAccepted(File file) {
		if (file.getName().toUpperCase().endsWith("RAW")){
			return true;
		}
		return false;
	}
	
	/*
	 *  (non-Javadoc)
	 * @see com.iver.cit.gvsig.fmap.drivers.raster.CmsRasterDriver#getTransformedFile(java.io.File)
	 */
	public File getTransformedFile(File file){
		OpenRawFileDefaultView view = new OpenRawFileDefaultView(file.getAbsolutePath());
		return view.getImageFile();
	}

}
