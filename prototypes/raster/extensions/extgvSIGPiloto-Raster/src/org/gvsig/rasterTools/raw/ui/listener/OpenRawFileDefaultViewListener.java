package org.gvsig.rasterTools.raw.ui.listener;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.gvsig.rasterTools.raw.tools.VRTFileCreator;
import org.gvsig.rasterTools.raw.ui.main.OpenRawFileControlsPanel;
import org.gvsig.rasterTools.raw.ui.main.OpenRawFileDefaultView;

import com.iver.andami.PluginServices;

/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
/* CVS MESSAGES:
 *
 * $Id: OpenRawFileDefaultViewListener.java 9045 2006-11-28 11:49:25Z nacho $
 * $Log$
 * Revision 1.1  2006-11-28 11:49:25  nacho
 * *** empty log message ***
 *
 * Revision 1.1.2.1  2006/11/15 12:58:01  nacho
 * *** empty log message ***
 *
 * Revision 1.1  2006/10/10 16:11:23  nacho
 * *** empty log message ***
 *
 * Revision 1.6  2006/09/05 16:11:23  nacho
 * *** empty log message ***
 *
 * Revision 1.5  2006/08/01 12:56:00  jorpiell
 * Se abre el cuadro de di�logo en el momento de abrir el fichero
 *
 * Revision 1.4  2006/08/01 11:19:24  jorpiell
 * Ahora ya no es la ventana la que carga la capa, sino el driver
 *
 * Revision 1.3  2006/07/31 10:44:04  jorpiell
 * Se han completado las acciones de los botones aceptar y cancelar
 *
 * Revision 1.2  2006/07/28 13:13:27  jorpiell
 * Unos peque�os cambios para que coja el ancho y el alto de la imagen
 *
 * Revision 1.1  2006/07/28 12:51:40  jorpiell
 * Primer commit de las clases empleadas para abrir raw
 *
 *
 */
/**
 * Listener for the open raw file window. It implements actions
 * for the "open file" and for the "close window" buttons. 
 * 
 * @author Jorge Piera Llodr� (piera_jor@gva.es)
 */
public class OpenRawFileDefaultViewListener implements ActionListener{
	private OpenRawFileDefaultView openRawView = null;
	private static Logger logger = Logger.getLogger(OpenRawFileDefaultViewListener.class.getName());
	
	/**
	 * Contructor
	 * @param view
	 * Open raw file view
	 */
	public OpenRawFileDefaultViewListener(OpenRawFileDefaultView view) {
		super();
		this.openRawView = view;
	}
	
	/*
	 *  (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getActionCommand() == "close") {
			closeButtonActionPerformed();
		}else if (event.getActionCommand() == "open") {
			openButtonActionPerformed();
		}		
	}

	/**
	 * Open raw file button action
	 *
	 */
	private void openButtonActionPerformed() {
		OpenRawFileControlsPanel controls = openRawView.getControlsPanel();
		if (!(controls.getFile().exists())){
			JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),
				 	PluginServices.getText(this,"file_doesn_exists"));
			return;
		}
		if (controls.getOutputHeaderFormat().equals("VRT")){
			String vrtFileName = controls.getFile().getAbsolutePath().replaceAll("\\.raw", ".vrt");
			VRTFileCreator vrt = new VRTFileCreator(vrtFileName);
			vrt.setImageWidth(controls.getImageWidth());
			vrt.setImageHeight(controls.getImageHeight());
			vrt.setBands(controls.getNumberOfBands());
			vrt.setHeaderSize(controls.getHeaderSize());
			vrt.setDataType(controls.getDataType().getVrtOptionName());
			vrt.setDataSize(controls.getDataType().getDataSize());
			vrt.setByteOrder(controls.getByteOrder());
			vrt.setInterleaving(controls.getInterleaving());
			vrt.setRawFile(controls.getFile().getName());
			try {
				vrt.writeFile();				
				openRawView.setImageFile(vrt.getM_File());
			} catch (IOException e) {
				JOptionPane.showMessageDialog((Component)PluginServices.getMainFrame(),
					 	PluginServices.getText(this,"cant_create_vrt_file"));
				logger.error(PluginServices.getText(this,"cant_create_vrt_file"),e);				
			}			
		}
		closeButtonActionPerformed();
	}

	/**
	 * Close window
	 */
	private void closeButtonActionPerformed() {
		OpenRawFileControlsPanel controls = openRawView.getControlsPanel();
		controls.stopThread();
		PluginServices.getMDIManager().closeWindow(openRawView);		
	}
}
