package org.gvsig.rasterTools.raw.ui.main;

import javax.swing.JPanel;
import javax.swing.JButton;

import com.iver.andami.PluginServices;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
/* CVS MESSAGES:
 *
 * $Id: GuessImagePanel.java 9045 2006-11-28 11:49:25Z nacho $
 * $Log$
 * Revision 1.1  2006-11-28 11:49:24  nacho
 * *** empty log message ***
 *
 * Revision 1.1  2006/10/10 16:11:22  nacho
 * *** empty log message ***
 *
 * Revision 1.1  2006/07/28 12:51:40  jorpiell
 * Primer commit de las clases empleadas para abrir raw
 *
 *
 */
/**
 * This panel is a JButton for the "Guess image geometry"
 * option.
 * @author Jorge Piera Llodr� (piera_jor@gva.es)
 */
public class GuessImagePanel extends JPanel {
	private final int BUTTON_WIDTH = 250;
	private final int BUTTON_HEIGHT = 19;
	
	private JButton guessImageButton = null;

	
	public GuessImagePanel() {
		super();
		initialize();
		// TODO Auto-generated constructor stub
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0,0,0,0);
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridx = 0;
        this.setLayout(new GridBagLayout());
        this.add(getGuessImageButton(), gridBagConstraints);
			
	}

	/**
	 * This method initializes guessImageButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getGuessImageButton() {
		if (guessImageButton == null) {
			guessImageButton = new JButton();
			guessImageButton.setPreferredSize(new java.awt.Dimension(BUTTON_WIDTH,BUTTON_HEIGHT));
			guessImageButton.setText(PluginServices.getText(this,"guess_image_geometry"));
			
		}
		return guessImageButton;
	}
}
