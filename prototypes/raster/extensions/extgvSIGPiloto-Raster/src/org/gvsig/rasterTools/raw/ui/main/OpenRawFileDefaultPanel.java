package org.gvsig.rasterTools.raw.ui.main;

import javax.swing.JPanel;

import org.gvsig.rasterTools.raw.ui.listener.OpenRawFileDefaultViewListener;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
/* CVS MESSAGES:
 *
 * $Id: OpenRawFileDefaultPanel.java 9045 2006-11-28 11:49:25Z nacho $
 * $Log$
 * Revision 1.1  2006-11-28 11:49:25  nacho
 * *** empty log message ***
 *
 * Revision 1.1  2006/10/10 16:11:22  nacho
 * *** empty log message ***
 *
 * Revision 1.2  2006/08/01 11:19:04  jorpiell
 * Ajustado el tama�o de las ventanas
 *
 * Revision 1.1  2006/07/28 12:51:40  jorpiell
 * Primer commit de las clases empleadas para abrir raw
 *
 *
 */
/**
 * This is the main open raw file panel. It is composed
 * by all the other panels
 * @author Jorge Piera Llodr� (piera_jor@gva.es)
 */
public class OpenRawFileDefaultPanel extends JPanel{

	private OpenRawFileControlsPanel controlsPanel = null;
	private OpenRawFileButtonsPanel buttonsPanel = null;
		
	private String rawFileName = null;

	public OpenRawFileDefaultPanel(String rawFileName) {
		super();
		this.rawFileName = rawFileName;
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.insets = new java.awt.Insets(0,0,0,0);
        gridBagConstraints1.gridy = 1;
        gridBagConstraints1.gridx = 0;
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0,0,0,0);
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridx = 0;
        this.setLayout(new GridBagLayout());
        this.add(getControlsPanel(), gridBagConstraints);
        this.add(getButtonsPanel(), gridBagConstraints1);
			
	}

	/**
	 * This method initializes controlsPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	public OpenRawFileControlsPanel getControlsPanel() {
		if (controlsPanel == null) {
			controlsPanel = new OpenRawFileControlsPanel(rawFileName);
			controlsPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.SoftBevelBorder.RAISED));
		}
		return controlsPanel;
	}

	/**
	 * This method initializes buttonsPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private OpenRawFileButtonsPanel getButtonsPanel() {
		if (buttonsPanel == null) {
			buttonsPanel = new OpenRawFileButtonsPanel();
		}
		return buttonsPanel;
	}
	
	/**
	 * Sets the buttons listener
	 * @param listener
	 * Buttons listener
	 */
	public void setActionListener(OpenRawFileDefaultViewListener listener){
		getButtonsPanel().setActionListener(listener);
	}
	
	
	
}
