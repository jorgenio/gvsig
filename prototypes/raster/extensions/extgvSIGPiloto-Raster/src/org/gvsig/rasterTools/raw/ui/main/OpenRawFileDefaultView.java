package org.gvsig.rasterTools.raw.ui.main;

import java.io.File;

import org.gvsig.rasterTools.raw.ui.listener.OpenRawFileDefaultViewListener;

import com.iver.andami.PluginServices;
import com.iver.andami.ui.mdiManager.IWindow;
import com.iver.andami.ui.mdiManager.WindowInfo;

/* gvSIG. Sistema de Informaci�n Geogr�fica de la Generalitat Valenciana
 *
 * Copyright (C) 2004 IVER T.I. and Generalitat Valenciana.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * For more information, contact:
 *
 *  Generalitat Valenciana
 *   Conselleria d'Infraestructures i Transport
 *   Av. Blasco Ib��ez, 50
 *   46010 VALENCIA
 *   SPAIN
 *
 *      +34 963862235
 *   gvsig@gva.es
 *      www.gvsig.gva.es
 *
 *    or
 *
 *   IVER T.I. S.A
 *   Salamanca 50
 *   46005 Valencia
 *   Spain
 *
 *   +34 963163400
 *   dac@iver.es
 */
/* CVS MESSAGES:
 *
 * $Id: OpenRawFileDefaultView.java 9045 2006-11-28 11:49:25Z nacho $
 * $Log$
 * Revision 1.1  2006-11-28 11:49:25  nacho
 * *** empty log message ***
 *
 * Revision 1.1.2.1  2006/11/15 12:58:01  nacho
 * *** empty log message ***
 *
 * Revision 1.1  2006/10/10 16:11:23  nacho
 * *** empty log message ***
 *
 * Revision 1.5  2006/08/03 06:36:51  nacho
 * *** empty log message ***
 *
 * Revision 1.4  2006/08/03 06:18:29  jorpiell
 * *** empty log message ***
 *
 * Revision 1.3  2006/08/01 12:56:00  jorpiell
 * Se abre el cuadro de di�logo en el momento de abrir el fichero
 *
 * Revision 1.2  2006/08/01 11:19:04  jorpiell
 * Ajustado el tama�o de las ventanas
 *
 * Revision 1.1  2006/07/28 12:51:40  jorpiell
 * Primer commit de las clases empleadas para abrir raw
 *
 *
 */
/**
 * This class implemens the View interface to add the open
 * raw panel in gvSIG
 * @author Jorge Piera Llodr� (piera_jor@gva.es)
 */
public class OpenRawFileDefaultView extends OpenRawFileDefaultPanel implements IWindow{
	private File imageFile = null;
	
	/**
	 * Constructor. It create and sets the listener for the window
	 * buttons.
	 * @param rawFile
	 * Raw file name
	 */
	public OpenRawFileDefaultView(String rawFileName){
		super(rawFileName);
		setActionListener(new OpenRawFileDefaultViewListener(this));
		PluginServices.getMDIManager().addWindow(this);	
	}
		
	/*
	 *  (non-Javadoc)
	 * @see com.iver.andami.ui.mdiManager.View#getViewInfo()
	 */
	public WindowInfo getWindowInfo() {
		WindowInfo m_viewinfo=new WindowInfo(WindowInfo.MODALDIALOG);
		m_viewinfo.setTitle(PluginServices.getText(this, "open_raw_file"));
		m_viewinfo.setHeight(270);
		m_viewinfo.setWidth(540);
		return m_viewinfo;
	}	

	/**
	 * @return Returns the imageFile.
	 */
	public File getImageFile() {
		return imageFile;
	}

	/**
	 * @param imageFile The imageFile to set.
	 */
	public void setImageFile(File imageFile) {
		this.imageFile = imageFile;
	}

}